//
//  TripViewController.m
//  SATCAR
//
//  Created by Percept Infotech on 12/20/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import "TripViewController.h"
#import "TripTableViewCell.h"
#import "ManualTripEntryViewController.h"
#import "MapViewController.h"
@interface TripViewController ()<UIAlertViewDelegate>
{
    NSDate *currentMonth,*nextMonth;
    NSString *strCurrentDate,*userRegisterDate,*strMeasure,*strCurrency,*userType,*parentID,*userID,*undefined_tripstotal;
    int selectedSegment,selectedIndex,removeIndex,selectedSegment2;   // 0 = all, 1 = private, 2 = business
    NSMutableArray *allTripArray;
    NSArray *purposeArray;
    float borderWidth,firstLoad;
    float pri_trips_spilt,bus_trips_spilt,pri_trips_percentage,bus_trips_percentage;
    NSDictionary *userInfo,*dataDict;
    AppDelegate *appdelegate;
    
}
@end

@implementation TripViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    _viewReimbursement.hidden = true;
    _viewTripSplit.hidden = true;
    _lblProgessIndicator.backgroundColor = [Helper privateColor];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self addTitleView2:NSLocalizedString(@"Trips", @"")];
    selectedSegment2 = 0;
    _ViewSegment2.layer.cornerRadius = 25.0;
    _ViewSegment2.clipsToBounds = YES;
    _ViewSegment2.layer.borderWidth = 1;
    _ViewSegment2.layer.borderColor = [Helper defaultColor].CGColor;
    [_BtnPrivateSegment2 setTitle:NSLocalizedString(@"ExpPrivate", @"") forState:UIControlStateNormal];
    [_BtnBusinessSegment2 setTitle:NSLocalizedString(@"ExpBusiness", @"") forState:UIControlStateNormal];
    
    [_BtnPrivateSegment2 setBackgroundColor:[UIColor clearColor]];
    [_BtnPrivateSegment2.titleLabel setTextColor:[UIColor grayColor]];
    _BtnPrivateSegment2.titleLabel.textColor = [UIColor grayColor];
    [_BtnPrivateSegment2 setTintColor: [UIColor grayColor]];
    _BtnPrivateSegment2.layer.borderWidth = 1;
    _BtnPrivateSegment2.layer.borderColor = [Helper defaultColor].CGColor;
    
    [_BtnBusinessSegment2 setBackgroundColor:[UIColor clearColor]];
    _BtnBusinessSegment2.layer.borderColor = [Helper defaultColor].CGColor;
    _BtnBusinessSegment2.layer.borderWidth = 1;
    [_BtnBusinessSegment2.titleLabel setTextColor:[UIColor grayColor]];
    [_BtnBusinessSegment2 setTintColor: [UIColor grayColor]];
    
    [_BtnBusinessSegment2 setTitle:NSLocalizedString(@"ExpPrivate", @"") forState:UIControlStateNormal];
    [_BtnBusinessSegment2 setTitle:NSLocalizedString(@"ExpBusiness", @"") forState:UIControlStateNormal];
    
    removeIndex = 0;
    parentID = @"";
    UIButton *btnAdd = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnAdd setImage:[UIImage imageNamed:@"img_plus_menu"] forState:UIControlStateNormal];
    btnAdd.frame = CGRectMake(30, 0,32, 32);
    btnAdd.imageEdgeInsets = UIEdgeInsetsMake(7, 7, 7, 7);
    btnAdd.layer.cornerRadius = 10;
    btnAdd.clipsToBounds = YES;
    [btnAdd setTintColor:[UIColor whiteColor]];
    [btnAdd addTarget:self action:@selector(AddTrip) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* item = [[UIBarButtonItem alloc] initWithCustomView:btnAdd];
    self.navigationItem.rightBarButtonItem = item;
    _lblNotification.hidden = true;
    _IBLayoutConstraintNotifHeight.constant = 0;
   

}



- (IBAction)btnPrivateClick:(id)sender {
    
    selectedSegment2 = 1;
    [_BtnPrivateSegment2 setBackgroundColor:[Helper privateColor]];
    [_BtnPrivateSegment2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _BtnPrivateSegment2.layer.borderWidth = 1;
    _BtnPrivateSegment2.layer.borderColor = [Helper privateColor].CGColor;
    
    _ViewSegment2.layer.borderWidth = 1;
    _ViewSegment2.layer.borderColor = [Helper privateColor].CGColor;
    
    [_BtnBusinessSegment2 setBackgroundColor:[UIColor clearColor]];
    _BtnBusinessSegment2.layer.borderColor = [Helper privateColor].CGColor;
    _BtnBusinessSegment2.layer.borderWidth = 1;
    [_BtnBusinessSegment2 setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    
   // _IBLayoutConstraintPurposeTop.constant = 0;
    //_IBLayoutConstraintPurposeHeight.constant = 0;
    //_viewPurpose.hidden = true;
   // [_btnSave setBackgroundColor:[Helper privateColor]];
    
}

- (IBAction)btnbusinessClick:(id)sender {
    
    selectedSegment2 = 2;
    
    [_BtnBusinessSegment2 setBackgroundColor:[Helper BusinessColor]];
    [_BtnBusinessSegment2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _BtnBusinessSegment2.tintColor = [UIColor whiteColor];
    [_BtnBusinessSegment2.titleLabel setTextColor:[UIColor whiteColor]];
    _BtnBusinessSegment2.layer.borderWidth = 1;
    _BtnBusinessSegment2.layer.borderColor = [Helper BusinessColor].CGColor;
    
    _ViewSegment2.layer.borderWidth = 1;
    _ViewSegment2.layer.borderColor = [Helper BusinessColor].CGColor;
    
    [_BtnPrivateSegment2 setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [_BtnPrivateSegment2 setBackgroundColor:[UIColor clearColor]];
    _BtnPrivateSegment2.layer.borderColor = [Helper BusinessColor].CGColor;
    _BtnPrivateSegment2.layer.borderWidth = 1;
    
    //_IBLayoutConstraintPurposeTop.constant = 10;
    //_IBLayoutConstraintPurposeHeight.constant = 50;
    //_viewPurpose.hidden = false;
    //[_btnSave setBackgroundColor:[Helper BusinessColor]];
    
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.view endEditing:true];
}

-(void)viewWillAppear:(BOOL)animated {
    
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    userID = [Helper getPREF:@"userID"];
    [Helper setBorderWithColorInView:_viewType];
    [Helper setBorderWithColorInView:_viewDistance];
    [Helper setBorderWithColorInView:_viewSTime];
    [Helper setBorderWithColorInView:_viewETime];
    [Helper setBorderWithColorInView:_viewDuration];
    [Helper setBorderWithColorInView:_viewDate];
    
    [Helper setBorderWithColorInText:_txtStartPlaceValue];
    [Helper setBorderWithColorInText:_txtEndValue];
    
    [Helper setBorderWithColorInView:_viewNote];
    [Helper setBorderWithColorInView:_viewPurpose];
    
    if ([appdelegate.isRelaodTrip isEqualToString:@"1"]) {
        borderWidth = 0.5;
        currentMonth = [NSDate date];
        firstLoad = true;
        strCurrentDate = [self getMonthYearDay:currentMonth];
        _tableTrips.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        _lblMessage.text = NSLocalizedString(@"NoDataFound", @"");
        _lblMessage.hidden = true;
        _viewTripSplit.hidden = true;
        _viewReimbursement.hidden = true;
        /*_viewBackTrip.layer.cornerRadius = 5.0;
        _viewBackTrip.clipsToBounds = true;*/
        
        [Helper setBorderWithColorInButton:_btnEditManually];
        [Helper setBorderWithColorInButton:_btnConfirm];
        [Helper setBorderWithColorInButton:_btnMap];
        
        _lblReimbursementTotal.text = NSLocalizedString(@"ReimbursementTotal", @"");
        userRegisterDate = [Helper getPREF:@"userRegisterDate"];
        
        selectedIndex = -1;
        [self hideTripEditView];
        
        _ViewSegment.layer.cornerRadius = 23.0;
        _ViewSegment.clipsToBounds = YES;
        
        [_BtnAllSegment setBackgroundColor:[UIColor colorWithRed:0.000 green:0.647 blue:0.929 alpha:1.00]];
        [_BtnAllSegment.titleLabel setTextColor:[UIColor whiteColor]];
        _BtnAllSegment.layer.borderWidth = borderWidth;
        _BtnAllSegment.layer.borderColor = [UIColor colorWithRed:0.000 green:0.647 blue:0.929 alpha:1.00].CGColor;
        [_BtnAllSegment setTintColor: [UIColor whiteColor]];
        
        _ViewSegment.layer.borderWidth = 1;
        _ViewSegment.layer.borderColor = [UIColor colorWithRed:0.000 green:0.647 blue:0.929 alpha:1.00].CGColor;
        
        _viewFinancialSplic.layer.cornerRadius = 20;
        _viewFinancialSplic.clipsToBounds = YES;
        
        [_BtnPrivateSegment setBackgroundColor:[UIColor clearColor]];
        _BtnPrivateSegment.layer.borderColor = [UIColor colorWithRed:0.000 green:0.647 blue:0.929 alpha:1.00].CGColor;
        _BtnPrivateSegment.layer.borderWidth = borderWidth;
        [_BtnPrivateSegment.titleLabel setTextColor:[UIColor grayColor]];
        [_BtnPrivateSegment setTintColor: [UIColor grayColor]];
        
        [_BtnBusinessSegment setBackgroundColor:[UIColor clearColor]];
        _BtnBusinessSegment.layer.borderColor = [UIColor colorWithRed:0.000 green:0.647 blue:0.929 alpha:1.00].CGColor;
        _BtnBusinessSegment.layer.borderWidth = borderWidth;
        [_BtnBusinessSegment.titleLabel setTextColor:[UIColor grayColor]];
        [_BtnBusinessSegment setTintColor: [UIColor grayColor]];
        
        [_BtnAllSegment setTitle:NSLocalizedString(@"All", @"") forState:UIControlStateNormal];
        [_BtnPrivateSegment setTitle:NSLocalizedString(@"ExpPrivate", @"") forState:UIControlStateNormal];
        [_BtnBusinessSegment setTitle:NSLocalizedString(@"ExpBusiness", @"") forState:UIControlStateNormal];
        _tableTrips.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        _lblFinancialSplit.text = NSLocalizedString(@"TRIPSPLIT", @"");
        userRegisterDate = [Helper getPREF:@"userRegisterDate"];
        
        _lblTripMessage.text =NSLocalizedString(@"TripDetails", @"");
        _txtTypeValue.placeholder = NSLocalizedString(@"Type", @"");
        _txtDistanceValue.placeholder = NSLocalizedString(@"Distance", @"");
        _txtStartTimeValue.placeholder = NSLocalizedString(@"StartTime", @"");
        _txtEndTimeValue.placeholder = NSLocalizedString(@"EndTime", @"");
        _txtDurationValue.placeholder = NSLocalizedString(@"Duration", @"");
        _txtDateValue.placeholder = NSLocalizedString(@"Date", @"");
        _txtStartPlaceValue.placeholder = NSLocalizedString(@"StartAddress", @"");
        _txtEndValue.placeholder = NSLocalizedString(@"EndAddress", @"");
        _txtChoosePurposeValue.placeholder = NSLocalizedString(@"Purpospe", @"");
        _txtNoteValue.placeholder = NSLocalizedString(@"Notes", @"");
        
        [_btnEditManually setTitle:NSLocalizedString(@"EditMenually", @"") forState:UIControlStateNormal];
        [_btnMap setTitle:NSLocalizedString(@"ShowMap", @"") forState:UIControlStateNormal];
        [_btnConfirm setTitle:NSLocalizedString(@"ShowMap", @"") forState:UIControlStateNormal];
        [_btnClose setTitle:NSLocalizedString(@"Close_Trip", @"") forState:UIControlStateNormal];
        
        
        NSData *userdata = [Helper getDataValue:@"userInfo"];
        if (![userdata isKindOfClass:[NSNull class]]) {
            dataDict = [self dataToDicitonary:userdata];
            userInfo = [dataDict valueForKey:@"data"];
            if (userInfo != nil) {
                NSDictionary *generalSetting = [userInfo valueForKey:@"general_setting"];
                strMeasure = [generalSetting valueForKey:@"unit"];
                strCurrency = [generalSetting valueForKey:@"currency"];
            }else{
                //[Helper displayAlertView:@"" message:@"Data not available"];
            }
            currentMonth = [NSDate date];
            _scrollview.delegate = self;
            [self initializeData];
        }else{
            [Helper displayAlertView:@"" message:NSLocalizedString(@"something_went_wrong_msg", @"")];
        }
        
    }else{
        appdelegate.isRelaodTrip = @"1";
        //_viewTripSplit.hidden = true;
    }
    
    NSData *userdata = [Helper getDataValue:@"userInfo"];
    if (![userdata isKindOfClass:[NSNull class]]) {
        dataDict = [self dataToDicitonary:userdata];
        userInfo = [dataDict valueForKey:@"data"];
        if (userInfo != nil) {
            parentID = [Helper isStringIsNull:[userInfo valueForKey:@"parent"]];
        }
    }
    if ([parentID integerValue] > 0) {
        _viewTripSplit.hidden = true;
        _viewReimbursement.hidden = true;
        _IBLayoutConstraintTripTableviewBottom.constant = 10;
    }
    [_scrollview setContentSize:CGSizeMake(_scrollview.frame.size.width,_viewTripDisplay.frame.size.height + 100)];
}

-(void)viewDidLayoutSubviews{
    [_scrollview setContentSize:CGSizeMake(_scrollview.frame.size.width,_viewTripDisplay.frame.size.height + 100)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return allTripArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([userType isEqualToString:@"10"] || [userType isEqualToString:@"12"]){
        NSDictionary *tripData = [[NSDictionary alloc]init];
        tripData = [allTripArray objectAtIndex:indexPath.row];
        NSString *tripType = [tripData valueForKey:@"trip_type"];
        NSNumber *reim_amt = [tripData valueForKey:@"reim_amt"] ;
        
        if (![tripType isEqualToString:@"1"] && ![tripType isEqualToString:@"0"]) {
            if (reim_amt > 0) {
                return 104;
            }else{
                return 80;
            }
        }else{
            return 80;
        }
    }else{
         return 80;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TripTableViewCell *cell = (TripTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSDictionary *tripData = [[NSDictionary alloc]init];
    tripData = [allTripArray objectAtIndex:indexPath.row];
    
    NSString *tripType = [tripData valueForKey:@"trip_type"];
    if([tripType isEqualToString:@"0"]){
        cell.lblTripColor.backgroundColor = [Helper defaultColor];
        cell.IBLayoutConstraintReimTop.constant = 0;
        cell.IBLayoutConstraintReimHeight.constant = 0;
    }else if ([tripType isEqualToString:@"1"]) {
        cell.lblTripColor.backgroundColor = [Helper privateColor];
        cell.IBLayoutConstraintReimTop.constant = 0;
        cell.IBLayoutConstraintReimHeight.constant = 0;
        //cell.IBLayoutConstraintKMTop.constant = -20;
    }else{
        cell.lblTripColor.backgroundColor = [Helper BusinessColor];
        if ([userType isEqualToString:@"10"] || [userType isEqualToString:@"12"]){
            
            NSString *reim = [NSString stringWithFormat:@"%.2f",[[tripData valueForKey:@"reim_amt"]floatValue] ];
            cell.lblTripReim.text = [NSString stringWithFormat:NSLocalizedString(@"Reimbursement_DKK", @""),reim];
            cell.IBLayoutConstraintReimTop.constant = 4;
            cell.IBLayoutConstraintReimHeight.constant = 20;
            //cell.IBLayoutConstraintKMTop.constant = 4;
        }else{
            cell.IBLayoutConstraintReimTop.constant = 0;
            cell.IBLayoutConstraintReimHeight.constant = 0;
            //cell.IBLayoutConstraintKMTop.constant = -20;
        }
    }
    
    NSString *date = [tripData valueForKey:@"trip_date"];
    NSString *time = [tripData valueForKey:@"start_time"];
    NSString *duration = [tripData valueForKey:@"duration"];
    
    if (date != nil) {
        NSString *strdate = [self getCellDisplayDateFormat:date];
        NSString *strtime = [self getCellDisplayTimeFormat:time];
        cell.lblTripDate.text = [NSString stringWithFormat:@"%@ - %@",strdate,strtime];
    }
    
    if (duration.length <= 0) {
        cell.lblTripTime.text = [NSString stringWithFormat:@"0h 0m"];
    }else{
        cell.lblTripTime.text = duration;
    }
    
    NSString *created_by = [tripData valueForKey:@"created_by"];
    if (![created_by isEqualToString:userID]) {
        // Display user name who created trip
        NSString *name = [tripData valueForKey:@"first_name"];
        cell.viewCreated.hidden = false;
        cell.lblCreatedName.text = name;
    }else{
        // Hide user name view
        cell.viewCreated.hidden = true;
        cell.lblCreatedName.text = @"";
    }
    
    NSString *strmiles = strMeasure.lowercaseString;
    if ([strmiles isEqualToString:@"miles"]) {
        strmiles = @"mi";
    }

    float distance = [[tripData valueForKey:@"distance"] floatValue];
    NSString *distancetr = [self ConvertToDKCurrency: [NSString stringWithFormat:@"%.2f",distance]];
    cell.lblTripMeter.text = [NSString stringWithFormat:@"%@ %@",distancetr,strmiles.uppercaseString];
    
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *tripData = [[NSDictionary alloc]init];
    tripData = [allTripArray objectAtIndex:indexPath.row];
    selectedIndex = (int)indexPath.row;
    [self fillData];
    if (_txtNoteValue.text.length > 0) {
        _IBLayoutConstraintNoteTop.constant = 10;
        _IBLayoutConstraintNoteHeight.constant = 50;
        _viewNote.hidden = false;
    }else{
        _IBLayoutConstraintNoteTop.constant = 0;
        _IBLayoutConstraintNoteHeight.constant = 0;
        _viewNote.hidden = true;
    }
    
    NSString *tripType = [tripData valueForKey:@"trip_type"];
    if( [tripType isEqualToString:@"0"])
    {
        _ViewSegment2.layer.cornerRadius = 25.0;
        _ViewSegment2.clipsToBounds = YES;
        _ViewSegment2.layer.borderWidth = 1;
        _ViewSegment2.layer.borderColor = [Helper defaultColor].CGColor;
       
        [_BtnPrivateSegment2 setBackgroundColor:[UIColor clearColor]];
        [_BtnPrivateSegment2.titleLabel setTextColor:[UIColor grayColor]];
        _BtnPrivateSegment2.titleLabel.textColor = [UIColor grayColor];
        [_BtnPrivateSegment2 setTintColor: [UIColor grayColor]];
        _BtnPrivateSegment2.layer.borderWidth = 1;
        _BtnPrivateSegment2.layer.borderColor = [Helper defaultColor].CGColor;
        
        [_BtnBusinessSegment2 setBackgroundColor:[UIColor clearColor]];
        _BtnBusinessSegment2.layer.borderColor = [Helper defaultColor].CGColor;
        _BtnBusinessSegment2.layer.borderWidth = 1;
        [_BtnBusinessSegment2 setBackgroundColor:[UIColor clearColor]];
        _BtnBusinessSegment2.layer.borderColor = [Helper defaultColor].CGColor;
        [_BtnBusinessSegment2 setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
        
        
        [_BtnBusinessSegment2 setTitle:NSLocalizedString(@"ExpPrivate", @"") forState:UIControlStateNormal];
        [_BtnBusinessSegment2 setTitle:NSLocalizedString(@"ExpBusiness", @"") forState:UIControlStateNormal];
        
       [self.segment2hight setConstant:50];
        
    }else{
         [self.segment2hight setConstant:0];
        [self.segement2Bottom setConstant: 20];
        
    }
    
    if ([tripType isEqualToString:@"1"] || [tripType isEqualToString:@"0"]) {
        _IBLayoutConstraintPurposeTop.constant = 0;
        _IBLayoutConstraintPurposeHeight.constant = 0;
        
        _viewPurpose.hidden = true;
    }else{
        if (_txtChoosePurposeValue.text.length > 0) {
            _IBLayoutConstraintPurposeTop.constant = 10;
            _IBLayoutConstraintPurposeHeight.constant = 50;
            _viewPurpose.hidden = false;
        }else{
            _IBLayoutConstraintPurposeTop.constant = 0;
            _IBLayoutConstraintPurposeHeight.constant = 0;
            _viewPurpose.hidden = true;
        }
       
    }
    [self showTripEditView];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.row == allTripArray.count-1) {
        //[self getAllTripData];
        if ((indexPath.row + 1) % 10 == 0) {
            [self getAllTripData];
        }
    }
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {

        removeIndex = (int)indexPath.row;
        NSDictionary *tripData = [[NSDictionary alloc]init];
        tripData = [allTripArray objectAtIndex:indexPath.row];
        NSString *tripID = [tripData valueForKey:@"id"];
        NSDictionary *parameter=@{ @"id":tripID };
        [self callDeleteTrip:parameter];
    }
}

-(void)fillData{
    NSDictionary *tripData = [allTripArray objectAtIndex:selectedIndex];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onConfirm:)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    
    [_scrollview addGestureRecognizer:tapGestureRecognizer];
    
    /*_lblStartTimeValue.text = [tripData valueForKey:@"start_time"];
    _lblEndTimeValue.text = [tripData valueForKey:@"end_time"];
    _lblDurationValue.text = [tripData valueForKey:@"duration"];
    _lblDateValue.text = [Helper getDateFromString:[tripData valueForKey:@"trip_date"]];
    _lblStartPlaceValue.text = [tripData valueForKey:@"start_address"];
    _lblEndValue.text = [tripData valueForKey:@"end_address"];
    _lblChoosePurposeValue.text = [tripData valueForKey:@"purpose_trip"];
    _lblNoteValue.text = [tripData valueForKey:@"note"];*/
    
    _txtStartTimeValue.text = [tripData valueForKey:@"start_time"];
    _txtEndTimeValue.text = [tripData valueForKey:@"end_time"];
    _txtDurationValue.text = [tripData valueForKey:@"duration"];
    _txtDateValue.text = [Helper getDateFromString:[tripData valueForKey:@"trip_date"]];
    _txtStartPlaceValue.text = [tripData valueForKey:@"start_address"];
    _txtEndValue.text = [tripData valueForKey:@"end_address"];
    _txtChoosePurposeValue.text = [tripData valueForKey:@"purpose_trip"];
    _txtNoteValue.text = [tripData valueForKey:@"note"];
    
    
    NSString *distancetr = [self ConvertToDKCurrency:[tripData valueForKey:@"distance"]];
    _txtDistanceValue.text = [NSString stringWithFormat:@"%@ %@",distancetr,strMeasure.uppercaseString];
    
    NSString *tripType = [tripData valueForKey:@"trip_type"];
    UIColor *privateColor = [Helper privateColor];
    UIColor *businessColor = [Helper BusinessColor];
    UIColor *defultColor = [Helper defaultColor];
    
    if ([tripType isEqualToString:@"0"]) {
        _txtTypeValue.text = [NSString stringWithFormat:@"%@",NSLocalizedString(@"undefined",@"")];
        _btnEditManually.layer.borderColor = defultColor.CGColor;
        _btnConfirm.layer.borderColor = defultColor.CGColor;
        _btnMap.layer.borderColor = defultColor.CGColor;
        [_btnEditManually setTitleColor:defultColor forState:UIControlStateNormal];
        [_btnConfirm setTitleColor:defultColor forState:UIControlStateNormal];
        [_btnMap setTitleColor:defultColor forState:UIControlStateNormal];
        _lblTripMessage.backgroundColor = defultColor;
    }else if ([tripType isEqualToString:@"1"]) {
        _txtTypeValue.text = [NSString stringWithFormat:@"%@",NSLocalizedString(@"LblPrivate",@"")];
        _btnEditManually.layer.borderColor = privateColor.CGColor;
        _btnConfirm.layer.borderColor = privateColor.CGColor;
        _btnMap.layer.borderColor = privateColor.CGColor;
        [_btnEditManually setTitleColor:privateColor forState:UIControlStateNormal];
        [_btnConfirm setTitleColor:privateColor forState:UIControlStateNormal];
        [_btnMap setTitleColor:privateColor forState:UIControlStateNormal];
        _lblTripMessage.backgroundColor = privateColor;
    }else{
        _txtTypeValue.text = [NSString stringWithFormat:@"%@",NSLocalizedString(@"LblBusiness",@"")];
        _txtChoosePurposeValue.text = [NSString stringWithFormat:@"%@",[tripData valueForKey:@"purpose_trip"]];
        _btnEditManually.layer.borderColor = businessColor.CGColor;
        _btnConfirm.layer.borderColor = businessColor.CGColor;
        [_btnEditManually setTitleColor:businessColor forState:UIControlStateNormal];
        [_btnConfirm setTitleColor:businessColor forState:UIControlStateNormal];
        
        _btnMap.layer.borderColor = businessColor.CGColor;
        [_btnMap setTitleColor:businessColor forState:UIControlStateNormal];
        _lblTripMessage.backgroundColor = businessColor;
    }
}

#pragma mark - Segment action
- (IBAction)SelctSegment:(id)sender {
    
    if([sender tag] == 1)
    {
        _viewReimbursement.hidden = true;
        _viewTripSplit.hidden = true;
        /*if ([userType isEqualToString:@"10"] || [userType isEqualToString:@"12"]) {
            _viewReimbursement.hidden = false;
        }*/
        selectedSegment = 0;
        allTripArray = [[NSMutableArray alloc]init];
        [_tableTrips reloadData];
        [self getAllTripData];
        
        //[UIColor colorWithRed:0.000 green:0.647 blue:0.929 alpha:1.00]
        [_BtnAllSegment setBackgroundColor: [Helper defaultColor]];
        [_BtnAllSegment.titleLabel setTextColor:[UIColor whiteColor]];
        _BtnAllSegment.layer.borderWidth = borderWidth;
        _BtnAllSegment.layer.borderColor =  [Helper defaultColor].CGColor;
        [_BtnPrivateSegment setTintColor: [UIColor whiteColor]];
        
        _ViewSegment.layer.borderWidth = 1;
        _ViewSegment.layer.borderColor =  [Helper defaultColor].CGColor;
        
        [_BtnPrivateSegment setBackgroundColor:[UIColor clearColor]];
        _BtnPrivateSegment.layer.borderColor =  [Helper defaultColor].CGColor;
        _BtnPrivateSegment.layer.borderWidth =  borderWidth;
        [_BtnPrivateSegment.titleLabel setTextColor:[UIColor grayColor]];
        
        [_BtnBusinessSegment setBackgroundColor:[UIColor clearColor]];
        _BtnBusinessSegment.layer.borderColor =  [Helper defaultColor].CGColor;
        _BtnBusinessSegment.layer.borderWidth =  borderWidth;
        [_BtnBusinessSegment.titleLabel setTextColor:[UIColor grayColor]];
        
    }else if ([sender tag] == 2)
    {
        _viewReimbursement.hidden = true;
        _viewTripSplit.hidden = true;
        /*if ([userType isEqualToString:@"10"] || [userType isEqualToString:@"12"]) {
            _viewReimbursement.hidden = true;
        }*/
        selectedSegment = 1;
        allTripArray = [[NSMutableArray alloc]init];
        [_tableTrips reloadData];
        [self getAllTripData];
        
        [_BtnPrivateSegment setBackgroundColor:[Helper privateColor]];
        [_BtnPrivateSegment.titleLabel setTextColor:[UIColor whiteColor]];
        _BtnPrivateSegment.titleLabel.textColor = [UIColor whiteColor];
        [_BtnPrivateSegment setTintColor: [UIColor whiteColor]];
        _BtnPrivateSegment.layer.borderWidth =  borderWidth;
        _BtnPrivateSegment.layer.borderColor = [Helper privateColor].CGColor;
        
        _ViewSegment.layer.borderWidth = 1;
        _ViewSegment.layer.borderColor = [Helper privateColor].CGColor;
       
        [_BtnAllSegment setBackgroundColor:[UIColor clearColor]];
        _BtnAllSegment.layer.borderColor = [Helper privateColor].CGColor;
        [_BtnAllSegment.titleLabel setTextColor:[UIColor grayColor]];
        _BtnAllSegment.layer.borderWidth =  borderWidth;
        
        [_BtnBusinessSegment setBackgroundColor:[UIColor clearColor]];
        _BtnBusinessSegment.layer.borderColor =[Helper privateColor].CGColor;
        _BtnBusinessSegment.layer.borderWidth =  borderWidth;
        [_BtnBusinessSegment.titleLabel setTextColor:[UIColor grayColor]];
        
    }
    else if ([sender tag] == 3)
    {
        _viewReimbursement.hidden = true;
        _viewTripSplit.hidden = true;
        /*if ([userType isEqualToString:@"10"] || [userType isEqualToString:@"12"]) {
            _viewReimbursement.hidden = false;
        }*/
        selectedSegment = 2;
        allTripArray = [[NSMutableArray alloc]init];
        [_tableTrips reloadData];
        [self getAllTripData];
        
        [_BtnBusinessSegment setBackgroundColor:[Helper BusinessColor]];
        [_BtnBusinessSegment.titleLabel setTextColor:[UIColor whiteColor]];
        _BtnBusinessSegment.layer.borderWidth =  borderWidth;
        _BtnBusinessSegment.layer.borderColor = [Helper BusinessColor].CGColor;
        [_BtnBusinessSegment setTintColor: [UIColor whiteColor]];
        
        _ViewSegment.layer.borderWidth = 1;
        _ViewSegment.layer.borderColor = [Helper BusinessColor].CGColor;
        
        [_BtnPrivateSegment setBackgroundColor:[UIColor clearColor]];
        _BtnPrivateSegment.layer.borderColor = [Helper BusinessColor].CGColor;
        _BtnPrivateSegment.layer.borderWidth =  borderWidth;
        [_BtnPrivateSegment.titleLabel setTextColor:[UIColor grayColor]];
        
        
        [_BtnAllSegment setBackgroundColor:[UIColor clearColor]];
        _BtnAllSegment.layer.borderColor = [Helper BusinessColor].CGColor;
        _BtnAllSegment.layer.borderWidth =  borderWidth;
        [_BtnAllSegment.titleLabel setTextColor:[UIColor grayColor]];
    }
}

// Deprecated
/*#pragma mark - alertView delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 8888) {
        if (buttonIndex == 0) {
            NSLog(@"Payment pressed");
            [self performSegueWithIdentifier:@"segueFreeTripToPaidRegister" sender:self];
        }else{
            NSLog(@"cancel pressed");
        }
    }
}*/


#pragma mark - Button action
- (IBAction)onFinancesSplit:(id)sender {
    [self performSegueWithIdentifier:@"TripToSplit" sender:self];
}

- (IBAction)onPreviousDate:(id)sender {
    
    NSDate *prev = [self getPrevoiusMonthDate:currentMonth];
    currentMonth = prev;
    [self checkDataAvailable:currentMonth];
    strCurrentDate = [self getMonthYearDay:currentMonth];
    _lblMonth.text = [self getHeaderDisplayDateFormat:currentMonth];
    allTripArray = [[NSMutableArray alloc]init];
    [_tableTrips reloadData];
    [self getAllTripData];
    
}

- (IBAction)onNextDate:(id)sender {
    
    NSDate *next = [self getNextMonthDate:currentMonth];
    currentMonth = next;
    [self checkNextDataAvailable:currentMonth];
    strCurrentDate = [self getMonthYearDay:currentMonth];
    _lblMonth.text = [self getHeaderDisplayDateFormat:currentMonth];
    allTripArray = [[NSMutableArray alloc]init];
    [_tableTrips reloadData];
    [self getAllTripData];
    
}

- (IBAction)onEditManually:(id)sender {
    [self.view endEditing:TRUE];
    [self hideTripEditView];
    self.navigationController.navigationBar.hidden = false;
    [self performSegueWithIdentifier:@"TripToEditManualEntry" sender:self];
}

- (IBAction)onConfirm:(id)sender {
    //selectedSegment = 1;
    [self.view endEditing:TRUE];
    [self hideTripEditView];
    [self onSave];
}


- (IBAction)onMAp:(id)sender {
    _isFromMap = @"1";
    [self.view endEditing:TRUE];
    [self hideTripEditView];
    [self performSegueWithIdentifier:@"segueTripToMap" sender:self];
}


#pragma mark - custome funcitons
-(void)initializeData{
    tempTrips = [[NSMutableArray alloc]init];
    allTripArray = [[NSMutableArray alloc]init];
    [_tableTrips reloadData];
    selectedSegment = 0;
    [self checkDataAvailable:currentMonth];
    [self checkNextDataAvailable:currentMonth];
    _lblMonth.text = [self getHeaderDisplayDateFormat:currentMonth];
    [self getAllTripData ];
}

-(void)checkDataAvailable:(NSDate *)date{
    BOOL previousAvailable = [self comapreDateByMonthAndYear:date registerDate:userRegisterDate];
    [self tempNext:date];
    _btnPrivious.hidden = true;
    if (previousAvailable) {
        _btnPrivious.hidden = false;
    }else{
        _btnPrivious.hidden = true;
    }
}

-(void)checkNextDataAvailable:(NSDate *)date{
    BOOL nextAvailable = [self comapreNextDate:date];
    [self tempPrevious:date];
    
    _btnNext.hidden = true;
    if (nextAvailable) {
        _btnNext.hidden = false;
    }else{
        _btnNext.hidden = true;
    }
}

-(void)tempPrevious:(NSDate *)date{
    
    BOOL previousAvailable = [self comapreDateByMonthAndYear:date registerDate:userRegisterDate];
    _btnPrivious.hidden = true;
    if (previousAvailable) {
        _btnPrivious.hidden = false;
    }else{
        _btnPrivious.hidden = true;
    }
}
-(void)tempNext:(NSDate *)date{
    BOOL nextAvailable = [self comapreNextDate:date];
    _btnNext.hidden = true;
    if (nextAvailable) {
        _btnNext.hidden = false;
    }else{
        _btnNext.hidden = true;
    }

}

-(void)isDataAvailable{
    if ([userType isEqualToString:@"10"] || [userType isEqualToString:@"12"]) {
        if (allTripArray.count <= 0) {
            _lblMessage.hidden = false;
            _viewReimbursement.hidden = true;
            _viewTripSplit.hidden = true;
        }else{
            _lblMessage.hidden = true;
            if (selectedSegment == 1) {
                _viewReimbursement.hidden = true;
                _viewTripSplit.hidden = false;
            }else{
                _viewReimbursement.hidden = false;
                _viewTripSplit.hidden = true;
            }
        }
    }else{
        if (allTripArray.count <= 0) {
            _lblMessage.hidden = false;
            _viewTripSplit.hidden = true;
            _viewReimbursement.hidden = true;
            
        }else{
            _lblMessage.hidden = true;
            _viewTripSplit.hidden = false;
            _viewReimbursement.hidden = true;
        }
    }
    
    // Hide bottom view if user is subuser
    if ([parentID integerValue] > 0) {
        _viewTripSplit.hidden = true;
        _viewReimbursement.hidden = true;
        _IBLayoutConstraintTripTableviewBottom.constant = 10;
    }
}

-(void)AddTrip
{
    NSString *user_type = [Helper getPREF:@"user_type"];
    NSString *tripcount = [Helper getPREF:@"tripcount"];
    int count = [tripcount intValue];
    
    if ([user_type isEqualToString:@"12"]) {
        if (count >= [appdelegate.freeTrip intValue]) {
            
            UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"" message:appdelegate.freeUserMessage                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* choose = [UIAlertAction actionWithTitle:NSLocalizedString(@"Choose", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                NSLog(@"Payment pressed");
                [self performSegueWithIdentifier:@"segueFreeTripToPaidRegister" sender:self];
            }];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"Ok", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                [alert dismissViewControllerAnimated:true completion:nil];
            }];
            
            [alert addAction:choose];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            
            // Deprecated
            /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:appdelegate.freeUserMessage delegate:self cancelButtonTitle:NSLocalizedString(@"Choose", @"") otherButtonTitles:NSLocalizedString(@"Ok", @""),nil];
            alert.tag = 8888;
            [alert show];*/
            
        }else{
            [self performSegueWithIdentifier:@"TripToManualEntry" sender:self];
        }
    }else{
        [self performSegueWithIdentifier:@"TripToManualEntry" sender:self];
    }
    
}

-(void)getAllTripData{
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //Background Thread
        dispatch_async(dispatch_get_main_queue(), ^(void){
            //Run UI Updates
            NSString *count = [NSString stringWithFormat:@"%d",(int)allTripArray.count];
            NSString *trip = [NSString stringWithFormat:@"%d",selectedSegment];
            [self callTripService:trip date:strCurrentDate limit:count];
        });
    });
    
}

-(void) tripSplitCalculate {
   // NSLog(@"calulate");
    double totalDistance =0.0;
    double privateTripTotalDistace =0.0;
    
    privateTripTotalDistace = pri_trips_spilt * 100;
    totalDistance = (pri_trips_spilt + bus_trips_spilt);
    float progress = (float)(privateTripTotalDistace/totalDistance);
    if (progress < 0 || isnan(progress))
    {
        progress = 0;
        _IBLayoutConstraintprivateWidth.constant = 0;
        NSString *PTPstr = [NSString stringWithFormat:@"%.2f",progress];
        
        double BusinessTripTotalDistace =0.0;
        BusinessTripTotalDistace = bus_trips_spilt * 100;
        totalDistance = (pri_trips_spilt + bus_trips_spilt);
        float progress = (float)(BusinessTripTotalDistace/totalDistance);
        if (progress < 0 || isnan(progress)){
            progress = 0;
        }
        NSString *BTPstr = [NSString stringWithFormat:@"%.2f",progress];
        
        _lblPrivatePercentage.text = [NSString stringWithFormat:@"Private (%@%%)",PTPstr];
        _lblBusinessPercentage.text = [NSString stringWithFormat:@"Business (%@%%)",BTPstr];
        
        _lblPrivatePercentage.text = [NSString stringWithFormat:NSLocalizedString(@"PrivatePerc" ,@""),PTPstr];
        _lblBusinessPercentage.text = [NSString stringWithFormat:NSLocalizedString(@"BusinessPerc" ,@""),BTPstr];
        
    }else{
        float screenWidth = (float)self.view.frame.size.width - 40;
        float width = (screenWidth * progress ) / 100;
        _IBLayoutConstraintprivateWidth.constant = width;
        
        NSString *PTPstr = [NSString stringWithFormat:@"%.2f",progress];
        NSString *BTPstr = [NSString stringWithFormat:@"%.2f",(100-progress)];
        
        _lblPrivatePercentage.text = [NSString stringWithFormat:@"Private (%@%%)",PTPstr];
        _lblBusinessPercentage.text = [NSString stringWithFormat:@"Business (%@%%)",BTPstr];
        
        _lblPrivatePercentage.text = [NSString stringWithFormat:NSLocalizedString(@"PrivatePerc" ,@""),PTPstr];
        _lblBusinessPercentage.text = [NSString stringWithFormat:NSLocalizedString(@"BusinessPerc" ,@""),BTPstr];
    }
    
}

-(void)hideTripEditView{
    _viewTripDisplay.hidden = true;
    self.navigationController.navigationBar.hidden = false;
    [UIView animateWithDuration:5.0 delay:1.0 options:UIViewAnimationOptionTransitionNone animations:^{
        _IBLayoutConstraintTripPopupTop.constant = self.view.frame.size.height;
        _IBLayoutConstraintTripPopupHeight.constant = 0;
    }completion:nil];
}

-(void)showTripEditView{
    
    [UIView animateWithDuration:1.0 delay:1.0 options:UIViewAnimationOptionTransitionNone animations:^{
        self.navigationController.navigationBar.hidden = true;
        _viewTripDisplay.hidden = false;
        _IBLayoutConstraintTripPopupTop.constant = 0;
        _IBLayoutConstraintTripPopupHeight.constant = self.view.frame.size.height;
    }completion:nil];
}




#pragma mark - webservice call
-(void)callTripService:(NSString *)tripType date:(NSString *)tripDate limit:(NSString *)limit{
    
    _lblNotification.hidden = true;
    _IBLayoutConstraintNotifHeight.constant = 0;
    
    NSString *type,*date,*lmt;
    type = tripType;
    date = tripDate;
    lmt = limit;
    
    if(![self CheckNetworkConnection]) {
        return;
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *userID = [Helper getPREF:@"userID"];
    NSString *user_type = [Helper getPREF:@"user_type"];
    
    //NSString *sid = [Helper getPREF:@"sid"];
    NSDictionary *parameter=@{
                              @"userid":userID,
                              @"user_type":user_type,
                              @"trip_type":tripType,
                              @"t_date":tripDate,
                              @"limitstart":limit
                              };
    //NSLog(@"parameter : %@",parameter);
    
    
    
    
    [WebService httpPostWithCustomDelegateURLString:@".gettrips" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         if (error == nil) {
             NSError *localError = nil;
             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
             
             if (localError != nil) {
                 NSLog(@"%@",localError.description);
             }
             else {
               // NSDictionary *response = [parsedObject objectForKey:@"data"];
                 // NSLog(@"response : %@",parsedObject);
                 id response = [parsedObject objectForKey:@"data"];
                 if ([self CheckSession:response]) {
                      NSLog(@"response : %@",response);
                     tempTrips = [[NSMutableArray alloc]init];
                     tempTrips  = [response objectForKey:@"trips"];
                     pri_trips_spilt = [[response objectForKey:@"pri_trips_spilt"] floatValue];
                     bus_trips_spilt = [[response objectForKey:@"bus_trips_spilt"] floatValue];
                     pri_trips_percentage = [[response objectForKey:@"pri_trips_percentage"] floatValue];
                     bus_trips_percentage = [[response objectForKey:@"bus_trips_percentage"] floatValue];
                     
                     NSDictionary *generalSetting = [response valueForKey:@"general_setting"];
                     strMeasure = [generalSetting valueForKey:@"unit"];
                     NSString  *strcurrency = [generalSetting valueForKey:@"currency"];
                     
                     userType = [Helper getPREF:@"user_type"];
                     if ([userType isEqualToString:@"10"] || [userType isEqualToString:@"12"]) {
                         _viewTripSplit.hidden = true;
                         _viewReimbursement.hidden = false;
                     }else{
                         _viewTripSplit.hidden = false;
                         _viewReimbursement.hidden = true;
                     }
                     if ([user_type isEqualToString:@"11"]) {
                         undefined_tripstotal = [response objectForKey:@"undefined_tripstotal"];
                         if ([undefined_tripstotal integerValue] > 0) {
                             _lblNotification.hidden = false;
                             _lblNotification.text = [NSString stringWithFormat:NSLocalizedString(@"Splitleasing_end_of_month_notigication_message", @""),undefined_tripstotal];
                             _IBLayoutConstraintNotifHeight.constant = 80;
                             //[self showPOPUPA];
                         }else{
                             _lblNotification.hidden = true;
                             _lblNotification.text = @"";
                             _IBLayoutConstraintNotifHeight.constant = 0;
                         }
                     }
                     
                    // _IBLayoutConstraintLblNotifHeight.constant = 50;
                     
                     [Helper setPREFStringValue:strcurrency sKey:@"currency"];
                     [Helper setPREFStringValue:strMeasure sKey:@"unit"];
                     
                     if ([userType isEqualToString:@"10"] || [userType isEqualToString:@"12"])
                     {
                         float total = [[response objectForKey:@"r_total"] floatValue];
                         float tSplit = [[response objectForKey:@"bus_trips_spilt"] floatValue];
                         NSString *totalstr = [self ConvertToDKCurrency: [NSString stringWithFormat:@"%.2f",total]];
                         _lblRexpense.text = [NSString stringWithFormat:@"%@ %@",totalstr,strcurrency.uppercaseString];
                         NSString *tSplitstr = [self ConvertToDKCurrency: [NSString stringWithFormat:@"%.2f",tSplit]];
                         _lblRexpensetype.text = [NSString stringWithFormat:NSLocalizedString(@"Reembersment_total", @""),tSplitstr,strMeasure];
                     }
                     
                     [userInfo setValue:generalSetting forKey:@"general_setting"];
                     [Helper setPREFStringValue:strcurrency sKey:@"currency"];
                     [Helper setPREFStringValue:strMeasure sKey:@"unit"];
                     
                     [Helper setUserValue:generalSetting sKey:@"general_setting"];
                     
                     [dataDict setValue:userInfo forKey:@"data"];
                     NSData *data = [self dicitonaryToData:dataDict];
                     [Helper setDataValue:data sKey:@"userInfo"];
                     
                     [self tripSplitCalculate];
                     
                     if (tempTrips.count > 0) {
                         firstLoad = false;
                         //NSLog(@"Trips : %@",tempTrips);
                         
                         for (int i = 0; i < tempTrips.count; i++)
                         {
                             [allTripArray addObject:[tempTrips objectAtIndex:i]];
                         }
                         
                         NSLog(@"Tips Count %d",allTripArray.count);
                         [_tableTrips reloadData];
                     }
                     [self isDataAvailable];
                 }
             }
         }else{
             UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"request_time_out", @"") preferredStyle:UIAlertControllerStyleAlert];
             
             UIAlertAction *retry = [UIAlertAction actionWithTitle:NSLocalizedString(@"Retry", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                 [self callTripService:type date:date limit:lmt];
             }];
             UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                 [alert dismissViewControllerAnimated:true completion:nil];
             }];
             [alert addAction:retry];
             [alert addAction:cancel];
             [self presentViewController:alert animated:true completion:nil];
         }
     }];
}

-(void)showPOPUPA
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"popupA", @"") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *retry = [UIAlertAction actionWithTitle:NSLocalizedString(@"Trips", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        //[self callTripService:type date:date limit:lmt];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [alert dismissViewControllerAnimated:true completion:nil];
    }];
    [alert addAction:retry];
    [alert addAction:cancel];
    [self presentViewController:alert animated:true completion:nil];
}
-(void)showPOPUPB
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"popupB", @"") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *retry = [UIAlertAction actionWithTitle:NSLocalizedString(@"Trips", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        //[self callTripService:type date:date limit:lmt];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [alert dismissViewControllerAnimated:true completion:nil];
    }];
    [alert addAction:retry];
    [alert addAction:cancel];
    [self presentViewController:alert animated:true completion:nil];
}

- (void)callDeleteTrip:(NSDictionary *)parameter{
    [self.view endEditing:YES];
    
    if(![self CheckNetworkConnection]) {
        return;
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //NSLog(@"parameter : %@",parameter);
    
    [WebService httpPostWithCustomDelegateURLString:@".deleteTrip" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         if (error == nil) {
             NSError *localError = nil;
             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
             
             if (localError != nil) {
                 NSLog(@"%@",localError.description);
             }
             else {
                 NSDictionary *response = [parsedObject objectForKey:@"data"];
                 NSString* message = [response objectForKey:@"message"];
                 NSString* success = [response objectForKey:@"status"];
                 NSLog(@"Response : %@",response);
                 if ([success isEqualToString:@"Success"]) {
                     //[allTripArray removeObjectAtIndex:removeIndex];
                     tempTrips = [[NSMutableArray alloc]init];
                     allTripArray = [[NSMutableArray alloc]init];
                     [_tableTrips reloadData];
                     //selectedSegment = 0;
                     [self checkDataAvailable:currentMonth];
                     [self checkNextDataAvailable:currentMonth];
                     _lblMonth.text = [self getHeaderDisplayDateFormat:currentMonth];
                     [self getAllTripData ];
                     
                     
                 } else {
                     NSLog(@"error :: %@",message);
                     [Helper displayAlertView:@"" message:message];
                 }
             }
         }
     }];
}

- (void)onSave {
    
    
    NSDictionary *tripData = [[NSDictionary alloc]init];
    tripData = [allTripArray objectAtIndex:selectedIndex];
    
        NSString *userID = [Helper getPREF:@"userID"];
        NSString *created_by = [tripData valueForKey:@"created_by"];
            if (![created_by isEqualToString:userID]) {
                userID = created_by;
            }
    
        NSDictionary *parameter;
        NSString *strLocation = [tripData valueForKey:@"locations"];
    
        if (strLocation == nil || strLocation.length <= 0) {
            strLocation = @"";
        }
   
   
    
    NSString *tripType = [tripData valueForKey:@"trip_type"];
    if(tripType.intValue == 0)
    {
        
    }else{
        selectedSegment2 = tripType.intValue;
    }
    
        if (selectedSegment2 == 1) {
            parameter=@{ @"userid":[tripData valueForKey:@"user_id"],@"id":[tripData valueForKey:@"id"], @"trip_type":[NSString stringWithFormat:@"%d",selectedSegment2], @"start_address":_txtStartPlaceValue.text, @"end_address":_txtEndValue.text, @"trip_date":_txtDateValue.text, @"start_time":_txtStartTimeValue.text, @"end_time":_txtEndTimeValue.text,@"odometer_start":[tripData valueForKey:@"odometer_start"], @"odometer_finish":[tripData valueForKey:@"odometer_finish"], @"distance":_txtDistanceValue.text, @"note":_txtNoteValue.text, @"locations":strLocation, @"locations1":strLocation, @"version":appdelegate.appVersion, @"os":@"ios", @"build":appdelegate.appBuild };
        }else {
            parameter=@{ @"userid":[tripData valueForKey:@"user_id"],@"id":[tripData valueForKey:@"id"], @"trip_type":[NSString stringWithFormat:@"%d",selectedSegment2], @"start_address":_txtStartPlaceValue.text, @"end_address":_txtEndValue.text, @"trip_date":_txtDateValue.text, @"start_time":_txtStartTimeValue.text, @"end_time":_txtEndTimeValue.text,@"odometer_start":[tripData valueForKey:@"odometer_start"], @"odometer_finish":[tripData valueForKey:@"odometer_finish"], @"distance":_txtDistanceValue.text, @"note":_txtNoteValue.text, @"purpose_trip":_txtChoosePurposeValue.text , @"locations":strLocation, @"locations1":strLocation, @"version":appdelegate.appVersion, @"os":@"ios", @"build":appdelegate.appBuild};
        }
        [self callAddTripService:parameter];
    
}
-(void)callAddTripService:(NSDictionary *)parameter{
    
    if(![self CheckNetworkConnection]) {
        return;
    }
    [self.view endEditing:TRUE];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [WebService httpPostWithCustomDelegateURLString:@".saveTrip" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         if (error == nil) {
             NSError *localError = nil;
             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
             
             if (localError != nil) {
                 NSLog(@"%@",localError.description);
             }
             else {
                 NSDictionary *response = [parsedObject objectForKey:@"data"];
                 NSString* message = [response objectForKey:@"message"];
                 NSString* success = [response objectForKey:@"status"];
                 if ([success isEqualToString:@"Success"]) {
                     //[Helper displayAlertView:@"" message:message];
                     
                     
                    
                 } else {
                     NSLog(@"error :: %@",message);
                     [Helper displayAlertView:@"" message:message];
                 }
                 [self.tableTrips reloadData];
             }
         }
     }];
}



 #pragma mark - Navigation

 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     if ([segue.identifier isEqualToString:@"TripToEditManualEntry"]) {
         ManualTripEntryViewController *controller = segue.destinationViewController;
         NSDictionary *tripData = [allTripArray objectAtIndex:selectedIndex];
         controller.tripDataDictionary = tripData;
         controller.fromScreen = @"TripEdit";
     }else if([segue.identifier isEqualToString:@"segueTripToMap"]){
         NSDictionary *tripData = [allTripArray objectAtIndex:selectedIndex];
         //NSLog(@"trip data : %@",tripData);
         MapViewController *controller = segue.destinationViewController;
         controller.locationDataDictionary = tripData;
         controller.fromScreen = @"TripMap";
     }
 }


- (IBAction)onReimbursement:(id)sender {
    [self performSegueWithIdentifier:@"TripToSplit" sender:self];
}

- (IBAction)Cencel:(id)sender {
     [self hideTripEditView];
}
- (IBAction)onStartAddress:(id)sender {
    _isFromMap = @"1";
    [self hideTripEditView];
    [self performSegueWithIdentifier:@"segueTripToMap" sender:self];
}
@end
