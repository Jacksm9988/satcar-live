//
//  AddUserViewController.m
//  SATCAR
//
//  Created by Percept Infotech on 2018-04-30.
//  Copyright © 2018 Percept Infotech. All rights reserved.
//

#import "AddUserViewController.h"
#import "UserListTVC.h"
@interface AddUserViewController (){
    AppDelegate *appdelegate;
    NSMutableArray *userArray;
}

@end

@implementation AddUserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self addTitleView:NSLocalizedString(@"Add_user", @"")];
    appdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    _tableview.estimatedRowHeight = 50;
    _tableview.rowHeight = UITableViewAutomaticDimension;
    _tableview.hidden = true;
}

-(void)viewWillAppear:(BOOL)animated{
    _txtEmail.placeholder =NSLocalizedString(@"Email", @"");
    _txtPassowrd.placeholder =NSLocalizedString(@"Password", @"");
    [_btnAddUser setTitle:NSLocalizedString(@"Add_user", @"") forState:UIControlStateNormal];
   
    [Helper setBorderWithColorInText:_txtEmail];
    [Helper setBorderWithColorInText:_txtPassowrd];
    [Helper setBorderWithColorInButton:_btnAddUser];
    [self getUserList];
}

#pragma mark - Table View
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return userArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UserListTVC *cell = (UserListTVC *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSDictionary *user =  [userArray objectAtIndex:indexPath.row];
    cell.lblName.text = [user valueForKey:@"first_name"];
    cell.btnRemove.tag = (int)indexPath.row;
    [cell.btnRemove setTitle:NSLocalizedString(@"remove", @"") forState:UIControlStateNormal];
    [cell.btnRemove addTarget:self action:@selector(onRemoveUser:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}


- (void)onRemoveUser:(UIButton *)sender{
    int tag = (int)sender.tag;
    NSDictionary *user =  [userArray objectAtIndex:tag];
    [self RemoveUser:[user valueForKey:@"id"] withIndex:tag];
}


#pragma mark - validation
-(BOOL)isValidData{
    if (_txtEmail.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_enter_email_id",nil)];
        return false;
        
    }else if (![self validateEmailWithString:_txtEmail.text]){
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Invalid_email_id",nil)];
        return false;
        
    }else if (_txtPassowrd.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_your_password",nil)];
        return false;
    }
    return true;
}

#pragma mark - Textfield delegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    textField.textColor =  [UIColor colorWithRed:18.0/255.0 green:172.0/255.0 blue:243.0/255.0 alpha:1.0];
    textField.layer.borderWidth = 1;
    textField.layer.borderColor =  [UIColor colorWithRed:18.0/255.0 green:172.0/255.0 blue:243.0/255.0 alpha:1.0].CGColor;
    textField.clipsToBounds = YES;
    return true;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    textField.textColor =  [UIColor blackColor];
    textField.layer.borderWidth = 0;
    textField.layer.borderColor = [UIColor clearColor].CGColor;
    textField.clipsToBounds = YES;
    return true;
}

#pragma mark - Button action
- (IBAction)onAddUser:(id)sender {
    if(![self CheckNetworkConnection]) {
        return;
    }
    
    if ([self isValidData]) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSString *userID = [Helper getPREF:@"userID"];
        NSDictionary *parameter=@{ @"email":_txtEmail.text, @"password":_txtPassowrd.text, @"parent":userID};
        [WebService httpPostWithCustomDelegateURLString:@".CreateSubuser" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error) {
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (error == nil) {
                NSError *localError = nil;
                NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
                if (localError != nil) {
                    NSLog(@"%@",localError.description);
                }
                else {
                    NSDictionary *response = [parsedObject objectForKey:@"data"];
                    NSString* message = [response objectForKey:@"message"];
                    NSString* success = [response objectForKey:@"status"];
                    
                    if ([success isEqualToString:@"Success"]) {
                        [self.navigationController popViewControllerAnimated:true];
                        [Helper displayAlertView:@"" message:message];
                    }else{
                        [Helper displayAlertView:@"" message:message];
                    }
                }
            }
        }];
    }
}

- (void)getUserList{
    if(![self CheckNetworkConnection]) {
        return;
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *userID = [Helper getPREF:@"userID"];
    NSDictionary *parameter=@{ @"user_id":userID};
    
    [WebService httpPostWithCustomDelegateURLString:@".getAllSubUser" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error) {
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (error == nil) {
            NSError *localError = nil;
            NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
            if (localError != nil) {
                NSLog(@"%@",localError.description);
            }
            else {
                NSDictionary *response = [parsedObject objectForKey:@"data"];
                NSString* success = [response objectForKey:@"status"];
                NSString* message = [response objectForKey:@"message"];
                if ([success isEqualToString:@"Success"]) {
                    userArray = [[NSMutableArray alloc]init];
                    userArray = [response valueForKey:@"subuser"];
                    if (userArray.count > 0) {
                        _tableview.hidden = false;
                    }else{
                        _tableview.hidden = true;
                    }
                }else{
                    [Helper displayAlertView:@"" message:message];
                }
                [_tableview reloadData];
            }
        }
    }];
}

- (void)RemoveUser:(NSString*)uID withIndex:(int)ind{
    if(![self CheckNetworkConnection]) {
        return;
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSDictionary *parameter=@{ @"id":uID};
    
    [WebService httpPostWithCustomDelegateURLString:@".RemoveSubUser" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error) {
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (error == nil) {
            NSError *localError = nil;
            NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
            if (localError != nil) {
                NSLog(@"%@",localError.description);
            }
            else {
                NSDictionary *response = [parsedObject objectForKey:@"data"];
                NSString* message = [response objectForKey:@"message"];
                NSString* success = [response objectForKey:@"status"];
                
                if ([success isEqualToString:@"Success"]) {
                    [userArray removeObjectAtIndex:ind];
                    if (userArray.count > 0) {
                        _tableview.hidden = false;
                    }else{
                        _tableview.hidden = true;
                    }
                    [Helper displayAlertView:@"" message:message];
                }else{
                    [Helper displayAlertView:@"" message:message];
                }
                [_tableview reloadData];
            }
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
