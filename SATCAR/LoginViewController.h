//
//  LoginViewController.h
//  SATCAR
//
//  Created by Percept Infotech on 12/7/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController<UITextFieldDelegate>
{
    NSString *regnum,*Pwd;
}
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UIButton *BtnLogin;
@property (weak, nonatomic) IBOutlet UIButton *btnForgotPassword;
- (IBAction)LoginPress:(id)sender;
- (IBAction)ForgotPassWPress:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *BtnBack;
- (IBAction)BackPress:(id)sender;

@end
