//
//  TrackerLocaitonMapViewController.h
//  SATCAR
//
//  Created by Percept Infotech on 13/01/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@interface TrackerLocaitonMapViewController : UIViewController
@property (weak, nonatomic) IBOutlet MKMapView *mapview;
@property (weak, nonatomic) IBOutlet NSString *strLat;
@property (weak, nonatomic) IBOutlet NSString *strLon;
@end
