//
//  GeneralSettingViewController.m
//  SATCAR
//
//  Created by Percept Infotech on 30/12/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import "GeneralSettingViewController.h"


@interface GeneralSettingViewController ()<UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource>
{
    NSMutableDictionary *userInfo,*generalSetting;
    NSArray *unitArray,*currencyArray,*languageArray;
    UIPickerView *measurePicker,*currencyPicker,*languagePicker;
    BOOL touched;
    NSString *showPopupRoute,*settingID,*emailotif,*selectedLanguage;
    AppDelegate *appdelegate;
}
@end

@implementation GeneralSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self addTitleView:NSLocalizedString(@"GeneralSetting", @"")];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    UIButton *btnAdd = [UIButton buttonWithType:UIButtonTypeCustom];
    btnAdd.frame = CGRectMake(20,0,50,32);
    [btnAdd setTitle:NSLocalizedString(@"Save", @"") forState:UIControlStateNormal];
    [btnAdd setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnAdd.titleLabel.font = [UIFont systemFontOfSize:16];
    [btnAdd addTarget:self action:@selector(callSaveSettingWS) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* item = [[UIBarButtonItem alloc] initWithCustomView:btnAdd];
    self.navigationItem.rightBarButtonItem = item;

    _lblemail.text = NSLocalizedString(@"Emailnotification", @"");
    _lblMeasure.text = NSLocalizedString(@"Unit_of_measure", @"");
    _lblCurrency.text = NSLocalizedString(@"Currency", @"");
    _lblLanguage.text = NSLocalizedString(@"Language", @"");
    _lblNotification.text = NSLocalizedString(@"Notification", @"");
    
    [_txtCurrency addDoneOnKeyboardWithTarget:self action:@selector(onCurrencyDone)];
    [_txtMeasure addDoneOnKeyboardWithTarget:self action:@selector(onMeasureDone)];
    [_txtLanguage addDoneOnKeyboardWithTarget:self action:@selector(onLanguageDone)];
    touched = false;
}

-(void)onCurrencyDone{
    if (touched) {
        touched = false;
    }else{
         _txtCurrency.text = [currencyArray objectAtIndex:0];
    }
    [_txtCurrency resignFirstResponder];
}
-(void)onMeasureDone{
    if (touched) {
        touched = false;
    }else{
        _txtMeasure.text = [unitArray objectAtIndex:0];
    }
    [_txtMeasure resignFirstResponder];
}

-(void)onLanguageDone{
    if (touched) {
        touched = false;
    }else{
        _txtLanguage.text = [languageArray objectAtIndex:0];
        appdelegate.appLanguage = @"en-GB";
    }
    [_txtLanguage resignFirstResponder];
}

-(void)viewWillAppear:(BOOL)animated{
    unitArray = [[NSArray alloc]initWithObjects: @"KM", @"MILES", nil];
    currencyArray = [[NSArray alloc]initWithObjects:@"DKK",@"EUR",@"SEK",@"NOK",@"GBP",@"CHF", nil];
    languageArray = [[NSArray alloc]initWithObjects:@"English (UK)",@"Danish (da-DK)", nil];
    
    //[self callGetCredentialsService: [[userInfo valueForKey:@"user"] valueForKey:@"id"]];
    NSData *userdata = [Helper getDataValue:@"userInfo"];
    NSDictionary * dict = [self dataToDicitonary:userdata];
    
    if (dict != nil) {
        NSLog(@"DATA : %@",dict);
        userInfo = [dict valueForKey:@"data"];
        
        //emailotif = [[userInfo valueForKey:@"user"] valueForKey:@"sendEmail"];
        emailotif = [[NSUserDefaults standardUserDefaults] valueForKey:@"email_notif"];
        if ([emailotif isEqualToString:@"0"] || emailotif == nil) {
            [_SwitchEmailNotif setOn:false animated:true];
        }else{
            [_SwitchEmailNotif setOn:true animated:true];
        }
        
        NSDictionary *generalSetting2  = [userInfo valueForKey:@"general_setting"];
        generalSetting = [[NSMutableDictionary alloc]init];
        NSArray *allk = [generalSetting2 allKeys];
        for (int i=0; i<allk.count; i++) {
            NSString *st = [allk objectAtIndex:i];
            NSString *val =[generalSetting2 valueForKey:st];
            if (val == (NSString *)[NSNull null] || val == nil) {
                if([st isEqualToString:@"notification"])
                {
                    [generalSetting setObject:@"0" forKey:st];
                }else if([st isEqualToString:@"trip_notification"])
                {
                    [generalSetting setObject:@"0" forKey:st];
                }else{
                    [generalSetting setObject:@"" forKey:st];
                }
                
            }else{
                [generalSetting setObject:val forKey:st];
            }
        }
        
        bool isAvbl = [Helper containsKey:@"trip_notification" data:generalSetting];
        if (isAvbl) {
            appdelegate.appTrackNotification = [generalSetting valueForKey:@"trip_notification"];
        }else{
            appdelegate.appTrackNotification = @"1";
        }
        [Helper setPREFID:appdelegate.appTrackNotification :@"trip_notification"];
        if ([appdelegate.appTrackNotification isEqualToString:@"0"]) {
            [_switchNotification setOn:false animated:true];
        }else{
            [_switchNotification setOn:true animated:true];
        }
        
        NSString *measure = [generalSetting valueForKey:@"unit"];
        _txtMeasure.text = measure.uppercaseString;
        
        NSString *currency = [generalSetting valueForKey:@"currency"];
        _txtCurrency.text = currency;
        settingID = [generalSetting valueForKey:@"id"];
        
        appdelegate.appLanguage = [generalSetting valueForKey:@"language"];
        
        if ([appdelegate.appLanguage isEqualToString:@""]) {
            _txtLanguage.text = [languageArray objectAtIndex:0];
            appdelegate.appLanguage = @"en-GB";
        }else if ([appdelegate.appLanguage isEqualToString:@"en-GB"]) {
            _txtLanguage.text = [languageArray objectAtIndex:0];
            appdelegate.appLanguage = @"en-GB";
        }else{
            _txtLanguage.text = [languageArray objectAtIndex:1];
            appdelegate.appLanguage = @"da-DK";
        }
        [Helper setPREFID:appdelegate.appLanguage :@"language"];
        [appdelegate setApplicationLanguage];
    }
    
    /*showPopupRoute = [Helper getPREF:@"showRoute"];
    if ([showPopupRoute isEqualToString:@"1"]) {
        [_SwitchShowRoute setOn:true animated:true];
    }else{
        [_SwitchShowRoute setOn:false animated:true];
    }*/
    
}

-(BOOL)isValidData{
    if (_txtMeasure.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_select_unit_of_measure",nil)];
        return false;
        
    }else if (_txtCurrency.text.length == 0){
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_select_currency",nil)];
        return false;
    }
    return true;
}

#pragma mark - textfield delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
   if(textField == _txtMeasure) {
        measurePicker = [[UIPickerView alloc] initWithFrame:CGRectZero];
        [measurePicker setDataSource: self];
        [measurePicker setDelegate: self];
        measurePicker.showsSelectionIndicator = YES;
        _txtMeasure.inputView = measurePicker;
        if (![_txtCurrency hasText]) {
           _txtMeasure.text = [unitArray objectAtIndex:0];
        }
       
    }else if(textField == _txtCurrency) {
        currencyPicker = [[UIPickerView alloc] initWithFrame:CGRectZero];
        [currencyPicker setDataSource: self];
        [currencyPicker setDelegate: self];
        currencyPicker.showsSelectionIndicator = YES;
        _txtCurrency.inputView = currencyPicker;
        if (![_txtCurrency hasText]) {
            _txtCurrency.text = [currencyArray objectAtIndex:0];
        }
    }else if(textField == _txtLanguage) {
        languagePicker = [[UIPickerView alloc] initWithFrame:CGRectZero];
        [languagePicker setDataSource: self];
        [languagePicker setDelegate: self];
        languagePicker.showsSelectionIndicator = YES;
        _txtLanguage.inputView = languagePicker;
        if (![_txtLanguage hasText]) {
            _txtLanguage.text = [languageArray objectAtIndex:0];
        }
    }
}

#pragma mark - Picker View Data source
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (pickerView == measurePicker) {
        return unitArray.count;
    }else if(pickerView == currencyPicker) {
        return currencyArray.count;
    }else if(pickerView == languagePicker) {
        return languageArray.count;
    }
    return 0;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component{
    if (pickerView == measurePicker) {
        [_txtMeasure setText:[unitArray objectAtIndex:row]];
    }else if(pickerView == currencyPicker) {
        [_txtCurrency setText:[currencyArray objectAtIndex:row]];
    }else if(pickerView == languagePicker) {
        [_txtLanguage setText:[languageArray objectAtIndex:row]];
        if (row == 0) {
            appdelegate.appLanguage = @"en-GB";
        }else{
            appdelegate.appLanguage = @"da-DK";
        }
    }
    touched = true;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow: (NSInteger)row forComponent:(NSInteger)component{
    if (pickerView == measurePicker) {
        return [NSString stringWithFormat:@"%@",[unitArray objectAtIndex:row]];
    }else if(pickerView == currencyPicker) {
        return [NSString stringWithFormat:@"%@",[currencyArray objectAtIndex:row]];
    }else if(pickerView == languagePicker) {
        return [NSString stringWithFormat:@"%@",[languageArray objectAtIndex:row]];
    }else{
        return 0;
    }
}

#pragma mark - Webservice call


-(void)callGetCredentialsService:(NSString *)uID{
    if(![self CheckNetworkConnection])
    {
        //[self AlertNoInternet:NSLocalizedString(@"NoInternet", @"") tag:454869];
        return;
    }
    NSString *userID = uID;
    NSLog(@"callGetCredentialsService");
    
    
    NSDictionary *parameter=@{@"userid":uID};
    UIWindow *window = [UIApplication sharedApplication].windows.lastObject;
    [MBProgressHUD showHUDAddedTo:window animated:true];
    
    [WebService httpPostWithCustomDelegateURLString:@".credentials" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error) {
        [MBProgressHUD hideHUDForView:window animated:true];
        if (error == nil)
        {
            NSError *localError = nil;
            NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
            
            if (localError != nil) {
                NSLog(@"%@",localError.description);
                
            }else
            {
                NSDictionary *response = [parsedObject objectForKey:@"data"];
                NSLog(@"GetCredential response : %@",response);
                NSString *status = [response valueForKey:@"status"];
                if ([status isEqualToString:@"Success"]) {
                    
                    // Save user value
                    NSDictionary* user = [response objectForKey:@"user"];
                    NSString *email = [user valueForKey:@"email"];
                    NSString *name = [user valueForKey:@"name"];
                    NSString *uID = [user valueForKey:@"id"];
                    
                    NSString* registerDate = [user objectForKey:@"registerDate"];
                    //NSString* sid = [response objectForKey:@"sid"];
                    NSString* userTyp = [response objectForKey:@"user_type"];
                    
                    [Helper setPREFStringValue:uID sKey:@"userID"];
                    //[Helper setPREFStringValue:sid sKey:@"sid"];
                    [Helper setPREFStringValue:name sKey:@"userName"];
                    [Helper setPREFStringValue:email sKey:@"userEmail"];
                    [Helper setPREFStringValue:userTyp sKey:@"user_type"];
                    [Helper setPREFStringValue:registerDate sKey:@"userRegisterDate"];
                    
                    [Helper setDataValue:data sKey:@"userInfo"];
                    
                    NSDictionary *generalSetting = [response objectForKey:@"general_setting"];
                    NSMutableDictionary *generalSetting2 = [[NSMutableDictionary alloc]init];
                    NSArray *allk = [generalSetting allKeys];
                    for (int i=0; i<allk.count; i++) {
                        NSString *st = [allk objectAtIndex:i];
                        NSString *val =[generalSetting valueForKey:st];
                        if (val == (NSString *)[NSNull null] || val == nil) {
                            if([st isEqualToString:@"notification"])
                            {
                                [generalSetting2 setObject:@"0" forKey:st];
                            }else if([st isEqualToString:@"trip_notification"])
                            {
                                [generalSetting2 setObject:@"0" forKey:st];
                            }else{
                                [generalSetting2 setObject:@"" forKey:st];
                            }
                            
                        }else{
                            [generalSetting2 setObject:val forKey:st];
                        }
                    }
                    
                    appdelegate.appTrackNotification = [generalSetting2 valueForKey:@"trip_notification"];
                    [Helper setPREFID:appdelegate.appTrackNotification :@"trip_notification"];
                    
                    [Helper setUserValue:generalSetting2 sKey:@"general_setting"];
                    [Helper setDataValue:data sKey:@"userInfo"];
                    
                    appdelegate.appLanguage   = [generalSetting2 valueForKey:@"language"];
                    [Helper setPREFID:appdelegate.appLanguage  :@"language"];
                    [appdelegate setApplicationLanguage];
                   
                    
                    //[self callLocationService:uID from:@""];
                    
                }
            }
        }
    }];
}


-(void)callSaveSettingWS{
    [self.view endEditing:YES];
    if(![self CheckNetworkConnection]) {
        return;
    }
    
    if ([self isValidData]) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSString *userID = [Helper getPREF:@"userID"];
        
        /*NSDictionary *parameter=@{
                                  @"userid":userID,
                                  @"unit":_txtMeasure.text.lowercaseString,
                                  @"currency":_txtCurrency.text.uppercaseString,
                                  @"language":appdelegate.appLanguage,
                                  @"trip_notification":appdelegate.appTrackNotification,
                                  @"id":settingID,
                                  };*/
        NSDictionary *parameter=@{
                                  
                                  @"userid":userID,
                                  @"unit":_txtMeasure.text.lowercaseString,
                                  @"currency":_txtCurrency.text.uppercaseString,
                                  @"language":appdelegate.appLanguage,
                                  @"trip_notification":appdelegate.appTrackNotification,
                                  @"id":settingID,
                                  @"notification" : emailotif,
                                  @"userid" : [[userInfo valueForKey:@"user"]valueForKey:@"id"]
                                  };
        
NSDictionary *parameter2=@{
                           @"general_setting" :parameter
                           };
        NSLog(@"Parameter : %@",parameter);
        
        [WebService httpPostWithCustomDelegateURLString:@".saveGeneralsetting" parameter:parameter2 isDebug:false callBackData:^(NSData *data, NSError *error)
         {
              [MBProgressHUD hideHUDForView:self.view animated:YES];
             if (error == nil) {
                 
                 NSError *localError = nil;
                 NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
                 
                 if (localError != nil) {
                     NSLog(@"%@",localError.description);
                 } else {
                     NSDictionary *response = [parsedObject objectForKey:@"data"];
                     NSString* message = [response objectForKey:@"message"];
                     NSString* success = [response objectForKey:@"status"];
                     NSLog(@"response %@",response.description);
                     if ([success isEqualToString:@"Success"]) {
                         [[NSUserDefaults standardUserDefaults] setValue:emailotif forKey:@"email_notif"];
                         [[NSUserDefaults standardUserDefaults] synchronize];
                         [Helper setPREFID:appdelegate.appLanguage :@"language"];
                         [appdelegate setApplicationLanguage];
                         
                         NSData *userdata = [Helper getDataValue:@"userInfo"];
                         NSDictionary *dataDict = [self dataToDicitonary:userdata];
                         userInfo = [dataDict valueForKey:@"data"];
                         
                         NSString *unit = _txtMeasure.text.lowercaseString;
                         NSDictionary *gSetting=@{ @"id":settingID, @"unit":unit, @"currency":_txtCurrency.text, @"language":appdelegate.appLanguage, @"trip_notification":appdelegate.appTrackNotification};
                         if (userInfo != nil) {
                             [userInfo setValue:gSetting forKey:@"general_setting"];
                             NSLog(@"DATA : %@",userInfo);
                             [dataDict setValue:userInfo forKey:@"data"];
                             NSData *data = [self dicitonaryToData:dataDict];
                             [Helper setDataValue:data sKey:@"userInfo"];
                         }
                         
                         NSLog(@"gSetting : %@",gSetting);
                         NSLog(@"DATA : %@",dataDict);
                         [Helper setPREFStringValue:_txtCurrency.text sKey:@"currency"];
                         [Helper setPREFStringValue:unit sKey:@"unit"];
                         [Helper setPREFStringValue:_txtLanguage.text sKey:@"language"];
                         [Helper setPREFStringValue:appdelegate.appTrackNotification sKey:@"trip_notification"];
                         
                         [Helper setUserValue:gSetting sKey:@"general_setting"];
                         [self callGetCredentialsService:[[userInfo valueForKey:@"user"]valueForKey:@"id"]];
                         [self viewWillAppear:true];
                         [self.navigationController popViewControllerAnimated:true];
                     } else {
                         NSLog(@"error :: %@",message);
                         [Helper displayAlertView:@"" message:message];
                     }
                 }
             }
         }];
    }else{
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onShowRoute:(id)sender {
    if ([sender isOn]) {
       showPopupRoute = @"1";
       [Helper setPREFStringValue:showPopupRoute sKey:@"showRoute"];
    }else{
        showPopupRoute = @"0";
        [Helper setPREFStringValue:showPopupRoute sKey:@"showRoute"];
    }
}

- (IBAction)onEmailNotif:(id)sender{
    if ([sender isOn]) {
        emailotif = @"1";
    }else{
        emailotif = @"0";
    }
    
    
    
}

- (IBAction)onNotification:(id)sender {
    
    if ([sender isOn]) {
        appdelegate.appTrackNotification = @"1";
       // [[UIApplication sharedApplication] registerForRemoteNotifications];
    }else{
        appdelegate.appTrackNotification = @"0";
        //[[UIApplication sharedApplication] unregisterForRemoteNotifications];
    }
    [Helper setPREFID:appdelegate.appTrackNotification :@"trip_notification"];
}
@end
