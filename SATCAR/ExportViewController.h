//
//  ExportViewController.h
//  SATCAR
//
//  Created by Percept Infotech on 29/12/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExportViewController : UIViewController
{
    
}
@property (weak, nonatomic) IBOutlet UILabel *lblWhatDoExport;
@property (weak, nonatomic) IBOutlet UILabel *lblPeriod;
@property (weak, nonatomic) IBOutlet UILabel *lblPeriodFrom;
@property (weak, nonatomic) IBOutlet UITextField *txtPeriodFromValue;
@property (weak, nonatomic) IBOutlet UILabel *lblPeriodTo;
@property (weak, nonatomic) IBOutlet UITextField *txtPeriodToValue;
@property (weak, nonatomic) IBOutlet UILabel *lblFormate;
@property (weak, nonatomic) IBOutlet UITextField *txtFormate;


@property (weak, nonatomic) IBOutlet UIView *ViewSegment;
@property (weak, nonatomic) IBOutlet UIButton *BtnAllSegment;
@property (weak, nonatomic) IBOutlet UIButton *BtnPrivateSegment;
@property (weak, nonatomic) IBOutlet UIButton *BtnBusinessSegment;
@property (weak, nonatomic) IBOutlet UIButton *BtnOverviewSegment;
@property (weak, nonatomic) IBOutlet UIButton *BtnPrivateReimbursement;
- (IBAction)SelctSegment:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *viewPrivateExport;
@property (weak, nonatomic) IBOutlet UIView *viewPrivateReimbursement;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintPrivateExportViewTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintPrivateExportViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintPrivateReimbursementTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintPrivateReimbursementHeight;

@property (weak, nonatomic) IBOutlet UILabel *lblPSelectAll;
@property (weak, nonatomic) IBOutlet UILabel *lblPType;
@property (weak, nonatomic) IBOutlet UILabel *lblPDate;
@property (weak, nonatomic) IBOutlet UILabel *lblPStartTime;
@property (weak, nonatomic) IBOutlet UILabel *lblPEndTime;
@property (weak, nonatomic) IBOutlet UILabel *lblPDistance;
@property (weak, nonatomic) IBOutlet UILabel *lblPOStart;
@property (weak, nonatomic) IBOutlet UILabel *lblPOEnd;
@property (weak, nonatomic) IBOutlet UILabel *lblPSAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblPEAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblPSCoordinate;
@property (weak, nonatomic) IBOutlet UILabel *lblPECoordinate;
@property (weak, nonatomic) IBOutlet UILabel *lblPReimbursement;
@property (weak, nonatomic) IBOutlet UILabel *lblPNote;

@property (weak, nonatomic) IBOutlet UIImageView *imgPSelectAll;
@property (weak, nonatomic) IBOutlet UIImageView *imgPType;
@property (weak, nonatomic) IBOutlet UIImageView *imgPDate;
@property (weak, nonatomic) IBOutlet UIImageView *imgPStartTime;
@property (weak, nonatomic) IBOutlet UIImageView *imgPEndTime;
@property (weak, nonatomic) IBOutlet UIImageView *imgPDistance;
@property (weak, nonatomic) IBOutlet UIImageView *imgPOStart;
@property (weak, nonatomic) IBOutlet UIImageView *imgPOEnd;
@property (weak, nonatomic) IBOutlet UIImageView *imgPSAddress;
@property (weak, nonatomic) IBOutlet UIImageView *imgPEAddress;
@property (weak, nonatomic) IBOutlet UIImageView *imgPSCoordinate;
@property (weak, nonatomic) IBOutlet UIImageView *imgPECoordinate;
@property (weak, nonatomic) IBOutlet UIImageView *imgPReimbursement;
@property (weak, nonatomic) IBOutlet UIImageView *imgPNote;

- (IBAction)onPSelectAll:(id)sender;
- (IBAction)onPType:(id)sender;
- (IBAction)onPDate:(id)sender;
- (IBAction)onPStartTime:(id)sender;
- (IBAction)onPEndTime:(id)sender;
- (IBAction)onPDistance:(id)sender;
- (IBAction)onPOStart:(id)sender;
- (IBAction)onPOEnd:(id)sender;
- (IBAction)onPSAddress:(id)sender;
- (IBAction)onPEAddress:(id)sender;
- (IBAction)onPSCoordinate:(id)sender;
- (IBAction)onPECoordinate:(id)sender;
- (IBAction)onReimbursement:(id)sender;
- (IBAction)onPNote:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *viewBusinessExport;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintBusinessExportViewTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintBusinessExportViewHeight;
@property (weak, nonatomic) IBOutlet UILabel *lblBIncludeExpo;
@property (weak, nonatomic) IBOutlet UILabel *lblBSelectAll;
@property (weak, nonatomic) IBOutlet UILabel *lblBAccountTpe;
@property (weak, nonatomic) IBOutlet UILabel *lblBDate;
@property (weak, nonatomic) IBOutlet UILabel *lblBName;
@property (weak, nonatomic) IBOutlet UILabel *lblBAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblBCurrency;
@property (weak, nonatomic) IBOutlet UILabel *lblBNotes;
@property (weak, nonatomic) IBOutlet UILabel *lblBZip;

@property (weak, nonatomic) IBOutlet UIImageView *imgBSelectAll;
@property (weak, nonatomic) IBOutlet UIImageView *imgBAccountTpe;
@property (weak, nonatomic) IBOutlet UIImageView *imgBDate;
@property (weak, nonatomic) IBOutlet UIImageView *imgBName;
@property (weak, nonatomic) IBOutlet UIImageView *imgBAmount;
@property (weak, nonatomic) IBOutlet UIImageView *imgBCurrency;
@property (weak, nonatomic) IBOutlet UIImageView *imgBNotes;
@property (weak, nonatomic) IBOutlet UIImageView *imgBZip;

- (IBAction)onBSelectAll:(id)sender;
- (IBAction)onBAccountTpe:(id)sender;
- (IBAction)onBDate:(id)sender;
- (IBAction)onBName:(id)sender;
- (IBAction)onBAmount:(id)sender;
- (IBAction)onBCurrency:(id)sender;
- (IBAction)onBNotes:(id)sender;
- (IBAction)onBZip:(id)sender;


@property (weak, nonatomic) IBOutlet UIView *viewOverviewExport;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintOverviewViewTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintOverviewViewHeight;
@property (weak, nonatomic) IBOutlet UILabel *lblBOverview;
@property (weak, nonatomic) IBOutlet UILabel *lblOSelectAll;
@property (weak, nonatomic) IBOutlet UILabel *lblOTripSplit;
@property (weak, nonatomic) IBOutlet UILabel *lblOFinancialSplit;
@property (weak, nonatomic) IBOutlet UILabel *lblOTotals;

@property (weak, nonatomic) IBOutlet UIImageView *imgOSelectAll;
@property (weak, nonatomic) IBOutlet UIImageView *imgOTripSplit;
@property (weak, nonatomic) IBOutlet UIImageView *imgOFinancialSplit;
@property (weak, nonatomic) IBOutlet UIImageView *imgOTotals;
- (IBAction)onOSelectAll:(id)sender;
- (IBAction)onOTripSplit:(id)sender;
- (IBAction)onOFinancialSplit:(id)sender;
- (IBAction)onOTotals:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *lblAdditional;
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (weak, nonatomic) IBOutlet UIButton *btnExport;
- (IBAction)onExport:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintTblHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintTableviewTop;
@property (weak, nonatomic) IBOutlet UILabel *lblPrivateBottom;
@property (weak, nonatomic) IBOutlet UILabel *lblBusinessBottom;
@property (weak, nonatomic) IBOutlet UILabel *lblOverviewBottom;


@end
