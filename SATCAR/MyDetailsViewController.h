//
//  MyDetailsViewController.h
//  SATCAR
//
//  Created by Percept Infotech on 30/12/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyDetailsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblCPR;
@property (weak, nonatomic) IBOutlet UILabel *lblBY;
@property (weak, nonatomic) IBOutlet UILabel *lblTelefon;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblEmailAdd;
@property (weak, nonatomic) IBOutlet UILabel *lblPost;
@property (weak, nonatomic) IBOutlet UILabel *lblWantOffer;
@property (weak, nonatomic) IBOutlet UILabel *lblPwd;
//@property (weak, nonatomic) IBOutlet UITextField *txtHouseNumber;

@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextField *txtCPR;
@property (weak, nonatomic) IBOutlet UITextField *txtBY;
@property (weak, nonatomic) IBOutlet UITextField *txtTelefon;
@property (weak, nonatomic) IBOutlet UITextField *txtAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtEmailAdd;
@property (weak, nonatomic) IBOutlet UITextField *txtPost;
@property (weak, nonatomic) IBOutlet UISwitch *switchWantOffer;

@end
