//
//  HomeViewController.h
//  SATCAR
//
//  Created by Percept Infotech on 12/16/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <MapKit/MKAnnotation.h>
#import <CoreLocation/CoreLocation.h>
#import "CallOutAnnotationView.h"
#import "ModelManager.h"

@interface HomeViewController : UIViewController<UIGestureRecognizerDelegate,MKMapViewDelegate,CLLocationManagerDelegate,UISearchDisplayDelegate, UISearchBarDelegate,UIAlertViewDelegate>
{
    MKPointAnnotation *selectedPlaceAnnotation;
    NSMutableArray *locationArr;
    //NSString *startTrip;
}
@property (weak, nonatomic) IBOutlet UIButton *BtnBeconView;
@property (weak, nonatomic) IBOutlet UILabel *Lbl_beacon_status_msg;
@property (weak, nonatomic) IBOutlet UIView *viewBeaconStatus;
@property (nonatomic, strong) CallOutAnnotationView *calloutView;
@property (nonatomic, strong) NSMutableArray *calloutViewArray;
@property (weak, nonatomic) IBOutlet NSString *fromTrip;
@property (weak, nonatomic) IBOutlet UIImageView *imgArrow;
@property (weak, nonatomic) IBOutlet UIView *ViewBottom;
@property (strong, nonatomic) IBOutlet MKMapView *MapViewMy;
@property (weak, nonatomic) IBOutlet UIButton *btnShareLocation;
@property (weak, nonatomic) IBOutlet UIButton *btnPrivateTrip;
@property (weak, nonatomic) IBOutlet UIButton *BtnBusinessTrip;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) NSMutableArray *offlineTripArray;
@property (strong, nonatomic) Model *model;

- (IBAction)onYes:(id)sender;
- (IBAction)onNo:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintBackRouteTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintBackRouteHeight;
- (IBAction)onPrivateTrip:(id)sender;
- (IBAction)onBusinessTrip:(id)sender;
-(void)onPrivateTripFunction;

@property (weak, nonatomic) IBOutlet UIView *viewDistance;
@property (weak, nonatomic) IBOutlet UIView *viewFinishTrip;
@property (weak, nonatomic) IBOutlet UILabel *lblKM;
@property (weak, nonatomic) IBOutlet UILabel *lblHours;
@property (weak, nonatomic) IBOutlet UIButton *btnFinishTrip;
- (IBAction)onFinishTrip:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewTripBack;
@property (strong, nonatomic) NSTimer *timer;
@property (strong, nonatomic) CLLocation *startLocation;
-(void)LoadMapRoute;
-(void)initData;
+ (HomeViewController *)sharedMySingleton;
-(void)viewDidLoadViewWillAppear;
-(void)oldMapRouteLoad;
@end
