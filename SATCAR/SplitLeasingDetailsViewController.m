//
//  SplitLeasingDetailsViewController.m
//  SATCAR
//
//  Created by Percept Infotech on 30/12/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import "SplitLeasingDetailsViewController.h"
#import "UIViewController+VCHelpers.h"
#import "ECSlidingViewController.h"
#import "LeftSideViewController.h"
#import "NavigationController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "UIViewController+VCHelpers.h"
#import "EmailTVC.h"
#import "AddEmailTVC.h"

@interface SplitLeasingDetailsViewController ()<UITextFieldDelegate>
{
    int tblHeight;
    NSMutableArray *emailArray;
    EmailTVC *emailCell;
    float cellHeight;
    NSMutableDictionary *userInfo;

}
@end

@implementation SplitLeasingDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _tableview.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    tblHeight = 150;
    cellHeight = 70;
    _IBLayoutConstraintTblHeight.constant = tblHeight;
    emailArray = [[NSMutableArray alloc]initWithObjects:@"", nil];
}

-(void)viewWillAppear:(BOOL)animated{
    [self addTitleView:NSLocalizedString(@"SplitLeasingSettings", @"")];
    
    _lblAmountPaid.text = NSLocalizedString(@"SplitMessage", @"");
    _lblMonthlyCost.text = NSLocalizedString(@"MonthlyCost", @"");
    _lblDurationCost.text = NSLocalizedString(@"DurationOfYear", @"");
    
    _btnSendEmail.layer.cornerRadius = 25;
    [_btnSendEmail setTitle:NSLocalizedString(@"SendEmail", @"") forState:UIControlStateNormal];
    
    _lblDKK.text = NSLocalizedString(@"DKK", @"");
    _lblSendMonthlyInvoice.text = NSLocalizedString(@"SendMonthlyInvoiceTo", @"");
    _lblMonth.text = NSLocalizedString(@"Month", @"");
    
    NSData *userdata = [Helper getDataValue:@"userInfo"];
    NSDictionary *dataDict = [self dataToDicitonary:userdata];
    if (![dataDict isKindOfClass:[NSNull class]]) {
        userInfo = [dataDict valueForKey:@"data"];
        NSString *amt_upfront = [self ConvertToDKCurrency:[userInfo valueForKey:@"amt_upfront"]];
        NSString *monthly_cost = [self ConvertToDKCurrency:[userInfo valueForKey:@"monthly_cost"]];
        NSString *duration_of_contract = [self ConvertToDKCurrency:[userInfo valueForKey:@"duration_of_contract"]];
        _txtAmountPaid.text =amt_upfront;
        _txtMonthlyCost.text = monthly_cost;
        _txtDurationCost.text = duration_of_contract;
    }else{
        [Helper displayAlertView:@"" message:NSLocalizedString(@"something_went_wrong_msg", @"")];
    }
    
}

#pragma mark - Table View datasource and delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return emailArray.count + 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return cellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < emailArray.count) {
        EmailTVC *cell = (EmailTVC *)[tableView dequeueReusableCellWithIdentifier:@"cellEamil"];
        cell.txtEmail.placeholder = NSLocalizedString(@"Email", @"");
        emailCell = cell;
        return cell;
    }else{
        AddEmailTVC *cell = (AddEmailTVC *)[tableView dequeueReusableCellWithIdentifier:@"cellAddEamil"];
        [cell.btnAddEmail addTarget:self action:@selector(onAddEmail:) forControlEvents:UIControlEventTouchUpInside];
        cell.lblAddEmail.text = NSLocalizedString(@"AddAnotherEmail", @"");
        return cell;
    }
}

#pragma mark - Button action
- (IBAction)onAddEmail:(id)sender {
    if (emailArray.count <= 2) {
        [emailArray addObject:@""];
        tblHeight = 150 + ((int)(emailArray.count - 1) * 70);
        _IBLayoutConstraintTblHeight.constant = tblHeight;
        [_tableview reloadData];
    }
}

- (IBAction)onSendEmail:(id)sender {
    NSString *emailStr = @"";
    for (int i = 0; i < emailArray.count; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        emailCell = [_tableview cellForRowAtIndexPath:indexPath];
        if (emailCell.txtEmail.text.length > 0) {
            if (i == emailArray.count - 1) {
                emailStr = [emailStr stringByAppendingString:[NSString stringWithFormat:@"%@",emailCell.txtEmail.text]];
            }else{
                emailStr = [emailStr stringByAppendingString:[NSString stringWithFormat:@"%@,",emailCell.txtEmail.text]];
            }
        }
    }
    if ([emailStr length] > 0) {
        NSString *lastChar = [emailStr substringFromIndex:[emailStr length] - 1];
        if ([lastChar isEqualToString:@","]) {
            emailStr = [emailStr substringToIndex:[emailStr length] - 1];
        }
        NSLog(@"Email : %@",emailStr);
        NSString *userID = [Helper getPREF:@"userID"];
        NSDictionary *parameter=@{
                                  @"userid":userID,
                                  @"email":emailStr
                                  };
        //NSLog(@"parameter : %@",parameter);
        [self callSendSplitLeasingEmailWS:parameter];
        
    }else{
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_enter_email_id",nil)];
    }
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    textField.textColor =  [UIColor colorWithRed:18.0/255.0 green:172.0/255.0 blue:243.0/255.0 alpha:1.0];
    textField.layer.borderWidth = 1;
    textField.layer.borderColor =  [UIColor colorWithRed:18.0/255.0 green:172.0/255.0 blue:243.0/255.0 alpha:1.0].CGColor;
    textField.clipsToBounds = YES;
    return true;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    textField.textColor =  [UIColor blackColor];
    textField.layer.borderWidth = 0;
    textField.layer.borderColor = [UIColor clearColor].CGColor;
    textField.clipsToBounds = YES;
    return true;
}

#pragma mark - Webservice call
-(void)callSendSplitLeasingEmailWS:(NSDictionary *)parameter{
    if(![self CheckNetworkConnection])
    {
        //[self AlertNoInternet:NSLocalizedString(@"NoInternet", @"") tag:454869];
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [WebService httpPostWithCustomDelegateURLString:@".split" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         if (error == nil) {
             
             NSError *localError = nil;
             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
             
             if (localError != nil) {
                 NSLog(@"%@",localError.description);
             }
             else {
                 NSDictionary *response = [parsedObject objectForKey:@"data"];
                 NSString* message = [response objectForKey:@"message"];
                 NSString* success = [response objectForKey:@"status"];
                 if ([success isEqualToString:@"Success"]) {
                     [Helper displayAlertView:@"" message:message];
                     [self.navigationController popViewControllerAnimated:true];
                 } else {
                     NSLog(@"error :: %@",message);
                     [Helper displayAlertView:@"" message:message];
                 }
             }
         }
     }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
