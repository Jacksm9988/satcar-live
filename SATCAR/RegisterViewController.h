//
//  RegisterViewController.h
//  SATCAR
//
//  Created by Percept Infotech on 12/7/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterViewController : UIViewController<UITextFieldDelegate>
{
    NSString *fname,*lname,*email,*regid,*pwd;
    UIToolbar *TollBar;
    UIBarButtonItem *Flexibalspace;
}
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
- (IBAction)RegisterPress:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtFname;
@property (weak, nonatomic) IBOutlet UITextField *txtLname;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtId;
@property (weak, nonatomic) IBOutlet UITextField *txtPwd;
@property (weak, nonatomic) IBOutlet UILabel *lblNote;
- (IBAction)SwitchChange:(id)sender;
@property(nonatomic,strong) NSString *userFrom;
@property(nonatomic,strong) NSString *amount;
@property(nonatomic,strong) NSString *car_beep;
@property(nonatomic,strong) NSString *gps_tracker;
@property (weak, nonatomic) IBOutlet UISwitch *switchNews;
//@property (weak, nonatomic) IBOutlet UITextField *txtHouseNumber;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;
@property (nonatomic, strong) NSDictionary *shipDict;
@property(nonatomic, strong) NSString *ordernum;
@property(nonatomic, strong) NSString *trackerSelected;

@property (weak, nonatomic) IBOutlet UITextField *txtAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtpostcode;
@property (weak, nonatomic) IBOutlet UITextField *txtcity;
@property (weak, nonatomic) IBOutlet UITextField *txtcountry;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintAddressTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintAddressHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintNoteTop;

@end
