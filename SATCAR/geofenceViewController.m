//
//  geofenceViewController.m
//  SATCAR
//
//  Created by Percept Infotech on 03/04/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import "geofenceViewController.h"
#import "WildcardGestureRecognizer.h"
#import "CustomMapOverlay.h"


/* Default radius size in PX*/
double const circleRadius = 0;

/* Default Location */
#define TOKYO_LATITUDE 35.6895
#define TOKYO_LONGITUDE 139.6917
#define ZOOM_DISTANCE 500
#define DEFAULT_RADIUS 500


double oldoffset;
double setRadius = DEFAULT_RADIUS;

bool panEnabled = YES;
CLLocationCoordinate2D droppedAt;
MKMapPoint lastPoint;
CustomMKCircleOverlay *circleView;

@interface geofenceViewController ()<MKMapViewDelegate,CLLocationManagerDelegate>
{
    AppDelegate *appdelegate;
    CLGeocoder *geocoder;
    NSArray *carLocationArray;
    CLLocationCoordinate2D Coordinates;
    CLLocationCoordinate2D PreviousCoordinates;
    //MKCircle *circle;
    int mapClicked,circleClicked,afterMoving;
    UITapGestureRecognizer *mapTap;
    UITapGestureRecognizer *circleTap;
    NSString *tempDistance;
    int locationNotFound;
}
@end

@implementation geofenceViewController
@synthesize mapview;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addTitleView:NSLocalizedString(@"Geofence", @"")];
    mapClicked = 0;
    locationNotFound = 0;
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    if ([appdelegate.selectedRange doubleValue] <= 0) {
        appdelegate.selectedRange = @"500";
    }
    setRadius = [appdelegate.selectedRange doubleValue];
    
    if(setRadius > 1000){
        _lblRadius.text = [NSString stringWithFormat:@"Radius : %.02f km", setRadius / 1000];
        tempDistance = [NSString stringWithFormat:@"%.02f",setRadius / 1000];
    }else{
        _lblRadius.text = [NSString stringWithFormat:@"Radius : %.f m", setRadius];
        tempDistance = [NSString stringWithFormat:@"%.0f",setRadius];
    }
    circleClicked = 1;
    tempDistance = @"";
    // appdelegate instance
    geocoder = [[CLGeocoder alloc] init] ;
    
    // setup locationManager
    _locationManager.delegate = self;
    _locationManager.distanceFilter = kCLLocationAccuracyNearestTenMeters;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    mapview.delegate = self;
    
    UIButton *btnAdd = [UIButton buttonWithType:UIButtonTypeCustom];
    btnAdd.frame = CGRectMake(20,0,50,32);
    [btnAdd setTitle:NSLocalizedString(@"Save", @"") forState:UIControlStateNormal];
    [btnAdd setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnAdd.titleLabel.font = [UIFont systemFontOfSize:16];
    [btnAdd addTarget:self action:@selector(onSave:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* item = [[UIBarButtonItem alloc] initWithCustomView:btnAdd];
    self.navigationItem.rightBarButtonItem = item;
    
    //mapTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapTouchTap:)];
   // [mapview addGestureRecognizer:mapTap];
    
    _locationManager = [CLLocationManager new];
    if ([_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [_locationManager requestAlwaysAuthorization]; 
    }
    [self callInitialDanttrackerApi];
}

#pragma mark - Mapview Delegate
-(void)Initialized:(CLLocationCoordinate2D)location{
    mapview.delegate = self;
    mapClicked = 0;
    afterMoving = 0;
    circleClicked = 0;
    droppedAt = CLLocationCoordinate2DMake(location.latitude, location.longitude);
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(droppedAt,setRadius*2,setRadius*2);
    [mapview setRegion:[mapview regionThatFits:region] animated:YES];
    /* Add Custom MKCircle with Annotation*/
    [self addCircle];
    
    /* Setup Touch listener for custom circle */
    WildcardGestureRecognizer * tapInterceptor = [[WildcardGestureRecognizer alloc] init];
    tapInterceptor.touchesBeganCallback = ^(NSSet * touches, UIEvent * event) {
        UITouch *touch = [touches anyObject];
        CGPoint p = [touch locationInView:mapview];
        CLLocationCoordinate2D coord = [mapview convertPoint:p toCoordinateFromView:mapview];
        
        NSLog(@"Touch begin");
        MKMapPoint mapPoint = MKMapPointForCoordinate(coord);
        MKMapRect mapRect = [circleView circlebounds];
        
        double xPath = mapPoint.x - (mapRect.origin.x - (mapRect.size.width/2));
        double yPath = mapPoint.y - (mapRect.origin.y - (mapRect.size.height/2));
        
        /* Test if the touch was within the bounds of the circle */
        if(xPath >= 0 && yPath >= 0 && xPath < mapRect.size.width && yPath < mapRect.size.height){
            //NSLog(@"Disable Map Panning");
            NSLog(@"Circle clicked");
            /*
             This block is to ensure scrollEnabled = NO happens before the any move event.
             */
            circleClicked = circleClicked + 1;
            mapview.scrollEnabled = NO;
            
            __block int t = 0;
            dispatch_async(dispatch_get_main_queue(), ^{
                t = 1;
                mapview.scrollEnabled = NO;
                
                panEnabled = NO;
                oldoffset = [circleView getCircleRadius];
            });
        }else{
            // mapview.scrollEnabled = YES;
        }
        lastPoint = mapPoint;
    };
    
    tapInterceptor.touchesMovedCallback = ^(NSSet * touches, UIEvent * event) {
        if(!panEnabled && [event allTouches].count == 1){
            UITouch *touch = [touches anyObject];
            CGPoint p = [touch locationInView:mapview];
            
            CLLocationCoordinate2D coord = [mapview convertPoint:p toCoordinateFromView:mapview];
            MKMapPoint mapPoint = MKMapPointForCoordinate(coord);
            NSLog(@"radius: %f", [circleView getCircleRadius]);
            
            double meterDistance = (mapPoint.x - lastPoint.x)/MKMapPointsPerMeterAtLatitude(mapview.centerCoordinate.latitude)+oldoffset;
            if(meterDistance > 0){
                [circleView setCircleRadius:meterDistance];
            }
            afterMoving = 1;
            setRadius = circleView.getCircleRadius;
            
            NSString *distance;
            if(setRadius > 1000){
                distance = [NSString stringWithFormat:@"%.02f km", setRadius / 1000];
                tempDistance = [NSString stringWithFormat:@"%.02f",setRadius / 1000];
            }else{
                distance = [NSString stringWithFormat:@"%.f m", setRadius];
                tempDistance = [NSString stringWithFormat:@"%.0f",setRadius];
            }
            //[self.distanceLabel setText:distance];
            NSLog(@"Distance : %@",distance);
        }
    };
    
    tapInterceptor.touchesEndedCallback = ^(NSSet * touches, UIEvent * event) {

        NSLog(@"Touch End");
        UITouch *touch = [touches anyObject];
        CGPoint p = [touch locationInView:mapview];
        CLLocationCoordinate2D coord = [mapview convertPoint:p toCoordinateFromView:mapview];
        
        if (afterMoving != 1) {
            if (mapClicked == 0) {
                mapClicked = mapClicked + 1;
                NSLog(@"Map cliked : %d",mapClicked);
                [self addCircle];
                panEnabled = YES;
                mapview.scrollEnabled = YES;
                mapview.userInteractionEnabled = YES;
            }else{
                mapClicked = 0;
                NSLog(@"Map cliked : %d",mapClicked);
                droppedAt = coord;
                [self addCircle];
            }
        }else{
            afterMoving = 0;
        }
    };
    
    [mapview addGestureRecognizer:tapInterceptor];
}

- (void)mapView:(MKMapView *)mapView  annotationView:(MKAnnotationView *)annotationView didChangeDragState:(MKAnnotationViewDragState)newState fromOldState:(MKAnnotationViewDragState)oldState {
    if(newState == MKAnnotationViewDragStateStarting){
        panEnabled = YES;
    }
    if (newState == MKAnnotationViewDragStateEnding) {
        droppedAt = annotationView.annotation.coordinate;
        //NSLog(@"dropped at %f,%f", droppedAt.latitude, droppedAt.longitude);
        [self addCircle];
    }
}
- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated{
    NSLog(@"regionWillChangeAnimated ");
    afterMoving = 1;
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated{
    NSLog(@"regionDidChangeAnimated ");
    /*let zoomWidth = mapView.visibleMapRect.size.width
     let zoomFactor = Int(log2(zoomWidth)) - 9
     print("...REGION DID CHANGE: ZOOM FACTOR \(zoomFactor)")*/
    
    float zoomWidth = mapview.visibleMapRect.size.width;
    int zoomFactor = log2(zoomWidth) - 9 ;
    NSLog(@"...REGION DID CHANGE: ZOOM FACTOR %d ",zoomFactor);
    afterMoving = 1;
}


- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    MKPinAnnotationView *annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"pin"];
    annotationView.image =  [UIImage imageNamed:@"car_position"];
    [annotationView setDraggable:false];
    // annotationView.pinColor = MKPinAnnotationColorPurple;
    [annotationView setSelected:YES animated:YES];
    
    /*
     NSLog(@"%@",mapview.userLocation);
     static NSString *annotationViewReuseIdentifier = @"annotationViewReuseIdentifier";
     MKAnnotationView *annotationView = (MKAnnotationView *)[mapview dequeueReusableAnnotationViewWithIdentifier:annotationViewReuseIdentifier];
     
     if (annotationView == nil) {
     annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationViewReuseIdentifier];
     }
     UIGraphicsEndImageContext();
     annotationView.image =  [UIImage imageNamed:@"car_position"];
     annotationView.annotation = annotation;
     annotationView.canShowCallout = YES;
     [annotationView setDraggable:true];
     [annotationView setSelected:true];
     */
    return [annotationView init];
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id < MKOverlay >)overlay{
    
    circleView = [[CustomMKCircleOverlay alloc] initWithCircle:overlay];
    circleView.fillColor = [UIColor blueColor];
    circleView.delegate = self;
    circleView.border = 100;
    
    circleView.alpha = 0.5f;
    if (mapClicked % 2 == 0) {
        
        circleView.border = 30;
        circleView.strokeColor = [UIColor blackColor];
        panEnabled = false;
        mapview.scrollEnabled = false;
        mapview.userInteractionEnabled = true;
        
    }else{
        circleView.border = 0.1;
        circleView.strokeColor = [UIColor blackColor];
    }
    return circleView;
}

#pragma mark - button actions
- (IBAction)onSave:(id)sender {
    
    appdelegate.selectedRange = tempDistance;
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)onRadiusChange:(CGFloat)radius{
    //NSLog(@"on radius change: %f", radius);
}

-(void)addCircle{
    
    if(circle != nil)
    [mapview removeOverlay:circle];
    circle = [MKCircle circleWithCenterCoordinate:droppedAt radius:1000.0]; //Set the radius to the maximum circle size or larger
    [mapview addOverlay: circle];
    [circleView setCircleRadius:setRadius];
    
    if(point == nil){
        point = [[MKPointAnnotation alloc] init];
        point.coordinate = droppedAt;
        [mapview addAnnotation:point];
    }
}

/*-(void)tapTouchTap:(UITapGestureRecognizer*)touchGesture {
    
    PreviousCoordinates = Coordinates;
    CGPoint touchPoint = [touchGesture locationInView:mapview];
    CLLocationCoordinate2D Coordinates1 = [mapview convertPoint:touchPoint toCoordinateFromView:mapview];
    BOOL isCircle = [self isPoint:Coordinates1 insideCircleCentre:PreviousCoordinates radius:100];
    NSLog(@"isCircle : %d",isCircle);
    if (!isCircle) {
        if (mapClicked == 0) {
            mapClicked = mapClicked + 1;
            [self removeCircle];
            [self addCircle:Coordinates];
        }else{
            mapClicked = 0;
            CGPoint touchPoint = [touchGesture locationInView:mapview];
            Coordinates = [mapview convertPoint:touchPoint toCoordinateFromView:mapview];
            NSLog(@"Location found from Map: %f %f",Coordinates.latitude,Coordinates.longitude);
            [self removeCircle];
            [self addCircle:Coordinates];
        }
    }else{
        mapClicked = 0;
        [self removeCircle];
        [self addCircle:Coordinates];
    }
    
    MKMapView *mapView = (MKMapView *)touchGesture.view;
    
    CGPoint tapPoint = [touchGesture locationInView:mapView];
    NSLog(@"tapPoint = %f,%f",tapPoint.x, tapPoint.y);
    CLLocationCoordinate2D tapCoordinate = [mapView convertPoint:tapPoint toCoordinateFromView:mapView];
    MKMapPoint point = MKMapPointForCoordinate(tapCoordinate);
    if (mapView.overlays.count > 0 ) {
        for (id<MKOverlay> overlay in mapView.overlays) {
            if ([overlay isKindOfClass:[MKCircle class]]) {
                MKCircle *circle = overlay;
                MKCircleRenderer *circleRenderer = (MKCircleRenderer *)[mapView rendererForOverlay:circle];
                CGPoint datpoint = [circleRenderer pointForMapPoint:point];
                [circleRenderer invalidatePath];
                if (CGPathContainsPoint(circleRenderer.path, nil, datpoint, false)){
                    NSLog(@"tapped on overlay");
                    break;
                }
            }
        }
    }
}

-(void)addCircle:(CLLocationCoordinate2D )location{
    
    _calloutView = [[CallOutAnnotationView alloc] initWithName:@"" address:@"" phonenumber:@"" coordinate:location];
    _calloutView.contentView.frame = CGRectMake(_calloutView.contentView.frame.origin.x, _calloutView.contentView.frame.origin.y, _calloutView.contentView.frame.size.width, 300);
    [mapview addAnnotation:_calloutView];
    circle = [MKCircle circleWithCenterCoordinate:location radius:100];
    [mapview addOverlay:circle];
}

-(void)removeCircle{
    [mapview removeAnnotations:mapview.annotations];
    [mapview removeOverlays: mapview.overlays];
}

- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated{
    NSLog(@"regionWillChangeAnimated ");
    
}
- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated{
    NSLog(@"regionDidChangeAnimated ");
    float zoomWidth = mapview.visibleMapRect.size.width;
    int zoomFactor = log2(zoomWidth) - 9 ;
    NSLog(@"...REGION DID CHANGE: ZOOM FACTOR %d ",zoomFactor);
}

-(BOOL)isPoint:(CLLocationCoordinate2D)point insideCircleCentre:(CLLocationCoordinate2D)centre radius:(CGFloat)radius {
    CLLocation *pointALocation = [[CLLocation alloc] initWithLatitude:point.latitude longitude:point.longitude];
    CLLocation *pointBLocation = [[CLLocation alloc] initWithLatitude:centre.latitude longitude:centre.longitude];
    double distanceMeters = [pointALocation distanceFromLocation:pointBLocation];
    if (distanceMeters > radius) {
        return NO;
    } else {
        return YES;
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView1 viewForAnnotation:(id <MKAnnotation>)annotation {
    if([annotation isKindOfClass:[CallOutAnnotationView class]]) {
        
        NSLog(@"%@",mapView1.userLocation);
        static NSString *annotationViewReuseIdentifier = @"annotationViewReuseIdentifier";
        MKAnnotationView *annotationView = (MKAnnotationView *)[mapView1 dequeueReusableAnnotationViewWithIdentifier:annotationViewReuseIdentifier];
        
        if (annotationView == nil) {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationViewReuseIdentifier];
        }
        UIGraphicsEndImageContext();
        annotationView.image =  [UIImage imageNamed:@"car_position"];
        annotationView.annotation = annotation;
        annotationView.canShowCallout = YES;
        return annotationView;
    } else {
        return nil;
    }
}


-(MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay {
    MKCircleRenderer *circleRenderer = [[MKCircleRenderer alloc] initWithCircle:overlay];
    circleRenderer.fillColor = [UIColor blueColor];
    circleRenderer.alpha = 0.5f;
    if (mapClicked % 2 == 0) {
        circleRenderer.lineWidth = 2.0;
        circleRenderer.strokeColor = [UIColor blackColor];
    }else{
        circleRenderer.lineWidth = 0.1;
        circleRenderer.strokeColor = [UIColor blackColor];
    }
    return circleRenderer;
}*/


#pragma mark - Webservice call
-(void)callInitialDanttrackerApi{
    NSLog(@"callInitialDanttrackerApi from geofence");
    
    NSData *userdata = [Helper getDataValue:@"userInfo"];
    NSDictionary *userInfo = [[self dataToDicitonary:userdata] mutableCopy];
    
    NSDictionary *userdataDict = [userInfo valueForKey:@"data"];
    NSString *trackerID = [userdataDict valueForKey:@"tracker_id"];
    
    NSDictionary *dict = [userInfo valueForKey:@"data"];
    trackerID = [dict valueForKey:@"tracker_id"];
    if (trackerID.length <= 0 || trackerID == nil || trackerID == (NSString *)[NSNull null] || [trackerID isEqualToString:@""])
    {}else{
        
        NSDictionary *dantracker = [Helper getUserValue:appdelegate.dantracker];
        NSString *username = [NSString stringWithFormat:@"%@",[dantracker valueForKey:@"username"]];
        NSString *password = [NSString stringWithFormat:@"%@",[dantracker valueForKey:@"password"]];
        
        if(![self CheckNetworkConnection]) {
            return;
        }
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSDictionary *dict = [userInfo valueForKey:@"data"];
        trackerID = [dict valueForKey:@"tracker_id"];
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'hh:mm:ss'Z'"];
        NSString *currentDateString =[dateFormatter stringFromDate:[NSDate date]];
        
        [WebService httpGetWithCustomDelegateURLString:[NSString stringWithFormat:@"http://api.dantracker.com/trackers/%@/positions?minDateTime=%@",trackerID,currentDateString] userName:username pasword:password isDebug:false callBackData:^(NSData *data, NSError *error) {
            
             [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (error == nil) {
                NSError *localError = nil;
                
                NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
                if (localError != nil) {
                    NSLog(@"%@",localError.description);
                     [self alertErrorwithMessage:NSLocalizedString(@"Wecouldnotfindyoucarlocation", @"")];
                }else {
                    if (jsonArray.count > 0) {
                        locationNotFound = 1;
                        carLocationArray = [[NSArray alloc]init];
                        carLocationArray = [jsonArray mutableCopy];
                        [self zoomToCarLocation];
                    }else{
                        
                        // If today's car location not found then please display previous date location of car
                        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
                        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'hh:mm:ss'Z'"];
                        
                        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
                        NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
                        [offsetComponents setDay:-5]; // replace "-1" with "1" to get the datePlusOneDay
                        NSDate *dateMinusfiveDay = [gregorian dateByAddingComponents:offsetComponents toDate:[NSDate date] options:0];
                        NSString *currentDateString =[dateFormatter stringFromDate:dateMinusfiveDay];
                        
                        [WebService httpGetWithCustomDelegateURLString:[NSString stringWithFormat:@"http://api.dantracker.com/trackers/%@/positions?minDateTime=%@",trackerID,currentDateString] userName:username pasword:password isDebug:true callBackData:^(NSData *data, NSError *error) {
                            
                            if (error == nil) {
                                NSError *localError = nil;
                                
                                NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
                                if (localError != nil) {
                                    NSLog(@"%@",localError.description);
                                    [self alertErrorwithMessage:NSLocalizedString(@"Wecouldnotfindyoucarlocation", @"")];
                                }else {
                                    if (jsonArray.count > 0) {
                                        locationNotFound = 1;
                                        carLocationArray = [[NSArray alloc]init];
                                        carLocationArray = [jsonArray mutableCopy];
                                        [self zoomToCarLocation];
                                    }else{
                                        locationNotFound = 0;
                                        [self alertErrorwithMessage:NSLocalizedString(@"Wecouldnotfindyoucarlocation", @"")];
                                    }
                                }
                            }else{
                                [self alertErrorwithMessage:NSLocalizedString(@"Wecouldnotfindyoucarlocation", @"")];
                            }
                        }];
                    }
                }
            }else{
            }
        }];
    }
}

-(void)zoomToCarLocation{
    if (carLocationArray.count > 0) {
        NSDictionary *locationDict = [carLocationArray objectAtIndex:carLocationArray.count - 1];
        double lat  = [[locationDict valueForKey:@"lat"] doubleValue];
        double lon  = [[locationDict valueForKey:@"lng"] doubleValue];
        
        CLLocation *loc = [[CLLocation alloc] initWithLatitude:lat longitude:lon];
        Coordinates = loc.coordinate;
        
        __block NSString *Address;
        [geocoder reverseGeocodeLocation:loc completionHandler:^(NSArray *placemarks, NSError *error)
         {
             if (!(error))
             {
                 CLPlacemark *placemark = [placemarks objectAtIndex:0];
                 NSLog(@"\nCurrent Location Detected\n");
                 NSLog(@"placemark %@",placemark);
                 NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
                 Address = [[NSString alloc]initWithString:[Helper isStringIsNull:locatedAt]];
                 NSString *Area = [[NSString alloc]initWithString:[Helper isStringIsNull:placemark.locality]];
                 NSString *Country = [[NSString alloc]initWithString:[Helper isStringIsNull:placemark.country]];
                 NSString *CountryArea = [NSString stringWithFormat:@"%@, %@", Area,Country];
                 NSLog(@"%@",CountryArea);
                 
             }else {
                 NSLog(@"Geocode failed with error %@", error);
                 NSLog(@"\nCurrent Location Not Detected\n");
             }
         }];
        
        [mapview setCenterCoordinate:Coordinates animated:YES];
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance ( loc.coordinate, 1000, 1000);
        [mapview setRegion:region animated:NO];
        [self Initialized:Coordinates];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
