//
//  TrackerIDViewController.h
//  SATCAR
//
//  Created by Percept Infotech on 06/02/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrackerIDViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *txtTrackerID;
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;
@property (weak, nonatomic) IBOutlet UIButton *btnConnect;
- (IBAction)onConnect:(id)sender;
- (IBAction)onFromReimConnect:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblTrackerMsg;
@property (weak, nonatomic) IBOutlet UILabel *lblHEad;
@property (weak, nonatomic) IBOutlet UIButton *btnFromReimConnect;
@property(nonatomic,strong) NSString *from;
- (IBAction)onDisconnect:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintConnectBottom;
@property (weak, nonatomic) IBOutlet UIButton *btnDisconnect;

@end
