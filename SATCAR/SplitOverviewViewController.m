//
//  SplitOverviewViewController.m
//  SATCAR
//
//  Created by Percept Infotech on 28/12/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import "SplitOverviewViewController.h"
#import "UIViewController+VCHelpers.h"
#import "ECSlidingViewController.h"
#import "LeftSideViewController.h"
#import "NavigationController.h"
#import "UIViewController+ECSlidingViewController.h"

@interface SplitOverviewViewController ()
{
    NSString *strMeasure,*strCurrency,*strCurrentDate,*userRegisterDate,*sendExport;
    double PTP,BTP,PEP,BEP;
    NSDate *currentMonth,*nextMonth;
    NSDictionary *splitresponse,*userInfo;
    NSString *userType;
    AppDelegate *appdelegate;
}
@end

@implementation SplitOverviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    _lblTripIndicator.backgroundColor = [Helper privateColor];
    _lblFinanceIndicator.backgroundColor = [Helper privateColor];
    
    NSData *userdata = [Helper getDataValue:@"userInfo"];
    NSDictionary *dataDict = [self dataToDicitonary:userdata];
    
    if (![dataDict isKindOfClass:[NSNull class]]) {
        userInfo = [dataDict valueForKey:@"data"];
        NSDictionary *generalSetting = [userInfo valueForKey:@"general_setting"];
        NSLog(@"DATA : %@",userInfo);
        strMeasure = [generalSetting valueForKey:@"unit"];
        strCurrency = [generalSetting valueForKey:@"currency"];
        
        if ([strMeasure.lowercaseString isEqualToString:@"miles"]) {
            strMeasure = @"mi";
        }
    }else{
        [Helper displayAlertView:@"" message:NSLocalizedString(@"something_went_wrong_msg", @"")];
    }

    currentMonth = [NSDate date];
    strCurrentDate = [self getMonthYearDay:currentMonth];
    userRegisterDate = [Helper getPREF:@"userRegisterDate"];
    sendExport = @"";
    [self initializeData];
}

-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self addTitleView:NSLocalizedString(@"SplitOverview", @"")];
    UIButton *btnAdd = [UIButton buttonWithType:UIButtonTypeCustom];
    btnAdd.frame = CGRectMake(20,0,80,32);
    [btnAdd setTitle:NSLocalizedString(@"Export", @"") forState:UIControlStateNormal];
    [btnAdd setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnAdd.titleLabel.font = [UIFont systemFontOfSize:16];
    [btnAdd addTarget:self action:@selector(onExport:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* item = [[UIBarButtonItem alloc] initWithCustomView:btnAdd];
    _lblMonth.text = [self getHeaderDisplayDateFormat:currentMonth];
    self.navigationItem.rightBarButtonItem = item;
    
    _viewTripSplit.layer.cornerRadius = 13;
    _viewTripSplit.clipsToBounds = YES;
    _viewFinancialSplit.layer.cornerRadius = 13;
    _viewFinancialSplit.clipsToBounds = YES;
    
    _btnExport.layer.cornerRadius = 25;
     _btnExport.layer.borderWidth = 1;
    _btnExport.layer.borderColor =[UIColor colorWithRed:0.000 green:0.659 blue:0.922 alpha:1.00].CGColor;
    [_btnExport setTitle:NSLocalizedString(@"Export", @"") forState:UIControlStateNormal];
    
    _lblTripSplit.text = NSLocalizedString(@"Tripsplit", @"");
    _lblTripPrivate.text = NSLocalizedString(@"Private", @"");
    _lblTripBusiness.text = NSLocalizedString(@"Business", @"");
    
    _lblFinancialSplit.text = NSLocalizedString(@"Financesplit", @"");
    _lblFinancialPrivate.text = NSLocalizedString(@"Private", @"");
    _lblFinancialBusiness.text = NSLocalizedString(@"Business", @"");
 
    _lblTotal.text = NSLocalizedString(@"Totals", @"");
    _lblExpenseTotal.text = NSLocalizedString(@"ExpensesTotal", @"");
    _lblTripTotal.text = NSLocalizedString(@"tripTotal", @"");
 
    _lblToPay.text = NSLocalizedString(@"Topay", @"");
    _lblMessage.text = NSLocalizedString(@"ThePrivateAccountText", @"");
    
    userType = [Helper getPREF:@"user_type"];
    if ([userType isEqualToString:@"10"] || [userType isEqualToString:@"12"]) {
        _IBLayoutConstraintFinanceSplitHeight.constant = 0;
        _IBLayoutConstraintTotalHeight.constant = 0;
        _IBLayoutConstraintPayHeight.constant = 0;
        _viewFinancialSplit.hidden = true;
        _viewFSplit.hidden = true;
        _viewTotal.hidden = true;
        _viewToPay.hidden = true;
        _lblGap1.hidden = true;
        _lblGap2.hidden = true;
        _lblGap3.hidden = true;
        
        _IBLayoutConstraintTotalTop.constant = 0;
        _IBLayoutConstraintPayTop.constant = 0;
        _IBLayoutConstraintSplitGap.constant = 0;
    }else{
        _IBLayoutConstraintFinanceSplitHeight.constant = 170;
        _IBLayoutConstraintTotalHeight.constant = 100;
        _IBLayoutConstraintPayHeight.constant = 100;
        _viewFinancialSplit.hidden = false;
        _viewFSplit.hidden = false;
        _viewTotal.hidden = false;
        _viewToPay.hidden = false;
        _lblGap1.hidden = false;
        _lblGap2.hidden = false;
        _lblGap3.hidden = false;
        _IBLayoutConstraintTotalTop.constant = 9;
        _IBLayoutConstraintPayTop.constant = 9;
        _IBLayoutConstraintSplitGap.constant = 9;
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)onTripSplit:(id)sender{
}
- (IBAction)onFinancesSplit:(id)sender{
}
- (IBAction)onExport:(id)sender {
    sendExport = @"1";
    [self getSplitData:sendExport];
}
- (IBAction)onPrevious:(id)sender {
    NSDate *prev = [self getPrevoiusMonthDate:currentMonth];
    currentMonth = prev;
    [self checkDataAvailable:currentMonth];
    strCurrentDate = [self getMonthYearDay:currentMonth];
    _lblMonth.text = [self getHeaderDisplayDateFormat:currentMonth];
    sendExport = @"";
    [self getSplitData:sendExport];
}
- (IBAction)onNext:(id)sender {
    NSDate *next = [self getNextMonthDate:currentMonth];
    currentMonth = next;
    [self checkNextDataAvailable:currentMonth];
    strCurrentDate = [self getMonthYearDay:currentMonth];
    _lblMonth.text = [self getHeaderDisplayDateFormat:currentMonth];
    sendExport = @"";
    [self getSplitData:sendExport];
}

#pragma mark - custome funcitons
-(void)initializeData{
    [self checkDataAvailable:currentMonth];
    [self checkNextDataAvailable:currentMonth];
    _lblMonth.text = [self getHeaderDisplayDateFormat:currentMonth];//strCurrentDate;
    sendExport = @"";
    [self getSplitData:sendExport];
}

-(void)checkDataAvailable:(NSDate *)date{
    
    BOOL previousAvailable = [self comapreDateByMonthAndYear:date registerDate:userRegisterDate];
    [self tempNext:date];
    _btnPrivious.hidden = true;
    if (previousAvailable) {
        _btnPrivious.hidden = false;
    }else{
        _btnPrivious.hidden = true;
    }
}

-(void)checkNextDataAvailable:(NSDate *)date{
    
    BOOL nextAvailable = [self comapreNextDate:date];
    [self tempPrevious:date];
    _btnNext.hidden = true;
    if (nextAvailable) {
        _btnNext.hidden = false;
    }else{
        _btnNext.hidden = true;
    }
}
-(void)tempPrevious:(NSDate *)date{
    
    BOOL previousAvailable = [self comapreDateByMonthAndYear:date registerDate:userRegisterDate];
    _btnPrivious.hidden = true;
    if (previousAvailable) {
        _btnPrivious.hidden = false;
    }else{
        _btnPrivious.hidden = true;
    }
}
-(void)tempNext:(NSDate *)date{
    BOOL nextAvailable = [self comapreNextDate:date];
    _btnNext.hidden = true;
    if (nextAvailable) {
        _btnNext.hidden = false;
    }else{
        _btnNext.hidden = true;
    }
}

-(void)getSplitData:(NSString *)export{
    NSString *userID = [Helper getPREF:@"userID"];
    NSDictionary *parameter = @{ @"userid":userID,@"date":strCurrentDate,@"export":export };
    [self callSplitService:parameter export:export];
}

-(void)callSplitService:(NSDictionary *)parameter export:(NSString *)exp{
    
    if(![self CheckNetworkConnection])
    {
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [WebService httpPostWithCustomDelegateURLString:@".getsplitoverview" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         if (error == nil) {
             NSError *localError = nil;
             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
             
             if (localError != nil) {
                 NSLog(@"%@",localError.description);
             } else {
                 id response = [parsedObject objectForKey:@"data"];
                 if ([self CheckSession:response]) {
                     splitresponse = [[NSDictionary alloc]init];
                     splitresponse = [parsedObject objectForKey:@"data"];;
                     if (![exp isEqualToString:@"1"]) {
                         [self displayData];
                     }else{
                         //[Helper  displayAlertView:@"" message:message];
                         [self.navigationController popViewControllerAnimated:true];
                     }
                 }else{
                     //[self moveToLogin ];
                 }
             }
         }
     }];
}

-(void)displayData{
    
    float TPValue = [[splitresponse valueForKey:@"Privatetrips"] floatValue];
    NSString *TPValuestr = [self ConvertToDKCurrency: [NSString stringWithFormat:@"%.2f",TPValue]];
    _lblTripPrivateValue.text = [NSString stringWithFormat:@"%@ %@",TPValuestr,strMeasure];
    float TBValue = [[splitresponse valueForKey:@"Businesstrips"] floatValue];
    NSString *TBValuestr = [self ConvertToDKCurrency: [NSString stringWithFormat:@"%.2f",TBValue]];
    _lblTripBusinessValue.text = [NSString stringWithFormat:@"%@ %@",TBValuestr,strMeasure];
    
    PTP = [[splitresponse valueForKey:@"Pri_trips_percentage"]doubleValue];
    BTP = [[splitresponse valueForKey:@"Bus_trips_percentage"]doubleValue];
    
    //
    float FPValue = [[splitresponse valueForKey:@"Private_expense"] floatValue];
    NSString *FPValuestr = [self ConvertToDKCurrency: [NSString stringWithFormat:@"%.2f",FPValue]];
    _lblFinancialPrivateValue.text = [NSString stringWithFormat:@"%@ %@",FPValuestr,strCurrency];
    float FBValue = [[splitresponse valueForKey:@"Business_expense"] floatValue];
    NSString *FBValuestr = [self ConvertToDKCurrency: [NSString stringWithFormat:@"%.2f",FBValue]];
    _lblFinancialBusinessValue.text = [NSString stringWithFormat:@"%@ %@",FBValuestr,strCurrency];
    
    PEP =[ [splitresponse valueForKey:@"Pri_exp_percentage"]doubleValue];
    BEP =[ [splitresponse valueForKey:@"Bus_exp_percentage"]doubleValue];
   // NSString *PEPstr = [self ConvertToDKCurrency: [NSString stringWithFormat:@"%.2f",PEP]];
   
    
    
    NSString *strpayPrivate = [splitresponse valueForKey:@"to_pay_private"];
    NSString *strpayBusiness = [splitresponse valueForKey:@"to_pay_business"];
    float spp = [strpayPrivate floatValue];
    float spb = [strpayBusiness floatValue];
    NSString *sppstr = [self ConvertToDKCurrency: [NSString stringWithFormat:@"%.2f",spp]];
    NSString *spbstr = [self ConvertToDKCurrency: [NSString stringWithFormat:@"%.2f",spb]];
    float TTValue = [[splitresponse valueForKey:@"trips_total"] floatValue];
    NSString *TTValuestr = [self ConvertToDKCurrency: [NSString stringWithFormat:@"%.2f",TTValue]];
    _lblTripTotalValue.text = [NSString stringWithFormat:@"%@ %@",TTValuestr,strMeasure];
    float ETValue = [[splitresponse valueForKey:@"expenses_total"] floatValue];
    NSString *ETValuestr = [self ConvertToDKCurrency: [NSString stringWithFormat:@"%.2f",ETValue]];
    _lblExpenseTotalValue.text = [NSString stringWithFormat:@"%@ %@",ETValuestr,strCurrency];
    
    _lblToPayPrivateValue.text = [NSString stringWithFormat:@"%@ %@",sppstr,strCurrency];
    _lblToPayBusinessValue.text = [NSString stringWithFormat:@"%@ %@",spbstr,strCurrency];
    
    NSString *msg = [splitresponse valueForKey:@"msg"];
    if (msg == (NSString *)[NSNull null]) {
        _lblMessage.text = @"";
    }else{
        _lblMessage.text = msg;
    }
    
    [self tripSplitCalculate];
    [self expenseSplitCalculate];
    /*_lblToPayPrivateValue.text = [NSString stringWithFormat:@"%@ %@",strpayPrivate,strCurrency];
    _lblToPayBusinessValue.text = [NSString stringWithFormat:@"%@ %@",strpayBusiness,strCurrency];
    _lblMessage.text = [splitresponse valueForKey:@"msg"];*/
}


-(void)tripSplitCalculate {
    NSLog(@"calulate");
    
    double progress = PTP;
    if (progress < 0) {
        progress = 0;
    }
    NSLog(@"progress %f",progress );
    
    float screenWidth = self.view.frame.size.width - 40;
    float width = (screenWidth * progress ) / 100;
    NSLog(@"width %f",width );
    NSLog(@"screenWidth %f",screenWidth );
    if (width <= 0) {
        width = 0;
    }
    if (width > 1 && width < 100) {
        _IBLayoutConstraintTripPrivateWidth.constant = width-1;
    }else{
        _IBLayoutConstraintTripPrivateWidth.constant = width;
    }
    
    NSString *PTPstr = [self ConvertToDKCurrency: [NSString stringWithFormat:@"%.2f",PTP]];
    NSString *BTPstr = [self ConvertToDKCurrency: [NSString stringWithFormat:@"%.2f",BTP]];
    
    _lblTripPrivatePercentage.text = [NSString stringWithFormat:@"Private (%@%%)",PTPstr];
    _lblTripBusinessPercentage.text = [NSString stringWithFormat:@"Business (%@%%)",BTPstr];
    
    _lblTripPrivatePercentage.text = [NSString stringWithFormat:NSLocalizedString(@"PrivatePerc" ,@""),PTPstr];
    _lblTripBusinessPercentage.text = [NSString stringWithFormat:NSLocalizedString(@"BusinessPerc" ,@""),BTPstr];
    
    _lblToPayPrivate.text = [NSString stringWithFormat:NSLocalizedString(@"PrivatePerc" ,@""),PTPstr];
    _lblToPayBusiness.text = [NSString stringWithFormat:NSLocalizedString(@"BusinessPerc" ,@""),BTPstr];
    
}
-(void)expenseSplitCalculate {
    NSLog(@"calulate");
    
    double progress = (PEP);
    if (progress <= 0) {
        progress = 0;
    }
    NSLog(@"progress %f",progress );
    
    float screenWidth = self.view.frame.size.width - 40;
    float width = (screenWidth * progress ) / 100;
    NSLog(@"width %f",width );
    NSLog(@"screenWidth %f",screenWidth );
    if (width <= 0) {
        width = 0;
    }
    if (width > 1 && width < 100) {
        _IBLayoutConstraintFinancialPrivateWidth.constant = width-1;
    }else{
        _IBLayoutConstraintFinancialPrivateWidth.constant = width;
    }
    
    
    if (progress <= 0) {
        NSString *PTPstr = [self ConvertToDKCurrency: [NSString stringWithFormat:@"%.2f",PEP]];
        NSString *BTPstr = [self ConvertToDKCurrency: [NSString stringWithFormat:@"%.2f",BEP]];
        
        _lblFinancialPrivatePercentage.text = [NSString stringWithFormat:@"Private (%@%%)",PTPstr];
        _lblFinancialBusinessPercentage.text = [NSString stringWithFormat:@"Business (%@%%)",BTPstr];
        
        _lblFinancialPrivatePercentage.text = [NSString stringWithFormat:NSLocalizedString(@"PrivatePerc" ,@""),PTPstr];
        _lblFinancialBusinessPercentage.text = [NSString stringWithFormat:NSLocalizedString(@"BusinessPerc" ,@""),BTPstr];
       
    }else{
        NSString *PTPstr = [self ConvertToDKCurrency: [NSString stringWithFormat:@"%.2f",PEP]];
        NSString *BTPstr = [self ConvertToDKCurrency: [NSString stringWithFormat:@"%.2f",BEP]];
        _lblFinancialPrivatePercentage.text = [NSString stringWithFormat:@"Private (%@%%)",PTPstr];
        _lblFinancialBusinessPercentage.text = [NSString stringWithFormat:@"Business (%@%%)",BTPstr];
        
        _lblFinancialPrivatePercentage.text = [NSString stringWithFormat:NSLocalizedString(@"PrivatePerc" ,@""),PTPstr];
        _lblFinancialBusinessPercentage.text = [NSString stringWithFormat:NSLocalizedString(@"BusinessPerc" ,@""),BTPstr];
       
    }
    
}

-(void)viewWillDisappear:(BOOL)animated{
    appdelegate.isRelaodTrip = @"0";
}
@end
