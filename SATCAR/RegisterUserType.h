//
//  RegisterUserType.h
//  SATCAR
//
//  Created by Percept Infotech on 23/01/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterUserType : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *BtnFreeUser;
@property (weak, nonatomic) IBOutlet UIButton *btnStandarUser;
@property (weak, nonatomic) IBOutlet UIButton *btnSplitleasing;
- (IBAction)onFreeUserPress:(id)sender;
- (IBAction)onStandarUserPress:(id)sender;
- (IBAction)onSplitleasingPress:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintviewFreeHeight;
@property (weak, nonatomic) IBOutlet UILabel *lblFreeMsg;
@property (weak, nonatomic) IBOutlet UILabel *LblstandardMsg;
@property (weak, nonatomic) IBOutlet UILabel *lblSplitMsg;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintSatndarTop;

@property (weak, nonatomic) IBOutlet UIView *viewFree;
@property (weak, nonatomic) IBOutlet UIView *viewStandard;
@property (weak, nonatomic) IBOutlet UIView *viewSplit;
- (IBAction)onNext:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewFreeBack;
@property (weak, nonatomic) IBOutlet UIView *viewStandardBack;
@property (weak, nonatomic) IBOutlet UIView *viewSplitBack;
@property (weak, nonatomic) IBOutlet UIImageView *imgFree;
@property (weak, nonatomic) IBOutlet UIImageView *imgStandard;
@property (weak, nonatomic) IBOutlet UIImageView *imgvSplit;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UILabel *lblFreePrice;
@property (weak, nonatomic) IBOutlet UILabel *lblStandardPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblSplitPrice;

@end
