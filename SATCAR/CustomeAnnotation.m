//
//  CustomeAnnotation.m
//  SATCAR
//
//  Created by Percept Infotech on 20/01/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import "CustomeAnnotation.h"

@implementation CustomeAnnotation
@synthesize coordinate,title,subtitle;

- (id)initWithLocation:(CLLocationCoordinate2D)coord{
    
    self = [super init];
    if (self) {
        coordinate = coord;
        
    }
    return self;
}
@end
