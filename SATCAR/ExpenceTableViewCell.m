//
//  ExpenceTableViewCell.m
//  SATCAR
//
//  Created by Percept Infotech on 04/01/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import "ExpenceTableViewCell.h"

@implementation ExpenceTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _viewName.layer.cornerRadius = 10;
    _viewName.clipsToBounds = true;
    _imgUser.layer.cornerRadius = 10;
    _imgUser.clipsToBounds = true;
    _imgUser.layer.borderColor = (__bridge CGColorRef _Nullable)([UIColor whiteColor]);
    _imgUser.layer.borderWidth = 1.0;
     _viewName.hidden = true;
    _IBLayoutConstraintViewNameHeight.constant = 0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
