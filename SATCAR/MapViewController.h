//
//  MapViewController.h
//  SATCAR
//
//  Created by Percept Infotech on 25/01/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@interface MapViewController : UIViewController<MKMapViewDelegate>
@property (weak, nonatomic) IBOutlet MKMapView *mapview;
@property(nonatomic,strong)NSDictionary *locationDataDictionary;
@property(nonatomic,strong)NSString *fromScreen;
@property (weak, nonatomic) IBOutlet UIButton *btnSnapToRoad;
@property (weak, nonatomic) IBOutlet UIButton *btnOrignalTracked;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintMapBottom;
@end
