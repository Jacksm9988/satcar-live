//
//  ReimbursementRentsViewController.m
//  SATCAR
//
//  Created by Percept Infotech on 10/02/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import "ReimbursementRentsViewController.h"
#import "ReimbursementRentsTVC.h"
#import "EditReimbursementViewController.h"
@interface ReimbursementRentsViewController ()
{
    NSArray *reimbursementArray;
}
@end

@implementation ReimbursementRentsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addTitleView:NSLocalizedString(@"Reimbursement_rates", @"")];
    reimbursementArray = [[NSArray alloc]init];
    _tableview.estimatedRowHeight = 143;
    _tableview.rowHeight = UITableViewAutomaticDimension;
    _viewReimDetail.layer.cornerRadius = 5.0;
    UIColor *bluecolor = [UIColor colorWithRed:0.000 green:0.651 blue:0.929 alpha:1.00];
    
    [_btnUpdate.layer setBorderColor:bluecolor.CGColor];
    _btnUpdate.layer.cornerRadius = 25;
    _btnUpdate.layer.borderWidth = 1;
    [_btnUpdate setTitleColor:bluecolor forState:UIControlStateNormal];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [self callGetRates];
    [self hideReimbursementDetailView];
    
    [_btnCancel setTitle:NSLocalizedString(@"Close_Trip", @"") forState:UIControlStateNormal];
    [_btnUpdate setTitle:NSLocalizedString(@"EditReimbursement", @"") forState:UIControlStateNormal];
   //_LblHeading.text =  NSLocalizedString(@"ReimbursementDetail", @"");
    
    _txtFKm.placeholder =  NSLocalizedString(@"FromKM", @"");
    _txtToKm.placeholder =  NSLocalizedString(@"ToKM", @"");
    _txtYear.placeholder =  NSLocalizedString(@"Year", @"");
    _txtAmount.placeholder =  NSLocalizedString(@"Amount", @"");
    _txtCountry.placeholder =  NSLocalizedString(@"Country", @"");
    _txtCurrency.placeholder =  NSLocalizedString(@"Currency", @"");
    
}
-(void)hideReimbursementDetailView{
    
    _viewReimDetail.hidden = true;
    [UIView animateWithDuration:5.0 delay:1.0 options:UIViewAnimationOptionTransitionNone animations:^{
        self.navigationController.navigationBar.hidden = false;
        _IBLayoutConstraintLocationPopupTop.constant = self.view.frame.size.height;
        _IBLayoutConstraintLocationPopupHeight.constant = 0;
    }completion:nil];
}
-(void)showReimbursementDetailView{
    
    [UIView animateWithDuration:5.0 delay:1.0 options:UIViewAnimationOptionTransitionNone animations:^{
        self.navigationController.navigationBar.hidden = true;
        _viewReimDetail.hidden = false;
        _IBLayoutConstraintLocationPopupTop.constant = 0;
        _IBLayoutConstraintLocationPopupHeight.constant = self.view.frame.size.height;
    }completion:nil];
}

#pragma mark - Table View
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return reimbursementArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ReimbursementRentsTVC *cell = (ReimbursementRentsTVC *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSDictionary *Data = [[NSDictionary alloc]init];
    Data = [reimbursementArray objectAtIndex:indexPath.row];
   
    cell.lblYear.text = [Data valueForKey:@"year"];
    cell.lblFromKMVal.text =  [self ConvertToDKCurrency:[Data valueForKey:@"from_km"]];
    cell.lblToKMVal.text =[self ConvertToDKCurrency: [Data valueForKey:@"to_km"]];
    cell.lblAmountVal.text = [self ConvertToDKCurrency:[Data valueForKey:@"amount_pr_km"]];
    cell.lblCountryVal.text = [Data valueForKey:@"country"];
    cell.lblCurrencyVal.text = [Data valueForKey:@"currency"];
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedIndex = (int)indexPath.row;
    [self fillData];
    [self showReimbursementDetailView];
}

-(void)fillData
{
    NSDictionary *Data = [[NSDictionary alloc]init];
    Data = [reimbursementArray objectAtIndex:selectedIndex];
    
    _txtYear.text = [Data valueForKey:@"year"];
    _txtFKm.text = [self ConvertToDKCurrency:[Data valueForKey:@"from_km"]];
    _txtToKm.text = [self ConvertToDKCurrency:[Data valueForKey:@"to_km"]];
    _txtAmount.text = [self ConvertToDKCurrency:[Data valueForKey:@"amount_pr_km"]];
    _txtCountry.text = [Data valueForKey:@"country"];
    _txtCurrency.text = [Data valueForKey:@"currency"];
}

#pragma mark - webservice call
-(void)callGetRates{
    
    if(![self CheckNetworkConnection]) {
        return;
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [WebService httpPostWithCustomDelegateURLString:@".getrates" parameter:nil isDebug:false callBackData:^(NSData *data, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         if (error == nil) {
             NSError *localError = nil;
             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
             
             if (localError != nil) {
                 NSLog(@"%@",localError.description);
             }
             else {
                 NSLog(@"response : %@",parsedObject);
                 id response = [parsedObject objectForKey:@"data"];
                 NSString *message = [response valueForKey:@"message"];
                 NSString *status = [response valueForKey:@"status"];
                 
                 if ([status.lowercaseString isEqualToString:@"success"]) {
                     reimbursementArray = [[NSMutableArray alloc]init];
                     reimbursementArray  = [response objectForKey:@"rates"];
                     if (reimbursementArray.count > 0)
                     {
                         [_tableview reloadData];
                     }else{
                         [Helper displayAlertView:@"" message:message];
                     }
                 }else{
                     //[self moveToLogin];
                 }
             }
         }
     }];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:@"segueEditReimbursement"]) {
        EditReimbursementViewController *controller = segue.destinationViewController;
        NSDictionary *Data = [[NSDictionary alloc]init];
        Data = [reimbursementArray objectAtIndex:selectedIndex];
        controller.Year =  _txtYear.text;
        controller.FromKm = [Data valueForKey:@"from_km"];
        controller.ToKm =  [Data valueForKey:@"to_km"];
        controller.Amount = [Data valueForKey:@"amount_pr_km"];
        controller.Country  = _txtCountry.text;
        controller.Currency  = _txtCurrency.text;
        controller.remId = [Data valueForKey:@"id"];
    }
}


- (IBAction)updateDetail:(id)sender {
    [self hideReimbursementDetailView];
    [self performSegueWithIdentifier:@"segueEditReimbursement" sender:self];
}

- (IBAction)Cancel:(id)sender {
    [self hideReimbursementDetailView];
}
@end
