//
//  ExpencesViewController.h
//  SATCAR
//
//  Created by Percept Infotech on 12/20/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExpencesViewController : UIViewController
{
    NSMutableArray *arrTrips;
}
@property (weak, nonatomic) IBOutlet UIButton *btnPrivious;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;

@property (weak, nonatomic) IBOutlet UIView *ViewSegment;
@property (weak, nonatomic) IBOutlet UIButton *BtnAllSegment;
@property (weak, nonatomic) IBOutlet UIButton *BtnPrivateSegment;
@property (weak, nonatomic) IBOutlet UIButton *BtnBusinessSegment;
- (IBAction)SelctSegment:(id)sender;

@property (weak, nonatomic) IBOutlet UITableView *tableExpences;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintprivateWidth;
@property (weak, nonatomic) IBOutlet UIView *viewFinancialSplic;
@property (weak, nonatomic) IBOutlet UILabel *lblPrivatePercentage;
@property (weak, nonatomic) IBOutlet UILabel *lblBusinessPercentage;
@property (weak, nonatomic) IBOutlet UILabel *lblFinancialSplit;
- (IBAction)onFinancesSplit:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewExpenseSplit;
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;

//Data show popup
@property (weak, nonatomic) IBOutlet UIView *viewTripDisplay;
@property (weak, nonatomic) IBOutlet UIView *viewBackTrip;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintTripPopupHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintTripPopupTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintImageHeight;

@property (weak, nonatomic) IBOutlet UILabel *lblTripmessage;
@property (weak, nonatomic) IBOutlet UIImageView *imgBusiness;

@property (weak, nonatomic) IBOutlet UITextField *txtEAccountValue;
@property (weak, nonatomic) IBOutlet UITextField *txtETypeValue;
@property (weak, nonatomic) IBOutlet UITextField *txtENameValue;
@property (weak, nonatomic) IBOutlet UITextField *txtEDateValue;
@property (weak, nonatomic) IBOutlet UITextField *txtAmountValue;
@property (weak, nonatomic) IBOutlet UITextField *txtNotesValue;



@property (weak, nonatomic) IBOutlet UIButton *btnConfirm;
@property (weak, nonatomic) IBOutlet UIButton *btnEditManually;
- (IBAction)onEditManually:(id)sender;
- (IBAction)onConfirm:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewImgBorder;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintViewImgHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintViewImgTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintViewTableviewBottom;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintViewNoteHeight;
@property (weak, nonatomic) IBOutlet UILabel *lblProgressIndicator;

@property (weak, nonatomic) IBOutlet UIView *viewEAccountValue;
@property (weak, nonatomic) IBOutlet UIView *viewETypeValue;
@property (weak, nonatomic) IBOutlet UIView *viewENameValue;
@property (weak, nonatomic) IBOutlet UIView *viewEDateValue;
@property (weak, nonatomic) IBOutlet UIView *viewAmountValue;
@property (weak, nonatomic) IBOutlet UIView *viewNotesValue;
@property (weak, nonatomic) IBOutlet UIView *viewNotification;
@property (weak, nonatomic) IBOutlet UILabel *lblNotification;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintNotifHeight;


@end
