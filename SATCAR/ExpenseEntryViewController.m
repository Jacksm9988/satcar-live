//
//  ExpenseEntryViewController.m
//  SATCAR
//
//  Created by Percept Infotech on 28/12/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import "ExpenseEntryViewController.h"
#import "imageCVC.h"
#import "LastImageCVC.h"

@interface ExpenseEntryViewController ()<UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate>
{
    NSMutableArray *imageArray;
    
    int selectedSegment,removeIndex;
    NSString *selectedExpense,*tripID,*strCurrency,*created_by,*userID,*selectedExpType;
    UIPickerView *expenseTypePicker,*currencyPicker,*vatPicker;
    UIDatePicker *datePicekr;
    NSArray *expenseTypeArray,*currencyArray,*expenseKeyArray,*Arrvat;
    BOOL fromImage,touched,isSaved;
    UILongPressGestureRecognizer *lpgr;
    AppDelegate *appdelegate;

}
@end

@implementation ExpenseEntryViewController
@synthesize fromScreen,tripDataDictionary;

- (void)viewDidLoad {
    [super viewDidLoad];
    selectedSegment = 1;
    appdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    /*expenseTypeArray = [[NSArray alloc]initWithObjects: @"Gasoline and Diesel", @"Service and Repair", @"Tires", @"Oil", @"Insurance", @"Road assistance", @"Parts", @"Parking", @"Other", nil];*/
   // expenseTypeArray = [[NSArray alloc]initWithObjects: NSLocalizedString(@"Gasoline_and_Diesel", @""), NSLocalizedString(@"Service_and_Repair", @""), NSLocalizedString(@"Tires", @""), NSLocalizedString(@"Oil", @""), NSLocalizedString(@"Insurance", @""), NSLocalizedString(@"Road_assistance", @""), NSLocalizedString(@"Parts", @""), NSLocalizedString(@"Parking", @""), NSLocalizedString(@"Other_Purpose", @""), nil];
    
    expenseTypeArray = [[NSArray alloc]initWithObjects:NSLocalizedString(@"COM_SATCAR_EXPENSE_OPTION_SERVICE_REPAIR", @""), NSLocalizedString(@"COM_SATCAR_EXPENSE_OPTION_CIRCLE_K", @""), NSLocalizedString(@"COM_SATCAR_EXPENSE_OPTION_Q8", @""), NSLocalizedString(@"COM_SATCAR_EXPENSE_OPTION_SHELL", @""), NSLocalizedString(@"COM_SATCAR_EXPENSE_OPTION_OTHER_FUEL", @""),NSLocalizedString(@"COM_SATCAR_EXPENSE_OPTION_CARWASH", @""), NSLocalizedString(@"COM_SATCAR_EXPENSE_OPTION_FINES", @""), NSLocalizedString(@"COM_SATCAR_EXPENSE_OPTION_OTHER_WITHOUT_VAT", @""), NSLocalizedString(@"COM_SATCAR_EXPENSE_OPTION_OTHER", @""), nil];
    
     expenseKeyArray = [[NSArray alloc]initWithObjects:@"COM_SATCAR_EXPENSE_OPTION_SERVICE_REPAIR", @"COM_SATCAR_EXPENSE_OPTION_CIRCLE_K", @"COM_SATCAR_EXPENSE_OPTION_Q8", @"COM_SATCAR_EXPENSE_OPTION_SHELL", @"COM_SATCAR_EXPENSE_OPTION_OTHER_FUEL",@"COM_SATCAR_EXPENSE_OPTION_CARWASH", @"COM_SATCAR_EXPENSE_OPTION_FINES", @"COM_SATCAR_EXPENSE_OPTION_OTHER_WITHOUT_VAT", @"COM_SATCAR_EXPENSE_OPTION_OTHER", nil];
   
     Arrvat = [[NSArray alloc]initWithObjects: @"1%",@"2%",@"3%",@"4%",@"5%",@"6%",@"7%",@"8%",@"9%",@"10%",@"11%",@"12%",@"13%",@"14%",@"15%",@"16%",@"17%",@"18%",@"19%",@"20%",@"21%",@"22%",@"23%",@"24%",@"25%",@"26%",@"27%",@"28%",@"29%",@"30", nil];
    
    currencyArray = [[NSArray alloc]initWithObjects:@"DKK",@"EUR",@"SEK",@"NOK",@"GBP",@"CHF", nil];
    fromImage = false;
    removeIndex = -1;
    lpgr = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.delegate = self;
    lpgr.delaysTouchesBegan = YES;
    touched= false;
    [_txtEType addPreviousNextDoneOnKeyboardWithTarget:self previousAction:@selector(onTypePrevious) nextAction:@selector(onTypeNext) doneAction:@selector(onTypeDone)];
    [_txtCurrency addPreviousNextDoneOnKeyboardWithTarget:self previousAction:@selector(onCurrencyPrevious) nextAction:@selector(onCurrencyNext) doneAction:@selector(onCurrencyDone)];
    [_txtvat addPreviousNextDoneOnKeyboardWithTarget:self previousAction:@selector(onVatPrevious) nextAction:@selector(onVatNext) doneAction:@selector(onVatDone)];
}

-(void)viewWillAppear:(BOOL)animated{
    if (!fromImage) {
        imageArray = [[NSMutableArray alloc]init];
        userID = [Helper getPREF:@"userID"];
        strCurrency = [Helper getPREF:@"currency"];
        
        [self.navigationController setNavigationBarHidden:NO animated:YES];
        [self addTitleView:NSLocalizedString(@"ExpenseEntry", @"")];
        UIButton *btnAdd = [UIButton buttonWithType:UIButtonTypeCustom];
        btnAdd.frame = CGRectMake(20,0,50,32);
        [btnAdd setTitle:NSLocalizedString(@"Save", @"") forState:UIControlStateNormal];
        [btnAdd setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        btnAdd.titleLabel.font = [UIFont systemFontOfSize:16];
        [btnAdd addTarget:self action:@selector(onSave:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem* item = [[UIBarButtonItem alloc] initWithCustomView:btnAdd];
        self.navigationItem.rightBarButtonItem = item;
        
        _ViewSegment.layer.cornerRadius = 25.0;
        _ViewSegment.clipsToBounds = YES;
        _ViewSegment.layer.borderWidth = 1;
        _ViewSegment.layer.borderColor = [Helper privateColor].CGColor;
        
        _txtvat.placeholder = NSLocalizedString(@"vat%", @"");
        
        _lblVat.layer.cornerRadius = 2.0;
        _lblVat.clipsToBounds = YES;
        _lblVat.text = NSLocalizedString(@"DoesitincludeVAT", @"");
        //_lblVat.layer.borderWidth = 1;
        //_lblVat.layer.borderColor = [UIColor blackColor].CGColor;
        
        [_BtnPrivateSegment setBackgroundColor:[Helper privateColor]];
        [_BtnPrivateSegment.titleLabel setTextColor:[UIColor whiteColor]];
        _BtnPrivateSegment.titleLabel.textColor = [UIColor whiteColor];
        [_BtnPrivateSegment setTintColor: [UIColor whiteColor]];
        _BtnPrivateSegment.layer.borderWidth = 1;
        _BtnPrivateSegment.layer.borderColor = [Helper privateColor].CGColor;
        
        [_BtnBusinessSegment setBackgroundColor:[UIColor clearColor]];
        _BtnBusinessSegment.layer.borderColor = [Helper privateColor].CGColor;
        _BtnBusinessSegment.layer.borderWidth = 1;
        [_BtnBusinessSegment.titleLabel setTextColor:[UIColor grayColor]];
        [_BtnBusinessSegment setTintColor: [UIColor grayColor]];
        
        [_BtnPrivateSegment setTitle:NSLocalizedString(@"ExpPrivate", @"") forState:UIControlStateNormal];
        [_BtnBusinessSegment setTitle:NSLocalizedString(@"ExpBusiness", @"") forState:UIControlStateNormal];
        
        _btnSave.layer.cornerRadius = 25;
        [_btnSave setTitle:NSLocalizedString(@"Save", @"") forState:UIControlStateNormal];
        [_btnSave setBackgroundColor:[Helper privateColor]];
        
        UIView *Rview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 30, 40)];
        UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(5, 10, 20, 20)];
        [img setImage:[UIImage imageNamed:@"img_down.png"]];
        [Rview addSubview:img];
        _txtEType.rightView = Rview;
        _txtEType.rightViewMode = UITextFieldViewModeAlways;
        
        UIView *RviewCurrency = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 30, 40)];
        UIImageView *imgCurrency = [[UIImageView alloc]initWithFrame:CGRectMake(5, 10, 20, 20)];
        [imgCurrency setImage:[UIImage imageNamed:@"img_down.png"]];
        [RviewCurrency addSubview:imgCurrency];
        _txtCurrency.rightView = RviewCurrency;
        _txtCurrency.rightViewMode = UITextFieldViewModeAlways;
        tripID = @"";
        
        _lblExpenseAccount.text = NSLocalizedString(@"ExpenseAccount", @"");
        _lblExpenseDetails.text = NSLocalizedString(@"ExpenseDetails", @"");
        _lblUploadDesign.text = NSLocalizedString(@"ImageDocumentation", @"");
        
        //_txtEType.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"ExpenseType",@"")];
        //_txtEName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"ExpenseName", @"")];
        
        //_txtDate.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Date", @"")];
        
        //_txtAmount.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Amount", @"")];
        [_SwitchVat setOn:FALSE];
        _txtEType.placeholder = NSLocalizedString(@"ExpenseType", @"");
        _txtEName.placeholder = NSLocalizedString(@"ExpenseName", @"");
        _txtDate.placeholder = NSLocalizedString(@"Date", @"");
        _txtAmount.placeholder = NSLocalizedString(@"Amount", @"");
        _txtCurrency.placeholder = NSLocalizedString(@"Currency", @"");
        //_txtNotes.placeholder = NSLocalizedString(@"Notes", @"");
        
        //_txtCurrency.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Currency", @"")];
        //_txtNotes.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Notes", @"")];
        
        if ([fromScreen isEqualToString:@"TripEdit"] && fromImage == false) {
            //[self disableEditMode];
            NSLog(@"Data : %@",tripDataDictionary);
            tripID = [tripDataDictionary valueForKey:@"id"];
            
            created_by = [tripDataDictionary valueForKey:@"created_by"];
            if (![created_by isEqualToString:userID]) {
                _IBLayoutConstraintImageTop.constant = 70;
                _txtUserName.hidden = false;
                _txtUserName.text = [tripDataDictionary valueForKey:@"first_name"];
            }else{
                _IBLayoutConstraintImageTop.constant = 20;
                _txtUserName.hidden = true;
            }
            
            selectedSegment = [[tripDataDictionary valueForKey:@"expense_account"] intValue];
            if (selectedSegment == 1) {
                [self onPrivateSelected];
            }else{
            _txtEType.text =  NSLocalizedString([tripDataDictionary valueForKey:@"expense_type"], @"");
            selectedExpType = [tripDataDictionary valueForKey:@"expense_type"];
            //_txtEType.text = [tripDataDictionary valueForKey:@"expense_type"]
            _txtEName.text = [tripDataDictionary valueForKey:@"name"];
                [self onBusinessSelected];
            }
            
            NSString *date = [tripDataDictionary valueForKey:@"expense_date"];
            if (date != nil) {
                NSString *strdate = [Helper getDateFromString:date];
                _txtDate.text = [NSString stringWithFormat:@"%@",strdate];
            }else{
                _txtDate.text = [tripDataDictionary valueForKey:@"expense_date"];
            }
            
            _txtAmount.text = [tripDataDictionary valueForKey:@"amount"];
            if([[tripDataDictionary valueForKey:@"include_vat"] isEqualToString: @"1"])
            {
                
                [_SwitchVat setOn:TRUE];
                _Vat_hight.constant = 50;
                _txtvat.text = [NSString stringWithFormat:@"%@%%",[tripDataDictionary valueForKey:@"vat"]];
        
            }else{
                [_SwitchVat setOn:FALSE];
                _Vat_hight.constant = 0;
                
            }
            
            if([[tripDataDictionary valueForKey:@"expense_type"] isEqualToString:@"COM_SATCAR_EXPENSE_OPTION_FINES"] || [[tripDataDictionary valueForKey:@"expense_type"] isEqualToString:@"COM_SATCAR_EXPENSE_OPTION_OTHER_WITHOUT_VAT"])
            {
                
                [_SwitchVat setEnabled:FALSE];
                
            }else{
                [_SwitchVat setEnabled:TRUE];
            }
            
            
            _txtCurrency.text = [NSString stringWithFormat:@"%@",[tripDataDictionary valueForKey:@"currency"]].uppercaseString;
           // _txtNotes.text = [tripDataDictionary valueForKey:@"note"];
            NSString *imgUrl = [tripDataDictionary valueForKey:@"image_doc"];
            if (imgUrl.length > 0) {
                NSString *url = [NSString stringWithFormat:@"%@%@",[WebService getExpenseImageUrl],imgUrl];
                NSURL *imgURL = [NSURL URLWithString:url];
                [NSURLConnection sendAsynchronousRequest:[NSURLRequest requestWithURL:imgURL] queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                    if (!connectionError) {
                        [imageArray addObject:data];
                        [_collectioview reloadData];
                    }else{
                        NSLog(@"%@",connectionError);
                    }
                }];
            }
            [_btnSave setTitle:NSLocalizedString(@"Update", @"") forState:UIControlStateNormal];
        }else{
            [self onPrivateSelected];
            _txtCurrency.text = [currencyArray objectAtIndex:0];
            [_SwitchVat setOn:FALSE];
            _Vat_hight.constant = 0;
            
        }
    }else{
        fromImage = false;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - collectionview delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return imageArray.count + 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (imageArray.count == 0 || indexPath.row == imageArray.count) {
        LastImageCVC *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"lastCell" forIndexPath:indexPath];
        [cell.btnAddImage addTarget:self action:@selector(onAddImage:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }else{
        imageCVC *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
        cell.imgDoc.image = [UIImage imageWithData:[imageArray objectAtIndex:indexPath.row]];
        cell.imgDoc.backgroundColor = [UIColor clearColor];
        cell.btnRemove.hidden = true;
        [cell.btnRemove addTarget:self action:@selector(onRemoveImage:) forControlEvents:UIControlEventTouchUpInside];
        [cell addGestureRecognizer:lpgr];
        return cell;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    //[collectionView reloadData];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(100, 100);
}


#pragma mark - textfield delegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    textField.textColor =  [UIColor colorWithRed:18.0/255.0 green:172.0/255.0 blue:243.0/255.0 alpha:1.0];
    textField.layer.borderWidth = 1;
    textField.layer.borderColor =  [UIColor colorWithRed:18.0/255.0 green:172.0/255.0 blue:243.0/255.0 alpha:1.0].CGColor;
    textField.clipsToBounds = YES;
    return true;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    textField.textColor =  [UIColor blackColor];
    textField.layer.borderWidth = 0;
    textField.layer.borderColor = [UIColor clearColor].CGColor;
    textField.clipsToBounds = YES;
    return true;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if (textField == _txtDate) {
        
        datePicekr = [[UIDatePicker alloc] initWithFrame:CGRectZero];
        [datePicekr setDatePickerMode:UIDatePickerModeDate];
        [datePicekr addTarget:self action:@selector(onDatePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
        datePicekr.maximumDate = [NSDate date];
        _txtDate.inputView = datePicekr;
        
        if (![_txtDate hasText]) {
            NSDateFormatter __autoreleasing *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
            [dateFormat setDateFormat:@"dd/MM/YYYY"];
            NSString *dateString =  [dateFormat stringFromDate:datePicekr.date];
            _txtDate.text = dateString;
        }
    }else if(textField == _txtEType) {
        expenseTypePicker = [[UIPickerView alloc] initWithFrame:CGRectZero];
        [expenseTypePicker setDataSource: self];
        [expenseTypePicker setDelegate: self];
        expenseTypePicker.showsSelectionIndicator = YES;
        _txtEType.inputView = expenseTypePicker;
        if (![_txtEType hasText]) {
            _txtEType.text =  NSLocalizedString([expenseKeyArray objectAtIndex:0], @"");
            selectedExpType = [expenseKeyArray objectAtIndex:0];
        }
    }else if(textField == _txtvat) {
        
        vatPicker = [[UIPickerView alloc] initWithFrame:CGRectZero];
        [vatPicker setDataSource: self];
        [vatPicker setDelegate: self];
        vatPicker.showsSelectionIndicator = YES;
        _txtvat.inputView = vatPicker;
        if ([[_txtvat text] isEqualToString:@"25%"]) {
            [vatPicker selectRow:24 inComponent:0 animated:YES];
        }
        if (![_txtvat hasText]) {
            
            _txtvat.text =  [Arrvat objectAtIndex:0];
        }
    }
    else if(textField == _txtCurrency) {
        currencyPicker = [[UIPickerView alloc] initWithFrame:CGRectZero];
        [currencyPicker setDataSource: self];
        [currencyPicker setDelegate: self];
        currencyPicker.showsSelectionIndicator = YES;
        _txtCurrency.inputView = currencyPicker;
        
        if (![_txtCurrency hasText]) {
            _txtCurrency.text = [currencyArray objectAtIndex:0];
        }
    }
}

#pragma mark - Picker View Data source
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (pickerView == expenseTypePicker) {
        return expenseKeyArray.count;
    }else if(pickerView == currencyPicker) {
        return currencyArray.count;
    }
    else if(pickerView == vatPicker) {
        return Arrvat.count;
    }
    return 0;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component{
    if (pickerView == expenseTypePicker) {
        if (row == expenseKeyArray.count - 1) {
            _txtEName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Other", @"")];
        }else{
            _txtEName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"ExpenseName", @"")];
        }
        
        if([[expenseKeyArray objectAtIndex:row] isEqualToString:@"COM_SATCAR_EXPENSE_OPTION_FINES"] || [[expenseKeyArray objectAtIndex:row] isEqualToString:@"COM_SATCAR_EXPENSE_OPTION_OTHER_WITHOUT_VAT"])
        {
            [_SwitchVat setOn:FALSE];
            _Vat_hight.constant = 0;
            _txtvat.text = @"";
            [_SwitchVat setEnabled:FALSE];
        }else{
            [_SwitchVat setEnabled:TRUE];
        }
        [_txtEType setText:NSLocalizedString([expenseKeyArray objectAtIndex:row], @"")];
        selectedExpType = [expenseKeyArray objectAtIndex:row];
    }else if(pickerView == currencyPicker) {
        [_txtCurrency setText:[currencyArray objectAtIndex:row]];
    }
    else if(pickerView == vatPicker) {
        [_txtvat setText:[Arrvat objectAtIndex:row]];
        
    }
    touched = true;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow: (NSInteger)row forComponent:(NSInteger)component{
    if (pickerView == expenseTypePicker) {
        return [NSString stringWithFormat:@"%@",NSLocalizedString([expenseKeyArray objectAtIndex:row], @"")];
    }else if(pickerView == currencyPicker) {
        return [NSString stringWithFormat:@"%@",[currencyArray objectAtIndex:row]];
    }else if(pickerView == vatPicker) {
      return [NSString stringWithFormat:@"%@",[Arrvat objectAtIndex:row]];
        
    }else{
        return 0;
    }
}
#pragma mark - keyboard delegate
-(void)onTypePrevious{
    [_txtEType becomeFirstResponder];
}
-(void)onTypeNext{
    [_txtEName becomeFirstResponder];
}
-(void)onTypeDone{
    if (touched) {
        touched = false;
    }else{
        //_txtEType.text = [expenseTypeArray objectAtIndex:0];
        _txtEType.text =  NSLocalizedString([expenseKeyArray objectAtIndex:0], @"");
        selectedExpType = [expenseKeyArray objectAtIndex:0];
    }
    [_txtEType resignFirstResponder];
}

-(void)onVatDone{
    if (touched) {
        touched = false;
    }else{
        //_txtEType.text = [expenseTypeArray objectAtIndex:0];
        _txtvat.text =  [Arrvat objectAtIndex:0];
    }
    [_txtvat resignFirstResponder];
}

-(void)onCurrencyPrevious{
    [_txtAmount becomeFirstResponder];
}
-(void)onCurrencyNext{
    [_txtNotes becomeFirstResponder];
}
-(void)onVatPrevious{
   [_txtNotes becomeFirstResponder];
}
-(void)onVatNext{
    [_txtvat resignFirstResponder];
}
-(void)onCurrencyDone{
    if (touched) {
        touched = false;
    }else{
        _txtCurrency.text = [currencyArray objectAtIndex:0];
    }
    [_txtCurrency resignFirstResponder];
}


#pragma mark - longpress gesture recognization
-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateEnded) {
        return;
    }
    CGPoint p = [gestureRecognizer locationInView:_collectioview];
    
    NSIndexPath *indexPath = [_collectioview indexPathForItemAtPoint:p];
    if (indexPath == nil){
        NSLog(@"couldn't find index path");
    } else {
        imageCVC* cell =  (imageCVC *)[_collectioview cellForItemAtIndexPath:indexPath];
        removeIndex = (int)indexPath.row;
        cell.btnRemove.hidden = false;
    }
}



-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == _txtAmount){
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    
        if([string isEqualToString:@","])
        {
            textField.text = [newString stringByReplacingOccurrencesOfString:@"," withString:@"."];
            return false;
        }else{
                return true;
        }
    }else{
        return true;
    }
}

#pragma mark - Button actions
- (IBAction)onSave:(id)sender {
    if ([self isValidData]) {
        
        
        NSString *userID = [Helper getPREF:@"userID"];
        if ([fromScreen isEqualToString:@"TripEdit"]){
            NSString *created_by = [tripDataDictionary valueForKey:@"created_by"];
            if (![created_by isEqualToString:userID]) {
                userID = created_by;
            }
        }
        
        NSDictionary *parameter;
        NSString* text =  _txtAmount.text;
        text = [text stringByReplacingOccurrencesOfString:@"," withString:@"."];
        
        NSString *expKey = selectedExpType; //_txtEType.text;
        NSString *vat_include = @"";
        NSString *vat = _txtvat.text;
        if([_SwitchVat isOn] == true)
        {
            vat_include = @"1";
            vat = [NSString stringWithFormat:@"%lu", [Arrvat indexOfObject:_txtvat.text] + 1];
        }else{
            vat_include = @"0";
            vat = @"0";
        }
        
        
        if (imageArray.count > 0) {
            //parameter=@{@"userid":userID, @"id":tripID, @"expense_account":[NSString stringWithFormat:@"%d",selectedSegment], @"expense_type":expKey, @"name":_txtEName.text, @"expense_date":_txtDate.text, @"amount":text, @"currency":_txtCurrency.text, @"image_doc":[imageArray objectAtIndex:0],@"include_vat":vat_include,@"vat":vat,@"note":_txtNotes.text };
             parameter=@{@"userid":userID, @"id":tripID, @"expense_account":[NSString stringWithFormat:@"%d",selectedSegment], @"expense_type":expKey, @"name":_txtEName.text, @"expense_date":_txtDate.text, @"amount":text, @"currency":_txtCurrency.text, @"image_doc":[imageArray objectAtIndex:0],@"include_vat":vat_include,@"vat":vat,@"note":@"" };
        }else{
            //parameter=@{@"userid":userID, @"id":tripID, @"expense_account":[NSString stringWithFormat:@"%d",selectedSegment], @"expense_type":expKey, @"name":_txtEName.text, @"expense_date":_txtDate.text, @"amount":text, @"currency":_txtCurrency.text, @"image_doc":@"",@"include_vat":vat_include,@"vat":vat,@"note":_txtNotes.text  };
             parameter=@{@"userid":userID, @"id":tripID, @"expense_account":[NSString stringWithFormat:@"%d",selectedSegment], @"expense_type":expKey, @"name":_txtEName.text, @"expense_date":_txtDate.text, @"amount":text, @"currency":_txtCurrency.text, @"image_doc":@"",@"include_vat":vat_include,@"vat":vat,@"note":@""  };
        }
        NSLog(@"Parameter %@: ",parameter);
        [self callAddExpenseService:parameter];
    }
}

- (IBAction)onRemoveImage:(id)sender {
    NSLog(@"removeIndex : %d",removeIndex);
    [imageArray removeObjectAtIndex:removeIndex];
    [_collectioview reloadData];
}

- (IBAction)SelctSegment:(id)sender{
    
    if ([sender tag] == 1) {
        [self onPrivateSelected];
    } else {
        [self onBusinessSelected];
    }
    
}

-(void)onPrivateSelected{
    selectedSegment = 1;
   [_BtnPrivateSegment setBackgroundColor:[Helper privateColor]];
    [_BtnPrivateSegment setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _BtnPrivateSegment.layer.borderWidth = 1;
    _BtnPrivateSegment.layer.borderColor = [Helper privateColor].CGColor;
    
    _ViewSegment.layer.borderWidth = 1;
    _ViewSegment.layer.borderColor = [Helper privateColor].CGColor;
    
    [_BtnBusinessSegment setBackgroundColor:[UIColor clearColor]];
    _BtnBusinessSegment.layer.borderColor = [Helper privateColor].CGColor;
    _BtnBusinessSegment.layer.borderWidth = 1;
    [_BtnBusinessSegment setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [_btnSave setBackgroundColor:[Helper privateColor]];
}

-(void)onBusinessSelected{
    selectedSegment = 2;
    [_BtnBusinessSegment setBackgroundColor:[Helper BusinessColor]];
    [_BtnBusinessSegment setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _BtnBusinessSegment.tintColor = [UIColor whiteColor];
    [_BtnBusinessSegment.titleLabel setTextColor:[UIColor whiteColor]];
    _BtnBusinessSegment.layer.borderWidth = 1;
    _BtnBusinessSegment.layer.borderColor = [Helper BusinessColor].CGColor;
    
    _ViewSegment.layer.borderWidth = 1;
    _ViewSegment.layer.borderColor = [Helper BusinessColor].CGColor;
    
    [_BtnPrivateSegment setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [_BtnPrivateSegment setBackgroundColor:[UIColor clearColor]];
    _BtnPrivateSegment.layer.borderColor = [Helper BusinessColor].CGColor;
    _BtnPrivateSegment.layer.borderWidth = 1;
    [_btnSave setBackgroundColor:[Helper BusinessColor]];
}


#pragma mark - date picker
- (void)onDatePickerValueChanged:(UIDatePicker *)datePicker {
    NSDateFormatter __autoreleasing *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormat setDateFormat:@"dd/MM/YYYY"];
    NSString *dateString =  [dateFormat stringFromDate:datePicker.date];
    _txtDate.text = dateString;
}

#pragma mark - Custom functions
-(BOOL)isValidData{
    if (_txtEType.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_select_expense_type",nil)];
        return false;
        
    }else if (_txtEName.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_expense_name",nil)];
        return false;
        
    }else if (_txtDate.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_date",nil)];
        return false;
        
    }else if (_txtAmount.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_amount",nil)];
        return false;
        
    }else if (_txtCurrency.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_currency",nil)];
        return false;
        
    }
//    else if (_txtNotes.text.length == 0) {
//        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_notes",nil)];
//        return false;
//        
//    }
    return true;
}
-(void)selectCamera
{
    if ([UIImagePickerController isCameraDeviceAvailable: UIImagePickerControllerCameraDeviceRear] || [UIImagePickerController isCameraDeviceAvailable: UIImagePickerControllerCameraDeviceFront])
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = false;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:NULL];
    }
}
- (IBAction)onAddImage:(id)sender{
    UIAlertView *messageAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Addimage", @"")                                                                message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"")                                                      otherButtonTitles:NSLocalizedString(@"Takenew", @""),NSLocalizedString(@"Fromgallery", @""),nil];
    messageAlert.tag = 987;
    [messageAlert show];
    
}


#pragma mark - webservice call
-(void)callAddExpenseService:(NSDictionary *)parameter{
    
    if(![self CheckNetworkConnection])
    {
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [WebService httpPostWithCustomDelegateMultipartURLString:@".saveExpense" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (error == nil) {
            NSError *localError = nil;
            NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
            
            if (localError != nil) {
                NSLog(@"%@",localError.description);
            }
            else {
                NSDictionary *response = [parsedObject objectForKey:@"data"];
                NSString* message = [response objectForKey:@"message"];
                NSString* success = [response objectForKey:@"status"];
                if ([success isEqualToString:@"Success"]) {
                    //[Helper displayAlertView:@"" message:message];
                    isSaved = true;
                    appdelegate.isRelaodTrip = @"1";
                    [self.navigationController popViewControllerAnimated:true];
                } else {
                    NSLog(@"error :: %@",message);
                    [Helper displayAlertView:@"" message:message];
                }
            }
        }
    }];
    
}

#pragma mark - alertView

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if (alertView.tag == 987)
    {
        if([title isEqualToString:NSLocalizedString(@"Takenew", @"")])
        {
            NSLog(@"camera was selected.");
            //[self selectCamera];
            if ([UIImagePickerController isCameraDeviceAvailable: UIImagePickerControllerCameraDeviceRear] || [UIImagePickerController isCameraDeviceAvailable: UIImagePickerControllerCameraDeviceFront])
            {
                UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                picker.delegate = self;
                picker.allowsEditing = false;
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                
                [self presentViewController:picker animated:YES completion:NULL];
            }else{
                [Helper displayAlertView:@"" message:@"Camera not available!"];
            }
        }
        else if([title isEqualToString:NSLocalizedString(@"Fromgallery", @"")])
        {
            NSLog(@"photo was selected.");
            UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
            cameraUI.delegate = self;
            cameraUI.allowsEditing = NO;
            cameraUI.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            
            [UIImagePickerController availableMediaTypesForSourceType: UIImagePickerControllerSourceTypePhotoLibrary];
            [self presentViewController:cameraUI animated:YES completion:nil];
        }
    }
}


#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    NSData* pictureData = UIImageJPEGRepresentation(info[UIImagePickerControllerOriginalImage], 0.5);
    //NSData *pictureData = [self compressResizeImage:info[UIImagePickerControllerOriginalImage]];
    if (pictureData != nil) {
        fromImage = true;
        imageArray = [[NSMutableArray alloc]init];
        [imageArray addObject:pictureData];
    }
    [picker dismissViewControllerAnimated:true completion:nil];
    [_collectioview reloadData];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:true completion:nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    if (!isSaved) {
        appdelegate.isRelaodTrip = @"0";
    }
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)ChangeSwitch:(id)sender {
    UISwitch *switch2 = (UISwitch *)sender;
    if([switch2 isOn])
    {
        _Vat_hight.constant = 50;
        if([_txtvat.text isEqualToString:@""]){
            _txtvat.text = @"25%";
            [vatPicker selectRow:24 inComponent:0 animated:YES];
            touched = TRUE;
        }
    }else{
        _Vat_hight.constant = 0;
    }
}

@end
