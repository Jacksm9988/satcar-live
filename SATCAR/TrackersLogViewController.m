//
//  TrackersLogViewController.m
//  SATCAR
//
//  Created by Percept Infotech on 13/01/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import "TrackersLogViewController.h"
#import "TracerDetailsTVC.h"
#import "TrackerLocaitonMapViewController.h"
@interface TrackersLogViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *trackerLogArray,*tempTrackerLogArray;
    int selectedIndex;
}
@end

@implementation TrackersLogViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    trackerLogArray = [[NSMutableArray alloc]init];
    selectedIndex = -1;
    _tableview.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [_tableview reloadData];
    [self getAllTrackingData];
}

-(void)viewWillAppear:(BOOL)animated{
    [self addTitleView:NSLocalizedString(@"Log", @"")];
}

#pragma mark - Table View
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return trackerLogArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TracerDetailsTVC *cell = (TracerDetailsTVC *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSDictionary *TrackData = [[NSDictionary alloc]init];
    TrackData = [trackerLogArray objectAtIndex:indexPath.row];
    
    NSString *tripType = [TrackData valueForKey:@"type"];
    if ([tripType isEqualToString:@"1"]) {
        cell.lblTripColor.backgroundColor = [Helper privateColor];
    }else{
        cell.lblTripColor.backgroundColor = [Helper BusinessColor];
    }
    
    NSString *date = [TrackData valueForKey:@"trake_date"];
    cell.lblDate.text = [NSString stringWithFormat:@"%@",date];
    
    NSString *strLat = [NSString stringWithFormat:@"%@",[TrackData valueForKey:@"lat"]];
    NSString *strLon = [NSString stringWithFormat:@"%@",[TrackData valueForKey:@"lng"]];
    float lat = [strLat floatValue];
    float lon = [strLon floatValue];
    cell.lblLatitude.text = [NSString stringWithFormat:@"%.4f",lat];
    cell.lblLongitude.text = [NSString stringWithFormat:@"%.4f",lon];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == trackerLogArray.count-1) {
        NSLog(@"please update all trip data");
        [self getAllTrackingData];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedIndex = (int)indexPath.row;
    [self performSegueWithIdentifier:@"segueTrackerLogMap" sender:self];
}

-(void)getAllTrackingData{
    NSString *count = [NSString stringWithFormat:@"%d",(int)trackerLogArray.count];
    [self callTrackingService:count];
}

-(void)callTrackingService:(NSString *)limit{
    
    if(![self CheckNetworkConnection]) {
        //[self AlertNoInternet:NSLocalizedString(@"NoInternet", @"") tag:454869];
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *userID = [Helper getPREF:@"userID"];
    NSDictionary *parameter=@{
                              @"userid":userID,
                              @"limitstart":limit
                              };
    //NSLog(@"parameter : %@",parameter);
    [WebService httpPostWithCustomDelegateURLString:@".trackerLog" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         if (error == nil) {
             NSError *localError = nil;
             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
             
             if (localError != nil) {
                 NSLog(@"%@",localError.description);
             }
             else {
                 id response = [parsedObject objectForKey:@"data"];
                 if ([self CheckSession:response]) {
                     tempTrackerLogArray = [[NSMutableArray alloc]init];
                     tempTrackerLogArray  = [response objectForKey:@"trackerlog"];
                     if (tempTrackerLogArray.count > 0) {
                         NSLog(@"Trips : %@",tempTrackerLogArray);
                         for (int i = 0; i < tempTrackerLogArray.count; i++) {
                             [trackerLogArray addObject:[tempTrackerLogArray objectAtIndex:i]];
                         }
                         [_tableview reloadData];
                     }
                 } else {
                     //[self moveToLogin ];
                 }
             }
         }
     }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    TrackerLocaitonMapViewController *controller = [segue destinationViewController];
    NSDictionary *TrackData = [trackerLogArray objectAtIndex:selectedIndex];
    controller.strLat = [TrackData valueForKey:@"lat"];
    controller.strLon = [TrackData valueForKey:@"lng"];
}


@end
