//
//  RegisterPayVC.m
//  SATCAR
//
//  Created by Percept Infotech on 2018-05-10.
//  Copyright © 2018 Percept Infotech. All rights reserved.
//

#import "RegisterPayVC.h"
#import "RegisterViewController.h"

@interface RegisterPayVC ()
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;
@property (weak, nonatomic) IBOutlet UIButton *btnPay;

@end

@implementation RegisterPayVC
@synthesize userInfo;

- (void)viewDidLoad {
    [super viewDidLoad];
    _btnPay.layer.cornerRadius = 25.0;
    _btnPay.layer.borderWidth = 1;
    _btnPay.layer.borderColor = [UIColor colorWithRed:0.000 green:0.651 blue:0.929 alpha:1.00].CGColor;
}

- (IBAction)onPay:(id)sender {
    [self performSegueWithIdentifier:@"segueUnpaidAToPay" sender:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    RegisterViewController *controller = segue.destinationViewController;
    controller.userFrom = @"PendingPayment";
}


@end
