//
//  BeaconConfigureViewController.h
//  SATCAR
//
//  Created by Percept Infotech on 11/07/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BeaconConfigureViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lblBluetooth;
@property (weak, nonatomic) IBOutlet UISwitch *swtchBluetooth;
@property (weak, nonatomic) IBOutlet UITableView *tableview;

//- (IBAction)onBluetooth:(id)sender;
@property (strong, nonatomic)KTKBeaconManager *KTKbeaconManager;
@property (nonatomic,strong) KTKBeaconRegion *KTKregion;
@end
