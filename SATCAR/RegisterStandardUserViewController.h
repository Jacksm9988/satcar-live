//
//  RegisterStandardUserViewController.h
//  SATCAR
//
//  Created by Percept Infotech on 24/01/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterStandardUserViewController : UIViewController

- (IBAction)onOneMonthDuration:(id)sender;
- (IBAction)onTweleveMonthDuration:(id)sender;
- (IBAction)on79DKK:(id)sender;
- (IBAction)on1799DKK:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgOneMonth;
@property (weak, nonatomic) IBOutlet UIImageView *imgTweleveMonth;
@property (weak, nonatomic) IBOutlet UIImageView *img79DKK;
@property (weak, nonatomic) IBOutlet UIImageView *img1799;
- (IBAction)onNext:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property(nonatomic,strong) NSString *userFrom;
@property (weak, nonatomic) IBOutlet UILabel *lblOneMonth;
@property (weak, nonatomic) IBOutlet UILabel *lblTweleveMonth;
@property (weak, nonatomic) IBOutlet UILabel *lbldurationhead;
@property (weak, nonatomic) IBOutlet UILabel *lblAddonedevice;
@property (weak, nonatomic) IBOutlet UILabel *lblSatcarBeep;
@property (weak, nonatomic) IBOutlet UILabel *lblSatcarGps;

@property (weak, nonatomic) IBOutlet UIView *viewDuration1M;
@property (weak, nonatomic) IBOutlet UIView *viewDuration12M;
@property (weak, nonatomic) IBOutlet UIView *viewAddOn79;
@property (weak, nonatomic) IBOutlet UIView *viewAddOn1799;

@end
