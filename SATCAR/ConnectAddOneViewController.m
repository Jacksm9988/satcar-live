//
//  ConnectAddOneViewController.m
//  SATCAR
//
//  Created by Percept Infotech on 08/02/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import "ConnectAddOneViewController.h"
#import "ConnectAddoneTVC.h"
#import "TrackerIDViewController.h"

@interface ConnectAddOneViewController ()<UITableViewDelegate,UITableViewDataSource> {
    NSMutableArray *menuArray;
}
@end

@implementation ConnectAddOneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     menuArray = [[NSMutableArray alloc]initWithObjects:@"SATCAR BEEP",@"SATCAR Tracker",nil];
    _tableview.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
   // [self addTitleView:NSLocalizedString(@"ConnectAddOne", @"")];
    [self addTitleView:NSLocalizedString(@"PairDevice", @"")];
    
}

#pragma mark - Table View
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return menuArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ConnectAddoneTVC *cell = (ConnectAddoneTVC *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.lblMenu.text = [menuArray objectAtIndex:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        [self performSegueWithIdentifier:@"segueConfigure" sender:self];
    }else{
        [self performSegueWithIdentifier:@"segueReimToTracker" sender:self];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"segueReimToTracker"]){
        TrackerIDViewController *controller = segue.destinationViewController;
        controller.from = @"Connect";
    }
}
@end
