//
//  MyDetailsViewController.m
//  SATCAR
//
//  Created by Percept Infotech on 30/12/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import "MyDetailsViewController.h"
#import "UIViewController+VCHelpers.h"
#import "ECSlidingViewController.h"
#import "LeftSideViewController.h"
#import "NavigationController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "UIViewController+VCHelpers.h"

@interface MyDetailsViewController ()<UITextFieldDelegate>
{
    NSDictionary *userInfo;
    UIButton *btnEdit;
    int isSave;
}
@end

@implementation MyDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    isSave = 0;
    NSData *userdata = [Helper getDataValue:@"userInfo"];
    NSDictionary * dict = [self dataToDicitonary:userdata];
    if (dict != nil) {
        userInfo = [dict valueForKey:@"data"];
        NSLog(@"user info : %@",userInfo);
        
       /* _txtName.text = [[userInfo valueForKey:@"user"]valueForKey:@"name"];
        _txtEmailAdd.text = [[userInfo valueForKey:@"user"]valueForKey:@"email"];
        //_txtHouseNumber.text = [userInfo valueForKey:@"house_number"];
        _txtAddress.text = [userInfo valueForKey:@"address"];
        _txtPost.text = [userInfo valueForKey:@"post_number"];
        _txtTelefon.text = [userInfo valueForKey:@"phone"];*/
        
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [self addTitleView:NSLocalizedString(@"MyDetails", @"")];
    
    btnEdit = [UIButton buttonWithType:UIButtonTypeCustom];
    btnEdit.frame = CGRectMake(20,0,70,32);
    [btnEdit setTitle:NSLocalizedString(@"Edit", @"") forState:UIControlStateNormal];
    [btnEdit setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnEdit.titleLabel.font = [UIFont systemFontOfSize:16];
    [btnEdit addTarget:self action:@selector(editDetail) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* item = [[UIBarButtonItem alloc] initWithCustomView:btnEdit];
    self.navigationItem.rightBarButtonItem = item;
    
    _lblName.text = NSLocalizedString(@"Name", @"");
    _lblTelefon.text = NSLocalizedString(@"Telefon", @"");
    _lblAddress.text = NSLocalizedString(@"Address", @"");
    _lblEmailAdd.text = NSLocalizedString(@"EmailAddress", @"");
    _lblPost.text = NSLocalizedString(@"Postnr", @"");
    _lblPwd.text = NSLocalizedString(@"ChangePassword", @"");
    [self disableAll];
   // [self getUserPersonal];
    [self callGetCredentialsService:[[userInfo valueForKey:@"user"]valueForKey:@"id"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    textField.textColor =  [UIColor colorWithRed:18.0/255.0 green:172.0/255.0 blue:243.0/255.0 alpha:1.0];
    textField.layer.borderWidth = 1;
    textField.layer.borderColor =  [UIColor colorWithRed:18.0/255.0 green:172.0/255.0 blue:243.0/255.0 alpha:1.0].CGColor;
    textField.clipsToBounds = YES;
    return true;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    textField.textColor =  [UIColor blackColor];
    textField.layer.borderWidth = 0;
    textField.layer.borderColor = [UIColor clearColor].CGColor;
    textField.clipsToBounds = YES;
    return true;
}*/

-(void)editDetail{
    if (isSave == 0) {
        isSave = 1;
        [btnEdit setTitle:NSLocalizedString(@"Save", @"") forState:UIControlStateNormal];
        [self enableAll];
    }else{
        if ([self isValidData]) {
            NSString *userID = [Helper getPREF:@"userID"];
    NSDictionary *parameter=@{ @"userid":userID,@"name":_txtName.text,@"address":_txtAddress.text,@"postno":_txtPost.text,@"phone":_txtTelefon.text };
            //NSLog(@"parameter : %@",parameter);
            [self callUpdateDetaillWS:parameter];
        }
    }
}

-(void)enableAll{
    
    _txtName.enabled = true;
    _txtAddress.enabled = true;
    _txtPost.enabled = true;
    _txtTelefon.enabled = true;
    _txtEmailAdd.enabled = true;
    //_txtHouseNumber.enazbled = true;
    [_txtName becomeFirstResponder];
}
-(void)disableAll{
    _txtName.enabled = false;
    _txtAddress.enabled = false;
    _txtPost.enabled = false;
    _txtTelefon.enabled = false;
    _txtEmailAdd.enabled = false;
   // _txtHouseNumber.enabled = false;
}

- (void)getUserPersonal{
    
    NSString *tracker_id = [Helper getPREF:@"tracker_id"];
    NSDictionary *parameter=@{ @"tracker_id":tracker_id };
    //NSLog(@"parameter : %@",parameter);
    [self callGetCarUserPersonalWS:parameter];
}

-(void)callGetCredentialsService:(NSString *)uID{
    if(![self CheckNetworkConnection])
    {
        //[self AlertNoInternet:NSLocalizedString(@"NoInternet", @"") tag:454869];
        return;
    }
    NSString *userID = uID;
    NSLog(@"callGetCredentialsService");
    
    
        NSDictionary *parameter=@{@"userid":uID};
        UIWindow *window = [UIApplication sharedApplication].windows.lastObject;
        [MBProgressHUD showHUDAddedTo:window animated:true];
        
        [WebService httpPostWithCustomDelegateURLString:@".credentials" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error) {
            [MBProgressHUD hideHUDForView:window animated:true];
            if (error == nil)
            {
                NSError *localError = nil;
                NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
                
                if (localError != nil) {
                    NSLog(@"%@",localError.description);
                    
                }else
                {
                    NSDictionary *response = [parsedObject objectForKey:@"data"];
                    NSLog(@"GetCredential response : %@",response);
                    NSString *status = [response valueForKey:@"status"];
                    if ([status isEqualToString:@"Success"]) {
                        
                            // Save user value
                            NSDictionary* user = [response objectForKey:@"user"];
                            NSString *email = [user valueForKey:@"email"];
                            NSString *name = [user valueForKey:@"name"];
                            NSString *uID = [user valueForKey:@"id"];
                        
                            NSString* registerDate = [user objectForKey:@"registerDate"];
                            //NSString* sid = [response objectForKey:@"sid"];
                            NSString* userTyp = [response objectForKey:@"user_type"];
                        
                            [Helper setPREFStringValue:uID sKey:@"userID"];
                            //[Helper setPREFStringValue:sid sKey:@"sid"];
                            [Helper setPREFStringValue:name sKey:@"userName"];
                            [Helper setPREFStringValue:email sKey:@"userEmail"];
                            [Helper setPREFStringValue:userTyp sKey:@"user_type"];
                            [Helper setPREFStringValue:registerDate sKey:@"userRegisterDate"];
                        
                            [Helper setDataValue:data sKey:@"userInfo"];
                            
                        _txtName.text = [user valueForKey:@"name"];
                        _txtTelefon.text = [response valueForKey:@"phone"];
                        // _txtHouseNumber.text = [CarDetail valueForKey:@"house_number"];
                        _txtAddress.text = [response valueForKey:@"address"];
                        _txtEmailAdd.text = [user valueForKey:@"email"];
                        _txtPost.text = [response valueForKey:@"post_number"];
                        
                        
                            //[self callLocationService:uID from:@""];
                       
                    }
                }
            }
        }];
}

-(void)callGetCarUserPersonalWS:(NSDictionary *)parameter{
    if(![self CheckNetworkConnection])
    {
        //[self AlertNoInternet:NSLocalizedString(@"NoInternet", @"") tag:454869];
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [WebService httpPostWithCustomDelegateURLStringForCar:@".getCarUserPersonal" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         if (error == nil) {
             
             NSError *localError = nil;
             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
             
             if (localError != nil) {
                 NSLog(@"%@",localError.description);
             }
             else {
                 NSLog(@"Response : %@",parsedObject);
                 NSDictionary *response = [parsedObject objectForKey:@"data"];
                 NSDictionary *CarDetail = [response objectForKey:@"CarDetail"];
                 NSString* message = [response objectForKey:@"message"];
                 NSString* success = [response objectForKey:@"status"];
                 if ([success isEqualToString:@"Success"]) {
                     _txtName.text = [CarDetail valueForKey:@"name"];
                     _txtTelefon.text = [CarDetail valueForKey:@"phone"];
                    // _txtHouseNumber.text = [CarDetail valueForKey:@"house_number"];
                     _txtAddress.text = [CarDetail valueForKey:@"address"];
                     _txtEmailAdd.text = [CarDetail valueForKey:@"email_address"];
                     _txtPost.text = [CarDetail valueForKey:@"post_nr"];
                 } else {
                     NSLog(@"error :: %@",message);
                     //[Helper displayAlertView:@"" message:message];
                 }
             }
         }
     }];
}

#pragma mark - Custom functions
-(BOOL)isValidData{
    
    if (_txtName.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_name",nil)];
        return false;
        
    }else if (_txtAddress.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_address",nil)];
        return false;
        
    }/*else if (_txtHouseNumber.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_address",nil)];
        return false;
        
    }*/else if (_txtPost.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_zipcode",nil)];
        return false;
        
    }else if (_txtTelefon.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_phone",nil)];
        return false;
    }return true;
}



-(void)callUpdateDetaillWS:(NSDictionary *)parameter{
    if(![self CheckNetworkConnection])
    {
        //[self AlertNoInternet:NSLocalizedString(@"NoInternet", @"") tag:454869];
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
     [WebService httpPostWithCustomDelegateURLString:@".updateProfile" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error)
    {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
       if (error == nil) {
             NSError *localError = nil;
             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
             
             if (localError != nil) {
                 NSLog(@"%@",localError.description);
             }
             else {
                 NSLog(@"Response : %@",parsedObject);
                 NSDictionary *response = [parsedObject objectForKey:@"data"];
                 NSString* message = [response objectForKey:@"message"];
                 NSString* success = [response objectForKey:@"status"];
                 if ([success isEqualToString:@"Success"]) {
                     
                     
                     if (_txtName.text.length > 0) {
                         NSDictionary *user = [userInfo valueForKey:@"user"];
                         [user setValue:_txtName.text forKey:@"name"];
                         [userInfo setValue:user forKey:@"user"];
                     }else{
                         NSDictionary *user = [userInfo valueForKey:@"user"];
                         [user setValue:@"" forKey:@"name"];
                         [userInfo setValue:user forKey:@"user"];
                     }
                     
                     if (_txtAddress.text.length > 0) {
                         [userInfo setValue:_txtAddress.text forKey:@"address"];
                     }else{
                         [userInfo setValue:@"" forKey:@"address"];
                     }
                    
                     if (_txtPost.text.length > 0) {
                         [userInfo setValue:_txtPost.text forKey:@"post_number"];
                     }else{
                         [userInfo setValue:@"" forKey:@"post_number"];
                     }
                     
                     /*if (_txtTelefon.text.length > 0) {
                         [userInfo setValue:_txtPost.text forKey:@"phone"];
                     }else{
                         [userInfo setValue:@"" forKey:@"phone"];
                     }*/
                     
                    /* if (_txtHouseNumber.text.length > 0) {
                         [userInfo setValue:_txtHouseNumber.text forKey:@"house_number"];
                     }else{
                         [userInfo setValue:@"" forKey:@"house_number"];
                     }*/
                     
                     NSLog(@"user info : %@",userInfo);
                     NSDictionary *dict = @{ @"data": userInfo };
                     NSLog(@"dict : %@",dict);
                     NSData *udata = [self dicitonaryToData:dict];
                     [Helper setDataValue:udata sKey:@"userInfo"];
                     
                     [self.navigationController popViewControllerAnimated:true];
                 } else {
                     NSLog(@"error :: %@",message);
                     //[Helper displayAlertView:@"" message:message];
                 }
             }
         }
     }];
}


#pragma mark - Button actions

- (IBAction)onChangePwd:(id)sender {
    [self performSegueWithIdentifier:@"SeguePersionalDTLTochangePwd" sender:self];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
