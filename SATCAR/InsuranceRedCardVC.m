//
//  InsuranceRedCardVC.m
//  SATCAR
//
//  Created by Percept Infotech on 17/03/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import "InsuranceRedCardVC.h"

@interface InsuranceRedCardVC ()

@end

@implementation InsuranceRedCardVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIColor *bluecolor = [UIColor colorWithRed:0.000 green:0.651 blue:0.929 alpha:1.00];
    float btnRadius = 25.0;
    [self addTitleView:NSLocalizedString(@"redcard_app", @"")];
    _btnIOS.layer.cornerRadius = btnRadius;
    _btnIOS.layer.borderWidth = 1;
    _btnIOS.layer.borderColor =bluecolor.CGColor;
    _btnIOS.backgroundColor = bluecolor;
    [_btnIOS setTitle:NSLocalizedString(@"ios_download", @"") forState:UIControlStateNormal];
    _lblMessage.text = NSLocalizedString(@"Redcard_message", @"");
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onIOS:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/dk/app/id374484612?mt=8"]];
}

- (IBAction)onAndroid:(id)sender {
}
@end
