//
//  UserListTVC.h
//  SATCAR
//
//  Created by Percept Infotech on 2018-05-17.
//  Copyright © 2018 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserListTVC : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIButton *btnRemove;

@end
