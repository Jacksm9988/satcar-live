//
//  AppDelegate.m
//  SATCAR
//
//  Created by Percept Infotech on 12/7/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import "AppDelegate.h"
#import <UserNotifications/UserNotifications.h>
#import "Util.h"
#import <AudioToolbox/AudioToolbox.h>
#import "UIViewController+VCHelpers.h"
#import "ECSlidingViewController.h"
#import "LeftSideViewController.h"
#import "NavigationController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "UIViewController+VCHelpers.h"
@import GoogleMaps;

#import "HomeViewController.h"
#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define BEACON_PROXIMITY_UUID [[NSUUID alloc] initWithUUIDString:@"B9407F30-F5F8-466E-AFF9-25556B57FE6D"]

NSString * const NotificationCategoryIdent  = @"ACTIONABLE";
NSString * const NotificationActionYesIdent = @"ACTION_YES";
NSString * const NotificationActionNoIdent = @"ACTION_NO";
NSString * const kKeyToCheck = @"category";

//#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
@interface AppDelegate ()<KTKBeaconManagerDelegate,UNUserNotificationCenterDelegate,UIApplicationDelegate,UIAlertViewDelegate,CLLocationManagerDelegate,CLLocationManagerDelegate,UNUserNotificationCenterDelegate,UNUserNotificationCenterDelegate> {
    HomeViewController *control;
    __block UIBackgroundTaskIdentifier bgTaskId, bgTaskReInitApp;
    __block UIBackgroundTaskIdentifier bgTaskForPopup;
    UIAlertController *alertUserMessage;
    int exittimerTime,popupWaitTime;
}

@property (strong, nonatomic) KTKBeaconRegion *KTKregion;
@property (strong, nonatomic)UINavigationController *navigationController;
@property (nonatomic, strong) void (^contentHandler)(UNNotificationContent *contentToDeliver);
@property (nonatomic, strong) UNMutableNotificationContent *bestAttemptContent;


@end

@implementation AppDelegate
@synthesize WebServiceUrl,freeUserMessage,locationManager,finalTrack,selectedRange;
@synthesize networkReachable,ConnectedbeaconRegion,ConnectionStatus,tracker_id;
@synthesize isAppStartWithoutNet,oldPwd,Newpwd,Email,SelectNote,SelectWeb,SelectPriority;
@synthesize internetActive,appTimer,appStartTime,appClockTime,appKM,appSTTime,appEDTime,appSTDate;
@synthesize appLogin,appStartLocation,epay_info,dantracker,appDistance,appLaunch,beaconRegionName;
@synthesize KTKbeaconManager,KTKregion;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    //[GMSPlacesClient provideAPIKey:@"AIzaSyDP9R_14MBvcQ219zT2nhRGfdeG2t5xa7g"];
    NSLog(@"didFinishLaunchingWithOptions");
    
    // Google key with client account
    _googleApiKey = @"AIzaSyCf19jIwAXiiwSPe40xQI4qNVYjFAtI2pg";
    [GMSPlacesClient provideAPIKey:_googleApiKey];
    [GMSServices provideAPIKey:@"YOUR_API_KEY"];
    [Kontakt setAPIKey:@"OAgjTvDCCUFOOZgdzLuWmwQudSYaqmkI"];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    exittimerTime = 30;
    popupWaitTime = 90;
    //[self performSelector:@selector(CallAfer) withObject:nil afterDelay:8.0];
    NSLog(@"Set counter to zero");
    _locationCounter = @"0";
    
    _app = [UIApplication sharedApplication];
    
    NSString *Appname = [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleNameKey];
    [[NSUserDefaults standardUserDefaults] setObject:Appname forKey:@"Appname"];
    appStartLocation = nil;
    epay_info = @"epay_info";
    dantracker = @"dantracker";
    beaconRegionName = @"SATCAR_REGION";
    _appLocationDataArray = [[NSMutableArray alloc]init];
    _dantrackerCarLocationArray = [[NSArray alloc]init];
    _appFirstLaunch = [Helper getPREF:@"FirstLaunch"];
    _isTripAutomatic = [Helper getPREF:@"isTripAutomatic"];
    appLogin = @"0";
    appDistance = @"0";
    appLaunch = @"1";
    finalTrack = @"0";
    _appTrackNotification = @"1";
    _exitTimerCount = 0;
    _appState = @"1";
    _isRelaodTrip = @"1";
    _freeTrip = @"30";
    ConnectionStatus = @"0";
    [Util copyFile:@"SATCAR_database.sqlite"];
    _isTripContinue = [Helper getPREF:@"isTripContinue"];
    _beaconWarningMessage = [Helper getPREF:@"beaconWarningMessage"];
    
    _appVersion = [Helper getVersionNumber];
    _appBuild = [Helper getBuildNumber];
    NSLog(@"Version : %@, Build : %@",_appVersion,_appBuild);
    
    [self setApplicationLanguage];
    
    if (![_appFirstLaunch isEqualToString:@"1"]) {
        [Helper setPREFStringValue:@"0" sKey:@"FirstLaunch"];
    }
    _navigationController = [[UINavigationController alloc]init];
    
    NSInteger state = (NSInteger)[Helper getPREFNSint:@"currentStage"];
    _currentStage = state;
    /*if (state == 0) {
        _currentStage = 0;
    }else{
        _currentStage = state;
    }*/
    
    freeUserMessage = NSLocalizedString(@"FreeUserFinishAllFreeTripsMsg", @"");
    application.applicationIconBadgeNumber = 0;
    [self registerForRemoteNotification];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotoHome) name:@"goToHomeScreen" object:nil];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"is_menushow"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"storyBoardIdentifier"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [[IQKeyboardManager sharedManager] setEnable:true];
   
    /*[TPAManager sharedManager].loggingDestinations = TPALoggingDestinationConsole|TPALoggingDestinationRemote;
    [TPAManager sharedManager].updateNotificationsEnabled = YES;
    [TPAManager sharedManager].crashReporting = TPACrashReportingAlwaysAsk;
    [TPAManager sharedManager].sessionRecordingEnabled = YES;
    [TPAManager sharedManager].analyticsEnabled = YES;
    [TPAManager sharedManager].feedbackInvocation = TPAFeedbackInvocationDisabled;
    [[TPAManager sharedManager] startManagerWithBaseUrl:@"https://weiswise.tpa.io/" projectUuid:@"f73d1341-4121-4410-ae35-92345ba567fa"];*/
    
    
    _BeaconNotifConnected = @"Connect";
    _BeaconNotifDisconnected = @"Disconnect";
    ConnectedbeaconRegion = @"";
    NSString *userID = @"";
    if ([[Helper getPREF:@"islogin"] isEqualToString:@"1"]) {
        userID = [Helper getPREF:@"userID"];
        appLogin = @"1";
    }else{
        appLogin = @"0";
    }
    //calling credential service either user login or not, to get payment info, detracker, user info and beacon info
    [self callGetCredentialsService:userID];

    locationManager = [[CLLocationManager alloc] init];
    #ifdef __IPHONE_8_0
    if(IS_OS_8_OR_LATER) {
        [locationManager requestAlwaysAuthorization];
        //[self.locationManager requestWhenInUseAuthorization];
    }
    #endif

    if(![CLLocationManager locationServicesEnabled]){
        
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"DeviceLocationServiceisOff", @"") message:NSLocalizedString(@"YoucanenableaccessinSettings", @"") preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"Ok", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)         {
           // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root=LOCATION_SERVICES"]];
            [[UIApplication sharedApplication] openURL: [NSURL URLWithString:UIApplicationOpenSettingsURLString]];
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alert addAction:yesButton];
        [self performSelector:@selector(showAlert:) withObject:alert afterDelay:1.0];
      
    }else{
        if([CLLocationManager locationServicesEnabled] && [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied) {
        } else {
            // show error
            
            if([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0){
                
                UIAlertController * alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"haveaccesstoLocationservice", @"")                                              message:NSLocalizedString(@"Youcanenableaccess", @"") preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yesButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"Ok", @"") style:UIAlertActionStyleDefault                                             handler:^(UIAlertAction * action) {
                    [alert dismissViewControllerAnimated:YES completion:nil];
                }];
                
                [alert addAction:yesButton];
                [self performSelector:@selector(showAlert:) withObject:alert afterDelay:1.0];
                
            } else {
                
                UIAlertController * alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"haveaccesstoLocationservice", @"")                                              message:NSLocalizedString(@"Youcanenableaccess", @"") preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yesButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"Ok", @"") style:UIAlertActionStyleDefault                                             handler:^(UIAlertAction * action) {
                    [alert dismissViewControllerAnimated:YES completion:nil];
                }];
                
                UIAlertAction* noButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"Settings", @"") style:UIAlertActionStyleDefault                                            handler:^(UIAlertAction * action) {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                    [alert dismissViewControllerAnimated:YES completion:nil];
                }];
                
                [alert addAction:yesButton];
                [alert addAction:noButton];
                [self performSelector:@selector(showAlert:) withObject:alert afterDelay:1.0];
            }
        }
    }

    Reachability *reach = [Reachability reachabilityWithHostname:@"www.google.com"];
    [reach startNotifier];
    isalert = FALSE;
    
    reach.unreachableBlock = ^(Reachability * reachability) {
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           networkReachable = FALSE;
                           isAppStartWithoutNet = TRUE;
                           internetActive = NO;
                           _hostActive = NO;
                           [self statusChange];
                       });
    };
    
    reach.reachableBlock = ^(Reachability *reach)  {
        dispatch_sync(dispatch_get_main_queue(), ^{
            internetActive = YES;
            _hostActive = YES;
            [self statusChange];
        });
    };
    return YES;
}
-(void)CallAfer{
 [[NSNotificationCenter defaultCenter] postNotificationName:@"BeaconConnection_Formsg" object:nil userInfo:nil];
}

-(BOOL )checkAutomaticTripIsContinue{
    NSLog(@"checkAutomaticTripIsContinue");
    NSUserDefaults*  def = [NSUserDefaults standardUserDefaults];
    _isTripContinue = [def valueForKey:@"isTripContinue"];
    
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
    if (state == UIApplicationStateInactive) {
        return FALSE;
    }else{
        if ([_isTripContinue isEqualToString:@"1"]) {
            NSString *AppTerminate = [def valueForKey:@"AppTerminate"];
            if ([AppTerminate isEqualToString:@"1"]) {
                
                if([[Helper getPREF:@"type"] isEqualToString:@"0"])
                {
                    [def setValue:@"0" forKey:@"AppTerminate"];
                    [def synchronize];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"startTripAfterRestart" object:nil userInfo:nil];
                    return TRUE;
                }
                else{
                    return FALSE;
                }
            }
            else{
                return FALSE;
            }
        }
        else{
            return FALSE;
        }
    }
    
}

-(BOOL )checkAutomaticTripifStateOutside:(CLRegionState)state {
    NSLog(@"checkAutomaticTripifStateOutside");
    NSUserDefaults*  def = [NSUserDefaults standardUserDefaults];
    _isTripContinue = [def valueForKey:@"isTripContinue"];
     NSString *isRestarted = [def valueForKey:@"AppTerminate"];
    
    if ([_isTripContinue isEqualToString:@"1"] && [isRestarted isEqualToString:@"1"] && [[Helper getPREF:@"type"] isEqualToString:@"0"])
    {
        NSLog(@"App Try to save after restart disconnrct becon");
        if (state == CLRegionStateOutside) {
            NSLog(@"App reset All value ");
            [def setValue:@"0" forKey:@"AppTerminate"];
            [def synchronize];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SaveTripAtAppReStarttimeIfBeconDisconnect" object:nil userInfo:nil];
            return TRUE;
        }else
            return FALSE;
    }else
        return FALSE;
}

- (BOOL)connected {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

#pragma mark - Class Methods

/**
 Notification Registration
 */
- (void)registerForRemoteNotification {
    NSLog(@"registerForRemoteNotification");
    
    UIMutableUserNotificationAction *action1;
    action1 = [[UIMutableUserNotificationAction alloc] init];
    [action1 setActivationMode:UIUserNotificationActivationModeBackground];
    [action1 setTitle:@"YES"];
    [action1 setIdentifier:NotificationActionYesIdent];
    [action1 setDestructive:NO];
    [action1 setAuthenticationRequired:NO];
    
    UIMutableUserNotificationAction *action2;
    action2 = [[UIMutableUserNotificationAction alloc] init];
    [action2 setActivationMode:UIUserNotificationActivationModeBackground];
    [action2 setTitle:@"NO"];
    [action2 setIdentifier:NotificationActionNoIdent];
    [action2 setDestructive:YES];
    [action2 setAuthenticationRequired:NO];
    
    UIMutableUserNotificationCategory *actionCategory;
    actionCategory = [[UIMutableUserNotificationCategory alloc] init];
    [actionCategory setIdentifier:NotificationCategoryIdent];
    [actionCategory setActions:@[action1, action2] forContext:UIUserNotificationActionContextDefault];
    
    NSSet *categories = [NSSet setWithObject:actionCategory];
    UIUserNotificationType types = (UIUserNotificationTypeAlert| UIUserNotificationTypeSound| UIUserNotificationTypeBadge);
    
    UIUserNotificationSettings *settings;
    settings = [UIUserNotificationSettings settingsForTypes:types categories:categories];
    
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")) {
        
        UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert | UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
            if( !error ){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] registerForRemoteNotifications];
                });
            }else{
                NSLog(@"Notif error : %@",error);
            }
        }];
        
        UNNotificationAction *ok = [UNNotificationAction actionWithIdentifier:NotificationActionYesIdent title:NSLocalizedString(@"Yes", nil)
        options:UNNotificationActionOptionForeground];
        
        UNNotificationAction *cancel = [UNNotificationAction actionWithIdentifier:NotificationActionNoIdent title:NSLocalizedString(@"No", nil) options:UNNotificationActionOptionForeground];
        
        NSArray *buttons = @[ ok, cancel ];
        
        // create a category for message failed
        UNNotificationCategory *buttonsAction = [UNNotificationCategory categoryWithIdentifier:NotificationCategoryIdent
                actions:buttons intentIdentifiers:@[] options:UNNotificationCategoryOptionCustomDismissAction];
        
        NSSet *categories = [NSSet setWithObjects:buttonsAction, nil];
        // registration
        [[UNUserNotificationCenter currentNotificationCenter] setNotificationCategories:categories];
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
       // [FIRMessaging messaging].remoteMessageDelegate = self;

    }
    else {
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    }
}

// Deprecated
/*- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void (^)(void))completionHandler {
}*/

#pragma mark - Remote Notification Delegate // <= iOS 9.x

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    dispatch_async(dispatch_get_main_queue(), ^{
        [application registerForRemoteNotifications];
    });
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    NSString *strDevicetoken = [[NSString alloc]initWithFormat:@"%@",[[[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]] stringByReplacingOccurrencesOfString:@" " withString:@""]];
    NSLog(@"Device Token = %@",strDevicetoken);
    self.strDeviceToken = strDevicetoken;
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo     {
    NSLog(@"Push Notification Information : %@",userInfo);
    [self application:application didReceiveRemoteNotification:userInfo fetchCompletionHandler:^(UIBackgroundFetchResult result){}];
}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"%@ = %@", NSStringFromSelector(_cmd), error);
    NSLog(@"Error = %@",error);
}

#pragma mark - UNUserNotificationCenter Delegate // >= iOS 10

-(void) application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void(^)(UIBackgroundFetchResult))completionHandler {
   
    // iOS 10 will handle notifications through other methods
   NSLog( @"iOS version >= 10. Let NotificationCenter handle this one." );
    // set a member variable to tell the new delegate that this is background
    
    if( [UIApplication sharedApplication].applicationState == UIApplicationStateInactive ) {
        NSLog( @"INACTIVE" );
        completionHandler( UIBackgroundFetchResultNewData );
    }
    else if( [UIApplication sharedApplication].applicationState == UIApplicationStateBackground ) {
        NSLog( @"BACKGROUND" );
        completionHandler( UIBackgroundFetchResultNewData );
    }
    else {
        NSLog( @"FOREGROUND" );
        completionHandler( UIBackgroundFetchResultNewData );
    }
    
    return;
}


- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler {
    NSLog( @"Handle push from foreground" );
    // custom code to handle push while app is in the foreground
    NSLog(@"%@", notification.request.content.userInfo);
    
    NSLog(@"Push received appdelegate");
    NSDictionary *userInfo = notification.request.content.userInfo;
    if (userInfo == nil) {
        // [self contentComplete];
        return;
    }
    
    NSString *customID = userInfo[@"custom-payload-id"];
    [UNUserNotificationCenter.currentNotificationCenter getDeliveredNotificationsWithCompletionHandler:^(NSArray<UNNotification *> * _Nonnull notifications) {
        
        UNNotification *notifarray = notifications.firstObject;
        NSDictionary *existingUserInfo = notifarray.request.content.userInfo;
        NSString *matching = existingUserInfo[@"custom-payload-id"];
        if([matching isEqualToString:customID]){
            NSLog(@"Found");
            NSArray *notifArray = [[NSArray alloc]initWithObjects:notifarray.request.identifier, nil];
            [UNUserNotificationCenter.currentNotificationCenter removeDeliveredNotificationsWithIdentifiers:notifArray];
        }
    }];
    completionHandler(UNNotificationPresentationOptionAlert);
}

- (void)didReceiveNotificationRequest:(UNNotificationRequest *)request withContentHandler:(void (^)(UNNotificationContent * _Nonnull))contentHandler {
    NSLog( @"Handle didReceiveNotificationRequest" );
    self.contentHandler = contentHandler;
    self.bestAttemptContent = [request.content mutableCopy];
    
    NSLog(@"Push received appdelegate");
    NSDictionary *userInfo = request.content.userInfo;
    if (userInfo == nil) {
       // [self contentComplete];
        return;
    }
    
    NSString *customID = userInfo[@"custom-payload-id"];
    [UNUserNotificationCenter.currentNotificationCenter getDeliveredNotificationsWithCompletionHandler:^(NSArray<UNNotification *> * _Nonnull notifications) {
        
        UNNotification *notifarray = notifications.firstObject;
        NSDictionary *existingUserInfo = notifarray.request.content.userInfo;
        NSString *matching = existingUserInfo[@"custom-payload-id"];
        if([matching isEqualToString:customID]){
            NSArray *notifArray = [[NSArray alloc]initWithObjects:notifarray.request.identifier, nil];
            [UNUserNotificationCenter.currentNotificationCenter removeDeliveredNotificationsWithIdentifiers:notifArray];
        }
    }];
}

-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    NSLog( @"Handle didReceiveNotificationResponse You are using your car" );
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response          withCompletionHandler:(void (^)(void))completionHandler{
    
    NSLog( @"Handle didReceiveNotificationResponse You are using your car" );
    if ([response.actionIdentifier isEqualToString:NotificationActionYesIdent])
    {
        NSLog(@"You are using your car.");
    }else if ([response.actionIdentifier isEqualToString:NotificationActionNoIdent])
    {
        NSLog(@"You are not using your car.");
        [self callCarStatusService];
    }else if ([response.notification.request.content.body.description isEqualToString:NSLocalizedString(@"Tracking_ended", @"")]){
        NSLog(@"Local notification.");
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        BeaconEditTripViewController *ivc = [storyboard instantiateViewControllerWithIdentifier:@"BeaconEditTrip"];
        UINavigationController *navigationController=[[UINavigationController alloc] initWithRootViewController:ivc];
        self.window.backgroundColor = [UIColor whiteColor];
        self.window.rootViewController =nil;
        self.window.rootViewController = navigationController;
        [self.window makeKeyAndVisible];
    }else {
        NSDictionary *userInfo = response.notification.request.content.userInfo;
        NSDictionary *aps = [userInfo valueForKey:@"aps"];
        if ([[aps valueForKey:@"category"] isEqualToString:@"ACTIONABLE"]) {
            NSString *title = [aps valueForKey:@"alert"];
            NSString *cat = [aps valueForKey:@"category"];
            
            UIAlertController * alert = [UIAlertController alertControllerWithTitle:nil message:title preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                //Handle your yes please button action here
                NSLog(@"You are using your car.");
                [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
                [[UIApplication sharedApplication] cancelAllLocalNotifications];
                [[UNUserNotificationCenter currentNotificationCenter] removeAllDeliveredNotifications];
                [alert dismissViewControllerAnimated:YES completion:nil];
                [self callAgainAfter1houre:title cat:cat];
            }];
            
            UIAlertAction* noButton = [UIAlertAction actionWithTitle:@"Alarm" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {                                            NSLog(@"You are not using your car.");
                [self callCarStatusService];
                [alert dismissViewControllerAnimated:YES completion:nil];
            }];
            
            [alert addAction:yesButton];
            [alert addAction:noButton];
            
            [self performSelector:@selector(showAlert:) withObject:alert afterDelay:1.0];
            
        }
        
    }
    if (completionHandler) {
        completionHandler();
    }
    NSLog(@"didReceiveNotificationResponse = %@",response.notification.request.content.userInfo);
}

-(void)callAgainAfter1houre:(NSString *)MSG cat:(NSString *)cat
{
  
    UNMutableNotificationContent *objNotificationContent = [[UNMutableNotificationContent alloc] init];
    
    objNotificationContent.title = [NSString localizedUserNotificationStringForKey:@"" arguments:nil];
   // NSString *msg = @"Din bil er i bevægelse uden dig. Hvis bilen bliver benyttet af dig, familien eller værksted, så tryk OK, hvis ikke så tryk Alarm for at alarmere centralen omkring eventuelt tyveri.";
    objNotificationContent.body = [NSString localizedUserNotificationStringForKey:MSG arguments:nil];
    
    objNotificationContent.sound = [UNNotificationSound defaultSound];
    [objNotificationContent setCategoryIdentifier:@"ACTIONABLE"];
    // 4. update application icon badge number
    objNotificationContent.badge = @([[UIApplication sharedApplication] applicationIconBadgeNumber] + 1);
    
    NSDictionary *infoDict1 = @{@"category" : cat,
                                @"alert" : MSG
                                };
    NSDictionary *infoDict = @{@"aps" : infoDict1};
    objNotificationContent.userInfo = infoDict;
    // Deliver the notification in five seconds.
    UNTimeIntervalNotificationTrigger *trigger =  [UNTimeIntervalNotificationTrigger  triggerWithTimeInterval:3600.f repeats:NO];
    
    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:@"ten"                                                                            content:objNotificationContent trigger:trigger];
    
    // 3. schedule localNotification
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    
    [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
        if (!error) {
            NSLog(@"Local Notification succeeded");
        } else {
            NSLog(@"Local Notification failed");
        }
    }];
}
/*- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler{
    
    
    NSLog( @"Handle didReceiveNotificationResponse You are using your car" );
    if ([response.actionIdentifier isEqualToString:NotificationActionYesIdent]) {
        NSLog(@"You are using your car.");
    }else if ([response.actionIdentifier isEqualToString:NotificationActionNoIdent]) {
        NSLog(@"You are not using your car.");
        [self callCarStatusService];
    }else if ([response.notification.request.content.body.description isEqualToString:NSLocalizedString(@"Tracking_ended", @"")]){
        NSLog(@"Local notification.");
        
        //[response.notification.request.content.body.description isEqualToString:NSLocalizedString(@"Tracking_started", @"")] ||
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        BeaconEditTripViewController *ivc = [storyboard instantiateViewControllerWithIdentifier:@"BeaconEditTrip"];
        UINavigationController *navigationController=[[UINavigationController alloc] initWithRootViewController:ivc];
        self.window.backgroundColor = [UIColor whiteColor];
        self.window.rootViewController =nil;
        self.window.rootViewController = navigationController;
        [self.window makeKeyAndVisible];
    }else {
        NSDictionary *userInfo = response.notification.request.content.userInfo;
        NSDictionary *aps = [userInfo valueForKey:@"aps"];
        if ([[aps valueForKey:@"category"] isEqualToString:@"ACTIONABLE"]) {
            NSString *title = [aps valueForKey:@"alert"];
            
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:nil
                                         message:title
                                         preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"Yes"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
                                            //Handle your yes please button action here
                                            NSLog(@"You are using your car.");
                                            [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
                                            [[UIApplication sharedApplication] cancelAllLocalNotifications];
                                            [[UNUserNotificationCenter currentNotificationCenter] removeAllDeliveredNotifications];
                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                        }];
            
            UIAlertAction* noButton = [UIAlertAction
                                       actionWithTitle:@"No"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           //Handle no, thanks button
                                           NSLog(@"You are not using your car.");
                                           [self callCarStatusService];
                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                       }];
            
            [alert addAction:yesButton];
            [alert addAction:noButton];
            UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
            [vc presentViewController:alert animated:YES completion:nil];
            
//             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:title delegate:self cancelButtonTitle: NSLocalizedString(@"Yes",@"Yes") otherButtonTitles:NSLocalizedString(@"No",@"No"),nil, nil];
//             alert.tag = 0123;
//             [alert show];
        }
        
    }
    if (completionHandler) {
        completionHandler();
    }
    NSLog(@"didReceiveNotificationResponse = %@",response.notification.request.content.userInfo);
}*/


-(void)gotoHome{
    NSLog(@"gotoHome");
    NSString *storyBoardIdentifier = @"initialslide";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *newViewController = [storyboard instantiateViewControllerWithIdentifier:storyBoardIdentifier];
    UINavigationController *navigationController=[[UINavigationController alloc] initWithRootViewController:newViewController];
    [[NSUserDefaults standardUserDefaults] setObject:storyBoardIdentifier forKey:@"storyBoardIdentifier"];
    navigationController.navigationBar.hidden = true;
    self.window.backgroundColor = [UIColor whiteColor];
    self.window.rootViewController =nil;
    self.window.rootViewController = navigationController;
    [self.window makeKeyAndVisible];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
   // [[FIRMessaging messaging] disconnect];
    //NSLog(@"Disconnected from FCM");
    [Helper setPREFStringValue:@"1" sKey:@"AppBackground"];
    _isTripContinue = [Helper getPREF:@"isTripContinue"];
    
    NSLog(@"Application is entered into background");
    if ([_isTripContinue isEqualToString:@"1"]) {
        NSLog(@"Start significant location manager");
        //[locationManager stopUpdatingLocation];
        
        _anotherLocationManager = [[CLLocationManager alloc] init];
    #ifdef __IPHONE_8_0
        if(IS_OS_8_OR_LATER) {
            [_anotherLocationManager requestWhenInUseAuthorization];
            [_anotherLocationManager requestAlwaysAuthorization];
        }
    #endif
        [_anotherLocationManager startMonitoringSignificantLocationChanges];
        _anotherLocationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
        _anotherLocationManager.delegate = self;
        _anotherLocationManager.allowsBackgroundLocationUpdates = TRUE;
        _anotherLocationManager.pausesLocationUpdatesAutomatically = FALSE;
        _anotherLocationManager.distanceFilter = kCLDistanceFilterNone;
    }
    
   // [self callAgainAfter1houre];
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    NSLog(@"applicationWillEnterForeground");
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    NSLog(@"applicationDidBecomeActive");
    if ([_isTripContinue isEqualToString:@"1"]) {
        NSLog(@"stop significant location manager");
        [locationManager startUpdatingLocation];
        //[locationManager stopMonitoringSignificantLocationChanges];
        [_anotherLocationManager stopMonitoringSignificantLocationChanges];
    }
}

- (void)applicationWillTerminate:(UIApplication *)application {
     NSLog(@"applicationWillTerminate");
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    _isTripContinue = [def valueForKey:@"isTripContinue"];
    if ([_isTripContinue isEqualToString:@"1"]) {
        [def setValue:@"1" forKey:@"AppTerminate"];
        NSLog(@"Application is Terminated and saved state");
    }else{
        NSLog(@"Application is Terminated");
    }
    [def synchronize];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"is_menushow"];
}

#pragma mark - beacon manager init function
-(void)beaconDetection{
    NSLog(@"beaconDetection");
    _isBeaconConnected = [Helper getPREF:@"isBeaconConnected"];
    if ([_isBeaconConnected isEqualToString:@"1"]) {
        NSLog(@"Beacon confiugured");
        [self initilizeBecaon];
    }else{
        NSLog(@"Beacon not confiugured");
    }
}

- (void)initilizeBecaon{
    
    NSLog(@"initilizeBecaon");
    KTKbeaconManager = [[KTKBeaconManager alloc] initWithDelegate:self];
    if ([KTKbeaconManager respondsToSelector:@selector(requestLocationAlwaysAuthorization)]) {
        [KTKbeaconManager requestLocationAlwaysAuthorization];
    }else{
        [KTKbeaconManager requestLocationAlwaysAuthorization];
        [KTKbeaconManager requestLocationWhenInUseAuthorization];
    }
    
    // Get list of beacon connected and start monitoring.
    _BeaconArray = [[Helper getUserValue:@"beaconsArray"] mutableCopy];
    //NSLog(@"_beacon Array : %@",_BeaconArray);
    //[KTKbeaconManager stopMonitoringForAllRegions];
    NSLog(@"stopMonitoringForAllRegions");
    for (int i =0; i < _BeaconArray.count; i++) {
        NSDictionary *beacon = [_BeaconArray objectAtIndex:i];
        //NSMutableDictionary *dataCopy = [beacon mutableCopy];
        
        NSUUID *proximityUUID = [[NSUUID alloc] initWithUUIDString:[beacon valueForKey:@"proximityUUID"]];
        _BeaconProximityUUID = [beacon valueForKey:@"proximityUUID"];
        _BeaconMajor = [beacon valueForKey:@"major"];
        _BeaconMinor = [beacon valueForKey:@"minor"];
        
        KTKBeaconRegion *region = [[KTKBeaconRegion alloc]initWithProximityUUID:proximityUUID major:[_BeaconMajor integerValue] minor:[_BeaconMinor integerValue] identifier:[NSString stringWithFormat:@"%d",i+1]];
        region.notifyOnEntry = true;
        region.notifyOnExit = true;
        
        region.notifyEntryStateOnDisplay = true;
        [KTKbeaconManager startMonitoringForRegion:region];
        NSLog(@"startMonitoringForRegion");
    }
}

-(void)stopBeaconDetection{
     NSLog(@"stopBeaconDetection");
    [KTKbeaconManager stopMonitoringForAllRegions];
}

- (void)beaconManager:(KTKBeaconManager*)manager didDetermineState:(CLRegionState)state forRegion:(__kindof KTKBeaconRegion*)regions
{
    //set connected beacon identifier
    NSLog(@"didDetermineState");
    ConnectedbeaconRegion = regions.identifier;
    NSLog(@"Region : %@",regions);
    
    _currentStage = (NSInteger)[Helper getPREFNSint:@"currentStage"];
    NSLog(@"%d",(int)state);
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"BeaconConnection_Formsg" object:nil userInfo:nil];
    
       if (state == CLRegionStateInside) {
            NSLog(@"Detected : CLRegionStateInside");
            _currentStage = state;
            [Helper setPREFint:(int)_currentStage :@"currentStage"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"BeaconConnection_Formsg" object:nil userInfo:nil];
            if (appTimer == nil) {
                
               if (![_exitTimer isValid]) {
                   
                   if(![self checkAutomaticTripIsContinue]) {
                       NSLog(@"Enter region %@", regions);
                       
                       // Fired notification when beacon enter into region.
                       if ([_appTrackNotification isEqualToString:@"1"]) {
                           /*UILocalNotification *notification = [UILocalNotification new];
                           notification.alertBody = NSLocalizedString(@"Tracking_started", @"");
                           [[UIApplication sharedApplication] presentLocalNotificationNow:notification];*/
                           [self displayLocalNotification:NSLocalizedString(@"Tracking_started", @"")];
                       }else{
                           NSLog(@"Track notification stop from general setting");
                       }
                       
                       // For
                       finalTrack = @"1";
                       [[HomeViewController sharedMySingleton] viewDidLoadViewWillAppear];
                       [[NSNotificationCenter defaultCenter] postNotificationName:@"BeaconConnection" object:nil userInfo:nil];
                       NSLog(@"****************");
                       ConnectionStatus = @"1";
                       [[NSNotificationCenter defaultCenter] postNotificationName:@"BeaconConnectionService" object:nil userInfo:nil];
                   }
                  
                   //[self callTrackingUpdateService:@"0"];
               }
            }else{
                NSLog(@"App timer already started");
            }
        }else if (state == CLRegionStateOutside) {
            
            
            if ((locationManager.location.speed * 3.6) > 5){
                NSLog(@"Moving");
            }else{
                NSLog(@"Not Moving");
            }
            
            NSLog(@"Detected : CLRegionStateOutside");
            _currentStage = state;
            [Helper setPREFint:_currentStage :@"currentStage"];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"BeaconConnection_Formsg" object:nil userInfo:nil];
           
            
            if([[Helper getPREF:@"type"] isEqualToString:@"0"]) {
                if (appTimer != nil){
                     [[NSNotificationCenter defaultCenter] postNotificationName:@"BeaconConnection_Formsg" object:nil userInfo:nil];
                    NSLog(@"Beacon disconnected but waitign for %d second. if beacon will reconnect.",popupWaitTime);
                    
                    UIApplicationState *stat  = (UIApplicationState *)[UIApplication sharedApplication].applicationState;
                    if (stat == (UIApplicationState *)UIApplicationStateBackground) {
                        
                        
                        // Register auto expiring background task
                        bgTaskForPopup = [_app beginBackgroundTaskWithExpirationHandler:^{
                            [_app endBackgroundTask:bgTaskForPopup];
                            bgTaskForPopup = UIBackgroundTaskInvalid;
                        }];
                        
                        // Execute background task on the main thread
                        dispatch_async( dispatch_get_main_queue(), ^{
                            
                            NSLog(@"In background timer");
                            popupTimer = [NSTimer scheduledTimerWithTimeInterval:popupWaitTime target:self selector:@selector(showMessageAfterDisconectBeacon) userInfo:nil repeats:NO];
                            [[NSRunLoop mainRunLoop] addTimer:popupTimer forMode:NSDefaultRunLoopMode];
                        });
                    }else{
                        popupTimer = [NSTimer scheduledTimerWithTimeInterval:popupWaitTime target:self selector:@selector(showMessageAfterDisconectBeacon) userInfo:nil repeats:NO];
                        [[NSRunLoop mainRunLoop] addTimer:popupTimer forMode:NSDefaultRunLoopMode];
                    }
                    //[self performSelector:@selector(showMessageAfterDisconectBeacon) withObject:nil afterDelay:popupWaitTime];
                }
            }
            NSLog(@"****************");
        }else if (state == CLRegionStateUnknown) {
            
            NSLog(@"Detected : CLRegionStateUnknown");
            //_BeaconStage = @"0";
            _currentStage = state;
            [Helper setPREFint:(int)_currentStage :@"currentStage"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"BeaconConnection_Formsg" object:nil userInfo:nil];
        }else{
            NSLog(@"");
        }
        _currentStage = state;
        [Helper setPREFint:(int)_currentStage :@"currentStage"];
    //}
}

-(void)checkCurrentStatus{
    NSLog(@"checkCurrentStatus");
    UIApplicationState *stat  = (UIApplicationState *)[UIApplication sharedApplication].applicationState;
    _currentStage = (NSInteger)[Helper getPREFNSint:@"currentStage"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"BeaconConnection_Formsg" object:nil userInfo:nil];
    if (stat == (UIApplicationState *)UIApplicationStateBackground) {
        
        // Register auto expiring background task
        bgTaskId = [_app beginBackgroundTaskWithExpirationHandler:^{
            [_app endBackgroundTask:bgTaskId];
            bgTaskId = UIBackgroundTaskInvalid;
        }];
        
        // Execute background task on the main thread
        dispatch_async( dispatch_get_main_queue(), ^{
            // ------------------
            // DO SOMETHING INVOLVING THE WEBVIEW - WHICH MUST OCCUR ON THE MAIN THREAD!
            if (_currentStage == CLRegionStateOutside) {
                if (!_exitTimer) {
                    //_exitTimer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(counter) userInfo:nil repeats:YES];
                    _exitTimer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(continueTripForSpecificTimeAfterDisconnectBeacon) userInfo:nil repeats:YES];
                    [[NSRunLoop mainRunLoop] addTimer:_exitTimer forMode:NSRunLoopCommonModes];
                }
            }   // ------------------
        });
    }else{
        if (_currentStage == CLRegionStateOutside) {
            if (!_exitTimer) {
                _exitTimer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(continueTripForSpecificTimeAfterDisconnectBeacon) userInfo:nil repeats:YES];
                [[NSRunLoop mainRunLoop] addTimer:_exitTimer forMode:NSRunLoopCommonModes];
            }
        }
    }
}

-(void)checkUserIntactionStatus{
    NSLog(@"checkUserIntactionStatus called");
    UIApplicationState *stat  = (UIApplicationState *)[UIApplication sharedApplication].applicationState;
    _currentStage = (NSInteger)[Helper getPREFNSint:@"currentStage"];
    if (stat == (UIApplicationState *)UIApplicationStateBackground) {
        NSLog(@"app is in background");
        // Register auto expiring background task
        bgTaskId = [_app beginBackgroundTaskWithExpirationHandler:^{
            [_app endBackgroundTask:bgTaskId];
            bgTaskId = UIBackgroundTaskInvalid;
        }];
        
        // Execute background task on the main thread
        dispatch_async( dispatch_get_main_queue(), ^{
            // ------------------
            // DO SOMETHING INVOLVING THE WEBVIEW - WHICH MUST OCCUR ON THE MAIN THREAD!
            if (_currentStage == CLRegionStateOutside) {
                if (!_exitTimer) {
                    NSLog(@"Exit timer started");
                    //_exitTimer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(counter) userInfo:nil repeats:YES];
                    _exitTimer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(userInteractionAtBeaconDisconnect) userInfo:nil repeats:YES];
                    [[NSRunLoop mainRunLoop] addTimer:_exitTimer forMode:NSRunLoopCommonModes];
                }
            }   // ------------------
        });
    }else{
        NSLog(@"app is in foreground");
        if (_currentStage == CLRegionStateOutside) {
            
            if (!_exitTimer) {
                NSLog(@"Exit timer started");
                _exitTimer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(userInteractionAtBeaconDisconnect) userInfo:nil repeats:YES];
                [[NSRunLoop mainRunLoop] addTimer:_exitTimer forMode:NSRunLoopCommonModes];
            }
        }
    }
}

/*-(void)counter{
     NSLog(@"counter");
    NSLog(@"Exit timer :%d",_exitTimerCount);
    
    _exitTimerCount = _exitTimerCount + 1;
    if (_exitTimerCount > 59) {
        _exitTimerCount = 0;
        
        _currentStage = (NSInteger)[Helper getPREFNSint:@"currentStage"];
        if (_currentStage == CLRegionStateOutside) {
            
            //NSLog(@"Exit region %@", regions);
            NSLog(@"Exit region");
            
            if ([_appTrackNotification isEqualToString:@"1"]) {
                UILocalNotification *notification = [UILocalNotification new];
                notification.alertBody = NSLocalizedString(@"Tracking_ended", @"");
                [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
            }else{
                NSLog(@"Track notification stop from general setting");
            }
            // Fired notification when beacon not inside region.
            
            NSLog(@"Timer stoped beacon is not in range");
            // Stop timer
            [_exitTimer invalidate];
            _exitTimer = nil;
            
            // finally timer exited
            finalTrack = @"0";
            [[HomeViewController sharedMySingleton] viewDidLoadViewWillAppear];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"BeaconDisConnection" object:nil userInfo:nil];
            
            ConnectionStatus = @"2";
            [[NSNotificationCenter defaultCenter] postNotificationName:@"BeaconDisConnectionService" object:nil userInfo:nil];
            
            [_app endBackgroundTask:bgTaskId];
            bgTaskId = UIBackgroundTaskInvalid;
            
            [_app endBackgroundTask:bgTaskForPopup];
            bgTaskForPopup = UIBackgroundTaskInvalid;
            
            //[self callTrackingUpdateService:@"1"];
            
        }else{
            NSLog(@"Timer continue beacon is in range");
            [_exitTimer invalidate];
            _exitTimer = nil;
            
 }
 }
}*/

- (void)beaconManager:(KTKBeaconManager*)manager didEnterRegion:(__kindof KTKBeaconRegion*)regions {
    NSLog(@"Enter region %@", regions);
    
    if ([_appTrackNotification isEqualToString:@"1"]) {
        [self displayLocalNotification:NSLocalizedString(@"Tracking_started", @"")];
    }
}

- (void)beaconManager:(KTKBeaconManager*)manager didExitRegion:(__kindof KTKBeaconRegion*)regions {
    NSLog(@"Exit region %@", regions);
}

#pragma mark - Network related function
-(void)statusChange
{
      NSLog(@"statusChange");
    if(isAppStartWithoutNet) {
        if (internetActive == YES && _hostActive == YES) {
            isAppStartWithoutNet = FALSE;
        } else {
            if(!isalert){
                isalert = TRUE;
            }
        }
    }else{
        if (internetActive == YES && _hostActive == YES){
            isAppStartWithoutNet = FALSE;
        } else {
            if(!isalert){
                isalert = TRUE;
            }
        }
    }
}

#pragma mark - Webservice call for update application data
-(void)callGetCredentialsService:(NSString *)uID{
    
    NSString *userID = uID;
    NSLog(@"callGetCredentialsService");
    
    if ([self connected]) {
        NSDictionary *parameter=@{@"userid":uID};
        UIWindow *window = [UIApplication sharedApplication].windows.lastObject;
        [MBProgressHUD showHUDAddedTo:window animated:true];
        
        [WebService httpPostWithCustomDelegateURLString:@".credentials" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error) {
            [MBProgressHUD hideHUDForView:window animated:true];
            if (error == nil)
            {
                NSError *localError = nil;
                NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
                
                if (localError != nil) {
                    NSLog(@"%@",localError.description);
                    
                }else
                {
                    NSDictionary *response = [parsedObject objectForKey:@"data"];
                    NSLog(@"GetCredential response : %@",response);
                    NSString *status = [response valueForKey:@"status"];
                    if ([status isEqualToString:@"Success"]) {
                        
                        [Helper setUserValue:[response valueForKey:epay_info] sKey:epay_info];
                        // Save dantracker info
                        [Helper setUserValue:[response valueForKey:dantracker] sKey:dantracker];
                        
                        if ([appLogin isEqualToString:@"1"]) {
                            
                            // Save user value
                            NSDictionary* user = [response objectForKey:@"user"];
                            NSString *email = [user valueForKey:@"email"];
                            NSString *name = [user valueForKey:@"name"];
                            NSString *uID = [user valueForKey:@"id"];
                            
                            NSString* parent = [Helper isStringIsNull:[response objectForKey:@"parent"]];
                            if (parent == (NSString *)[NSNull class]) {
                                [Helper setPREFStringValue:@"0" sKey:@"parent"];
                            }else{
                                [Helper setPREFStringValue:parent sKey:@"parent"];
                            }
                            
                            NSString* registerDate = [user objectForKey:@"registerDate"];
                            //NSString* sid = [response objectForKey:@"sid"];
                            NSString* userTyp = [response objectForKey:@"user_type"];
                            tracker_id = [Helper isStringIsNull:[response objectForKey:@"tracker_id"]];
                            
                           /* if (uID == (id)[NSNull null]) {
                                [Helper setPREFStringValue:@"0" sKey:@"userID"];
                            }else{
                                [Helper setPREFStringValue:uID sKey:@"userID"];
                            }
                            if (name == (id)[NSNull null]) {
                                [Helper setPREFStringValue:@"" sKey:@"userName"];
                            }else{
                                [Helper setPREFStringValue:name sKey:@"userName"];
                            }
                            if (email == (id)[NSNull null]) {
                                [Helper setPREFStringValue:@"" sKey:@"userEmail"];
                            }else{
                                [Helper setPREFStringValue:email sKey:@"userEmail"];
                            }
                            if (userTyp == (id)[NSNull null]) {
                                [Helper setPREFStringValue:@"" sKey:@"user_type"];
                            }else{
                                [Helper setPREFStringValue:userTyp sKey:@"user_type"];
                            }
                            if (registerDate == (id)[NSNull null]) {
                                [Helper setPREFStringValue:@"" sKey:@"userRegisterDate"];
                            }else{
                                [Helper setPREFStringValue:registerDate sKey:@"userRegisterDate"];
                            }
                            if (tracker_id == (id)[NSNull null]) {
                                [Helper setPREFStringValue:@"" sKey:@"tracker_id"];
                            }else{
                                [Helper setPREFStringValue:tracker_id sKey:@"tracker_id"];
                            }*/
                            
                            //[Helper setPREFStringValue:sid sKey:@"sid"];
                            [Helper setPREFStringValue:name sKey:@"userName"];
                            [Helper setPREFStringValue:email sKey:@"userEmail"];
                            [Helper setPREFStringValue:userTyp sKey:@"user_type"];
                            [Helper setPREFStringValue:registerDate sKey:@"userRegisterDate"];
                            [Helper setPREFStringValue:tracker_id sKey:@"tracker_id"];
                            
                            // Save Beacon Info
                            _BeaconArray = [[NSMutableArray alloc]init];
                            _BeaconArray = [response objectForKey:@"beep"];
                            [Helper setUserArrayValue:_BeaconArray sKey:@"beaconsArray"];
                            
                            //NSLog(@"_BeaconArray : %@",_BeaconArray);
                            if (_BeaconArray.count > 0) {
                                [Helper setPREFStringValue:@"1" sKey:@"isBeaconConnected"];
                                [self beaconDetection];
                            }
                            
                           /* "general_setting" =     {
                                currency = DKK;
                                id = 420;
                                language = "da-DK";
                                notification = 1;
                                "trip_notification" = 1;
                                unit = km;
                            };*/
                            
                            // Save general settings
                            NSDictionary *generalSetting33 = [response objectForKey:@"general_setting"];
                            NSMutableDictionary *generalSetting2 = [[NSMutableDictionary alloc]init];
                            NSArray *allk = [generalSetting33 allKeys];
                            for (int i=0; i<allk.count; i++) {
                                NSString *st = [allk objectAtIndex:i];
                                NSString *val =[generalSetting33 valueForKey:st];
                                if (val == (NSString *)[NSNull null] || val == nil) {
                                    if([st isEqualToString:@"notification"])
                                    {
                                        [generalSetting2 setObject:@"0" forKey:st];
                                    }else if([st isEqualToString:@"trip_notification"])
                                    {
                                        [generalSetting2 setObject:@"0" forKey:st];
                                    }else{
                                        [generalSetting2 setObject:@"" forKey:st];
                                    }
                                    
                                }else{
                                    [generalSetting2 setObject:val forKey:st];
                                }
                            }
                           
                           
                            _appTrackNotification = [generalSetting2 valueForKey:@"trip_notification"];
                            [Helper setPREFID:_appTrackNotification :@"trip_notification"];
                            
                            [Helper setUserValue:generalSetting2 sKey:@"general_setting"];
                            [Helper setDataValue:data sKey:@"userInfo"];
                            
                            _appLanguage = [generalSetting2 valueForKey:@"language"];
                            [Helper setPREFID:_appLanguage :@"language"];
                            [self setApplicationLanguage];
                            
                            //NSLog(@"user info : %@",user);
                            
                            //  Save Payment Info
                            NSDictionary *payment_info = [response objectForKey:@"payment_info"];
                            NSString *status = [Helper isStringIsNull:[payment_info valueForKey:@"status"]];
                            NSLog(@"payment_status : %@",status);
                            [Helper setPREFStringValue:status sKey:@"userStatus"];
                            
                            [self callLocationService_withTips:uID from:@""];
                            //[self callLocationService:uID from:@""];
                        }
                    }
                }
            }else{
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"request_time_out", @"") preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *retry = [UIAlertAction actionWithTitle:NSLocalizedString(@"Retry", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                    [self callGetCredentialsService:userID];
                }];
                UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                    [alert dismissViewControllerAnimated:true completion:nil];
                }];
                [alert addAction:retry];
                [alert addAction:cancel];
                [self showAlert:alert];
            }
        }];
    }else{
        /*UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"err_network_not_available",@"") preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"Ok",@"Ok") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alert addAction:ok];
        [self performSelector:@selector(showAlert:) withObject:alert afterDelay:1.0];
        return;*/
    }
}


-(long )getLast3Month:(NSDate *)date
{
    NSCalendar *c = [NSCalendar currentCalendar];
    
    
    NSRange days = [c rangeOfUnit:NSCalendarUnitDay
                           inUnit:NSCalendarUnitMonth
                          forDate:date];
    
    NSDate *earlier = [[NSDate alloc]initWithTimeIntervalSinceReferenceDate:1];
    NSDate *today = date;
    
    NSCalendar *gregorian = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
    
    // pass as many or as little units as you like here, separated by pipes
    NSUInteger units = NSCalendarUnitYear | NSCalendarUnitDay | NSCalendarUnitMonth;
    
    NSDateComponents *components = [gregorian components:units fromDate:earlier toDate:today options:0];
    
    NSInteger months = [components month];
    NSInteger daysd = [components day];
    long cur = (long)days.length - (long)daysd;
    
    
   NSInteger months2 =  [c component:NSCalendarUnitMonth fromDate:today];
    NSInteger day =  [c component:NSCalendarUnitDay fromDate:today];
    
  
    
   // NSLog(@"Left Days %ld = %ld - %ld",cur,(long)days.length,(long)daysd);
    return  cur;
    
    
    
    
}

-(NSString *)getMonthYearDay:(NSDate*)date
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];
    
    NSString *strMonth;
    int month = (int)[components month];
    if(month < 10)
    {
        strMonth = [NSString stringWithFormat:@"0%d",month];
    }else{
        strMonth = [NSString stringWithFormat:@"%d",month];
    }
    NSString *strYear = [NSString stringWithFormat:@"%d",(int)[components year]];
    NSString *dateStr = [NSString stringWithFormat:@"%@-%@",strYear,strMonth];
    return dateStr;
}
#pragma mark - webservice call
-(void)callLocationService_withTips:(NSString *)userID from:(NSString *)from{
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //Background Thread
        dispatch_async(dispatch_get_main_queue(), ^(void){
            //Run UI Updates
            Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
            NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
            if (networkStatus == NotReachable) {
                NSLog(@"There IS NO internet connection");
                [Helper displayAlertView:@"" message:NSLocalizedString(@"err_network_not_available",nil)];
            } else
            {
                //NSString *month = [Helper getPreviousMonth];
                NSString *monthCur = [Helper getCurMonth];
                NSString *userID = [Helper getPREF:@"userID"];
                NSString *tripDate = [self getMonthYearDay:[NSDate date]];
                NSString *user_type = [Helper getPREF:@"user_type"];
                
                //NSString *tripDate = monthCur; //[self getMonthYearDay:[NSDate date]];
                //NSString *sid = [Helper getPREF:@"sid"];
                NSDictionary *parameter=@{
                                          @"userid":userID,
                                          @"trip_type":@"0",
                                          @"t_date":tripDate,
                                          @"limitstart":@"0"
                                          };
                //NSLog(@"parameter : %@",parameter);
                
                [WebService httpPostWithCustomDelegateURLString:@".gettrips" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error)
                 {
                     int trips_count = 0;
                     if (error == nil) {
                         NSError *localError = nil;
                         NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
                         
                         if (localError != nil) {
                             NSLog(@"%@",localError.description);
                         }
                         else {
                             //NSDictionary *response = [parsedObject objectForKey:@"data"];
                             NSLog(@"response : %@",parsedObject);
                             id response = [parsedObject objectForKey:@"data"];
                             NSMutableArray *tempTrips = [[NSMutableArray alloc]init];
                             tempTrips  = [response objectForKey:@"trips"];
                             NSLog(@"Trip Count : %d",(int)tempTrips.count);
                             trips_count = tempTrips.count;
                         }
                     }
                     [self callLocationService:userID from:from undefine_trips_count:trips_count];
                     
                     
                 }];
            }
        });
    });
}


// Get all location of user and odometer value for start trip
-(void)callLocationService:(NSString *)userID from:(NSString *)from undefine_trips_count:(int)undefine_trips_count{
    
    NSString *uid = userID;
    NSLog(@"callLocationService");
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //Background Thread
        dispatch_async(dispatch_get_main_queue(), ^(void){
            //Run UI Updates
            Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
            NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
            if (networkStatus == NotReachable) {
                NSLog(@"There IS NO internet connection");
                [Helper displayAlertView:@"" message:NSLocalizedString(@"err_network_not_available",nil)];
            } else
            {
                NSDictionary *parameter=@{ @"userid":userID };
                [WebService httpPostWithCustomDelegateURLString:@".getLocations" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error) {
                    if (error == nil) {
                        NSError *localError = nil;
                        NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
                        if (localError != nil) {
                            NSLog(@"%@",localError.description);
                        } else {
                            id response = [parsedObject objectForKey:@"data"];
                            //NSLog(@"response : %@",response);
                            NSString *odometer_start = [Helper isStringIsNull:[response valueForKey:@"odometer_start"]];
                            [Helper setPREFStringValue:odometer_start sKey:@"odometer_start"];
                            
                            NSString *tripcount = [Helper isStringIsNull:[response valueForKey:@"tripcount"]];
                            if (tripcount == (NSString *)[NSNull null] || tripcount.length <= 0) {
                                tripcount = @"0";
                            }
                            [Helper setPREFStringValue:tripcount sKey:@"tripcount"];   
                            
                            NSArray *LocationArray = [[NSMutableArray alloc]init];
                            LocationArray  = [response objectForKey:@"locations"];
                            
                            if (LocationArray.count > 0) {
                               [Helper setUserArrayValue:LocationArray sKey:@"locationArray"];
                            }
                            NSString *month = [Helper getPreviousMonth];
                            NSString *monthCur = [Helper getCurMonth];
                            NSString *notif = [NSString stringWithFormat:@"%@", [response valueForKey:@"notification"]];;
                            long days = [Helper get3daysMonth];
                            //long days = [self getLast3Month:[NSDate date]];
                            
                            NSLog(@"undefine_trips_count : %d",undefine_trips_count);
                            if ([notif isEqualToString:@"1"]) {
                                NSString *user_type = [Helper getPREF:@"user_type"];
                                 // show popup of approv trip and expense
                                if ([user_type isEqualToString:@"11"]) {
                                    /*UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:[NSString stringWithFormat:NSLocalizedString(@"approv_msg", @""),month] preferredStyle:UIAlertControllerStyleAlert];
                                    
                                    UIAlertAction *approv = [UIAlertAction actionWithTitle:NSLocalizedString(@"click_here", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                        [self callApprovTripAndExpense:uid];
                                    }];
                                    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                        [alert dismissViewControllerAnimated:true completion:nil];
                                    }];
                                    [alert addAction:approv];
                                    [alert addAction:cancel];
                                    [self performSelector:@selector(showAlert:) withObject:alert afterDelay:0.5];*/
                                    
                                    if([from isEqualToString:@""])
                                    {
                                        
                                        
                                    }else{
                                        
                                       
                                        if(days <= 3 )
                                        {
                                            if([tripcount intValue] < 1 || undefine_trips_count > 0){
                                                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:[NSString stringWithFormat:NSLocalizedString(@"popupA", @""),monthCur] preferredStyle:UIAlertControllerStyleAlert];
                                                
                                                UIAlertAction *retry = [UIAlertAction actionWithTitle:NSLocalizedString(@"Trips", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                    //[self callTripService:type date:date limit:lmt];
                                                    /* UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                                     TripViewController* controller = [storyboard instantiateViewControllerWithIdentifier:@"TripPage"];
                                                     [self.navigationController pushViewController:controller animated:YES];*/
                                                    
                                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoTripsFromHome" object:nil userInfo:nil];
                                                    
                                                    
                                                    
                                                }];
                                                
                                                UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                    [alert dismissViewControllerAnimated:true completion:nil];
                                                }];
                                                [alert addAction:retry];
                                                [alert addAction:cancel];
                                                [self performSelector:@selector(showAlert:) withObject:alert afterDelay:0.5];
                                            }
                                        }else if(days == 30 )
                                        {
                                            if([tripcount intValue] > 1 || undefine_trips_count > 1){
                                                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:[NSString stringWithFormat:NSLocalizedString(@"popupB", @""),month] preferredStyle:UIAlertControllerStyleAlert];
                                                
                                                UIAlertAction *retry = [UIAlertAction actionWithTitle:NSLocalizedString(@"Reportnow", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                    
                                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoTripsFromHome" object:nil userInfo:nil];
                                                    
                                                }];
                                                
                                                UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                    [alert dismissViewControllerAnimated:true completion:nil];
                                                }];
                                                [alert addAction:retry];
                                                [alert addAction:cancel];
                                                [self performSelector:@selector(showAlert:) withObject:alert afterDelay:0.5];
                                            }
                                        }else{
                                            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:[NSString stringWithFormat:NSLocalizedString(@"popupC", @""),month] preferredStyle:UIAlertControllerStyleAlert];
                                            
                                            UIAlertAction *retry = [UIAlertAction actionWithTitle:NSLocalizedString(@"click_here", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                [self callApprovTripAndExpense:uid];
                                            }];
                                            
                                            UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Addappendix", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                
                                                [alert dismissViewControllerAnimated:true completion:nil];
                                                [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoExpenssFromHome" object:nil userInfo:nil];
                                            }];
                                            [alert addAction:retry];
                                            [alert addAction:cancel];
                                            [self performSelector:@selector(showAlert:) withObject:alert afterDelay:0.5];
                                        }
                                    }
                                    
                                    
                                }
                            }
                        }
                    }else{
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"request_time_out", @"") preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *retry = [UIAlertAction actionWithTitle:NSLocalizedString(@"Retry", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                            [self callLocationService:uid from:from undefine_trips_count:undefine_trips_count];
                            
                        }];
                        UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                            [alert dismissViewControllerAnimated:true completion:nil];
                        }];
                        [alert addAction:retry];
                        [alert addAction:cancel];
                        [self showAlert:alert];
                    }
                }];
            }
        });
    });
}

// Call service for approv trip and expense of previous month
-(void)callApprovTripAndExpense:(NSString *)userID{
    
    NSString *uid = userID;
    NSLog(@"callApprovTripAndExpense");
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //Background Thread
        dispatch_async(dispatch_get_main_queue(), ^(void){
            //Run UI Updates
            Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
            NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
            if (networkStatus == NotReachable) {
                NSLog(@"There IS NO internet connection");
                [Helper displayAlertView:@"" message:NSLocalizedString(@"err_network_not_available",nil)];
            } else
            {
                NSDictionary *parameter=@{ @"userid":userID };
                [WebService httpPostWithCustomDelegateURLString:@".getNotificationApproved" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error) {
                    if (error == nil) {
                        NSError *localError = nil;
                        NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
                        if (localError != nil) {
                            NSLog(@"%@",localError.description);
                        }else{
                            id response = [parsedObject objectForKey:@"data"];
                            NSLog(@"response : %@",response);
                        }
                    }else{
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"request_time_out", @"") preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *retry = [UIAlertAction actionWithTitle:NSLocalizedString(@"Retry", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                            [self callApprovTripAndExpense:uid];
                        }];
                        [alert addAction:retry];
                        [self showAlert:alert];
                    }
                }];
            }
        });
    });
}

// Call service when car move without beacon connection.
-(void)callCarStatusService{
    
    NSLog(@"callCarStatusService");
    tracker_id = [Helper isStringIsNull:[Helper getPREF:@"tracker_id"]];
    if ([tracker_id isEqualToString:@""]) {
        return;
        //[Helper displayAlertView:@"" message:@"Tracker id not found."];
    }else{
        NSDictionary *parameter=@{@"tracker_id":tracker_id};
        [WebService httpPostWithCustomDelegateURLString:@".alertsec_company" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error) {
            if (error == nil)
            {
                NSError *localError = nil;
                NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
                
                if (localError != nil) {
                    NSLog(@"%@",localError.description);
                }else{
                    NSDictionary *response = [parsedObject objectForKey:@"data"];
                    NSLog(@"response : %@",response);
                    NSString *status = [response valueForKey:@"status"];
                    if ([status isEqualToString:@"Success"]) {
                        NSLog(@"Car status sended");
                    }
                }
            }else{
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"request_time_out", @"") preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *retry = [UIAlertAction actionWithTitle:NSLocalizedString(@"Retry", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                    [self callCarStatusService];
                }];
                UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                    [alert dismissViewControllerAnimated:true completion:nil];
                }];
                [alert addAction:retry];
                [alert addAction:cancel];
                [self showAlert:alert];
            }
        }];
    }
    
}

-(void)callTrackingUpdateService:(NSString *)status{
    
    NSString *stus = status;
    NSLog(@"callTrackingUpdateService");
    NSDictionary *parameter;
    tracker_id = [Helper isStringIsNull:[Helper getPREF:@"tracker_id"]];
    
    if ([tracker_id isEqualToString:@""]) {
        //[Helper displayAlertView:@"" message:@"Tracker id not found."];
        return;
    }else{
        if ([status isEqualToString:@"0"]) {
            selectedRange = @"1000";
            parameter=@{ @"tracker_id":tracker_id, @"old_tracker_id":tracker_id, @"live":@"0", @"geofence":@"3", @"alarm":@"0",                               @"range":selectedRange,@"is_hold":@"1" };
        }else{
            selectedRange = @"0";
            parameter=@{ @"tracker_id":tracker_id, @"old_tracker_id":tracker_id, @"live":@"2", @"geofence":@"0", @"alarm":@"0",                               @"range":selectedRange,@"is_hold":@"1" };
        }
        NSLog(@"updatetrac_Set Parameter : %@",parameter);
        [WebService httpPostWithCustomDelegateURLString:@".updatetrac_Set" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error)
         {
             if (error == nil) {
                 NSError *localError = nil;
                 NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
                 if (localError != nil) {
                     NSLog(@"%@",localError.description);
                 }else {
                     NSDictionary *response = [parsedObject objectForKey:@"data"];
                     NSString* message = [response objectForKey:@"message"];
                     NSLog(@"Tracker Message : %@",message);
                 }
             }else{
                 /*UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"request_time_out", @"") preferredStyle:UIAlertControllerStyleAlert];
                 
                 UIAlertAction *retry = [UIAlertAction actionWithTitle:NSLocalizedString(@"Retry", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                     [self callTrackingUpdateService:stus];
                 }];
                 UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                     [alert dismissViewControllerAnimated:true completion:nil];
                 }];
                 [alert addAction:retry];
                 [alert addAction:cancel];
                 [self showAlert:alert];*/
             }
         }];
    }
}

-(void)setApplicationLanguage{
       NSLog(@"setApplicationLanguage");
    if ([[Helper getPREF:@"language"]isEqualToString:@"da-DK"]) {
        [NSBundle setLanguage:@"da"];
    }else{
        [NSBundle setLanguage:@"en"];
    }
}

// clear trip related data
-(void)clearMapData{
    
    NSLog(@"clearMapData");
    [appTimer invalidate];
    appTimer = nil;
    appKM = @"";
    appSTTime = @"";
    appEDTime = @"";
    appStartTime = nil;
    appSTDate = @"";
    appDistance = @"";
    appStartLocation = nil;
    selectedRange = @"0";
    _appLocationDataArray = [[NSMutableArray alloc]init];
    [_exitTimer invalidate];
    _exitTimer = nil;
}

# pragma mark reinit data if application is idle
- (void)resetIdleTimer {
   
    NSLog(@"reinit data every four hour if application is idle");
    UIApplicationState *stat  = (UIApplicationState *)[UIApplication sharedApplication].applicationState;
    if (stat == (UIApplicationState *)UIApplicationStateBackground) {
        
        // Register auto expiring background task
        bgTaskId = [_app beginBackgroundTaskWithExpirationHandler:^{
            [_app endBackgroundTask:bgTaskId];
            bgTaskId = UIBackgroundTaskInvalid;
        }];
        
        // Execute background task on the main thread
        dispatch_async( dispatch_get_main_queue(), ^{
            // ------------------
            // DO SOMETHING INVOLVING THE WEBVIEW - WHICH MUST OCCUR ON THE MAIN THREAD!
            if (!idleTimer) {
                idleTimer = [NSTimer scheduledTimerWithTimeInterval:kTimeoutUserInteraction target:self selector:@selector(idleTimerExceeded) userInfo:nil repeats:NO];
                [[NSRunLoop currentRunLoop] addTimer:idleTimer forMode:NSDefaultRunLoopMode];
            } else {
                if (fabs([idleTimer.fireDate timeIntervalSinceNow]) < kTimeoutUserInteraction - 1.) {
                    idleTimer.fireDate = [NSDate dateWithTimeIntervalSinceNow:kTimeoutUserInteraction];
                    //NSLog(@"%@",idleTimer.fireDate);
                }
            }
        });
    }else{
        // DO SOMETHING INVOLVING THE WEBVIEW - WHICH MUST OCCUR ON THE MAIN THREAD!
        if (!idleTimer) {
            idleTimer = [NSTimer scheduledTimerWithTimeInterval:kTimeoutUserInteraction target:self selector:@selector(idleTimerExceeded) userInfo:nil repeats:NO];
            [[NSRunLoop currentRunLoop] addTimer:idleTimer forMode:NSDefaultRunLoopMode];
        } else {
            if (fabs([idleTimer.fireDate timeIntervalSinceNow]) < kTimeoutUserInteraction - 1.) {
                idleTimer.fireDate = [NSDate dateWithTimeIntervalSinceNow:kTimeoutUserInteraction];
                //NSLog(@"%@",idleTimer.fireDate);
            }
        }
    }
}

- (void)idleTimerExceeded {
      NSLog(@"idleTimerExceeded");
    idleTimer = nil;
    [idleTimer invalidate];
    
    UIApplicationState *stat  = (UIApplicationState *)[UIApplication sharedApplication].applicationState;
    if (stat == (UIApplicationState *)UIApplicationStateBackground) {
        [self reInitAppData];
    }
    [self resetIdleTimer];
}

-(void)reInitAppData{
      NSLog(@"reInitAppData");
    NSString *userID = @"";
    if ([[Helper getPREF:@"islogin"] isEqualToString:@"1"]) {
        userID = [Helper getPREF:@"userID"];
        appLogin = @"1";
    }else{
        appLogin = @"0";
    }
    //calling credential service either user login or not, to get payment info, detracker, user info and beacon info
    [self callGetCredentialsService:userID];
}

-(void)showMessageAfterDisconectBeacon{
    
    //[_app endBackgroundTask:bgTaskForPopup];
    //bgTaskForPopup = UIBackgroundTaskInvalid;
    if ([popupTimer isValid]) {
        [popupTimer invalidate];
        popupTimer = nil;
    }
    
    NSLog(@"showMessageAfterDisconectBeacon");
    if([[Helper getPREF:@"type"] isEqualToString:@"0"])
    {
        
        NSLog(@"showMessageAfterDisconectBeacon automatic");
        alertUserMessage = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"msg_continue_trip", @"") preferredStyle:UIAlertControllerStyleAlert];
        
        // Continue trip
        UIAlertAction *continueTrip = [UIAlertAction actionWithTitle:NSLocalizedString(@"msg_continue", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            [_exitTimer invalidate];
            _exitTimer = nil;

            _beaconWarningMessage = [Helper getPREF:@"beaconWarningMessage"];
            if (_beaconWarningMessage == nil || [_beaconWarningMessage isEqualToString:@""]) {
                _beaconWarningMessage = @"0";
            }
            int total = [_beaconWarningMessage intValue];
            total = total + 1;
            _exitTimerCount = 0;
            NSString *str = [NSString stringWithFormat:@"%d",total];
            [Helper setPREFStringValue:str sKey:@"beaconWarningMessage"];
            _beaconWarningMessage = str;
            
            if (total > 2) {
                [Helper setPREFStringValue:@"" sKey:@"beaconWarningMessage"];
                _beaconWarningMessage = @"";
                [self stopTripAndSave];
            }else{
                [self checkCurrentStatus];
            }
            [alertUserMessage dismissViewControllerAnimated:true completion:nil];
        }];
        
        // Save trip
        UIAlertAction *saveTrip = [UIAlertAction actionWithTitle:NSLocalizedString(@"msg_end", @"") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action){
            [self stopTripAndSave];
        }];
        
        [alertUserMessage addAction:continueTrip];
        [alertUserMessage addAction:saveTrip];
        [self checkUserIntactionStatus];
        [self performSelector:@selector(showAlert:) withObject:alertUserMessage afterDelay:0.5];
        [self playSound];
    }
}

-(void)continueTripForSpecificTimeAfterDisconnectBeacon{
    NSLog(@"counter");
    NSLog(@"Exit timer :%d",_exitTimerCount);
    
    _exitTimerCount = _exitTimerCount + 1;
    
    if (_exitTimerCount > exittimerTime) {
        _exitTimerCount = 0;
        
        _currentStage = (NSInteger)[Helper getPREFNSint:@"currentStage"];
        if (_currentStage == CLRegionStateOutside) {
            
            [_exitTimer invalidate];
            _exitTimer = nil;
            [self showMessageAfterDisconectBeacon];
            
        }else{
            NSLog(@"Timer continue beacon is in range");
            _beaconWarningMessage = @"";
            [Helper setPREFStringValue:_beaconWarningMessage sKey:@"beaconWarningMessage"];
            [_exitTimer invalidate];
            _exitTimer = nil;
            
        }
    }
}

-(void)userInteractionAtBeaconDisconnect{
    NSLog(@"counter");
    NSLog(@"Exit timer :%d",_exitTimerCount);
    
    _exitTimerCount = _exitTimerCount + 1;
   
    
    if (_exitTimerCount > exittimerTime) {
        _exitTimerCount = 0;
         [alertUserMessage dismissViewControllerAnimated:true completion:nil];
        _currentStage = (NSInteger)[Helper getPREFNSint:@"currentStage"];
        
        if (_currentStage == CLRegionStateOutside) {
            
            [_exitTimer invalidate];
            _exitTimer = nil;
            [self stopTripAndSave];
            
        }else{
            NSLog(@"Timer continue beacon is in range");
            _beaconWarningMessage = @"";
            [Helper setPREFStringValue:_beaconWarningMessage sKey:@"beaconWarningMessage"];
            [_exitTimer invalidate];
            _exitTimer = nil;
            
        }
    }
}

-(void)playSound{
     NSLog(@"playSound");
    AudioServicesPlaySystemSound(1007);
}

-(void)stopTripAndSave{
     NSLog(@"stopTripAndSave");
    _exitTimerCount = 0;
     [Helper setPREFStringValue:@"" sKey:@"beaconWarningMessage"];
    
    if ([_appTrackNotification isEqualToString:@"1"]) {
        /*UILocalNotification *notification = [UILocalNotification new];
        notification.alertBody = NSLocalizedString(@"Tracking_ended", @"");
        [[UIApplication sharedApplication] presentLocalNotificationNow:notification];*/
        [self displayLocalNotification:NSLocalizedString(@"Tracking_ended", @"")];
    }else{
        NSLog(@"Track notification stop from general setting");
    }
    // Fired notification when beacon not inside region.
    
    NSLog(@"Timer stoped beacon is not in range");
    // Stop timer
    [_exitTimer invalidate];
    _exitTimer = nil;
    
    // finally timer exited
    finalTrack = @"0";
    [[HomeViewController sharedMySingleton] viewDidLoadViewWillAppear];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"BeaconDisConnection" object:nil userInfo:nil];
    
    ConnectionStatus = @"2";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"BeaconDisConnectionService" object:nil userInfo:nil];
    
   /* [_app endBackgroundTask:bgTaskId];
    bgTaskId = UIBackgroundTaskInvalid;
    
    [_app endBackgroundTask:bgTaskForPopup];
    bgTaskForPopup = UIBackgroundTaskInvalid;*/
    
}

-(void)showAlert:(UIAlertController *)alert {
    NSLog(@"Show alert");
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSLog(@"Show alert main queue");
        id rootViewController = [UIApplication sharedApplication].delegate.window.rootViewController;
        if([rootViewController isKindOfClass:[UINavigationController class]])
        {
            rootViewController = ((UINavigationController *)rootViewController).viewControllers.firstObject;
        }
        if([rootViewController isKindOfClass:[UITabBarController class]])
        {
            rootViewController = ((UITabBarController *)rootViewController).selectedViewController;
        }
        [rootViewController presentViewController:alert animated:YES completion:nil];
    });
}

-(void)displayLocalNotification:(NSString *)msg{
    NSLog(@"displayLocalNotification");
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")) {
        
        UNMutableNotificationContent *objNotificationContent = [[UNMutableNotificationContent alloc] init];
        objNotificationContent.body = msg;
       // objNotificationContent.badge = @([[UIApplication sharedApplication] applicationIconBadgeNumber] + 1);
        UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:1 repeats:NO];
        UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:@"identifier" content:objNotificationContent trigger:trigger];
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
            if (!error) {
            }
            else {
            }
        }];
    }
    else {
        UILocalNotification *localNotif = [[UILocalNotification alloc] init];
        localNotif.fireDate = [[NSDate date] dateByAddingTimeInterval:1];
        localNotif.alertBody = msg;
        localNotif.repeatInterval = NSCalendarUnitMinute;
        //localNotif.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
    }
}
@end
