//
//  ConnectAddOneViewController.h
//  SATCAR
//
//  Created by Percept Infotech on 08/02/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConnectAddOneViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableview;

@end
