//
//  TrackersLogViewController.h
//  SATCAR
//
//  Created by Percept Infotech on 13/01/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrackersLogViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableview;

@end
