//
//  ReimbursementRentsTVC.h
//  SATCAR
//
//  Created by Percept Infotech on 10/02/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReimbursementRentsTVC : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *viewBack;
@property (weak, nonatomic) IBOutlet UILabel *lblYear;
@property (weak, nonatomic) IBOutlet UILabel *lblYearVal;
@property (weak, nonatomic) IBOutlet UILabel *lblFromKM;
@property (weak, nonatomic) IBOutlet UILabel *lblFromKMVal;
@property (weak, nonatomic) IBOutlet UILabel *lblTomKM;
@property (weak, nonatomic) IBOutlet UILabel *lblToKMVal;
@property (weak, nonatomic) IBOutlet UILabel *lblAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblAmountVal;
@property (weak, nonatomic) IBOutlet UILabel *lblCountry;
@property (weak, nonatomic) IBOutlet UILabel *lblCountryVal;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrency;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrencyVal;
@end
