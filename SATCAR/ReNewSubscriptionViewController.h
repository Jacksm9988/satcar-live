//
//  ReNewSubscriptionViewController.h
//  SATCAR
//
//  Created by Percept Infotech on 09/02/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReNewSubscriptionViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;
@property (weak, nonatomic) IBOutlet UIButton *btnRenew;
- (IBAction)onReNewSubscription:(id)sender;
@end
