//
//  RegisterStandardUserSpeedmeterVC.h
//  SATCAR
//
//  Created by Percept Infotech on 24/01/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterStandardUserSpeedmeterVC : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *LblSpeedMtr;
@property (weak, nonatomic) IBOutlet UITextField *TxtKm;
@property (weak, nonatomic) IBOutlet UILabel *lblImageDes;
@property (weak, nonatomic) IBOutlet UITextField *txtKmMiles;
@property (weak, nonatomic) IBOutlet UIButton *btnImage;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
- (IBAction)ImagePress:(id)sender;
- (IBAction)NextPress:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintImgHeight;
@property (weak, nonatomic) IBOutlet UIImageView *imgview;
@property (weak, nonatomic) IBOutlet NSString *userType;
- (IBAction)onSkip:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnSkip;
@property (weak, nonatomic) IBOutlet UILabel *lblImage;
@property (weak, nonatomic) IBOutlet UIImageView *imgCam;
@property (weak, nonatomic) IBOutlet UILabel *lblAddImage;

@end
