//
//  BeaconTVC.m
//  SATCAR
//
//  Created by Percept Infotech on 20/04/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import "BeaconTVC.h"

@implementation BeaconTVC

- (void)awakeFromNib {
    [super awakeFromNib];
    _btnPair.hidden = true;
    _imgDetail.hidden = true;
    _lblInfo.hidden = true;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
