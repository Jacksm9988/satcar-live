//
//  ManualTripEntryViewController.h
//  SATCAR
//
//  Created by Percept Infotech on 28/12/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ManualTripEntryViewController : UIViewController
{
    
}
@property (weak, nonatomic) IBOutlet UILabel *lblTripType;
@property (weak, nonatomic) IBOutlet UIView *ViewSegment;
@property (weak, nonatomic) IBOutlet UIButton *BtnPrivateSegment;
@property (weak, nonatomic) IBOutlet UIButton *BtnBusinessSegment;
- (IBAction)SelctSegment:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *lblTripDetails;
@property (weak, nonatomic) IBOutlet UITextField *txtSAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtEAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtDate;
@property (weak, nonatomic) IBOutlet UITextField *txtSTime;
@property (weak, nonatomic) IBOutlet UITextField *txtETime;
@property (weak, nonatomic) IBOutlet UITextField *txtOStart;
@property (weak, nonatomic) IBOutlet UITextField *txtOEnd;
@property (weak, nonatomic) IBOutlet UITextField *txtDistance;
@property (weak, nonatomic) IBOutlet UITextField *txtPurpose;
@property (weak, nonatomic) IBOutlet UITextField *txtNotes;
@property (weak, nonatomic) IBOutlet UITextField *txtUser;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
- (IBAction)onSave:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintPurposeTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintSaveTop;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintPurposeHeight;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;

@property(nonatomic,strong)NSDictionary *tripDataDictionary;
@property(nonatomic,strong)NSString *fromScreen;
@property (weak, nonatomic) IBOutlet UIView *viewPurpose;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintPopupBottom;
@end
