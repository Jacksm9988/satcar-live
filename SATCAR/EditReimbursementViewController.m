//
//  EditReimbursementViewController.m
//  SATCAR
//
//  Created by Percept Infotech on 27/02/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import "EditReimbursementViewController.h"

@interface EditReimbursementViewController (){
    UIColor *borderColor;
}

@end

@implementation EditReimbursementViewController
@synthesize Year,FromKm,ToKm,Amount,Country,Currency,remId;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addTitleView:NSLocalizedString(@"Reimbursement_detail", @"")];
    _txtAmount.text = Amount;
    _txtFromKm.text = FromKm;
    _txtToKm.text = ToKm;
    _txtYear.text = Year;
    _txtCountry.text = Country;
    _txtCourncy.text = Currency;
    
  
    [_btnUpdate setTitle:NSLocalizedString(@"Update", @"") forState:UIControlStateNormal];
    _lblReimHead.text = NSLocalizedString(@"ReimbursementDetail", @"");

    _btnUpdate.layer.cornerRadius = 25;
    _btnUpdate.layer.borderWidth = 1;
    borderColor = [UIColor colorWithRed:18/255.0 green:172.0/255.0 blue:242.0/255.0 alpha:1.0];
    _btnUpdate.layer.borderColor = borderColor.CGColor;
    [_btnUpdate setTitleColor:borderColor forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)UpdateData:(id)sender {
    
    
    
    
    [self.view endEditing:YES];
    if([_txtFromKm.text isEqualToString:@""] || _txtFromKm.text == nil)
    {
        [self alertErrorwithMessage:NSLocalizedString(@"PleaseEnterFromKilloMeterValue", @"")];
        return;
    }
    if([_txtAmount.text isEqualToString:@""] || _txtAmount.text == nil)
    {
        [self alertErrorwithMessage:NSLocalizedString(@"PleaseEnterAmmount", @"")];
        return;
    }

    if(![self CheckNetworkConnection]) {
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *userID = [Helper getPREF:@"userID"];
    NSDictionary *parameter = @{
                    @"userid":userID,
                    @"id":remId,
                    @"year":Year,
                    @"from_km":_txtFromKm.text,
                    @"to_km":ToKm,
                    @"amount_pr_km":_txtAmount.text,
                    @"country":Country,
                    @"currency":Currency,
                    };
   
    
    //NSLog(@"parameter : %@",parameter);
    [WebService httpPostWithCustomDelegateURLString:@".saveReimbursement" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error){
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (error == nil) {
            NSError *localError = nil;
            NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
            
            if (localError != nil) {
                NSLog(@"%@",localError.description);
            }
            else {
                NSLog(@"response : %@",parsedObject);
                id response = [parsedObject objectForKey:@"data"];
                NSString* message = [response objectForKey:@"message"];
                NSString* success = [response objectForKey:@"status"];
                if ([self CheckSession:response])
                {
                    if ([success isEqualToString:@"Success"])
                    {
                        [self alertSucess:nil Message:[response objectForKey:@"message"]];
                    }else
                    {
                        [self alertErrorwithMessage:message];
                    }
                } else
                {
                    NSLog(@"error :: %@",message);
                    [Helper displayAlertView:@"" message:message];
                }
            }
        }
    }];
}
@end
