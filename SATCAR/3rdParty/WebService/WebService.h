//
//  WebService.m
//  MyPlan
//
//  Created by Percept Infotech on 02/01/17.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface WebService : NSObject

#pragma mark - Class Level Methods

+(WebService*)getSharedInstance;
+(void)httpGetWithCustomDelegateURLString:(NSString*)httpGetURLString userName:(NSString*)Uname pasword:(NSString *)pwd isDebug:(BOOL)isDebug callBackData:(void(^)(NSData* data,NSError* error))dataReceived;
+(void)httpPostWithCustomDelegateURLString:(NSString*)httpPOSTURLString parameter:(NSDictionary*)dictionary isDebug:(BOOL)isDebug callBackData:(void(^)(NSData* data,NSError* error))dataReceived;
+(void)httpPostWithCustomDelegateURLStringForCar:(NSString*)httpPOSTURLString parameter:(NSDictionary*)dictionary isDebug:(BOOL)isDebug callBackData:(void(^)(NSData* data,NSError* error))dataReceived;
+(void)httpPostWithCustomDelegateMultipartURLString:(NSString*)httpPOSTURLString parameter:(NSDictionary*)dictionary isDebug:(BOOL)isDebug callBackData:(void(^)(NSData* data,NSError* error))dataReceived;

+(void)getImageFromURL:(NSString*)imageUrl callBackData:(void(^)(UIImage * image,NSError* error))dataReceived;
+(NSString*)getFileNameAsDateAndTimeStemp;
+(NSString*)getExpenseImageUrl;

@end
