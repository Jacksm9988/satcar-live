	//
//  WebService.m
//  MyPlan
//
//  Created by Percept Infotech on 02/01/17.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import "WebService.h"

static NSString* stringUrlDomainForCar = @"https://www.weiswise.com/eadknew/index.php?option=com_carinsurance&task=webservices";
static NSString* stringUrlDomain = @"http://satcar.dk/index.php?option=com_satcar&task=webservice";
//static NSString* stringUrlDomain = @"http://prod.satcar.dk/index.php?option=com_satcar&task=webservice";

// Expense image url
static NSString *imageExpenseUrl = @"http://satcar.dk/images/satcar/expense/";
//static NSString *imageExpenseUrl = @"http://prod.satcar.dk/images/satcar/expense/";

@implementation WebService

+(WebService*)getSharedInstance{
    static WebService* sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[WebService alloc] init];
    });
    return sharedInstance;
}

+(NSString *)getServiceUrl{
    NSString *url = @"http://satcar.dk/index.php?option=com_satcar&task=webservice";
    
    NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
    if ([bundleIdentifier isEqualToString:@"com.SATCAR.Dev"]) {
        url = @"http://satcar.dk/index.php?option=com_satcar&task=webservice";
        // url = @"http://satcar.dk/index.php?option=com_satcar&task=webservice";
    }else{
        //url = @"http://prod.satcar.dk/index.php?option=com_satcar&task=webservice";
        url = @"http://satcar.dk/index.php?option=com_satcar&task=webservice";
    }
    return url;

}
+(NSString *)getimageExpenseUrl{
    NSString *url = @"http://satcar.dk/images/satcar/expense/";
    NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
    
    if ([bundleIdentifier isEqualToString:@"com.SATCAR.Dev"]) {
        url = @"http://satcar.dk/images/satcar/expense/";
       //  url = @"http://satcar.dk/images/satcar/expense/";
    }else{
        //url = @"http://prod.satcar.dk/images/satcar/expense/";
        url = @"http://satcar.dk/images/satcar/expense/";
    }
    
    return url;
}

#pragma mark - Http Get Method

+(void)httpGetWithCustomDelegateURLString:(NSString*)httpGetURLString userName:(NSString*)Uname pasword:(NSString *)pwd isDebug:(BOOL)isDebug callBackData:(void(^)(NSData* data,NSError* error))dataReceived {
    
    
    NSString *username = Uname;
    NSString *password = pwd;

    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    defaultConfigObject.timeoutIntervalForRequest = 30.0;
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration:defaultConfigObject];

    NSMutableString* finalUrl =[[NSMutableString alloc] initWithString:httpGetURLString];
    NSURL * url = [NSURL URLWithString:finalUrl];
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    NSLog(@"URL : %@",url);
    [urlRequest setHTTPMethod:@"GET"];
    [urlRequest addValue:@"application/x-www-form-urlencoded application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", username, password];
    NSData *authData = [authStr dataUsingEncoding:NSASCIIStringEncoding];
    NSString* authStrData = [[NSString alloc] initWithData:[authData base64EncodedDataWithOptions:NSDataBase64EncodingEndLineWithLineFeed] encoding:NSASCIIStringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", authStrData];
    [urlRequest setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error == nil) {
            if(isDebug == true){
                NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
                NSLog(@"Data = %@",text);
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                //NSLog(@"Response :: %@ ", response);
                dataReceived(data, nil);
            });
        }else if (error != nil){
            dispatch_async(dispatch_get_main_queue(), ^{
                //NSLog(@"Response :: %@", response);
                dataReceived(nil, error);
                NSLog(@"error = %@",error);
                //[Helper displayAlertView:@"" message:@"The request timed out."];
            });
        }
    }];
    [dataTask resume];
}
#pragma mark - Http Get post

+(void)httpPostWithCustomDelegateURLString:(NSString*)httpPOSTURLString parameter:(NSDictionary*)dictionary isDebug:(BOOL)isDebug callBackData:(void(^)(NSData* data,NSError* error))dataReceived {
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    defaultConfigObject.timeoutIntervalForRequest = 30.0;
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration:defaultConfigObject];
    
    NSMutableString* finalUrl =[[NSMutableString alloc] initWithString:[self getServiceUrl]]; //stringUrlDomain
    [finalUrl appendString:httpPOSTURLString];
    
    NSURL * url = [NSURL URLWithString:finalUrl];
    NSLog(@"url : %@",url);
    NSLog(@"parameter : %@",dictionary);
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    
    NSString * params = [WebService getkeyValueString:dictionary];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error == nil) {
            
            if(isDebug == true){
                NSLog(@"Response:%@ %@\n", response, error);
                NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
                NSLog(@"Data = %@",text);
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                dataReceived(data, nil);
            });
        }else if (error != nil){
            dispatch_async(dispatch_get_main_queue(), ^{
                dataReceived(nil, error);
                NSLog(@"error = %@",error);
            });
        }
    }];
    [dataTask resume];
}

#pragma mark - Http Get post

+(void)httpPostWithCustomDelegateURLStringForCar:(NSString*)httpPOSTURLString parameter:(NSDictionary*)dictionary isDebug:(BOOL)isDebug callBackData:(void(^)(NSData* data,NSError* error))dataReceived
{
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    defaultConfigObject.timeoutIntervalForRequest = 30.0;
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration:defaultConfigObject];
    
    NSMutableString* finalUrl =[[NSMutableString alloc] initWithString:stringUrlDomainForCar]; //stringUrlDomain
    [finalUrl appendString:httpPOSTURLString];
    
    NSURL * url = [NSURL URLWithString:finalUrl];
    NSLog(@"url : %@",url);
    NSLog(@"parameter : %@",dictionary);
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    
    NSString * params = [WebService getkeyValueString:dictionary];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest  completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error == nil) {
            if(isDebug == true){
                                                                   
                NSLog(@"Response:%@ %@\n", response, error);
                NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
                NSLog(@"Data = %@",text);
                                                                   
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                dataReceived(data, nil);
            });
        }else if (error != nil){
            dispatch_async(dispatch_get_main_queue(), ^{
                dataReceived(nil, error);
        });
        }}];
    [dataTask resume];
    
}
+(void)httpPostWithCustomDelegateMultipartURLString:(NSString*)httpPOSTURLString parameter:(NSDictionary*)dictionary isDebug:(BOOL)isDebug callBackData:(void(^)(NSData* data,NSError* error))dataReceived
{
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    defaultConfigObject.timeoutIntervalForRequest = 90.0;
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration:defaultConfigObject];
    
    NSMutableString* finalUrl =[[NSMutableString alloc] initWithString:[self getServiceUrl]]; //stringUrlDomain
    [finalUrl appendString:httpPOSTURLString];
    NSURL * url = [NSURL URLWithString:finalUrl];

    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [urlRequest setHTTPMethod:@"POST"];
    NSString *sBoundary = @"---------------------------14737809831466499882746641449";
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",sBoundary];
    [urlRequest addValue:contentType forHTTPHeaderField: @"Content-Type"];
    NSString* sContentType = @"application/octet-stream";
    
    NSLog(@"URL : %@",url);
    NSLog(@"Parameter : %@",dictionary);
    NSMutableData *body = [NSMutableData data];
    if([[dictionary allKeys] count]>0){
        for(NSString * sParam in [dictionary allKeys])
        {
            id objOfValue = [dictionary valueForKey:sParam];
            
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",sBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            if ([objOfValue isKindOfClass:[NSString class]]) {
                NSData* dataValue =[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@",sParam,(NSString*)objOfValue] dataUsingEncoding:NSUTF8StringEncoding];
                [body appendData:dataValue];
            }
            else if ([objOfValue isKindOfClass:[NSData class]]) {
                [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",sBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
                
                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: attachment; name=\"%@\"; filename=\"%@.jpg\"\r\n",sParam, [WebService getFileNameAsDateAndTimeStemp]] dataUsingEncoding:NSUTF8StringEncoding]];
                
                [body appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n",sContentType] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[NSData dataWithData:objOfValue]];
                [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",sBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
            }
        }
    }
    [urlRequest setHTTPBody:body];
    
    NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error == nil) {
            if(isDebug == true){
                NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
                NSLog(@"Data = %@",text);
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                //NSLog(@"Response :: %@", response);
                dataReceived(data, nil);
            });
        }else if (error != nil){
            dispatch_async(dispatch_get_main_queue(), ^{
                //NSLog(@"Response :: %@", response);
                dataReceived(nil, error);
                NSLog(@"error = %@",error);
                //[Helper displayAlertView:@"" message:@"The request timed out."];
            });
        }
        
    }];
    [dataTask resume];
    
}
+(NSString*)getkeyValueString:(NSDictionary*)dictionary{
    
    NSMutableString* keyValueString=[[NSMutableString alloc] init];
    
    int i =0;
    for (NSString* key in dictionary.allKeys) {
        
        [keyValueString appendString:key];
        [keyValueString appendString:[NSString stringWithFormat:@"=%@",[dictionary objectForKey:key]]];
        if (dictionary.count - 1 != i ) {
            [keyValueString appendString:@"&"];
        }
        i+=1;
    }
    return keyValueString;
}

+(NSData*)getJsonString:(NSDictionary*)dicParameter{
    NSError *error;
    
    // Pass 0 if you don't care about the readability of the generated string
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dicParameter options:NSJSONWritingPrettyPrinted error:&error];
    NSData* finalData=nil;
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
        finalData = nil;
    } else {
        // NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        finalData = jsonData;
    }
    return finalData;
}

+(void)getImageFromURL:(NSString*)imageUrl callBackData:(void(^)(UIImage * image,NSError* error))dataReceived{
    
    NSURLSessionConfiguration *sessionConfig =
    [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *session =
    [NSURLSession sessionWithConfiguration:sessionConfig];
    
    NSURLSessionDownloadTask *getImageTask =
    [session downloadTaskWithURL:[NSURL URLWithString:imageUrl] completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
        if (error!= nil){
            dispatch_async(dispatch_get_main_queue(), ^{
                dataReceived(nil,error);
            });
        }else {
            UIImage *downloadedImage = [UIImage imageWithData: [NSData dataWithContentsOfURL:location]];
            dispatch_async(dispatch_get_main_queue(), ^{
                dataReceived(downloadedImage,nil);
            });
        }
    }];
    [getImageTask resume];
}

+(NSString*)getFileNameAsDateAndTimeStemp {
    //Unique
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"YYYYMMddHHmmss"];
    [dateFormat setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    return [dateFormat stringFromDate:[NSDate  date]];
}

+(NSString*)getExpenseImageUrl{
    NSString *url = @"http://satcar.dk/images/satcar/expense/";
    NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
    
    if ([bundleIdentifier isEqualToString:@"com.SATCAR.Dev"]) {
       // url = @"http://test.satcar.dk/images/satcar/expense/";
       
        url = @"http://satcar.dk/images/satcar/expense/";
    }else{
        //url = @"http://prod.satcar.dk/images/satcar/expense/";
        url = @"http://satcar.dk/images/satcar/expense/";
    }
    return url;
    //return imageExpenseUrl;
}
@end
