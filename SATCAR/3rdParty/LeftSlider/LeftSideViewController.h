//
//  ECSlidingViewController.h
//  MyPlan
//
//  Created by Percept Infotech on 7/7/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "ECSlidingViewController.h"

#define ECLEFT 180
@interface LeftSideViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate,UIGestureRecognizerDelegate>
{
    NSArray *cellTitles;
    UITableView IBOutlet *theTableView;
    CGFloat _centerXX;
    
     
    CGFloat _centerX;
   // UISwipeGestureRecognizer *LeftRight;
}
- (IBAction)Close:(id)sender;


@property (weak, nonatomic) IBOutlet UIButton *BtnClose;
@property (weak, nonatomic) IBOutlet UILabel *LblFbUserName;
@property (weak, nonatomic) IBOutlet UIButton *BtnMyContect;
@property (weak, nonatomic) IBOutlet UIImageView *Img_Fb_User;
@property (weak, nonatomic) IBOutlet UIView *View_FbLogeIn;
@property (weak, nonatomic) IBOutlet UIView *View_FbLoggedin;
@end
