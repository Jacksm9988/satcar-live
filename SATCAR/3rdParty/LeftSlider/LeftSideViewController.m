 //
//  LeftSideViewController.h
//  MyPlan
//
//  Created by Percept Infotech on 7/7/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import "LeftSideViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "AppDelegate.h"
#import <MobileCoreServices/UTCoreTypes.h>
#import "ViewController.h"

//#import "ProgressHUD.h"

@interface LeftSideViewController ()

@end

@implementation LeftSideViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        
        }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //self.view.alpha = 0.2;
    
    self.view.backgroundColor = [UIColor clearColor];
    UISwipeGestureRecognizer *LeftRight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleGestureNext:)];
    LeftRight.delegate=self;
    [LeftRight setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.view addGestureRecognizer:LeftRight];
    self.view.userInteractionEnabled=YES;
    
       cellTitles =@[@"Symptoms",@"Strategies",@"Contacts",@"Hope Box",@"Mood Ratings",@" ",@"Nearest emergency room",@"Quick Messages",@"Share my location",@" ",@"Settings",@"",@"Log Out"];
    
    
    theTableView.backgroundColor =[self colorFromHexString:@"#0acfba"];
    
    UIColor *barColor = [self colorFromHexString:@"#0acfba"];
    self.navigationController.navigationBar.barTintColor=barColor;
    self.extendedLayoutIncludesOpaqueBars=YES;
    
}

- (void)handleLeftEdgeGesture:(UIScreenEdgePanGestureRecognizer *)gesture {
    // Get the current view we are touching
    UIView *view = [self.view hitTest:[gesture locationInView:gesture.view] withEvent:nil];
    
    if(UIGestureRecognizerStateBegan == gesture.state ||
       UIGestureRecognizerStateChanged == gesture.state) {
        CGPoint translation = [gesture translationInView:gesture.view];
        // Move the view's center using the gesture
        view.center = CGPointMake(_centerX + translation.x, view.center.y);
    } else {// cancel, fail, or ended
        // Animate back to center x
        [UIView animateWithDuration:.3 animations:^{
            
            view.center = CGPointMake(_centerX, view.center.y);
        }];
    }
}


-(void)slideToRightWithGestureRecognizer:(UISwipeGestureRecognizer *)gestureRecognizer{
    [UIView animateWithDuration:0.5 animations:^{
        self.view.frame = CGRectOffset(self.view.frame, 380, 0.0);

    }];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self getFbUserData];
}



-(void)getFbUserData
{
     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *dbuser = [defaults valueForKey:@"login_with"];
    if([defaults boolForKey:@"inRegister"] && [dbuser isEqualToString:@"facebook"])
    {
     
    }
    else
    {
   
    }
}


- (void)handlePanGesture:(UIPanGestureRecognizer *)gesture {
    
    CGPoint translation = [gesture translationInView:gesture.view];
   //NSLog(@"%@",[NSString stringWithFormat:@"(%0.0f, %0.0f)", translation.x, translation.y]);
    
    
}

-(void)handleGestureNext:(UISwipeGestureRecognizer *)recognizer
{
    //NSLog(@"Swipe Recevied");
     [self.slidingViewController resetTopViewAnimated:YES];
    
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    // You can customize the way in which gestures can work
    // Enabling multiple gestures will allow all of them to work together, otherwise only the topmost view's gestures will work (i.e. PanGesture view on bottom)
    return YES;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark- UITableViewDataSource


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return ([cellTitles count]);

}
- (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;

{
    tableView.backgroundColor =  [self colorFromHexString:@"#3a3934"];
    
    
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LeftCell"];
    if (!cell)
        {
            cell= [[UITableViewCell alloc ]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"LeftCell"];
            cell.textLabel.font =[UIFont systemFontOfSize:18];
            cell.backgroundColor =[self colorFromHexString:@"#0acfba"];

            UILabel *lbl = [[UILabel alloc]init];
            lbl.frame = CGRectMake(20, 5, 280, 30);
            lbl.tag = 87458;
            lbl.font = [UIFont systemFontOfSize:18];
            lbl.textColor = [UIColor whiteColor];
            [cell.contentView addSubview:lbl];
            
            UIImageView *imgarrow = [[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2+80, 15, 10, 15)];
            imgarrow.image = [[UIImage imageNamed:@"arrow.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [imgarrow setTintColor:[UIColor whiteColor]];
            if(indexPath.row == 5 || indexPath.row == 9 || indexPath.row == 11)
            {
                
            }else
            [cell.contentView addSubview:imgarrow];
            
        }
    UIImageView *iconimg = [[UIImageView alloc]init];
    iconimg.frame = CGRectMake(8, 5, 30, 30);
        if(indexPath.row == 0)
        {
            iconimg.image = [UIImage imageNamed:@"icon-home.png"];
        }
        else if(indexPath.row == 1)
        {
             iconimg.image = [UIImage imageNamed:@"icon-place-new-order.png"];
        }
        else if(indexPath.row == 2)
        {
            iconimg.image = [UIImage imageNamed:@"icon-receive.png"];
            
        }
        else if(indexPath.row == 3)
        {
            iconimg.image = [UIImage imageNamed:@"icon-my-account.png"];
            
        }
       /* else if(indexPath.row == 4)
        {
            iconimg.image = [UIImage imageNamed:@"icon-upcommingBday.png"];
            
        }*/
        else if(indexPath.row == 5)
        {
            iconimg.image = [UIImage imageNamed:@"icon-contact.png"];
            
        }
        else if(indexPath.row == 4)
        {
             iconimg.image = [UIImage imageNamed:@"icon-contact.png"];
            
        }
        else if(indexPath.row == 6)
        {
             iconimg.image = [UIImage imageNamed:@"icon-about.png"];
        }
        else if(indexPath.row == 7)
        {
            iconimg.image = [UIImage imageNamed:@"icon-legal.png"];
        }
        [cell.contentView addSubview:iconimg];
    UILabel *lbl = (UILabel *)[cell.contentView viewWithTag:87458];
    lbl.text = [cellTitles objectAtIndex:indexPath.row];
//        cell.textLabel.text=[cellTitles objectAtIndex:indexPath.row];
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.backgroundColor =[self colorFromHexString:@"#0acfba"];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
   
    
return (cell);


}



#pragma mark- UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *storyBoardIdentifier=@"";
    
   
    switch (indexPath.row) {
            
            case 0:
                storyBoardIdentifier=@"symptomsAppPath";
                //NSLog(@"Selected %ld",(long)indexPath.row);
                break;
            case 1:
                storyBoardIdentifier=@"strategiesAppPath";
                //NSLog(@"Selected %ld",(long)indexPath.row);
                break;
            case 2:
                storyBoardIdentifier=@"contactsAppPath";
                //NSLog(@"Selected %ld",(long)indexPath.row);
                break;
            case 3:
                storyBoardIdentifier=@"hopeboxAppPath";
                //NSLog(@"Selected %ld",(long)indexPath.row);
                break;
            case 4:
                storyBoardIdentifier=@"moodratingsAppPath";
                //NSLog(@"Selected %ld",(long)indexPath.row);
                break;
            case 5:
                storyBoardIdentifier=@"nochange";
                break;
            case 6:
                storyBoardIdentifier=@"nearestemergencyAppPath";
                break;
            case 7:
                storyBoardIdentifier=@"quickmessagesAppPath";
                break;
            case 8:
                storyBoardIdentifier=@"sharemylocaionAppPath";
                break;
            case 9:
                storyBoardIdentifier=@"nochange";
                break;
            case 10:
                storyBoardIdentifier=@"settingsAppPath";
                break;
            case 11:
                storyBoardIdentifier=@"nochange";
                break;
            case 12:
            
                storyBoardIdentifier=@"InitialAppPath";
            
                //NSLog(@"Selected %ld",(long)indexPath.row);
            
                break;
            
        default:
            break;
    }
    if([storyBoardIdentifier isEqualToString:@""])
    {
        [self.slidingViewController resetTopViewAnimated:YES];
    }else{
        if([storyBoardIdentifier isEqualToString:@"nochange"])
        {
        }else{
            
            NSString *oldidenti =[[NSUserDefaults standardUserDefaults] valueForKey:@"storyBoardIdentifier"];
            if(oldidenti ==nil){
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UIViewController *newViewController = [storyboard instantiateViewControllerWithIdentifier:storyBoardIdentifier];
                [self.slidingViewController setTopViewController:newViewController];
                [self.slidingViewController resetTopViewAnimated:YES];
                [[NSUserDefaults standardUserDefaults] setObject:storyBoardIdentifier forKey:@"storyBoardIdentifier"];

            }
            else{
                if([oldidenti isEqualToString:storyBoardIdentifier]){
                    [self.slidingViewController resetTopViewAnimated:YES];
                }else{
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UIViewController *newViewController = [storyboard instantiateViewControllerWithIdentifier:storyBoardIdentifier];
                [self.slidingViewController setTopViewController:newViewController];
                [self.slidingViewController resetTopViewAnimated:YES];
                [[NSUserDefaults standardUserDefaults] setObject:storyBoardIdentifier forKey:@"storyBoardIdentifier"];
                }
    
            }
        }
    }
    
    
  /*  switch(indexPath.row)
    {
        case 0://Home
            {
     
                storyBoardIdentifier=@"InitialAppPath";
                break;
            }
        case 1:// Place Order
            {
               storyBoardIdentifier=@"InitialAppPath";
                
                break;
            }
        case 2://TrakOrders
            {
                //storyBoardIdentifier=@"TrackOrderPath";
                //show_myaccount_view = @"myOrders";
                
                storyBoardIdentifier=@"ReceiveOrderPath";
                
                break;
            }
        
        case 3: // ReceiveOrders
            {
                //storyBoardIdentifier=@"ReceiveOrderPath";
               
                storyBoardIdentifier=@"TrackOrderPath";
               // show_myaccount_view = @"myAccount";
            
            break;
            }
 
               case 4: //Live Chat
            {
                
                  storyBoardIdentifier=@"ContactUsPath";
                break;
            
            }
        case 5:// Legal information
            {
                
                //storyBoardIdentifier=@"LegalInfoPath";
                storyBoardIdentifier=@"AboutPath";
                // [self performSegueWithIdentifier:@"AboutSegue" sender:self];
                // return;
                break;
                
                
               
            }
        case 6://My Account
        {
            
            //storyBoardIdentifier=@"TrackOrderPath";
            //show_myaccount_view = @"myAccount";
            
            storyBoardIdentifier=@"LegalInfoPath";
            break;
            
            
        }
      
        
    }*/
    
   /* UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
     UIViewController *newViewController = [storyboard instantiateViewControllerWithIdentifier:storyBoardIdentifier];
    
    [self.navigationController pushViewController:newViewController animated:YES];*/
    
    
    
   
    
}


-(IBAction)hideLeftHandPane:(id)sender
{
    [self.slidingViewController resetTopViewAnimated:YES];
}





- (IBAction)Close:(id)sender {
    
    NSString *storyBoardIdentifier=@"TrackOrderPath";
    //show_myaccount_view = @"myAccount";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhonenew" bundle:nil];
    UIViewController *newViewController = [storyboard instantiateViewControllerWithIdentifier:storyBoardIdentifier];
    [self.slidingViewController setTopViewController:newViewController];
    [self.slidingViewController resetTopViewAnimated:YES];
    
}
@end
