#import "Helper.h"
#import <objc/runtime.h>



static Helper * _sharedHelper = nil;

@implementation Helper

@synthesize playerVideo;
#pragma mark - image compress and resige
+(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


+ (Helper *) sharedHelper {
    if (_sharedHelper == nil) {
        _sharedHelper = [[Helper alloc] init];
    }
    return _sharedHelper;
}

+(CGRect)mainScreenBound{
    CGRect mainScreenBound = [[UIScreen mainScreen] bounds];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        mainScreenBound = [[[UIScreen mainScreen] fixedCoordinateSpace] bounds];
    }
    return mainScreenBound;
}

/*!
 @method viewFromNib
 @abstract Load view from Nib Files
 */

+ (UIView *)viewFromNib:(NSString *)sNibName {
	return (UIView *)[Helper viewFromNib:sNibName sViewName:@"UIView"];
}


#pragma mark - Theme Color
+(UIColor *)privateColor{
    return [UIColor colorWithRed:243.0/255.0 green:122/255.0 blue:29/255.0 alpha:1.00];
}
+(UIColor *)BusinessColor{
    return [UIColor colorWithRed:158/255.0 green:31/255.0 blue:99/255.0 alpha:1.00];
}
+(UIColor *)defaultColor{
    return [UIColor colorWithRed:0.000 green:0.647 blue:0.929 alpha:1.00];
}
+(UIColor *)greenColor{
    return [UIColor colorWithRed:102.0/255.0 green:184.0/255.0 blue:82.0/255.0 alpha:1.00];
}
+(void)setBorderWithColorInView:(UIView *)view{
    view.layer.cornerRadius = 5;
    view.layer.borderWidth = 1;
    view.layer.borderColor = [UIColor colorWithRed:0.902 green:0.902 blue:0.910 alpha:1.00].CGColor;
}
+(void)setBorderWithColorInText:(UITextField *)txt{
    txt.layer.cornerRadius = 5;
    txt.layer.borderWidth = 1;
    txt.layer.borderColor = [UIColor colorWithRed:0.902 green:0.902 blue:0.910 alpha:1.00].CGColor;
}
+(void)setBorderWithColorInButton:(UIButton *)btn{
    btn.layer.cornerRadius = 25;
    btn.layer.borderWidth = 1;
    btn.layer.borderColor = [UIColor colorWithRed:0.000 green:0.651 blue:0.929 alpha:1.00].CGColor;
}
+(void)setBorderWithColorInLable:(UILabel *)lbl{
    lbl.layer.cornerRadius = 5;
    lbl.layer.borderWidth = 1;
    lbl.layer.borderColor = [UIColor colorWithRed:0.902 green:0.902 blue:0.910 alpha:1.00].CGColor;
}

/*!
 @method Check getBuildNumber contail key
 @abstract return build number
 */
+(NSString *)getBuildNumber{
    NSString *appBuildString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    return appBuildString;
}
/*!
 @method Check getVersionNumber contail key
 @abstract return version number
 */
+(NSString *)getVersionNumber{
    NSString *appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    return appVersionString;
}

/*!
 @method viewFromNib
 @abstract Load view from Nib Files for dynemic class
 */
+ (id)viewFromNib:(NSString *)sNibName sViewName:(NSString *)sViewName
{
//	SALog(@"sNibName..%@",sNibName);
	Class className = NSClassFromString(sViewName);
	NSArray *xib = [[NSBundle mainBundle] loadNibNamed:sNibName owner:self options:nil];
	for (id _view in xib) { // have to iterate; index varies
		if ([_view isKindOfClass:[className class]]) return _view;
	}
	return nil;
}

/*!
 @method performBlock
 @abstract Perform Block Operation after delay
 */
+(void)performBlock:(void (^)(void))block afterDelay:(NSTimeInterval)delay{
	int64_t delta = (int64_t)(1.0e9 * delay);
	dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delta), dispatch_get_main_queue(), block);
}

/*!
 @method getPreferenceValueForKey
 @abstract To get the preference value for the key that has been passed
 */
+ (NSString *)getPREF:(NSString *)sKey {
    return (NSString *)[[NSUserDefaults standardUserDefaults] valueForKey:sKey];
}

/*!
 @method getPreferenceValueForKey
 @abstract To get the preference value for the key that has been passed
 */
+ (int)getPREFint:(NSString *)sKey {
    return (int)[[NSUserDefaults standardUserDefaults] integerForKey:sKey];
}

+ (NSInteger)getPREFNSint:(NSString *)sKey {
    return (NSInteger)[[NSUserDefaults standardUserDefaults] integerForKey:sKey];
}

/*!
 @method setPreferenceValueForKey
 @abstract To set the preference value for the key that has been passed
 */
+(void) setPREFStringValue:(NSString *)sValue sKey:(NSString *)sKey {
	[[NSUserDefaults standardUserDefaults] setValue:sValue forKey:sKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setUserValue:(NSDictionary *)sValue sKey:(NSString *)sKey {
    [[NSUserDefaults standardUserDefaults] setValue:sValue forKey:sKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void)setDataValue:(NSData *)data sKey:(NSString *)sKey {
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:sKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(NSData *)getDataValue:(NSString *)sKey{
    return (NSData *)[[NSUserDefaults standardUserDefaults] objectForKey:sKey];
}

+(void) setUserArrayValue:(NSArray *)sValue sKey:(NSString *)sKey {
    [[NSUserDefaults standardUserDefaults] setValue:sValue forKey:sKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) setLocationArrayValue:(NSMutableArray *)sValue sKey:(NSString *)sKey {
    [[NSUserDefaults standardUserDefaults] setValue:sValue forKey:sKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+(NSMutableArray *) getLocationArrayValue:(NSString *)sKey {
    return (NSMutableArray *)[[NSUserDefaults standardUserDefaults] valueForKey:sKey];
}

+(NSDictionary *) getUserValue:(NSString *)sKey {
    return (NSDictionary *)[[NSUserDefaults standardUserDefaults] valueForKey:sKey];
}

+(NSString *)isStringIsNull:(NSString *)value{
    if (value == (NSString *)[NSNull null] || value == nil) {
        return  @"";
    }else{
        return value;
    }
}

/*!
 @method setPreferenceValueForKey
 @abstract To set the preference value for the key that has been passed
 */
+(void) setPREFint:(int)iValue :(NSString *)sKey {
	[[NSUserDefaults standardUserDefaults] setInteger:iValue forKey:sKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
/*!
 @method getPreferenceValueForKey
 @abstract To get the preference value for the key that has been passed
 */
+ (id)getPREFID:(NSString *)sKey {
    return [[NSUserDefaults standardUserDefaults] valueForKey:sKey];
}



/*!
 @method setPreferenceValueForKey
 @abstract To set the preference value for the key that has been passed
 */
+(void) setPREFID:(id)sValue :(NSString *)sKey {
	[[NSUserDefaults standardUserDefaults] setValue:sValue forKey:sKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
/*!
 @method delPREF
 @abstract To delete the preference value for the key that has been passed
 */
+(void) delPREF:(NSString *)sKey {
	[[NSUserDefaults standardUserDefaults] removeObjectForKey:sKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
/*!
 @method displayAlertView
 @abstract To Display Alert Msg
 */
+ (void) displayAlertView :(NSString *) title message :(NSString *) message
{
    
    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:NSLocalizedString(@"Ok", nil) otherButtonTitles:nil];
    
    [alert show];
}

+ (BOOL)containsKey: (NSString *)key data:(NSDictionary *)dict{
    BOOL retVal = 0;
    NSArray *allKeys = [dict allKeys];
    retVal = [allKeys containsObject:key];
    return retVal;
}


/*!
 @method randomRange
 @abstract get random index between two
 */
+(int) randomRange: (int) min max:(int) max {
    int range = max - min;
    if (range == 0) return min;
	
    return (arc4random() % range) + min;
}

/*!
 @method iSConnectedToNetwork
 @absttract To Check Network Connection
 */

+ (BOOL) iSConnectedToNetwork
{	// Create zero addy
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
    // Recover reachability flags
    SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *)&zeroAddress);
    SCNetworkReachabilityFlags flags;
    BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
    CFRelease(defaultRouteReachability);
    if (!didRetrieveFlags)
    {
        return NO;
    }
    BOOL isReachable = flags & kSCNetworkFlagsReachable;
    BOOL needsConnection = flags & kSCNetworkFlagsConnectionRequired;
    return (isReachable && !needsConnection) ? YES : NO;
}

/* The UI_USER_INTERFACE_IDIOM() macro is provided for use when deploying to a version of the iOS less than 3.2. If the earliest version of iPhone/iOS that you will be deploying for is 3.2 or greater, you may use -[UIDevice userInterfaceIdiom] directly.
 */
#define UI_USER_INTERFACE_IDIOM() ([[UIDevice currentDevice] respondsToSelector:@selector(userInterfaceIdiom)] ? [[UIDevice currentDevice] userInterfaceIdiom] : UIUserInterfaceIdiomPhone)

#define UIDeviceOrientationIsPortrait(orientation)  ((orientation) == UIDeviceOrientationPortrait || (orientation) == UIDeviceOrientationPortraitUpsideDown)
#define UIDeviceOrientationIsLandscape(orientation) ((orientation) == UIDeviceOrientationLandscapeLeft || (orientation) == UIDeviceOrientationLandscapeRight)



+ (NSString* )getDigitFromString:(NSString*)inputString{
    
    NSString *originalString = inputString;
    NSMutableString *strippedString = [NSMutableString
                                       stringWithCapacity:originalString.length];
    
    NSScanner *scanner = [NSScanner scannerWithString:originalString];
    NSCharacterSet *numbers = [NSCharacterSet
                               characterSetWithCharactersInString:@"0123456789"];
    
    while ([scanner isAtEnd] == NO) {
        NSString *buffer;
        if ([scanner scanCharactersFromSet:numbers intoString:&buffer]) {
            [strippedString appendString:buffer];
            
        } else {
            [scanner setScanLocation:([scanner scanLocation] + 1)];
        }
    }
    return strippedString;
}
+(NSString*)getDateFromString:(NSString*)dateStr{
    
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"YYYY-MM-dd"];
     [dateFormat setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    // Convert date object to desired output format
    [dateFormat setDateFormat:@"dd/MM/YYYY"];
    dateStr = [dateFormat stringFromDate:date];
    
    return dateStr;
}

+(NSString *)getPreviousMonth{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MMMM yyyy"];
    
    NSDate *now = [NSDate date];
    
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setMonth:-1];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *newDate = [calendar dateByAddingComponents:dateComponents toDate:now options:0];
    NSString *month = [dateFormat stringFromDate:newDate];
    return month;
}
+(NSString *)getCurMonth{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MMMM yyyy"];
    
    NSDate *now = [NSDate date];
    
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *newDate = [calendar dateByAddingComponents:dateComponents toDate:now options:0];
    NSString *month = [dateFormat stringFromDate:newDate];
    return month;
}

+(long )get3daysMonth{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MMMM yyyy"];
    
    NSDate *now = [NSDate date];
    
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents day];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *newDate = [calendar dateByAddingComponents:dateComponents toDate:now options:0];
    NSString *month = [dateFormat stringFromDate:newDate];
     NSDate *dd = [dateFormat dateFromString:month];
    NSDate *earlier = [[NSDate alloc]initWithTimeIntervalSinceReferenceDate:1];
    NSCalendar *gregorian = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
    
    // pass as many or as little units as you like here, separated by pipes
    NSUInteger units = NSCalendarUnitYear | NSCalendarUnitDay | NSCalendarUnitMonth;
    NSDateComponents *components = [gregorian components:units fromDate:earlier toDate:dd options:0];
    NSArray *arr = [month componentsSeparatedByString:@" "];
    NSInteger ddd = 0;
    NSCalendar *c = [NSCalendar currentCalendar];
    
    
    NSRange days = [c rangeOfUnit:NSCalendarUnitDay
                           inUnit:NSCalendarUnitMonth
                          forDate:dd];
    NSInteger months = [components month];
    NSInteger daysd = [components day];
    
    if(arr.count > 0)
    {
        NSString *ff = [arr objectAtIndex:0];
        ddd = [ff intValue];
    }
    long cur = (long)days.length - ddd;
    NSLog(@"Left Days %ld = %ld - %ld",cur,(long)days.length,ddd);
    return cur;
}


-(BOOL)isLastSevenDaysOfMonth{
    
    return false;
}

+ (BOOL)saveImageToDocumentDirectoryOfAppImage:(UIImage*)image modulID:(NSString*)modulID nameOfImage:(NSString*)name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSError * error = nil;
    NSString* finalPath = [NSString stringWithFormat:@"%@/%@",documentsDirectory,modulID];
    [[NSFileManager defaultManager] createDirectoryAtPath:finalPath
                              withIntermediateDirectories:YES
                                               attributes:nil
                                                    error:&error];
    if (error != nil)
    {
        NSLog(@"error creating directory: %@", error);
        //..
    }
    
    NSString *savedImagePath = [finalPath stringByAppendingPathComponent:name];
     // imageView is my image from camera
    NSData *imageData = UIImageJPEGRepresentation(image,1);
    return [imageData writeToFile:savedImagePath atomically:NO];
}
+ (UIImage*)getImageFromDocumentDirectoryOfAppImageName:(NSString*)imageName modulID:(NSString*)modulID{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *getImagePath = [[NSString stringWithFormat:@"/%@/%@/",documentsDirectory,modulID] stringByAppendingPathComponent:imageName];
    
    return [UIImage imageWithContentsOfFile:getImagePath];
}
+ (UIImage *)imageFromColor:(UIColor *)color forSize:(CGSize)size withCornerRadius:(CGFloat)radius
{
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // Begin a new image that will be the new image with the rounded corners
    // (here with the size of an UIImageView)
    UIGraphicsBeginImageContext(size);
    
    // Add a clip before drawing anything, in the shape of an rounded rect
    [[UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:radius] addClip];
    // Draw your image
    [image drawInRect:rect];
    
    // Get the image, here setting the UIImageView image
    image = UIGraphicsGetImageFromCurrentImageContext();
    
    // Lets forget about that we were drawing
    UIGraphicsEndImageContext();
    
    return image;
}

@end


@implementation NSMutableArray (SelfSorting)

- (void)insertNumberAtSortedLocation:(NSNumber *)aNumber
{
    NSUInteger count = [self count];
    
    // if there are no contents yet, simply add it
    if (!count)
    {
        [self addObject:aNumber];
        return;
    }
    
    NSRange searchRange;
    searchRange.location = 0;
    searchRange.length = count;
    
    
    // bubble sort finding of insert point
    do
    {
        NSInteger index = searchRange.location + searchRange.length/2;
        
        NSNumber *testNumber = [self objectAtIndex:index];
        
        switch ([aNumber compare:testNumber])
        {
            case NSOrderedAscending:
            {
                //searchRange.length = searchRange.length / 2;
                searchRange.length = index - searchRange.location;
                break;
            }
            case NSOrderedDescending:
            {
                NSUInteger oldLocation = searchRange.location;
                searchRange.location = index+1;
                searchRange.length = searchRange.length - (searchRange.location - oldLocation);
                break;
            }
            case NSOrderedSame:
            {
                searchRange.length = 0;
                searchRange.location = index;
                break;
            }
        }
    }	while  (searchRange.length > 0);
    
    // insert at found point
    [self insertObject:aNumber atIndex:searchRange.location];
}

@end


static char kALGSessionDataTaskKey;


@implementation UIImageView (ImageFromWebDataTask)

-(void)setDataTask:(NSURLSessionDataTask*)dataTask
{
    objc_setAssociatedObject(self, &kALGSessionDataTaskKey, dataTask, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(NSURLSessionDataTask*)dataTask
{
    return (NSURLSessionDataTask *)objc_getAssociatedObject(self, &kALGSessionDataTaskKey);
}

-(void)setImageWithURL:(NSURL*)imageURL usingSession:(NSURLSession*)session
{
    if (self.dataTask) {
        [self.dataTask cancel];
    }
    //scale:(CGFloat)scale
    
  
    
    if (imageURL)
    {
        __weak typeof(self) weakSelf = self;
        
//        UIActivityIndicatorView* activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
//        [activityIndicator startAnimating];
//        activityIndicator.center = self.center;
//        [activityIndicator sizeToFit];
//        [self addSubview:activityIndicator];
        //[self bringSubviewToFront:activityIndicator];
        
        
        self.dataTask = [session dataTaskWithURL:imageURL
                               completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                   __strong __typeof(weakSelf) strongSelf = weakSelf;
                                   if (error) {
                                      // NSLog(@"ERROR: %@", error);
                                   }
                                   else {
                                       NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse *)response;
                                       if (200 == httpResponse.statusCode) {
                                           UIImage * image = [UIImage imageWithData:data];
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               
                                               //[activityIndicator stopAnimating];
                                               //[activityIndicator removeFromSuperview];
                                               
                                               strongSelf.image = image;
                                               
                                           });
                                       } else {
                                           NSLog(@"Couldn't load image at URL: %@", imageURL);
                                           NSLog(@"HTTP %ld", (long)httpResponse.statusCode);
                                       }
                                   }
                               }];
        [self.dataTask resume];
    }
    return;
}



@end
//+ (UIImage *)imageWithColor:(UIColor *)color {
//    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
//    UIGraphicsBeginImageContext(rect.size);
//    CGContextRef context = UIGraphicsGetCurrentContext();
//
//    CGContextSetFillColorWithColor(context, [color CGColor]);
//    CGContextFillRect(context, rect);
//
//    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//
//    return image;
//}
@implementation NSString (Validation)

- (BOOL)isValidEmail {
    NSString *regex = @"[^@]+@[A-Za-z0-9.-]+\\.[A-Za-z]+";
    NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [emailPredicate evaluateWithObject:self];
}

- (BOOL)isValidPassword {
    return (self.length >= 1);
}

- (BOOL)isValidName {
    return (self.length >= 1);
}


@end
