
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import <netdb.h>
#define allTrim( object ) [object stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]

/*
 *  System Versioning Preprocessor Macros
 */

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)


@interface Helper : NSObject<AVAudioPlayerDelegate>
{
	AVPlayerLayer *playerLayer;
}

@property (nonatomic, strong) AVPlayer *playerVideo;

+ (Helper *) sharedHelper;
+(CGRect)mainScreenBound;
/*!
 @method viewFromNib
 @abstract Load view from Nib Files
 */
+ (UIView *)viewFromNib:(NSString *)sNibName;
/*!
 @method viewFromNib
 @abstract Load view from Nib Files for dynemic class
 */
+ (id)viewFromNib:(NSString *)sNibName sViewName:(NSString *)sViewName;
/*!
 @method performBlock
 @abstract Perform Block Operation after delay
 */
+(void)performBlock:(void (^)(void))block afterDelay:(NSTimeInterval)delay;

#pragma mark - UserDefault

+(int)getPREFint:(NSString *)sKey;
+(id)getPREFID:(NSString *)sKey;
+(void) setPREFID:(id)sValue :(NSString *)sKey;
+(void) setPREFStringValue:(NSString *)sValue sKey:(NSString *)sKey;
+(void) setUserValue:(NSDictionary *)sValue sKey:(NSString *)sKey;
+(void)setDataValue:(NSData *)data sKey:(NSString *)sKey;
+(void) setPREFint:(int)iValue :(NSString *)sKey;
+(void) setUserArrayValue:(NSArray *)sValue sKey:(NSString *)sKey;
+(void) setLocationArrayValue:(NSMutableArray *)sValue sKey:(NSString *)sKey;
+(void) delPREF:(NSString *)sKey;
+(NSString *)getPREF:(NSString *)sKey;
+(NSInteger)getPREFNSint:(NSString *)sKey;
+(NSMutableArray *) getLocationArrayValue:(NSString *)sKey;
+(NSDictionary *) getUserValue:(NSString *)sKey;
+(NSData *)getDataValue:(NSString *)sKey;
+(long)get3daysMonth;
+(NSString *)getCurMonth;
#pragma mark - Theme Color
+(UIColor *)privateColor;
+(UIColor *)BusinessColor;
+(UIColor *)defaultColor;
+(UIColor *)greenColor;
+(void)setBorderWithColorInView:(UIView *)view;
+(void)setBorderWithColorInText:(UITextField *)txt;
+(void)setBorderWithColorInButton:(UIButton *)btn;
+(void)setBorderWithColorInLable:(UILabel *)lbl;

+(void) displayAlertView :(NSString *) title message :(NSString *) message;

/*!
 @method Check getBuildNumber contail key
 @abstract return build number
 */
+(NSString *)getBuildNumber;

/*!
 @method Check getVersionNumber contail key
 @abstract return version number
 */
+(NSString *)getVersionNumber;

/*!
 @method Check dictionary contail key
 @abstract Contain key then return true
 */
+ (BOOL)containsKey: (NSString *)key data:(NSDictionary *)dict;

/*!
 @method iSConnectedToNetwork
 @absttract To Check Network Connection
*/
+(BOOL) iSConnectedToNetwork;

/*!
 @method getDigitFromString
 @absttract To Get Digit From String
 */

+(NSString *)getPreviousMonth;

+ (NSString* )getDigitFromString:(NSString*)inputString;
/*!
 @method getDateFromString
 @absttract To Get Digit From String
 */
+(NSString*)getDateFromString:(NSString*)dateStr;
/*!
 @method saveImageToDocumentDirectoryOfAppImage
 @absttract To SaveImage in Application Own Document Directory
 */
//+ (BOOL)saveImageToDocumentDirectoryOfAppImage:(UIImage*)image nameOfImage:(NSString*)name;
+ (BOOL)saveImageToDocumentDirectoryOfAppImage:(UIImage*)image modulID:(NSString*)modulID nameOfImage:(NSString*)name;

/*!
 @method getImageFromDocumentDirectoryOfAppImageName
 @absttract To Get Image from Application Own Document Directory
 */

//+ (UIImage*)getImageFromDocumentDirectoryOfAppImageName:(NSString*)imageName;
+ (UIImage*)getImageFromDocumentDirectoryOfAppImageName:(NSString*)imageName modulID:(NSString*)modulID;
/*!
 @method imageFromColor
 @absttract To Get Image from color, size and CornerRadius
 */
+ (UIImage *)imageFromColor:(UIColor *)color forSize:(CGSize)size withCornerRadius:(CGFloat)radius;
+(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width;
+(NSString *)isStringIsNull:(NSString *)value;
@end

@interface NSMutableArray (SelfSorting)

- (void)insertNumberAtSortedLocation:(NSNumber *)aNumber;

@end


/**
 This is a lightweight category for adding asynchronous image loading to UIImageView. If you
 need something maximally configurable, full-featured, and compatible with old versions of iOS,
 you want AFNetworking's UIImageView category. If you want something you can fully comprehend in
 five minutes, this is it.
 
 The approach is just to use framework components as much as possibe.
 
 Instead of using NSOperationQueue to queue requests, use iOS7's NSURLSession. It will
 enqueue requests, restrict simultaneous connections, and allow group cancellation.
 
 Instead of creating a UIImage cache, set the NSURLCachePolicy on that session. This cache will
 spare you from repeated retrieval of the same data, which is the main thing to avoid. (However, I
 do not believe it will save you from the performance cost of repeated image decoding.)
 */
@interface UIImageView (ImageFromWebDataTask)

/**
 Asynchronously load an image from imageURL using session, canceling any previous request.
 
 @param imageURL The URL of the image.
 @param scale the scale factor applied to the image.
 @param session the NSURLSession to use for the request
 
 @discussion
 
 If you want caching of loaded images, then configure the URLCache and requestCachePolicy of your NSURLSession.
 For instance, to cache data aggressively (igorning expiration headers) you could do
 
 NSURLSessionConfiguration * config = [NSURLSessionConfiguration defaultSessionConfiguration];
 config.requestCachePolicy = NSURLRequestReturnCacheDataElseLoad;
 NSURLSession * session = [NSURLSession sessionWithConfiguration:config];
 
 
 Or to use a dedicated cache (separate from the shared default cache), you could do
 
 NSURLSessionConfiguration * config = [NSURLSessionConfiguration defaultSessionConfiguration];
 config.URLCache = [[NSURLCache alloc] initWithMemoryCapacity:1000000
 diskCapacity:2000000
 diskPath:@"headshotCache"];
 NSURLSession * session = [NSURLSession sessionWithConfiguration:config];
 
 
 If you want a placeholder image before loading completes, then just set that as the image before starting loading. For instance, you could do
 
 imageView.image = [UIImage imageNamed:@"myplaceholder"];
 [imageView setImageWithURL:myURL usingSession:session];
 
 */

-(void)setImageWithURL:(NSURL*)imageURL usingSession:(NSURLSession*)session;

@end

@interface NSString (Validation)
- (BOOL)isValidEmail;
- (BOOL)isValidPassword;
- (BOOL)isValidName;
@end


