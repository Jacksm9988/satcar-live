//
//  EditReimbursementViewController.h
//  SATCAR
//
//  Created by Percept Infotech on 27/02/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditReimbursementViewController : UIViewController<UITextFieldDelegate>
{
    NSString *Year,*FromKm,*ToKm,*Amount,*Country,*Currency,*remId;
}
@property (strong, nonatomic)NSString *Year,*FromKm,*ToKm,*Amount,*Country,*Currency,*remId;
@property (weak, nonatomic) IBOutlet UILabel *lblReimHead;
@property (weak, nonatomic) IBOutlet UITextField *txtYear;
@property (weak, nonatomic) IBOutlet UITextField *txtFromKm;
@property (weak, nonatomic) IBOutlet UITextField *txtToKm;
@property (weak, nonatomic) IBOutlet UITextField *txtAmount;
@property (weak, nonatomic) IBOutlet UITextField *txtCountry;
@property (weak, nonatomic) IBOutlet UITextField *txtCourncy;
@property (weak, nonatomic) IBOutlet UIButton *btnUpdate;
- (IBAction)UpdateData:(id)sender;

@end
