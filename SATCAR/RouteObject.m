//
//  RouteObject.m
//  SATCAR
//
//  Created by Percept Infotech on 2018-05-16.
//  Copyright © 2018 Percept Infotech. All rights reserved.
//

#import "RouteObject.h"

@interface RouteObject ()

@end

@implementation RouteObject

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)routeWithOrigin:(CLLocationCoordinate2D)start destination:(CLLocationCoordinate2D)destination{
    self.start = start;
    self.end = destination;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
