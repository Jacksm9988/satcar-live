//
//  InsuranceRedCardVC.h
//  SATCAR
//
//  Created by Percept Infotech on 17/03/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InsuranceRedCardVC : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *btnIOS;
@property (weak, nonatomic) IBOutlet UIButton *btnAndroid;
- (IBAction)onIOS:(id)sender;
- (IBAction)onAndroid:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;

@end
