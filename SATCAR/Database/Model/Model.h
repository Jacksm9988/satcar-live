//
//  Model.h
//  DataBaseDemo
//
//  Created by TheAppGuruz-New-6 on 22/02/14.
//  Copyright (c) 2014 TheAppGuruz-New-6. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Model : NSObject
@property (nonatomic,strong) NSString *u_id;
@property (nonatomic,strong) NSString *trip_id;
@property (nonatomic,strong) NSString *user_id;
@property (nonatomic,strong) NSString *trip_type;
@property (nonatomic,strong) NSString *start_address;
@property (nonatomic,strong) NSString *end_address;
@property (nonatomic,strong) NSString *trip_date;
@property (nonatomic,strong) NSString *start_time;
@property (nonatomic,strong) NSString *end_time;
@property (nonatomic,strong) NSString *odometer_start;
@property (nonatomic,strong) NSString *odometer_finish;
@property (nonatomic,strong) NSString *purpose_trip;
@property (nonatomic,strong) NSString *distance;
@property (nonatomic,strong) NSString *note;
@property (nonatomic,strong) NSString *locations;
@property (nonatomic,strong) NSString *locations1;
@end
