//
//  ModelManager.m
//  DataBaseDemo
//
//  Created by TheAppGuruz-New-6 on 22/02/14.
//  Copyright (c) 2014 TheAppGuruz-New-6. All rights reserved.
//

#import "ModelManager.h"


@implementation ModelManager
static ModelManager *instance=nil;

@synthesize database=_database;

+(ModelManager *) getInstance
{
    
    if(!instance)
    {
        instance=[[ModelManager alloc]init];
        instance.database=[FMDatabase databaseWithPath:[Util getFilePath:@"SATCAR_database.sqlite"]];
    }
    return instance;
}

-(void)insertData:(Model *)data
{
    [instance.database open];
    
    if ([data.trip_id isEqualToString:@""]) {
        data.trip_id = @" ";
    }if ([data.start_address isEqualToString:@""]) {
        data.start_address = @" ";
    }if ([data.end_address isEqualToString:@""]) {
        data.end_address = @" ";
    }if ([data.note isEqualToString:@""]) {
        data.note = @" ";
    }if ([data.distance isEqualToString:@""]) {
        data.distance = @" ";
    }if ([data.locations isEqualToString:@""]) {
        data.locations = @" ";
    }if ([data.purpose_trip isEqualToString:@""]) {
        data.purpose_trip = @" ";
    }
    
    NSString *insertSQL = [NSString stringWithFormat: @"INSERT INTO tbl_trip (trip_id,user_id,trip_type,start_address,end_address,trip_date,start_time,end_time,odometer_start,odometer_finish,purpose_trip,distance,note,locations,locations1) VALUES (\"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\",\"%@\",\"%@\")", data.trip_id,data.user_id,data.trip_type,data.start_address,data.end_address,data.trip_date,data.start_time,data.end_time,data.odometer_start,data.odometer_finish,data.purpose_trip,data.distance,data.note,data.locations,data.locations1];
    
    BOOL isInserted=[instance.database executeUpdate:insertSQL];
    [instance.database close];
    
    if(isInserted)
        NSLog(@"Inserted Successfully");
    else
        NSLog(@"Error occured while inserting");
}

-(void)updateData:(Model *)data
{
    /*[instance.database open];
    BOOL isUpdated=[instance.database executeUpdate:@"UPDATE tbl_trip SET name=? WHERE rollnum=?",data.name,data.rollnum];
    [instance.database close];
    
    if(isUpdated)
        NSLog(@"Updated Successfully");
    else
        NSLog(@"Error occured while Updating");*/
    NSLog(@"updateData function commented");
    
}

-(void)deleteTripData:(NSString *)unique_id{
    [instance.database open];
    
    NSString *insertSQL = [NSString stringWithFormat:@"DELETE FROM tbl_trip WHERE u_id = %@",unique_id];
    
    BOOL isDeleted = [instance.database executeUpdate:insertSQL];
    [instance.database close];
    
    if(isDeleted)
        NSLog(@"Deleted Successfully");
    else
        NSLog(@"Error occured while Deleting");
}

-(void)deleteAllTripData:(NSString *)user_id{
    [instance.database open];
    NSString *insertSQL = [NSString stringWithFormat:@"DELETE FROM tbl_trip WHERE user_id = %@",user_id];
    BOOL isDeleted = [instance.database executeUpdate:insertSQL];
    [instance.database close];
    if(isDeleted)
        NSLog(@"Deleted Successfully");
    else
        NSLog(@"Error occured while Deleting");
}

-(BOOL)checkOfflineTripAvailable:(NSString *)user_id{
    [instance.database open];
    NSString *insertSQL = [NSString stringWithFormat:@"SELECT * FROM tbl_trip WHERE user_id = %@",user_id];
    FMResultSet *resultSet = [instance.database executeQuery:insertSQL];
    if(resultSet){
        return true;
    }else{
        return false;
    }
}

-(NSMutableArray *)getAllLocalTrip:(NSString *)user_id {
    [instance.database open];
    NSString *insertSQL = [NSString stringWithFormat:@"SELECT * FROM tbl_trip WHERE user_id = %@",user_id];
    
    FMResultSet *resultSet = [instance.database executeQuery:insertSQL];
    NSMutableArray *tripArray = [[NSMutableArray alloc]init];
    if(resultSet)
    {
        while([resultSet next]){
            
            NSString *u_id = [resultSet stringForColumn:@"u_id"];
            NSString *trip_id = [resultSet stringForColumn:@"trip_id"];
            NSString *user_id = [resultSet stringForColumn:@"user_id"];
            NSString *trip_type = [resultSet stringForColumn:@"trip_type"];
            NSString *start_address = [resultSet stringForColumn:@"start_address"];
            NSString *end_address = [resultSet stringForColumn:@"end_address"];
            NSString *trip_date = [resultSet stringForColumn:@"trip_date"];
            NSString *start_time = [resultSet stringForColumn:@"start_time"];
            NSString *end_time = [resultSet stringForColumn:@"end_time"];
            NSString *odometer_start = [resultSet stringForColumn:@"odometer_start"];
            NSString *odometer_finish = [resultSet stringForColumn:@"odometer_finish"];
            NSString *purpose_trip = [resultSet stringForColumn:@"purpose_trip"];
            NSString *distance = [resultSet stringForColumn:@"distance"];
            NSString *note = [resultSet stringForColumn:@"note"];
            NSString *locations = [resultSet stringForColumn:@"locations"];
            NSString *locations1 = [resultSet stringForColumn:@"locations1"];
            
            if ([trip_id isEqualToString:@" "]) {
                trip_id = @"";
            }if ([start_address isEqualToString:@""]) {
                start_address = @"";
            }if ([end_address isEqualToString:@""]) {
                end_address = @"";
            }if ([note isEqualToString:@""]) {
                note = @"";
            }if ([distance isEqualToString:@""]) {
                distance = @"";
            }if ([locations isEqualToString:@""]) {
                locations = @"";
            }if ([purpose_trip isEqualToString:@""]) {
                purpose_trip = @"";
            }
            
            NSDictionary *parameter = @{@"u_id":u_id,@"trip_id":trip_id, @"userid":user_id, @"trip_type":trip_type, @"start_address":start_address, @"end_address":end_address, @"trip_date":trip_date, @"start_time":start_time, @"end_time":end_time, @"odometer_start":odometer_start, @"odometer_finish":odometer_finish,@"purpose_trip":purpose_trip, @"distance":distance, @"note":note, @"locations":locations, @"locations1":locations1};
            NSLog(@"Data : %@",parameter);
            [tripArray addObject:parameter];
        }
        NSLog(@"Found %d rows",(int)tripArray.count);
    }
    [instance.database close];
    return tripArray;
}

@end
