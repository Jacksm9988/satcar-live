//
//  ViewController.m
//  SATCAR
//
//  Created by Percept Infotech on 12/7/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import "ViewController.h"
#import "ECSlidingViewController.h"
#import "LeftSideViewController.h"
#import "NavigationController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "UIViewController+VCHelpers.h"
#import "HomeViewController.h"
#import "RegisterPayVC.h"
//#import <ThePerfectApp/ThePerfectApp.h>
#import <CoreBluetooth/CoreBluetooth.h>
@interface ViewController ()<CBCentralManagerDelegate>
{
    AppDelegate *appdelegate; 
    NSString *userID,*tripCount,*userType,*isLogin;
    NSDictionary *dataDict,*info;
    BOOL isBluetoothOn;
}
@property (strong, nonatomic) CBCentralManager *bluetoothManager;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"is_menushow"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    isLogin = [Helper getPREF:@"islogin"];
   
    if(!_bluetoothManager) {
        _bluetoothManager = [[CBCentralManager alloc] initWithDelegate:self queue:dispatch_get_main_queue()] ;
    }
    [self centralManagerDidUpdateState:_bluetoothManager]; // Show initial state*/
    
    
    userID = @"";
}

-(void)viewWillAppear:(BOOL)animated
{
   // [[TPAAnalytics sharedInstance] send:[TPAAppEventMessage createScreenDidAppearMessageForScreen:NSLocalizedString(@"Homescreen", @"") metaInfo:nil]];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    _BtnResiter.layer.cornerRadius = 25;
    _BtnResiter.layer.borderWidth = 1;
    _BtnResiter.layer.borderColor =[UIColor colorWithRed:0.000 green:0.651 blue:0.929 alpha:1.00].CGColor;
    
    _btnLogin.layer.cornerRadius = 25;
    _btnLogin.layer.borderWidth = 1;
    _btnLogin.layer.borderColor = [UIColor colorWithRed:0.000 green:0.651 blue:0.929 alpha:1.00].CGColor;
     [_BtnResiter setTitle:NSLocalizedString(@"Register", @"") forState:UIControlStateNormal];
     [_btnLogin setTitle:NSLocalizedString(@"Login", @"") forState:UIControlStateNormal];
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        return;
    }else{
        if ([[Helper getPREF:@"islogin"] isEqualToString:@"1"]) {
            appdelegate.appLogin = @"1";
            
            NSData *userdata = [Helper getDataValue:@"userInfo"];
            if (userdata != nil) {
                dataDict = [self dataToDicitonary:userdata];
                if (dataDict != nil) {
                    info = [dataDict valueForKey:@"data"];
                    NSDictionary *userInfo = [info valueForKey:@"user"];
                    userID = [userInfo valueForKey:@"id"];
                    NSDictionary *paymentInfo = [info valueForKey:@"payment_info"];
                    userType = [Helper isStringIsNull:[info valueForKey:@"user_type"]];
                    NSString *status = [paymentInfo valueForKey:@"status"];
                    NSString *parent = [Helper isStringIsNull:[info valueForKey:@"parent"]];
                    
                    if (userID == nil) {
                        userID = @"";
                    }
                    
                    if ([userType isEqualToString:@"12"]) {
                        //NSLog(@"from viewcontroller");
                        [self gotoHomepage];
                    }else{
                        if ([parent integerValue] > 0) {
                            [self gotoHomepage];
                        }else{
                            if(status == (NSString *)[NSNull null] || status == nil){
                                [self moveToLogin];
                            }else if([status.lowercaseString isEqualToString:@"pending"] && status.length != 0) {
                                //[self performSegueWithIdentifier:@"segueInitialRenew" sender:self];
                                [self performSegueWithIdentifier:@"segueProcessToPay" sender:self];
                            }else {
                                [self gotoHomepage];
                            }
                        }
                    }
                }
            }
        }else{
            appdelegate.appLogin = @"0";
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)RegisterPress:(id)sender {
    [_btnLogin setBackgroundColor:[UIColor whiteColor]];
    [_btnLogin setTitleColor:[UIColor colorWithRed:0.000 green:0.651 blue:0.933 alpha:1.00] forState:UIControlStateNormal];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self performSegueWithIdentifier:@"segueRegisterUserType" sender:nil];
}

- (IBAction)LoginPress:(id)sender {
    [_BtnResiter setBackgroundColor:[UIColor whiteColor]];
    [_BtnResiter setTitleColor:[UIColor colorWithRed:0.000 green:0.651 blue:0.933 alpha:1.00] forState:UIControlStateNormal];
    [self performSegueWithIdentifier:@"Loginpage" sender:nil];
}


-(void)showBluthoothAlert
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message: NSLocalizedString(@"bluetooth_permission_message", @"") preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Ok", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [alert dismissViewControllerAnimated:true completion:nil];
    }];
    
    [alert addAction:cancel];
    [self presentViewController:alert animated:true completion:nil];
}

#pragma mark - central manager delegate
- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    NSString *stateString = nil;
    switch(_bluetoothManager.state) {
        case CBCentralManagerStateResetting:
            isBluetoothOn = false;
             [[NSUserDefaults standardUserDefaults] setBool:isBluetoothOn forKey:@"BluetoothStat"];
           // [_swtchBluetooth setOn:false animated:true];
            //[_btnStart setTitle:@"stop" forState:UIControlStateNormal];
            break;
            
        case CBCentralManagerStateUnsupported:
        case CBCentralManagerStatePoweredOff:
            stateString = @"Bluetooth is currently powered off.";
             isBluetoothOn = false;
            [self showBluthoothAlert];
            [[NSUserDefaults standardUserDefaults] setBool:isBluetoothOn forKey:@"BluetoothStat"];
            
           
           // [_swtchBluetooth setOn:false animated:true];
            //[_btnStart setTitle:@"stop" forState:UIControlStateNormal];
            //_beaconsArray  = [[NSMutableArray alloc]init];
            //[_tableview reloadData];
            break;
            
        case CBCentralManagerStatePoweredOn:
            stateString = @"Bluetooth is currently powered on and available to use.";
            
            isBluetoothOn = true;
             [[NSUserDefaults standardUserDefaults] setBool:isBluetoothOn forKey:@"BluetoothStat"];
            //[_swtchBluetooth setOn:true animated:true];
            //[_btnStart setTitle:@"start" forState:UIControlStateNormal];
            //[self initializeBeacon];
            
            break;
        default:
            stateString = @"State unknown, update imminent.";
            isBluetoothOn = false;
             [[NSUserDefaults standardUserDefaults] setBool:isBluetoothOn forKey:@"BluetoothStat"];
           // [_swtchBluetooth setOn:false animated:true];
            //[_btnStart setTitle:@"stop" forState:UIControlStateNormal];
            //_beaconsArray  = [[NSMutableArray alloc]init];
            //[_tableview reloadData];
            break;
    }
    NSLog(@"Bluetooth State : %@",stateString);
}


-(void)changed:(id)sender{
    /*if ([sender isOn]) {
        
        isBluetoothOn = true;
        if (_bluetoothManager.state != CBCentralManagerStatePoweredOn) {
            NSURL *url = [NSURL URLWithString:@"prefs:root=Bluetooth"];
            if ([[UIApplication sharedApplication] canOpenURL:url]) {   //Pre iOS 10
                [[UIApplication sharedApplication] openURL:url];
            } else  {   //iOS 10
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-Prefs:root=Bluetooth"]];
            }
        }
       // [_btnStart setTitle:@"start" forState:UIControlStateNormal];
        //[activityIndicator startAnimating];
        
    } else {
        
        isBluetoothOn = false;
        NSURL *url = [NSURL URLWithString:@"prefs:root=Bluetooth"];
        if ([[UIApplication sharedApplication] canOpenURL:url]) {   //Pre iOS 10
            [[UIApplication sharedApplication] openURL:url];
        } else  {   //iOS 10
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-Prefs:root=Bluetooth"]];
        }
       // [_btnStart setTitle:@"stop" forState:UIControlStateNormal];
        //[activityIndicator stopAnimating];
    }*/
}

-(void)refreshItems{
    NSLog(@"b state : %d",(int)_bluetoothManager.state);
    
    if (_bluetoothManager.state == CBCentralManagerStatePoweredOn) {
        //[_swtchBluetooth setOn:true animated:true];
       // [_btnStart setTitle:@"start" forState:UIControlStateNormal];
        //[activityIndicator startAnimating];
        
    }else{
        //[_swtchBluetooth setOn:false animated:true];
        //[_btnStart setTitle:@"stop" forState:UIControlStateNormal];
        //[activityIndicator stopAnimating];
    }
}



#pragma mark - Custom functions
-(void)gotoHomepage {
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    HomeViewController* controller = [storyboard instantiateViewControllerWithIdentifier:@"Home"];
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)gotoSplitUser{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    TripViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"TripPage"];
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    if ([segue.identifier isEqualToString:@"segueProcessToPay"]) {
        RegisterPayVC *controller = [storyboard instantiateViewControllerWithIdentifier:@"RegisterPayVC"];
        controller.userInfo = info;
    }else if([segue.identifier isEqualToString:@"Loginpage"]) {
        
    }else if([segue.identifier isEqualToString:@"Registerpage"]) {
        
    }
    
}
@end
