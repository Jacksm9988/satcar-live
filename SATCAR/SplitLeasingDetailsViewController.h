//
//  SplitLeasingDetailsViewController.h
//  SATCAR
//
//  Created by Percept Infotech on 30/12/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SplitLeasingDetailsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintTblHeight;

@property (weak, nonatomic) IBOutlet UILabel *lblAmountPaid;
@property (weak, nonatomic) IBOutlet UILabel *lblDKK;
@property (weak, nonatomic) IBOutlet UILabel *lblMonthlyCost;
@property (weak, nonatomic) IBOutlet UILabel *lblDurationCost;
@property (weak, nonatomic) IBOutlet UILabel *lblMonth;

@property (weak, nonatomic) IBOutlet UITextField *txtAmountPaid;
@property (weak, nonatomic) IBOutlet UITextField *txtMonthlyCost;
@property (weak, nonatomic) IBOutlet UITextField *txtDurationCost;

@property (weak, nonatomic) IBOutlet UIButton *btnSendEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblSendMonthlyInvoice;

@end
