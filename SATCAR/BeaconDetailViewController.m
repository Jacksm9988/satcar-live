//
//  BeaconDetailViewController.m
//  SATCAR
//
//  Created by Percept Infotech on 21/04/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import "BeaconDetailViewController.h"

@interface BeaconDetailViewController ()<KTKBeaconManagerDelegate>{
    BOOL status;
    AppDelegate *appdelegate;
}

@end

@implementation BeaconDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addTitleView:[_beaconDetail valueForKey:@"navTitle"]];
    appdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    _lblID.text = NSLocalizedString(@"ID", @"");
    _lblMajor.text = NSLocalizedString(@"Major_lbl", @"");
    _lblMinor.text = NSLocalizedString(@"Minor_lbl", @"");
    _lblDistance.text = NSLocalizedString(@"Distance", @"");
    _lblBattery.text = NSLocalizedString(@"Battery", @"");
    [_btnDisconnect setTitle:NSLocalizedString(@"DISCONNECT", @"") forState:UIControlStateNormal];
    _btnDisconnect.layer.cornerRadius = 25;
    
    _lblIDValue.text =  [NSString stringWithFormat:@"%@",[_beaconDetail valueForKey:@"id"]];
    _lblMajorValue.text = [NSString stringWithFormat:@"%@",[_beaconDetail valueForKey:@"major"]];
    _lblMinorValue.text = [NSString stringWithFormat:@"%@",[_beaconDetail valueForKey:@"minor"]];
    _lblDistanceValue.text = [NSString stringWithFormat:@"%@",[_beaconDetail valueForKey:@"measuredPower"]];
    _lblBatteryValue.text = [NSString stringWithFormat:@"%@",[_beaconDetail valueForKey:@"rssi"]];
    
    if ([[_beaconDetail valueForKey:@"connectStatus"] isEqualToString:@"1"] ) {
        status = true;
        _btnDisconnect.hidden = false;
    }else{
        _btnDisconnect.hidden = true;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onDisconnect:(id)sender {
    NSString *beaconID = [_beaconDetail valueForKey:@"beaconID"];
    NSDictionary *parameter=@{ @"id":beaconID };
    [self callDeleteBeacon:parameter];
}

-(void)removeBeaconFromLocal{
    NSString *Major = [_beaconDetail valueForKey:@"major"];
    NSString *Minor = [_beaconDetail valueForKey:@"minor"];
    
    NSMutableArray *tempBeaconArray = [[Helper getPREFID:@"beaconsArray"] mutableCopy];
    for (int i =0; i < tempBeaconArray.count; i++) {
        
        NSDictionary *storeBeacon = [tempBeaconArray objectAtIndex:i];
        NSString *BeaconMajor = [storeBeacon valueForKey:@"major"];
        NSString *BeaconMinor = [storeBeacon valueForKey:@"minor"];
        
        if ([Major isEqualToString:BeaconMajor] && [Minor isEqualToString:BeaconMinor]) {
            [tempBeaconArray removeObjectAtIndex:i];
        }
        [appdelegate stopBeaconDetection];
        if (tempBeaconArray.count <= 0) {
            [Helper setPREFStringValue:@"0" sKey:@"isBeaconConnected"];
            tempBeaconArray = [[NSMutableArray alloc]init];
        }
        [Helper setUserArrayValue:tempBeaconArray sKey:@"beaconsArray"];
        appdelegate.BeaconArray = [tempBeaconArray mutableCopy];
        [appdelegate beaconDetection];
    }
}


-(void)removeBeaconFromLocal1{
    NSString *Major = [_beaconDetail valueForKey:@"major"];
    NSString *Minor = [_beaconDetail valueForKey:@"minor"];
    
    NSMutableArray *tempBeaconArray = [[Helper getPREFID:@"beaconsArray"] mutableCopy];
    for (int i =0; i < tempBeaconArray.count; i++) {
        
        NSDictionary *storeBeacon = [tempBeaconArray objectAtIndex:i];
        NSString *BeaconMajor = [storeBeacon valueForKey:@"major"];
        NSString *BeaconMinor = [storeBeacon valueForKey:@"minor"];
        
        if ([Major isEqualToString:BeaconMajor] && [Minor isEqualToString:BeaconMinor]) {
            [tempBeaconArray removeObjectAtIndex:i];
            NSString *uuid = [[tempBeaconArray objectAtIndex:i] valueForKey:@"proximityUUID"];
            
            NSUUID *proximityUUID = [[NSUUID alloc] initWithUUIDString:uuid];
            KTKBeaconRegion *region = [[KTKBeaconRegion alloc]initWithProximityUUID:proximityUUID major:[BeaconMajor integerValue] minor:[BeaconMinor integerValue] identifier:[NSString stringWithFormat:@"%d",i+1]];
            
            region.notifyOnEntry = false;
            region.notifyOnExit = false;
            region.notifyEntryStateOnDisplay = false;
            [appdelegate.KTKbeaconManager stopMonitoringForRegion:region];
            
            [tempBeaconArray removeObjectAtIndex:i];
            if (tempBeaconArray.count <= 0) {
                [Helper setPREFStringValue:@"0" sKey:@"isBeaconConnected"];
            }
            [Helper setUserArrayValue:tempBeaconArray sKey:@"beaconsArray"];
            appdelegate.BeaconArray = [tempBeaconArray mutableCopy];
           // [appdelegate beaconDetection];
        }
    }
}


- (void)callDeleteBeacon:(NSDictionary *)parameter{
    [self.view endEditing:YES];
    
    if(![self CheckNetworkConnection]) {
        return;
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //NSLog(@"parameter : %@",parameter);
    
    [WebService httpPostWithCustomDelegateURLString:@".deleteBeep" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         if (error == nil) {
             NSError *localError = nil;
             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
             
             if (localError != nil) {
                 NSLog(@"%@",localError.description);
             }
             else {
                 NSDictionary *response = [parsedObject objectForKey:@"data"];
                 NSString* message = [response objectForKey:@"message"];
                 NSString* success = [response objectForKey:@"status"];
                 NSLog(@"Response : %@",response);
                 if ([success isEqualToString:@"Success"]) {
                     
                     NSArray *beaconsArray = [response valueForKey:@"beep"];
                     NSLog(@"Array : %@",beaconsArray);
                     [self removeBeaconFromLocal];
                     
                     [self.navigationController popViewControllerAnimated:true];
                     _btnDisconnect.hidden = true;
                 } else {
                     NSLog(@"error :: %@",message);
                     [Helper displayAlertView:@"" message:message];
                 }
             }
         }
     }];
}
@end
