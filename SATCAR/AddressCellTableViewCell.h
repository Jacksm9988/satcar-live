//
//  AddressCellTableViewCell.h
//  SATCAR
//
//  Created by Percept Infotech on 18/01/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddressCellTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *viewBAck;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblAddressDetail;
@property (weak, nonatomic) IBOutlet UIImageView *imgSearchType;

@end
