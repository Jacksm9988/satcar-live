//
//  AddressSearchViewController.m
//  SATCAR
//
//  Created by Percept Infotech on 18/01/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import "AddressSearchViewController.h"
#import <GooglePlaces/GooglePlaces.h>
#import "AddressCellTableViewCell.h"

@interface AddressSearchViewController ()<GMSAutocompleteFetcherDelegate>
{
    UITextField *_textField;
    UITextView *_resultText;
    GMSAutocompleteFetcher* _fetcher;
    
    UITableViewController *_resultsController;
    GMSAutocompleteTableDataSource *_tableDataSource;
    GMSPlacesClient *_placesClient;
    GMSAutocompleteFilter *filter;
    NSMutableArray *addressArray,*locationArray,*searchLocationArray;
    NSMutableDictionary *dict;
    NSString *primary,*secondary;
    GMSAutocompletePrediction *predictionRow;
 
}
@end

@implementation AddressSearchViewController
@synthesize fromScreen,fromScreenField;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    addressArray = [[NSMutableArray alloc]init];
    searchLocationArray = [[NSMutableArray alloc]init];
    // Set bounds to inner-west Sydney Australia.
    CLLocationCoordinate2D neBoundsCorner = CLLocationCoordinate2DMake(-33.843366, 151.134002);
    CLLocationCoordinate2D swBoundsCorner = CLLocationCoordinate2DMake(-33.875725, 151.200349);
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:neBoundsCorner coordinate:swBoundsCorner];
    
    // Set up the autocomplete filter.
    _placesClient = [GMSPlacesClient sharedClient];
    
    filter = [[GMSAutocompleteFilter alloc] init];
    filter.type = kGMSPlacesAutocompleteTypeFilterNoFilter ;
    //filter.country = @"DK";
    //acController.delegate = self;
    //acController.autocompleteFilter = filter;
    
    // Create the fetcher.
    _fetcher = [[GMSAutocompleteFetcher alloc] initWithBounds:bounds filter:filter];
    _fetcher.delegate = self;
    
    [_txtSearch addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    UIView *Rview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 30, 40)];
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 5, 30, 30)];
    [btn setImage:[UIImage imageNamed:@"img_black_close96.png"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(onClear:) forControlEvents:UIControlEventTouchUpInside];
    [Rview addSubview:btn];
    
    _txtSearch.rightView = Rview;
    _txtSearch.rightViewMode = UITextFieldViewModeAlways;
    
    locationArray = [[NSMutableArray alloc]init];
    locationArray = [Helper getPREFID:@"locationArray"];
    //NSLog(@"location array : %@",locationArray);
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];//UIKeyboardDidShowNotification
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [_txtSearch becomeFirstResponder];
}

- (IBAction)onClear:(id)sender{
    addressArray = [[NSMutableArray alloc]init];
    searchLocationArray = [[NSMutableArray alloc]init];
    [_tableview reloadData];
    _txtSearch.text = @"";
}


- (void)textFieldDidChange:(UITextField *)textField {
    //NSLog(@"%@", textField.text);
    [_fetcher sourceTextHasChanged:textField.text];
}

- (void)keyboardWasShown:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    int height = MIN(keyboardSize.height,keyboardSize.width);
    _IBLayoutConstraintTableBottom.constant = (self.view.frame.size.height-100) - (height + 50);
}

- (void)keyboardWasHide:(NSNotification *)notification
{
    _IBLayoutConstraintTableBottom.constant = 10;
}

#pragma mark - GMSAutocompleteFetcherDelegate
- (void)didAutocompleteWithPredictions:(NSArray *)predictions {
    
    addressArray = [[NSMutableArray alloc]init];
    searchLocationArray = [[NSMutableArray alloc]init];
    NSString *searchStr = _txtSearch.text;
    for (NSDictionary *locArray in locationArray) {
        NSLog(@"location : %@",locArray);
        NSString *value = [locArray valueForKey:@"name"];
        NSRange range = [value rangeOfString: searchStr options: NSCaseInsensitiveSearch];
        if (range.location != NSNotFound) {
            [searchLocationArray addObject:locArray];
        }
    }
    for (GMSAutocompletePrediction *prediction in predictions) {
        //NSLog(@"prediction : %@",prediction);
        [addressArray addObject:prediction];
    }
    [_tableview reloadData];
}

- (void)didFailAutocompleteWithError:(NSError *)error {
    //_resultText.text = [NSString stringWithFormat:@"%@", error.localizedDescription];
}

#pragma mark - Table View
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return searchLocationArray.count;
    }else{
        return addressArray.count;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 56;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    AddressCellTableViewCell *cell = (AddressCellTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.section == 0) {
        NSDictionary *address = [searchLocationArray objectAtIndex:indexPath.row];
        cell.lblAddress.text = [address valueForKey:@"name"];
        
        NSString *zip_city = [address valueForKey:@"zip_and_city"];
        NSArray *zipCityarray = [zip_city componentsSeparatedByString:@" "];
        if (zipCityarray.count > 0) {
            cell.lblAddressDetail.text = [NSString stringWithFormat:@"%@, %@, %@",[address valueForKey:@"address"],[zipCityarray objectAtIndex:0],[zipCityarray objectAtIndex:1]];
        }
        cell.imgSearchType.image = [UIImage imageNamed:@"map96.png"];
    }else{
        predictionRow = [addressArray objectAtIndex:indexPath.row];
        primary = [NSString stringWithFormat:@"%@",[predictionRow.attributedPrimaryText string]];
        secondary = [NSString stringWithFormat:@"%@",[predictionRow.attributedSecondaryText string]];
        cell.lblAddress.text = primary;
        cell.lblAddressDetail.text = secondary;
        cell.imgSearchType.image = [UIImage imageNamed:@"plus96.png"];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.view endEditing:true];
    if (indexPath.section == 0) {
       NSDictionary *address = [searchLocationArray objectAtIndex:indexPath.row];
        NSString *one = [address valueForKey:@"address"];
        NSArray *arr = [[address valueForKey:@"zip_and_city"] componentsSeparatedByString:@" "];
        NSString *city,*pincode;
        if (arr.count > 0) {
            city = [arr objectAtIndex:0];
            pincode = [arr objectAtIndex:1];
        }else{
            pincode = [arr objectAtIndex:0];
        }
        
        
        NSString *placeAddress = [NSString stringWithFormat:@"%@,",one];
        if (city.length > 0) {
            NSString *pcode = [NSString stringWithFormat:@"%@,",city];
            placeAddress = [NSString stringWithFormat:@"%@ %@",placeAddress,pcode];
        }if (pincode.length > 0) {
            NSString *pcode = [NSString stringWithFormat:@"%@,",pincode];
            placeAddress = [NSString stringWithFormat:@"%@ %@",placeAddress,pcode];
        }
        //[placeAddress stringByAppendingString:country];
        NSString *country = [address valueForKey:@"country"];
        placeAddress = [NSString stringWithFormat:@"%@ %@",placeAddress,country];
        //NSString *combine = [NSString stringWithFormat:@"%@,%@,%@,%@",one,pincode,city,country];
        [self postNotification:placeAddress];
        
    }else{
        
        predictionRow = [addressArray objectAtIndex:indexPath.row];
        NSString *placeID = predictionRow.placeID;
        
        [_placesClient lookUpPlaceID:placeID callback:^(GMSPlace *place, NSError *error) {
            if (error != nil) {
                NSLog(@"Place Details error %@", [error localizedDescription]);
                return;
            }
            
            if (place != nil) {
                NSString *placeAddress = @"";
               
                NSString *locality = @"",*country = @"",*postal_code = @"";
                for (int i = 0; i < place.addressComponents.count; i++) {
                    GMSAddressComponent *add = [place.addressComponents objectAtIndex:i];
                    //NSLog(@"type : %@",add.type);
                    //NSLog(@"type : %@",add.name);
                    if ([add.type containsString:@"locality"]) {
                        locality = add.name;
                    }else if ([add.type containsString:@"country"]) {
                        country = add.name;
                    }else if ([add.type containsString:@"postal_code"]) {
                        postal_code = add.name;
                    }
                }
                
                placeAddress = [NSString stringWithFormat:@"%@,",place.name];
                if (postal_code.length > 0) {
                    NSString *pcode = [NSString stringWithFormat:@"%@,",postal_code];
                    placeAddress = [NSString stringWithFormat:@"%@ %@",placeAddress,pcode];
                }if (locality.length > 0) {
                    NSString *pcode = [NSString stringWithFormat:@"%@,",locality];
                    placeAddress = [NSString stringWithFormat:@"%@ %@",placeAddress,pcode];
                }
                //[placeAddress stringByAppendingString:country];
                placeAddress = [NSString stringWithFormat:@"%@ %@",placeAddress,country];
                //placeAddress = [NSString stringWithFormat:@"%@, %@, %@, %@",place.name,postal_code,locality,country];
                
                NSLog(@"Address : %@",placeAddress);
                [self postNotification:placeAddress];
            } else {
                NSLog(@"No place details for %@", placeID);
            }
        }];
    }
    [self dismissViewControllerAnimated:true completion:^{}];
}

/*- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *sectionName;
    switch (section)
    {
        case 0:
            if ([searchLocationArray count] > 0 ) {
                sectionName = @"Suggestions from Local";
            }else{
                sectionName = @"";
            }
            break;
        case 1:
            if ([addressArray count] > 0 ) {
                sectionName = @"Suggestions from Google";
            }else{
                sectionName = @"";
            }
            break;
        default:
            sectionName = @"";
            break;
    }
    return sectionName;
}*/
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionHeaderView;
    
    sectionHeaderView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, tableView.frame.size.width, 25.0)];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame: CGRectMake(sectionHeaderView.frame.origin.x,sectionHeaderView.frame.origin.y , sectionHeaderView.frame.size.width, sectionHeaderView.frame.size.height)];
    
    headerLabel.backgroundColor = [UIColor whiteColor];
    [headerLabel setFont:[UIFont systemFontOfSize:12]];
    [sectionHeaderView addSubview:headerLabel];
    
    switch (section) {
        case 0:
            if ([searchLocationArray count] > 0 ) {
                headerLabel.text = @"Suggestions from Local";
                return sectionHeaderView;
            }else{
                return nil;
            }
        case 1:
            if ([addressArray count] > 0 ) {
                headerLabel.text = @"Suggestions from Google";
                return sectionHeaderView;
            }else{
                return nil;
            }
        default:
            break;
    }
    return sectionHeaderView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30.0f;
}

#pragma mark - Notification
- (void)postNotification:(NSString *)address //post notification method and logic
{
    NSString *notificationName = @"";
    NSString *key = @"AddressValue";
    NSDictionary *dictionary;
    if ([fromScreen isEqualToString:@"ManualTrip"]) {
        notificationName = @"ManualTripAddress";
        dictionary = [NSDictionary dictionaryWithObjectsAndKeys: address, key,fromScreenField, @"fromScreenField",@"address",@"screen", nil];
    }else if ([fromScreen isEqualToString:@"AddLocation"]){
        notificationName = @"LocationAddAddress";
        dictionary = [NSDictionary dictionaryWithObjectsAndKeys: address, key,fromScreenField, @"fromScreenField",@"address",@"screen", nil];
    }else if ([fromScreen isEqualToString:@"EditLocation"]){
        notificationName = @"LocationAddAddress";
        dictionary = [NSDictionary dictionaryWithObjectsAndKeys: address, key,fromScreenField, @"fromScreenField",@"LocationEdit",@"screen", nil];
    }else if ([fromScreen isEqualToString:@"ConfirmTrip"]){
        notificationName = @"ConfirmTripAddress";
        dictionary = [NSDictionary dictionaryWithObjectsAndKeys: address, key,fromScreenField, @"fromScreenField",@"address",@"screen", nil];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:nil userInfo:dictionary];
}

/*
#pragma mark - GMSAutocompleteTableDataSourceDelegate

- (void)tableDataSource:(GMSAutocompleteTableDataSource *)tableDataSource didAutocompleteWithPlace:(GMSPlace *)place {
    [_textField resignFirstResponder];
    //[self autocompleteDidSelectPlace:place];
    NSLog(@"Place : %@",place);
    NSString *placeID = place.placeID;
    
    NSString *address = [NSString stringWithFormat:@"%@,%@",place.name,place.formattedAddress];
    _textField.text = address;
}

- (void)tableDataSource:(GMSAutocompleteTableDataSource *)tableDataSource didFailAutocompleteWithError:(NSError *)error {
    [_textField resignFirstResponder];
    //[self autocompleteDidFail:error];
    _textField.text = @"";
}

- (void)didRequestAutocompletePredictionsForTableDataSource: (GMSAutocompleteTableDataSource *)tableDataSource {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [_resultsController.tableView reloadData];
}

- (void)didUpdateAutocompletePredictionsForTableDataSource:(GMSAutocompleteTableDataSource *)tableDataSource {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [_resultsController.tableView reloadData];
}
#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    if (textField == _textField) {
        [self addChildViewController:_resultsController];
        
        // Add the results controller.
        _resultsController.view.translatesAutoresizingMaskIntoConstraints = NO;
        _resultsController.view.alpha = 0.0f;
        [self.view addSubview:_resultsController.view];
        
        // Layout it out below the text field using auto layout.
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_searchField]-[resultView]-(0)-|" options:0 metrics:nil views:@{ @"_searchField" : _textField, @"resultView" : _resultsController.view }]];
       
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(0)-[resultView]-(0)-|" options:0 metrics:nil
            views:@{ @"resultView" : _resultsController.view }]];
        
        // Force a layout pass otherwise the table will animate in weirdly.
        [self.view layoutIfNeeded];
        
        // Reload the data.
        [_resultsController.tableView reloadData];
        
        // Animate in the results.
        [UIView animateWithDuration:0.5 animations:^{
            _resultsController.view.alpha = 1.0f;
        } completion:^(BOOL finished) {
            [_resultsController didMoveToParentViewController:self];
        }];
    }
}


- (void)textFieldDidEndEditing:(UITextField *)textField {
    // Dismiss the results.
    if (textField == _textField) {
        [_resultsController willMoveToParentViewController:nil];
        [UIView animateWithDuration:0.5 animations:^{
                             _resultsController.view.alpha = 0.0f;
                         }
                         completion:^(BOOL finished) {
                             [_resultsController.view removeFromSuperview];
                             [_resultsController removeFromParentViewController];
                         }];
    }
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    [textField resignFirstResponder];
    textField.text = @"";
    return NO;
}*/

#pragma mark - Private Methods

/*- (void)textFieldDidChange:(UITextField *)textField {
    if (textField == _txtAddress) {
        [_tableDataSource sourceTextHasChanged:textField.text];
    }
}*/

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onBack:(id)sender {
    [self dismissViewControllerAnimated:true completion:^{}];
}
@end
