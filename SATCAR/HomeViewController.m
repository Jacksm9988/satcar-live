//
//  HomeViewController.m
//  SATCAR
//
//  Created by Percept Infotech on 12/16/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import "HomeViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "ManualTripEntryViewController.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "CustomeAnnotation.h"
#import "TrackerIDViewController.h"
#import <CoreMotion/CoreMotion.h>
#import "BeaconConfigureViewController.h"

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
@interface HomeViewController () <MKMapViewDelegate,CLLocationManagerDelegate>
{
    NSString *isPrivate;
    NSDate *startTime;
    NSMutableArray *locationDictArray;
    BOOL trackerPopup;
    int counter,firstTimePoint;
    NSString *savedHour,*savedKM,*showPopupRoute,*trackerID,*paymentStatus,*strMeasure;
    AppDelegate *appdelegate;
    CLGeocoder *geocoder;
    //NSArray *carLocationArray;
    NSDictionary *userInfo,*userdataDict;
    int addedNotif,sameScreen;
    NSTimer *carDisplayTimer;
    int carDisplayCounter;
    NSUserDefaults *def;
    int offlineCounter;
    NSString *sTempLocation,*eTempLocation,*userID,*parent,*tracker_id;
    
}
@end

@implementation HomeViewController
@synthesize fromTrip,timer,model,offlineTripArray;
double timerInterval = 1.0f;

+ (HomeViewController *)sharedMySingleton
{
    NSLog(@"sharedMySingleton");
    static dispatch_once_t shared_initialized;
    static HomeViewController *shared_instance = nil;
    
    dispatch_once(&shared_initialized, ^ {
        shared_instance = [[HomeViewController alloc] init];
        [shared_instance add];
    });
    return shared_instance;
}

-(void)add{
    addedNotif = true;
}

#pragma mark - view life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"viewDidLoad");
    def = [NSUserDefaults standardUserDefaults];
    appdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appdelegate setApplicationLanguage];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DisplayMapData" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DisplayMap" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MODELVIEW DISMISS" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"RemoveMapData" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"BeaconConnection" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"BeaconDisConnection" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"BeaconConnectionService" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"BeaconDisConnectionService" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"startTripAfterRestart" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"SaveTripAtAppReStarttimeIfBeconDisconnect" object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(displayMapData:) name:@"DisplayMapData" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(LoadMapRoute) name:@"DisplayMap" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handler:) name:@"MODELVIEW DISMISS" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initData) name:@"RemoveMapData" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPrivateTripFunction) name:@"BeaconConnection" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onFinishTripFunction) name:@"BeaconDisConnection" object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startTripAfterRestart) name:@"startTripAfterRestart" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callBeaconConnectedStatusService) name:@"BeaconConnectionService" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callBeaconDisConnectedStatusService) name:@"BeaconDisConnectionService" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(SaveTripAtAppReStarttimeIfBeconDisconnect) name:@"SaveTripAtAppReStarttimeIfBeconDisconnect" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"GotoTripsFromHome" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotoTrips) name:@"GotoTripsFromHome" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"GotoExpenssFromHome" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotoExpenss) name:@"GotoExpenssFromHome" object:nil];
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"BeaconConnection_Formsg" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(Checkbeacone) name:@"BeaconConnection_Formsg" object:nil];
    
    sameScreen = 1;
    
    if([self CheckNetworkConnection]) {
        [self checkOfflineTripAvailable];
    }
}
-(void)gotoTrips
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //ECSlidingViewController *slidingViewController = [[ECSlidingViewController alloc]init];
    UIViewController *newViewController = [storyboard instantiateViewControllerWithIdentifier:@"TripPath"];
    [self.slidingViewController setTopViewController:newViewController];
    [self.slidingViewController resetTopViewAnimated:YES];
    [[NSUserDefaults standardUserDefaults] setObject:@"TripPath" forKey:@"storyBoardIdentifier"];
}
-(void)gotoExpenss
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //ECSlidingViewController *slidingViewController = [[ECSlidingViewController alloc]init];
    UIViewController *newViewController = [storyboard instantiateViewControllerWithIdentifier:@"ExpensesPath"];
    [self.slidingViewController setTopViewController:newViewController];
    [self.slidingViewController resetTopViewAnimated:YES];
    [[NSUserDefaults standardUserDefaults] setObject:@"ExpensesPath" forKey:@"storyBoardIdentifier"];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self checkLocationPermission];
    //[self setBeaconeConnectMSG];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"BeaconConnection_Formsg" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(Checkbeacone) name:@"BeaconConnection_Formsg" object:nil];
    [self Checkbeacone];
    userID = [Helper getPREF:@"userID"];
    NSLog(@"viewWillAppear");
    model = [[Model alloc]init];
    locationDictArray = [[NSMutableArray alloc]init];
    offlineTripArray = [[NSMutableArray alloc]init];
    appdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    _calloutViewArray = [[NSMutableArray alloc]init];
    carDisplayCounter = 0;
    firstTimePoint = 0;
    //[self setBeaconeConnectMSG];
    //NSLog(@"Location data : %@",[appdelegate.appLocationDataArray objectAtIndex:0]);
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self addTitleView2:NSLocalizedString(@"Ready_to_track", @"")];
    
    parent = [Helper isStringIsNull:[Helper getPREF:@"parent"]];
    tracker_id = [Helper getPREF:@"tracker_id"];
    
    if ([parent integerValue] > 0)
    {
        if (![self checkTrackerID:tracker_id])
        {
            UIButton *carButton = [UIButton buttonWithType:UIButtonTypeCustom];
            
            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                carButton.frame = CGRectMake(0, 0, 40, 40);
            }else{
                carButton.frame = CGRectMake(30,0,35,35);
            }
            [carButton setImage:[UIImage imageNamed:@"showme_car"] forState:UIControlStateNormal];
            carButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
            [carButton addTarget:self action:@selector(displayCarLocation) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem* item = [[UIBarButtonItem alloc                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       ] initWithCustomView:carButton];
            self.navigationItem.rightBarButtonItem = item;
        }
    }else{
        UIButton *carButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
            carButton.frame = CGRectMake(0, 0, 40, 40);
        }else{
            carButton.frame = CGRectMake(30,0,35,35);
        }
        [carButton setImage:[UIImage imageNamed:@"showme_car"] forState:UIControlStateNormal];
        carButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [carButton addTarget:self action:@selector(displayCarLocation) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem* item = [[UIBarButtonItem alloc                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       ] initWithCustomView:carButton];
        self.navigationItem.rightBarButtonItem = item;
    }
    
    _btnPrivateTrip.layer.cornerRadius = 25;
    [_btnPrivateTrip setTitle:NSLocalizedString(@"TrackPrivate", @"") forState:UIControlStateNormal];
    [_btnPrivateTrip setBackgroundColor:[Helper privateColor]];
    _BtnBusinessTrip.layer.cornerRadius = 25;
    [_BtnBusinessTrip setTitle:NSLocalizedString(@"TrackBusiness", @"") forState:UIControlStateNormal];
    [_BtnBusinessTrip setBackgroundColor:[Helper BusinessColor]];
    _ViewBottom.layer.cornerRadius = 6;
    [_btnFinishTrip setTitle: NSLocalizedString(@"Finishtrip", @"") forState:UIControlStateNormal];
    _viewTripBack.layer.cornerRadius = 0;
    _viewTripBack.clipsToBounds = true;
    _btnFinishTrip.layer.cornerRadius = 25;
    _btnFinishTrip.clipsToBounds = true;

    
    UISwipeGestureRecognizer *LeftRight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleGestureNext:)];
    LeftRight.delegate=self;
    [LeftRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.view addGestureRecognizer:LeftRight];
    _MapViewMy.delegate = self;
    _MapViewMy.showsUserLocation = YES;
    
    geocoder = [[CLGeocoder alloc] init] ;
    isPrivate = @"1";
    
    [self showPrivateRoutePopup];
    [self hideRoutePopup];
    
    if (appdelegate.appTimer) {
        [self oldMapRouteLoad];
        [self showTripKM];
    }else{
        [self hideTripKM];
        [self clearMapAnnotation ];
    }
    
    NSData *userdata = [Helper getDataValue:@"userInfo"];
    userInfo = [[self dataToDicitonary:userdata] mutableCopy];
    
    if (![userInfo isKindOfClass:[NSNull class]]) {
        userdataDict = [userInfo valueForKey:@"data"];
        trackerID = [userdataDict valueForKey:@"tracker_id"];
        NSDictionary *generalSetting = [userdataDict valueForKey:@"general_setting"];
        strMeasure = [generalSetting valueForKey:@"unit"];
        if (sameScreen == 1) {
            // Zoom car data = 1 otherwise 0
            //[self callInitialDanttrackerApi:@"0"];
        }
    }else{
        [Helper displayAlertView:@"" message:NSLocalizedString(@"user_detail_not_found", @"")];
    }
    
    if (![appdelegate.appFirstLaunch isEqualToString:@"1"]) {
        [self locationStart];
    }
    [self zoomIn:self];
    [self performSelector:@selector(checkTripIsContinue) withObject:self afterDelay:2.0];
    //[appdelegate callLocationService:userID from:@"home"];
    [appdelegate callLocationService_withTips:userID from:@"home"];
    
}

-(void)checkTripIsContinue{
    
    appdelegate.isTripContinue = [def valueForKey:@"isTripContinue"];
    if ([appdelegate.isTripContinue isEqualToString:@"1"]) {
        NSLog(@"TripIsContinue");
        NSString *isRestarted = [def valueForKey:@"AppTerminate"];
        if ([isRestarted isEqualToString:@"1"]) {
            if([[Helper getPREF:@"type"] isEqualToString:@"1"])
            {
                [def setValue:@"0" forKey:@"AppTerminate"];
                [def synchronize];
                [self startTripAfterRestart];
            }
        }
    }else{
        NSLog(@"TripIsNotContinue");
    }
}

-(void)viewDidLoadViewWillAppear{
    NSLog(@"viewDidLoadViewWillAppear");
    _calloutViewArray = [[NSMutableArray alloc]init];
    appdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DisplayMapData" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DisplayMap" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MODELVIEW DISMISS" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"RemoveMapData" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"BeaconConnection" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"BeaconDisConnection" object:nil];
    
    
    //[[NSNotificationCenter defaultCenter] removeObserver:self name:@"BeaconDisConnection_Formsg" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"BeaconConnectionService" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"BeaconDisConnectionService" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"startTripAfterRestart" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"SaveTripAtAppReStarttimeIfBeconDisconnect" object:nil];
   
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(displayMapData:) name:@"DisplayMapData" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(LoadMapRoute) name:@"DisplayMap" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handler:) name:@"MODELVIEW DISMISS" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initData) name:@"RemoveMapData" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPrivateTripFunction) name:@"BeaconConnection" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onFinishTripFunction) name:@"BeaconDisConnection" object:nil];
  
   // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ondisconnectbeconemsg) name:@"BeaconDisConnection_Formsg" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callBeaconConnectedStatusService) name:@"BeaconConnectionService" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callBeaconDisConnectedStatusService) name:@"BeaconDisConnectionService" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startTripAfterRestart) name:@"startTripAfterRestart" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(SaveTripAtAppReStarttimeIfBeconDisconnect) name:@"SaveTripAtAppReStarttimeIfBeconDisconnect" object:nil];
    
    
    locationDictArray = [[NSMutableArray alloc]init];
    appdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    firstTimePoint = 0;
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self addTitleView2:NSLocalizedString(@"Ready_to_track", @"")];
    
    UIButton *carButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [carButton setImage:[UIImage imageNamed:@"showme_car"] forState:UIControlStateNormal];
    carButton.frame = CGRectMake(30, 0,50, 50);
    carButton.imageEdgeInsets = UIEdgeInsetsMake(7, 7, 7, 7);
    [carButton setTintColor:[UIColor whiteColor]];
    [carButton addTarget:self action:@selector(displayCarLocation) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* item = [[UIBarButtonItem alloc] initWithCustomView:carButton];
    self.navigationItem.rightBarButtonItem = item;
    
    _btnPrivateTrip.layer.cornerRadius = 25;
    [_btnPrivateTrip setTitle:NSLocalizedString(@"TrackPrivate", @"") forState:UIControlStateNormal];
    [_btnPrivateTrip setBackgroundColor:[Helper privateColor]];
    _BtnBusinessTrip.layer.cornerRadius = 25;
    [_BtnBusinessTrip setTitle:NSLocalizedString(@"TrackBusiness", @"") forState:UIControlStateNormal];
    [_BtnBusinessTrip setBackgroundColor:[Helper BusinessColor]];
    _ViewBottom.layer.cornerRadius = 6;
    
    [_btnFinishTrip setTitle: NSLocalizedString(@"Finishtrip", @"") forState:UIControlStateNormal];
    _viewTripBack.layer.cornerRadius = 20;
    _viewTripBack.clipsToBounds = true;
    _btnFinishTrip.layer.cornerRadius = 15;
    _btnFinishTrip.clipsToBounds = true;
    
    
    UISwipeGestureRecognizer *LeftRight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleGestureNext:)];
    LeftRight.delegate=self;
    [LeftRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.view addGestureRecognizer:LeftRight];
    _MapViewMy.delegate = self;
    _MapViewMy.showsUserLocation = YES;
    geocoder = [[CLGeocoder alloc] init] ;
    
    [self zoomIn:self];
    isPrivate = @"1";
    
    [self showPrivateRoutePopup];
    [self hideRoutePopup];
    
    if (appdelegate.appTimer) {
        [self oldMapRouteLoad];
        [self showTripKM];
    }else{
        [self hideTripKM];
        [self clearMapAnnotation ];
    }
    
    NSData *userdata = [Helper getDataValue:@"userInfo"];
    userInfo = [[self dataToDicitonary:userdata] mutableCopy];
    
    if (![userInfo isKindOfClass:[NSNull class]]) {
        userdataDict = [userInfo valueForKey:@"data"];
        trackerID = [userdataDict valueForKey:@"tracker_id"];
        NSDictionary *generalSetting = [userdataDict valueForKey:@"general_setting"];
        strMeasure = [generalSetting valueForKey:@"unit"];
        NSLog(@"Unit : %@",strMeasure);

    }else{
        [Helper displayAlertView:@"" message:NSLocalizedString(@"something_went_wrong_msg", @"")];
    }
    
    if (sameScreen == 1) {
        // Zoom car data = 1 otherwise 0
        //[self callInitialDanttrackerApi:@"0"];
    }
    
    if (![appdelegate.appFirstLaunch isEqualToString:@"1"]) {
        [self locationStart];
    }
    [self zoomIn:self];
}

-(void)initData{
    NSLog(@"init");
    
    NSLog(@"initData");
    if (appdelegate.appTimer) {
        [self oldMapRouteLoad];
        [self showTripKM];
    }else{
        [self hideTripKM];
        [self clearMapAnnotation ];
        [_MapViewMy removeAnnotations:_MapViewMy.annotations];
        [_MapViewMy removeOverlays: _MapViewMy.overlays];
    }
    
    _MapViewMy.delegate = self;
    _MapViewMy.showsUserLocation = YES;
    geocoder = [[CLGeocoder alloc] init] ;
    
    [self zoomIn:self];
    [self hideRoutePopup];
   
    if (![appdelegate.appFirstLaunch isEqualToString:@"1"]) {
        [self locationStart];
    }
    
    NSData *userdata = [Helper getDataValue:@"userInfo"];
    userInfo = [[self dataToDicitonary:userdata] mutableCopy];
    
    if (![userInfo isKindOfClass:[NSNull class]]) {
        NSDictionary *dict = [userInfo valueForKey:@"data"];
        trackerID = [dict valueForKey:@"tracker_id"];
    }else{
        [Helper displayAlertView:@"" message:NSLocalizedString(@"something_went_wrong_msg", @"")];
    }
    [self zoomIn:self];
}

#pragma mark - Navigation handler

-(void)handler:(NSNotification *)notice{
    
    NSLog(@"handler");
    NSString *str = [notice object];
    if ([str isEqualToString:@"fromTrip"]) {
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ManualTripEntryViewController* controller = [storyboard instantiateViewControllerWithIdentifier:@"ManualTrip"];
        controller.fromScreen = appdelegate.appTempFromScreen;
        controller.tripDataDictionary = [appdelegate.appTempSendDataDict mutableCopy];
        [self.navigationController pushViewController:controller animated:YES];
    }
}

- (void)displayMapData:(NSNotification *)notification {
    //NSLog(@"displayMapData");
    _lblHours.text = appdelegate.appClockTime;
    if ([strMeasure.lowercaseString isEqualToString:@"miles"] || [strMeasure.lowercaseString isEqualToString:@"mi"]) {
        if (appdelegate.appKM == (NSString *)[NSNull null]) {
            [self calculateKMToMiles:0];
        }else{
            [self calculateKMToMiles:[appdelegate.appKM floatValue]];
        }
    }else{
        if (appdelegate.appKM == (NSString *)[NSNull null] || appdelegate.appKM == nil) {
            _lblKM.text = [NSString stringWithFormat:@"0.0 km"];
        }else{
            _lblKM.text = [NSString stringWithFormat:@"%@ km",appdelegate.appKM];
        }
    }
}

#pragma mark - Location manager

-(void)locationStart{
    NSLog(@"location update started");
    appdelegate.locationManager = [[CLLocationManager alloc] init];
    appdelegate.appLocationDataArray = [[NSMutableArray alloc]init];
    
    #ifdef __IPHONE_8_0
    if(IS_OS_8_OR_LATER) {
          [appdelegate.locationManager requestWhenInUseAuthorization];
          [appdelegate.locationManager requestAlwaysAuthorization];
    }
    #endif
    [appdelegate.locationManager startUpdatingLocation];
    appdelegate.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    appdelegate.locationManager.delegate = self;
    appdelegate.locationManager.allowsBackgroundLocationUpdates = TRUE;
    appdelegate.locationManager.pausesLocationUpdatesAutomatically = FALSE;
    appdelegate.locationManager.distanceFilter = 10;
    appdelegate.appStartLocation = nil;
    appdelegate.appDistance = @"";
    
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
    if (state == UIApplicationStateBackground) {
        NSLog(@"Start significant location manager");
        
        appdelegate.anotherLocationManager = [[CLLocationManager alloc] init];
#ifdef __IPHONE_8_0
        if(IS_OS_8_OR_LATER) {
            [appdelegate.anotherLocationManager requestWhenInUseAuthorization];
            [appdelegate.anotherLocationManager requestAlwaysAuthorization];
        }
#endif
        [appdelegate.anotherLocationManager startMonitoringSignificantLocationChanges];
        appdelegate.anotherLocationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
        appdelegate.anotherLocationManager.delegate = self;
        appdelegate.anotherLocationManager.allowsBackgroundLocationUpdates = TRUE;
        appdelegate.anotherLocationManager.pausesLocationUpdatesAutomatically = FALSE;
        appdelegate.anotherLocationManager.distanceFilter = kCLDistanceFilterNone;
        
        
        [appdelegate.locationManager startMonitoringSignificantLocationChanges];
    }
    
    CLAuthorizationStatus authorizationStatus= [CLLocationManager authorizationStatus];
    if (authorizationStatus == kCLAuthorizationStatusAuthorizedAlways ||
        authorizationStatus == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [appdelegate.locationManager startUpdatingLocation];
    }
    
    if([appdelegate.locationManager respondsToSelector:@selector(setAllowsBackgroundLocationUpdates:)])
        [appdelegate.locationManager setAllowsBackgroundLocationUpdates:YES];
}

-(void)checkLocationPermission{
    CLAuthorizationStatus authorizationStatus= [CLLocationManager authorizationStatus];
    if (authorizationStatus == kCLAuthorizationStatusAuthorizedWhenInUse) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Background Location Access" message:@"We need location service also when application is in background to track your trip. Please select location authorized always." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *choose = [UIAlertAction actionWithTitle:@"Open Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
            NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
        }];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [alert dismissViewControllerAnimated:true completion:nil];
        }];
        [alert addAction:choose];
        [alert addAction:ok];
        [self presentViewController:alert animated:true completion:nil];
    }
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    if (status == kCLAuthorizationStatusDenied) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Background Location Access Disabled" message:@"We need location service to track your trip." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *choose = [UIAlertAction actionWithTitle:@"Open Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
            NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
        }];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [alert dismissViewControllerAnimated:true completion:nil];
        }];
        [alert addAction:choose];
        [alert addAction:ok];
        [self presentViewController:alert animated:true completion:nil];
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    //[Helper displayAlertView:@"Location services are off you must turn on 'Always' in the Location Services Settings" message:@"Location services are off you must turn on 'Always' in the Location Services Settings"];
    NSLog(@"Location Error : %@",error);
}

/*-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    
    NSLog(@"locations : %@",locations);
    
    if (locations.lastObject != nil) {
        appdelegate.appUserLocation = [locations lastObject];
        if (![appdelegate.appFirstLaunch isEqualToString:@"1"]) {
            appdelegate.appFirstLaunch = @"1";
            [Helper setPREFStringValue:appdelegate.appFirstLaunch sKey:@"FirstLaunch"];
            [appdelegate.locationManager stopUpdatingLocation];
            appdelegate.locationManager.delegate = nil;
        }else{
            
            CLLocation *loc = [locations lastObject];
            NSString *currentLatitude = [[NSString alloc] initWithFormat:@"%+.4f", loc.coordinate.latitude];
            NSString *currentLongitude = [[NSString alloc] initWithFormat:@"%+.4f", loc.coordinate.longitude];
            if (appdelegate.appStartLocation == nil){
                appdelegate.appStartLocation = [locations lastObject];
                appdelegate.appOldLocation = [locations lastObject];
                
                NSString *lat = [[NSString alloc] initWithFormat:@"%f", appdelegate.appStartLocation.coordinate.latitude];
                NSString *lng = [[NSString alloc] initWithFormat:@"%f", appdelegate.appStartLocation.coordinate.longitude];
                NSString *str = [NSString stringWithFormat:@"%@,%@",lat,lng];
                [self saveTripStartLocation:str];
            }
            
            CLLocation *newLocation = [locations lastObject];
            CLLocationDistance currentDistance = [newLocation distanceFromLocation:appdelegate.appOldLocation] / 1000;
            float totalDist = [appdelegate.appDistance floatValue];
            //NSLog(@"Total Dist : %@",appdelegate.appDistance);
            totalDist = totalDist + currentDistance;
            
            if (totalDist <= 0) {
                totalDist = 0.00;
            }
            
            NSString *newDist = [[NSString alloc] initWithFormat:@"%.2f", totalDist];
            appdelegate.appKM = newDist;
            
            // used for add new distance without formating
            appdelegate.appDistance = [[NSString alloc] initWithFormat:@"%f", totalDist];
            [self saveTripDistance:appdelegate.appDistance];
            
            if ([strMeasure.lowercaseString isEqualToString:@"miles"]) {
                [self calculateKMToMiles:[appdelegate.appKM floatValue]];
            }else{
                if (appdelegate.appKM == (NSString *)[NSNull null]|| appdelegate.appKM == nil) {
                    _lblKM.text = [NSString stringWithFormat:@"0.0 km"];
                }else{
                    _lblKM.text = [NSString stringWithFormat:@"%@ km",appdelegate.appKM];
                }
                //_lblKM.text = [NSString stringWithFormat:@"%@ km",appdelegate.appKM];
            }
            NSLog(@"Location manager : %@",appdelegate.locationManager.location);
            NSLog(@"Latest location : %@",locations.lastObject);
            
            [geocoder reverseGeocodeLocation:appdelegate.locationManager.location completionHandler:^(NSArray *placemarks, NSError *error)
             {
                 NSString *Address = @"", *Area = @"", *Country = @"";
                 if (!(error))
                 {
                     CLPlacemark *placemark = [placemarks objectAtIndex:placemarks.count-1];
                     NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@","];
                     
                     Address = [[NSString alloc]initWithString:[Helper isStringIsNull:locatedAt]];
                     Area = [[NSString alloc]initWithString:[Helper isStringIsNull:placemark.locality]];
                     Country = [[NSString alloc]initWithString:[Helper isStringIsNull:placemark.country]];
                 }
                 
                 if (appdelegate.appOldLocation.coordinate.latitude != newLocation.coordinate.longitude || appdelegate.appOldLocation.coordinate.longitude != newLocation.coordinate.longitude) {
                     
                     appdelegate.appOldLocation = newLocation;
                     NSDictionary *parameter;
                     if (!(error)){
                         parameter=@{ @"latitude":currentLatitude, @"longitude":currentLongitude, @"Address":[self printAddress:Address :Area :Country]};
                     }else{
                         parameter=@{ @"latitude":currentLatitude, @"longitude":currentLongitude, @"Address":@""};
                     }
                     
                     [locationDictArray addObject:parameter];
                     [appdelegate.appLocationDataArray addObject:parameter];
                     [self saveTripLocationDataArray:appdelegate.appLocationDataArray];
                     
                     if( [UIApplication sharedApplication].applicationState == UIApplicationStateActive ){
                         if ([appdelegate.appState isEqualToString:@"2"]) {
                             
                             // 1 means active, // 2 or other means not active, //NSLog(@"State active");
                             [self clearMapAnnotation];
                             appdelegate.appState = @"1";
                             [self oldMapRouteLoad];
                         }
                         [[NSNotificationCenter defaultCenter] postNotificationName:@"DisplayMap" object:nil userInfo:nil];
                     }else{
                         appdelegate.appState = @"2";
                     }
                 }
             }];
        }
    }
}*/

// Latest
//-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
//{
//
//    if (newLocation != nil && oldLocation != nil) {
//        appdelegate.appUserLocation = newLocation;
//        if (![appdelegate.appFirstLaunch isEqualToString:@"1"]) {
//            appdelegate.appFirstLaunch = @"1";
//            [Helper setPREFStringValue:appdelegate.appFirstLaunch sKey:@"FirstLaunch"];
//            [appdelegate.locationManager stopUpdatingLocation];
//            appdelegate.locationManager.delegate = nil;
//        }else{
//
//            NSString *currentLatitude = [[NSString alloc] initWithFormat:@"%+f", newLocation.coordinate.latitude];
//            NSString *currentLongitude = [[NSString alloc] initWithFormat:@"%+f", newLocation.coordinate.longitude];
//            if (appdelegate.appStartLocation == nil){
//                appdelegate.appStartLocation = newLocation;
//
//                NSString *lat = [[NSString alloc] initWithFormat:@"%f", appdelegate.appStartLocation.coordinate.latitude];
//                NSString *lng = [[NSString alloc] initWithFormat:@"%f", appdelegate.appStartLocation.coordinate.longitude];
//                NSString *str = [NSString stringWithFormat:@"%@,%@",lat,lng];
//                [self saveTripStartLocation:str];
//
//            }
//            NSLog(@"Location manager : %@",appdelegate.locationManager.location);
//            NSLog(@"Latitude : %f:%f",newLocation.coordinate.latitude,oldLocation.coordinate.latitude);
//            NSLog(@"Longitude : %f:%f",newLocation.coordinate.longitude,oldLocation.coordinate.longitude);
//
//            if (newLocation.coordinate.latitude != oldLocation.coordinate.latitude && newLocation.coordinate.longitude !=   oldLocation.coordinate.longitude ) {
//                NSLog(@"Not equal location");
//                CLLocationDistance currentDistance = [newLocation distanceFromLocation:oldLocation] / 1000;
//                float totalDist = [appdelegate.appDistance floatValue];
//                //NSLog(@"Total Dist : %@",appdelegate.appDistance);
//                totalDist = totalDist + currentDistance;
//
//                if (totalDist <= 0) {
//                    totalDist = 0.00;
//                }
//
//                NSString *newDist = [[NSString alloc] initWithFormat:@"%.2f", totalDist];
//                appdelegate.appKM = newDist;
//
//                // used for add new distance without formating
//                appdelegate.appDistance = [[NSString alloc] initWithFormat:@"%f", totalDist];
//                [self saveTripDistance:appdelegate.appDistance];
//
//                if ([strMeasure.lowercaseString isEqualToString:@"miles"]) {
//                    [self calculateKMToMiles:[appdelegate.appKM floatValue]];
//                }else{
//                    if (appdelegate.appKM == (NSString *)[NSNull null]|| appdelegate.appKM == nil) {
//                        _lblKM.text = [NSString stringWithFormat:@"0.0 km"];
//                    }else{
//                        _lblKM.text = [NSString stringWithFormat:@"%@ km",appdelegate.appKM];
//                    }
//                }
//
//                NSLog(@"Location manager : %@",appdelegate.locationManager.location);
//                if (appdelegate.locationManager.location != nil) {
//
//
//
//                    [geocoder reverseGeocodeLocation:appdelegate.locationManager.location completionHandler:^(NSArray *placemarks, NSError *error)
//                     {
//                         NSString *Address = @"", *Area = @"", *Country = @"";
//                         if (!(error))
//                         {
//                             CLPlacemark *placemark = [placemarks objectAtIndex:placemarks.count-1];
//                             NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@","];
//
//                             Address = [[NSString alloc]initWithString:[Helper isStringIsNull:locatedAt]];
//                             Area = [[NSString alloc]initWithString:[Helper isStringIsNull:placemark.locality]];
//                             Country = [[NSString alloc]initWithString:[Helper isStringIsNull:placemark.country]];
//                         }
//
//                         if (oldLocation.coordinate.latitude != newLocation.coordinate.longitude || oldLocation.coordinate.longitude != newLocation.coordinate.longitude) {
//
//                             NSDictionary *parameter;
//                             if (!(error)){
//                                 parameter=@{ @"latitude":currentLatitude, @"longitude":currentLongitude, @"Address":[self printAddress:Address :Area :Country]};
//                             }else{
//                                 parameter=@{ @"latitude":currentLatitude, @"longitude":currentLongitude, @"Address":@""};
//                             }
//
//                             [locationDictArray addObject:parameter];
//                             [appdelegate.appLocationDataArray addObject:parameter];
//                             [self saveTripLocationDataArray:appdelegate.appLocationDataArray];
//
//                             if( [UIApplication sharedApplication].applicationState == UIApplicationStateActive ){
//                                 if ([appdelegate.appState isEqualToString:@"2"]) {
//
//                                     // 1 means active, // 2 or other means not active, //NSLog(@"State active");
//                                     [self clearMapAnnotation];
//                                     appdelegate.appState = @"1";
//                                     [self oldMapRouteLoad];
//                                 }
//                                 [[NSNotificationCenter defaultCenter] postNotificationName:@"DisplayMap" object:nil userInfo:nil];
//                             }else{
//                                 appdelegate.appState = @"2";
//                             }
//                         }
//                     }];
//                }
//            }
//
//        }
//    }
//}




// Old route draw fucntion of feb
-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    //appdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    appdelegate.appUserLocation = newLocation;
    
    if (![appdelegate.appFirstLaunch isEqualToString:@"1"]) {
        appdelegate.appFirstLaunch = @"1";
        [Helper setPREFStringValue:appdelegate.appFirstLaunch sKey:@"FirstLaunch"];
        [appdelegate.locationManager stopUpdatingLocation];
        appdelegate.locationManager.delegate = nil;
    }else{
        NSLog(@"Speed : %f",appdelegate.locationManager.location.speed * 3.5);
        /*BOOL isAdded = [self filterLocation:newLocation];
        if (isAdded) {
            
            if(CMMotionActivityManager.isActivityAvailable){
                appdelegate.activityManager = [[CMMotionActivityManager alloc] init];
                [appdelegate.activityManager startActivityUpdatesToQueue:[[NSOperationQueue alloc] init]
                                                      withHandler:
                 ^(CMMotionActivity *activity) {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         if ([activity stationary]) {
                             NSLog(@"Stationary");
                             if ([appdelegate.locAccuracy isEqualToString:@"1"]) {
                                 appdelegate.locAccuracy = @"0";
                                 appdelegate.locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
                             }
                         }
                         else if ([activity walking]) {
                             NSLog(@"Walking");
                             if ([appdelegate.locAccuracy isEqualToString:@"1"]) {
                                 appdelegate.locAccuracy = @"0";
                                 appdelegate.locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
                             }
                         }
                         else if ([activity running]) {
                             NSLog(@"Running");
                             if ([appdelegate.locAccuracy isEqualToString:@"1"]) {
                                 appdelegate.locAccuracy = @"0";
                                 appdelegate.locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
                             }
                         }
                         else if ([activity automotive]) {
                             NSLog(@"Vehicle");
                             if ([appdelegate.locAccuracy isEqualToString:@"0"]) {
                                 appdelegate.locAccuracy = @"1";
                                 appdelegate.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
                             }
                             //[Helper displayAlertView:@"" message:@"Vehicle"];
                         }
                         else if ([activity cycling]) {
                             NSLog(@"Cycling");
                             if ([appdelegate.locAccuracy isEqualToString:@"1"]) {
                                 appdelegate.locAccuracy = @"0";
                                 appdelegate.locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
                             }
                         }
                         else {
                             NSLog(@"Unknown");
                         }
                     });
                 }];
            }*/
            
            
            NSString *currentLatitude = [[NSString alloc] initWithFormat:@"%f", newLocation.coordinate.latitude];
            NSString *currentLongitude = [[NSString alloc] initWithFormat:@"%f", newLocation.coordinate.longitude];
            if (appdelegate.appStartLocation == nil){
                appdelegate.appStartLocation = newLocation;
                
                NSString *lat = [[NSString alloc] initWithFormat:@"%f", appdelegate.appStartLocation.coordinate.latitude];
                NSString *lng = [[NSString alloc] initWithFormat:@"%f", appdelegate.appStartLocation.coordinate.longitude];
                NSString *str = [NSString stringWithFormat:@"%@,%@",lat,lng];
                [self saveTripStartLocation:str];
            }
            NSLog(@"Latitude : %f:%f",newLocation.coordinate.latitude,oldLocation.coordinate.latitude);
            NSLog(@"Longitude : %f:%f",newLocation.coordinate.longitude,oldLocation.coordinate.longitude);
            CLLocationDistance currentDistance = [newLocation distanceFromLocation:oldLocation] / 1000;
            float totalDist = [appdelegate.appDistance floatValue];
            //NSLog(@"Total Dist : %@",appdelegate.appDistance);
            totalDist = totalDist + currentDistance;
            
            if (totalDist <= 0) {
                totalDist = 0.00;
            }
            NSString *newDist = [[NSString alloc] initWithFormat:@"%.2f", totalDist];
            appdelegate.appKM = newDist;
            
            // used for add new distance without formating
            appdelegate.appDistance = [[NSString alloc] initWithFormat:@"%f", totalDist];
            [self saveTripDistance:appdelegate.appDistance];
            
            if ([strMeasure.lowercaseString isEqualToString:@"miles"] || [strMeasure.lowercaseString isEqualToString:@"mi"]) {
                [self calculateKMToMiles:[appdelegate.appKM floatValue]];
            }else{
                if (appdelegate.appKM == (NSString *)[NSNull null]|| appdelegate.appKM == nil) {
                    _lblKM.text = [NSString stringWithFormat:@"0.0 km"];
                }else if ([appdelegate.appKM isEqualToString:@""] || [appdelegate.appKM isEqualToString:@"0"]){
                    _lblKM.text = [NSString stringWithFormat:@"0.0 km"];
                }else{
                    _lblKM.text = [NSString stringWithFormat:@"%@ km",appdelegate.appKM];
                }
            }

            if (oldLocation.coordinate.latitude != newLocation.coordinate.longitude || oldLocation.coordinate.longitude != newLocation.coordinate.longitude) {
                NSDictionary *parameter = @{ @"latitude":currentLatitude, @"longitude":currentLongitude, @"Address":@""};
                
                [locationDictArray addObject:parameter];
                [appdelegate.appLocationDataArray addObject:parameter];
                [self saveTripLocationDataArray:appdelegate.appLocationDataArray];
                
                if( [UIApplication sharedApplication].applicationState == UIApplicationStateActive ){
                    if ([appdelegate.appState isEqualToString:@"2"]) {
                        // 1 means active // 2 or other means not active
                        appdelegate.appState = @"1";
                        [self redrawWithClear];
                    }
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"DisplayMap" object:nil userInfo:nil];
                }else{
                    appdelegate.appState = @"2";
                }
            }
        }
    //}
}

-(BOOL)filterLocation:(CLLocation *)location{
    NSTimeInterval age = -location.timestamp.timeIntervalSinceNow;
    if (age > 10){
        return false;
    }
    if (location.horizontalAccuracy < 0){
        return false;
    }
    if (location.horizontalAccuracy > 100){
        return false;
    }
    return TRUE;
}

/*-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    
    appdelegate.appUserLocation = newLocation;
    
    if (![appdelegate.appFirstLaunch isEqualToString:@"1"]) {
        appdelegate.appFirstLaunch = @"1";
        [Helper setPREFStringValue:appdelegate.appFirstLaunch sKey:@"FirstLaunch"];
        [appdelegate.locationManager stopUpdatingLocation];
        //[appdelegate.locationManager stopMonitoringSignificantLocationChanges];
        appdelegate.locationManager.delegate = nil;
    }else{
        
        //NSString *currentLatitude = [[NSString alloc] initWithFormat:@"%+.4f", newLocation.coordinate.latitude];
        //NSString *currentLongitude = [[NSString alloc] initWithFormat:@"%+.4f", newLocation.coordinate.longitude];
        
        BOOL isAdded = [self filterLocation:newLocation];
        if (isAdded) {
            
        }
        
        NSString *currentLatitude = [[NSString alloc] initWithFormat:@"%f", newLocation.coordinate.latitude];
        NSString *currentLongitude = [[NSString alloc] initWithFormat:@"%f", newLocation.coordinate.longitude];
        if (appdelegate.appStartLocation == nil){
            appdelegate.appStartLocation = newLocation;
            
            NSString *lat = [[NSString alloc] initWithFormat:@"%f", appdelegate.appStartLocation.coordinate.latitude];
            NSString *lng = [[NSString alloc] initWithFormat:@"%f", appdelegate.appStartLocation.coordinate.longitude];
            NSString *str = [NSString stringWithFormat:@"%@,%@",lat,lng];
            [self saveTripStartLocation:str];
            
        }
        NSLog(@"Latitude : %f:%f",newLocation.coordinate.latitude,oldLocation.coordinate.latitude);
        NSLog(@"Longitude : %f:%f",newLocation.coordinate.longitude,oldLocation.coordinate.longitude);
        CLLocationDistance currentDistance = [newLocation distanceFromLocation:oldLocation] / 1000;
        float totalDist = [appdelegate.appDistance floatValue];
        //NSLog(@"Total Dist : %@",appdelegate.appDistance);
        totalDist = totalDist + currentDistance;
        
        if (totalDist <= 0) {
            totalDist = 0.00;
        }
        NSString *newDist = [[NSString alloc] initWithFormat:@"%.2f", totalDist];
        appdelegate.appKM = newDist;
        
        // used for add new distance without formating
        appdelegate.appDistance = [[NSString alloc] initWithFormat:@"%f", totalDist];
        [self saveTripDistance:appdelegate.appDistance];
        
        if ([strMeasure.lowercaseString isEqualToString:@"miles"]) {
            [self calculateKMToMiles:[appdelegate.appKM floatValue]];
        }else{
            if (appdelegate.appKM == (NSString *)[NSNull null]|| appdelegate.appKM == nil) {
                _lblKM.text = [NSString stringWithFormat:@"0.0 km"];
            }else if ([appdelegate.appKM isEqualToString:@""] || [appdelegate.appKM isEqualToString:@"0"]){
                _lblKM.text = [NSString stringWithFormat:@"0.0 km"];
            }else{
                _lblKM.text = [NSString stringWithFormat:@"%@ km",appdelegate.appKM];
            }
        }
        
        
        if (oldLocation.coordinate.latitude != newLocation.coordinate.longitude || oldLocation.coordinate.longitude != newLocation.coordinate.longitude) {
            NSDictionary *parameter = @{ @"latitude":currentLatitude, @"longitude":currentLongitude, @"Address":@""};
            
            [locationDictArray addObject:parameter];
            [appdelegate.appLocationDataArray addObject:parameter];
            [self saveTripLocationDataArray:appdelegate.appLocationDataArray];
            
            if( [UIApplication sharedApplication].applicationState == UIApplicationStateActive ){
                if ([appdelegate.appState isEqualToString:@"2"]) {
                    // 1 means active // 2 or other means not active
                    appdelegate.appState = @"1";
                    [self redrawWithClear];
                }
                [[NSNotificationCenter defaultCenter] postNotificationName:@"DisplayMap" object:nil userInfo:nil];
            }else{
                appdelegate.appState = @"2";
            }
        }
    }
}*/

// Backup on 24/05/2018 due to map not draw good
/*-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {

    if (newLocation != nil && oldLocation != nil) {
        appdelegate.appUserLocation = newLocation;
        if (![appdelegate.appFirstLaunch isEqualToString:@"1"]) {
            appdelegate.appFirstLaunch = @"1";
            [Helper setPREFStringValue:appdelegate.appFirstLaunch sKey:@"FirstLaunch"];
            [appdelegate.locationManager stopUpdatingLocation];
            appdelegate.locationManager.delegate = nil;
        }else{

            NSString *currentLatitude = [[NSString alloc] initWithFormat:@"%+f", newLocation.coordinate.latitude];
            NSString *currentLongitude = [[NSString alloc] initWithFormat:@"%+f", newLocation.coordinate.longitude];
            if (appdelegate.appStartLocation == nil){
                appdelegate.appStartLocation = newLocation;

                NSString *lat = [[NSString alloc] initWithFormat:@"%f", appdelegate.appStartLocation.coordinate.latitude];
                NSString *lng = [[NSString alloc] initWithFormat:@"%f", appdelegate.appStartLocation.coordinate.longitude];
                NSString *str = [NSString stringWithFormat:@"%@,%@",lat,lng];
                [self saveTripStartLocation:str];

            }
            NSLog(@"Latitude : %f:%f",newLocation.coordinate.latitude,oldLocation.coordinate.latitude);
            NSLog(@"Longitude : %f:%f",newLocation.coordinate.longitude,oldLocation.coordinate.longitude);

            if (newLocation.coordinate.latitude != oldLocation.coordinate.latitude && newLocation.coordinate.longitude !=   oldLocation.coordinate.longitude ) {

                NSLog(@"Not equal location");
                CLLocationDistance currentDistance = [newLocation distanceFromLocation:oldLocation] / 1000;
                float totalDist = [appdelegate.appDistance floatValue];
                //NSLog(@"Total Dist : %@",appdelegate.appDistance);
                totalDist = totalDist + currentDistance;

                if (totalDist <= 0) {
                    totalDist = 0.00;
                }

                NSString *newDist = [[NSString alloc] initWithFormat:@"%.2f", totalDist];
                appdelegate.appKM = newDist;

                // used for add new distance without formating
                appdelegate.appDistance = [[NSString alloc] initWithFormat:@"%f", totalDist];
                [self saveTripDistance:appdelegate.appDistance];

                if ([strMeasure.lowercaseString isEqualToString:@"miles"]) {
                    [self calculateKMToMiles:[appdelegate.appKM floatValue]];
                }else{
                    if (appdelegate.appKM == (NSString *)[NSNull null]|| appdelegate.appKM == nil) {
                        _lblKM.text = [NSString stringWithFormat:@"0.0 km"];
                    }else if ([appdelegate.appKM isEqualToString:@""] || [appdelegate.appKM isEqualToString:@"0"]){
                        _lblKM.text = [NSString stringWithFormat:@"0.0 km"];
                    }else{
                        _lblKM.text = [NSString stringWithFormat:@"%@ km",appdelegate.appKM];
                    }
                }

                NSDictionary *parameter = @{ @"latitude":currentLatitude, @"longitude":currentLongitude, @"Address":@""};
                [locationDictArray addObject:parameter];
                [appdelegate.appLocationDataArray addObject:parameter];
                [self saveTripLocationDataArray:appdelegate.appLocationDataArray];

                NSLog(@"app state : %@",appdelegate.appState);
                if( [UIApplication sharedApplication].applicationState == UIApplicationStateActive ){
                    if ([appdelegate.appState isEqualToString:@"2"]) {
                        
                        // 1 means active, // 2 or other means not active, //NSLog(@"State active");
                        appdelegate.appState = @"1";
                        //[self clearMapAnnotation];
                        //[self oldMapRouteLoad];
                        [self redrawWithClear];
                    }
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"DisplayMap" object:nil userInfo:nil];
                }else{
                    appdelegate.appState = @"2";
                    NSLog(@"app state set to 2");
                }
            }
        }
    }
}*/

// Backup on 15/05/2018 with .4 digit of location
/*-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    
    if (newLocation != nil && oldLocation != nil) {
        appdelegate.appUserLocation = newLocation;
        if (![appdelegate.appFirstLaunch isEqualToString:@"1"]) {
            appdelegate.appFirstLaunch = @"1";
            [Helper setPREFStringValue:appdelegate.appFirstLaunch sKey:@"FirstLaunch"];
            [appdelegate.locationManager stopUpdatingLocation];
            appdelegate.locationManager.delegate = nil;
        }else{
            
            //NSString *currentLatitude = [[NSString alloc] initWithFormat:@"%+f", newLocation.coordinate.latitude];
            //NSString *currentLongitude = [[NSString alloc] initWithFormat:@"%+f", newLocation.coordinate.longitude];
            NSString *currentLatitude = [[NSString alloc] initWithFormat:@"%+.4f", newLocation.coordinate.latitude];
            NSString *currentLongitude = [[NSString alloc] initWithFormat:@"%+.4f", newLocation.coordinate.longitude];
            
            if (appdelegate.appStartLocation == nil){
                appdelegate.appStartLocation = newLocation;
 
                NSString *lat = [[NSString alloc] initWithFormat:@"%f", appdelegate.appStartLocation.coordinate.latitude];
                NSString *lng = [[NSString alloc] initWithFormat:@"%f", appdelegate.appStartLocation.coordinate.longitude];
                NSString *str = [NSString stringWithFormat:@"%@,%@",lat,lng];
                [self saveTripStartLocation:str];
                
            }
            NSLog(@"Location manager : %@",appdelegate.locationManager.location);
            NSLog(@"Latitude : %f:%f",newLocation.coordinate.latitude,oldLocation.coordinate.latitude);
            NSLog(@"Longitude : %f:%f",newLocation.coordinate.longitude,oldLocation.coordinate.longitude);
            
            NSLog(@"Not equal location");
            CLLocationDistance currentDistance = [newLocation distanceFromLocation:oldLocation] / 1000;
            float totalDist = [appdelegate.appDistance floatValue];
            //NSLog(@"Total Dist : %@",appdelegate.appDistance);
            totalDist = totalDist + currentDistance;
            
            if (totalDist <= 0) {
                totalDist = 0.00;
            }
            
            NSString *newDist = [[NSString alloc] initWithFormat:@"%.2f", totalDist];
            appdelegate.appKM = newDist;
            
            // used for add new distance without formating
            appdelegate.appDistance = [[NSString alloc] initWithFormat:@"%f", totalDist];
            [self saveTripDistance:appdelegate.appDistance];
            
            if ([strMeasure.lowercaseString isEqualToString:@"miles"]) {
                [self calculateKMToMiles:[appdelegate.appKM floatValue]];
            }else{
                if (appdelegate.appKM == (NSString *)[NSNull null]|| appdelegate.appKM == nil) {
                    _lblKM.text = [NSString stringWithFormat:@"0.0 km"];
                }else{
                    _lblKM.text = [NSString stringWithFormat:@"%@ km",appdelegate.appKM];
                }
            }
            
            if (newLocation.coordinate.latitude != oldLocation.coordinate.latitude && newLocation.coordinate.longitude !=   oldLocation.coordinate.longitude ) {
                if (appdelegate.locationManager.location != nil) {
                    [geocoder reverseGeocodeLocation:appdelegate.locationManager.location completionHandler:^(NSArray *placemarks, NSError *error)
                     {
                         NSString *Address = @"", *Area = @"", *Country = @"";
                         if (!(error))
                         {
                             CLPlacemark *placemark = [placemarks objectAtIndex:placemarks.count-1];
                             NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@","];
                             
                             Address = [[NSString alloc]initWithString:[Helper isStringIsNull:locatedAt]];
                             Area = [[NSString alloc]initWithString:[Helper isStringIsNull:placemark.locality]];
                             Country = [[NSString alloc]initWithString:[Helper isStringIsNull:placemark.country]];
                         }
                         
                         NSDictionary *parameter = @{ @"latitude":currentLatitude, @"longitude":currentLongitude, @"Address":Address};
                         [locationDictArray addObject:parameter];
                         [appdelegate.appLocationDataArray addObject:parameter];
                         [self saveTripLocationDataArray:appdelegate.appLocationDataArray];
                         
                         if( [UIApplication sharedApplication].applicationState == UIApplicationStateActive ){
                             if ([appdelegate.appState isEqualToString:@"2"]) {
                                 
                                 // 1 means active, // 2 or other means not active, //NSLog(@"State active");
                                 [self clearMapAnnotation];
                                 appdelegate.appState = @"1";
                                 [self oldMapRouteLoad];
                             }
                             [[NSNotificationCenter defaultCenter] postNotificationName:@"DisplayMap" object:nil userInfo:nil];
                         }else{
                             appdelegate.appState = @"2";
                         }
                     }];
                }
            }
        }
    }
}*/
#pragma mark - mapview delegate
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    
    appdelegate.appUserLocation = (CLLocation *)userLocation;
    if (![carDisplayTimer isValid]) {
        if (locationDictArray.count <= 0) {
            MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 1000, 1000);
            [_MapViewMy setRegion:[_MapViewMy regionThatFits:region] animated:YES];
        }else{
            //NSLog(@"----------Map Update---------");
            
            if ([carDisplayTimer isValid]) {
                
                // for five second stop user location update only show car location
                if (carDisplayCounter >= 5) {
                    [carDisplayTimer invalidate];
                    carDisplayTimer = nil;
                    carDisplayCounter = 0;
                    NSLog(@"car timer stopped");
                    [_MapViewMy setCenterCoordinate:userLocation.location.coordinate animated:YES];
                }
            }else{
                [_MapViewMy setCenterCoordinate:userLocation.location.coordinate animated:YES];
            }
        }
    }else{
        
    }
    
    
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView1 viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ([carDisplayTimer isValid]) {
        if([annotation isKindOfClass:[CallOutAnnotationView class]]) {
            
            //NSLog(@"%@",mapView1.userLocation);
            static NSString *annotationViewReuseIdentifier = @"annotationViewReuseIdentifier";
            MKAnnotationView *annotationView = (MKAnnotationView *)[mapView1 dequeueReusableAnnotationViewWithIdentifier:annotationViewReuseIdentifier];
            
            if (annotationView == nil) {
                annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationViewReuseIdentifier];
            }
            UIGraphicsEndImageContext();
            annotationView.image =  [UIImage imageNamed:@"car_position"];
            annotationView.annotation = annotation;
            annotationView.canShowCallout = YES;
            
            return annotationView;
            
        } else {
            return nil;
        }
    }else{
        if ([annotation isKindOfClass:[MKUserLocation class]]) {
            static NSString *annotationViewReuseIdentifier = @"annotationViewReuseIdentifier";
            MKAnnotationView *annotationView = (MKAnnotationView *)[mapView1 dequeueReusableAnnotationViewWithIdentifier:annotationViewReuseIdentifier];
            
            if (annotationView == nil) {
                annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationViewReuseIdentifier];
            }
            UIGraphicsEndImageContext();
            
            annotationView.image =  [UIImage imageNamed:@"mypos_android"];
            annotationView.annotation = annotation;
            annotationView.canShowCallout = YES;
            return annotationView;
            
        }else if ([annotation isKindOfClass:[CustomeAnnotation class]]) {
            static NSString *annotationViewReuseIdentifier = @"annotationViewReuseIdentifier";
            MKAnnotationView *annotationView = (MKAnnotationView *)[mapView1 dequeueReusableAnnotationViewWithIdentifier:annotationViewReuseIdentifier];
            
            if (annotationView == nil) {
                annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationViewReuseIdentifier];
            }
            UIGraphicsEndImageContext();
            annotationView.image =  [UIImage imageNamed:@"start_route"];
            annotationView.centerOffset = CGPointMake(0, - annotationView.frame.size.height / 2 + 5);
            annotationView.annotation = annotation;
            annotationView.canShowCallout = YES;
            return annotationView;
            
        }else if([annotation isKindOfClass:[CallOutAnnotationView class]]) {
            
            //NSLog(@"%@",mapView1.userLocation);
            static NSString *annotationViewReuseIdentifier = @"annotationViewReuseIdentifier";
            MKAnnotationView *annotationView = (MKAnnotationView *)[mapView1 dequeueReusableAnnotationViewWithIdentifier:annotationViewReuseIdentifier];
            
            if (annotationView == nil) {
                annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationViewReuseIdentifier];
            }
            UIGraphicsEndImageContext();
            annotationView.image =  [UIImage imageNamed:@"car_position"];
            annotationView.annotation = annotation;
            annotationView.canShowCallout = YES;
            
            return annotationView;
            
        } else {
            return nil;
        }
    }
    
    
}

-(MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay {
    // NSLog(@"rendererForOverlay");
    MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
    renderer.strokeColor = [UIColor blueColor];
    renderer.lineWidth = 4.0;
    return renderer;
}

-(void)clearMapAnnotation
{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"clearMapAnnotation");
        if (_MapViewMy.annotations.count > 0) {
            [_MapViewMy removeAnnotations:_MapViewMy.annotations];
        }
        if (_MapViewMy.overlays.count > 0) {
            [_MapViewMy removeAnnotations:_MapViewMy.annotations];
            [_MapViewMy removeOverlays:_MapViewMy.overlays];
        }
    });
}

-(void)redrawWithClear{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"clearMapAnnotation");
        if (_MapViewMy.annotations.count > 0) {
            [_MapViewMy removeAnnotations:_MapViewMy.annotations];
        }
        if (_MapViewMy.overlays.count > 0) {
            [_MapViewMy removeAnnotations:_MapViewMy.annotations];
            [_MapViewMy removeOverlays:_MapViewMy.overlays];
        }
        [self oldMapRouteLoad];
    });
}

#pragma mark - Display Route functions
-(void)oldMapRouteLoad
{
     NSLog(@"oldMapRouteLoad");
    NSMutableArray *tempLocationArray = [[NSMutableArray alloc]init];
    tempLocationArray = [appdelegate.appLocationDataArray mutableCopy];
    //NSLog(@"temp Array count : %d",(int)tempLocationArray.count);
    CLLocationCoordinate2D Coordinates[[tempLocationArray count]];
    for (int i = 0; i < tempLocationArray.count; i++) {
        if (i == 0) {
            NSDictionary *dict = [tempLocationArray objectAtIndex:i];
            NSString *titleStr =[dict valueForKey:@"Address"] ;
            //NSLog(@"title is:%@",titleStr);
            
            double lat = [[dict valueForKey:@"latitude"]doubleValue];
            double lon = [[dict valueForKey:@"longitude"]doubleValue];
            CLLocation *loc = [[CLLocation alloc] initWithLatitude:lat longitude:lon];
            Coordinates[i] = loc.coordinate;
            
            CustomeAnnotation *point=[[CustomeAnnotation alloc] initWithLocation:Coordinates[i]];
            point.title =titleStr;
            [_MapViewMy addAnnotation:point];
        }else{
            CLLocationCoordinate2D Coordinates[2];
            
            NSDictionary *first = [tempLocationArray objectAtIndex:i - 1];
            NSString *titleStr =[first valueForKey:@"Address"] ;
            //NSLog(@"first address is:%@",titleStr);
            
            double lat = [[first valueForKey:@"latitude"]doubleValue];
            double lon = [[first valueForKey:@"longitude"]doubleValue];
            CLLocation *loc = [[CLLocation alloc] initWithLatitude:lat longitude:lon];
            Coordinates[0] = loc.coordinate;
            
            NSDictionary *second = [tempLocationArray objectAtIndex:i];
            titleStr =[second valueForKey:@"Address"] ;
            //NSLog(@"second address is:%@",titleStr);
            
            lat = [[second valueForKey:@"latitude"]doubleValue];
            lon = [[second valueForKey:@"longitude"]doubleValue];
            loc = [[CLLocation alloc] initWithLatitude:lat longitude:lon];
            Coordinates[1] = loc.coordinate;
            
            MKPolyline *polyLine = [MKPolyline polylineWithCoordinates:Coordinates count: 2];
            [_MapViewMy addOverlay:polyLine];
        }
    }
}

-(void)LoadMapRoute {
    
    
    NSLog(@"LoadMapRoute");
    CLLocationCoordinate2D Coordinates[[appdelegate.appLocationDataArray count]];
    if (firstTimePoint == 0) {
        NSLog(@"firstTimePoint");
        firstTimePoint = 1;
        NSDictionary *dict = [appdelegate.appLocationDataArray objectAtIndex:0];
        NSString *titleStr =[dict valueForKey:@"Address"] ;
        //NSLog(@"title is:%@",titleStr);
        
        double lat = [[dict valueForKey:@"latitude"]doubleValue];
        double lon = [[dict valueForKey:@"longitude"]doubleValue];
        CLLocation *loc = [[CLLocation alloc] initWithLatitude:lat longitude:lon];
        Coordinates[0] = loc.coordinate;
        
        CustomeAnnotation *point=[[CustomeAnnotation alloc] initWithLocation:Coordinates[0]];
        point.title =titleStr;
        [_MapViewMy addAnnotation:point];
    }
    
    if (appdelegate.appLocationDataArray.count > 1){
        CLLocationCoordinate2D Coordinates[2];
        
        NSDictionary *first = [appdelegate.appLocationDataArray objectAtIndex:appdelegate.appLocationDataArray.count - 2];
        NSString *titleStr =[first valueForKey:@"Address"] ;
        ////NSLog(@"first address is:%@",titleStr);
        
        double lat = [[first valueForKey:@"latitude"]doubleValue];
        double lon = [[first valueForKey:@"longitude"]doubleValue];
        CLLocation *loc = [[CLLocation alloc] initWithLatitude:lat longitude:lon];
        Coordinates[0] = loc.coordinate;
        
        NSDictionary *second = [appdelegate.appLocationDataArray objectAtIndex:appdelegate.appLocationDataArray.count - 1];
        titleStr =[second valueForKey:@"Address"] ;
        //NSLog(@"second address is:%@",titleStr);
        
        lat = [[second valueForKey:@"latitude"]doubleValue];
        lon = [[second valueForKey:@"longitude"]doubleValue];
        loc = [[CLLocation alloc] initWithLatitude:lat longitude:lon];
        Coordinates[1] = loc.coordinate;
        
        MKPolyline *polyLine = [MKPolyline polylineWithCoordinates:Coordinates count: 2];
        [_MapViewMy addOverlay:polyLine];
    }
}

#pragma mark - Custom functions

-(void)showRoutePopup{
    
    NSLog(@"showRoutePopup");
    [UIView animateWithDuration:5.0 delay:1.0 options:UIViewAnimationOptionTransitionNone animations:^{
        self.navigationController.navigationBar.hidden = true;
        _IBLayoutConstraintBackRouteTop.constant = 0;
        _IBLayoutConstraintBackRouteHeight.constant = self.view.frame.size.height;
    }completion:nil];
}

-(void)hideRoutePopup{
    
    //NSLog(@"hideRoutePopup");
    self.navigationController.navigationBar.hidden = false;
    [UIView animateWithDuration:5.0 delay:1.0 options:UIViewAnimationOptionTransitionNone animations:^{
        _IBLayoutConstraintBackRouteTop.constant = self.view.frame.size.height;
        _IBLayoutConstraintBackRouteHeight.constant = 0;
    }completion:nil];
}

-(void)showPrivateRoutePopup{
    //NSLog(@"showPrivateRoutePopup");
    isPrivate = @"1";
    _btnFinishTrip.backgroundColor = [Helper privateColor];
}

-(void)showBusinessRoutePopup{
    //NSLog(@"showBusinessRoutePopup");
    isPrivate = @"2";
    _btnFinishTrip.backgroundColor = [Helper BusinessColor];
}


-(void)showTripKM {
    
    NSString *type = [self getTripType];
    if ([type isEqualToString:@"1"] || [type isEqualToString:@"0"])
    {
        [_btnFinishTrip setBackgroundColor:[Helper privateColor]];
    }else{
        [_btnFinishTrip setBackgroundColor:[Helper BusinessColor]];
    }
    _viewTripBack.hidden = false;
    [self hideTripButton];
}

-(void)hideTripKM {
    
    //NSLog(@"hideTripKM");
    _viewTripBack.hidden = true;
    [self showTripButton];
}

-(void)hideTripButton{
    
    _btnPrivateTrip.hidden = true;
    _BtnBusinessTrip.hidden = true;
}

-(void)showTripButton{
    _btnPrivateTrip.hidden = false;
    _BtnBusinessTrip.hidden = false;
}

-(void)GotoBack {
    [self performSelector:@selector(goBack) withObject:nil afterDelay:0.5];
}
-(void)goBack {
    [self.navigationController popViewControllerAnimated:YES];
}

-(NSString *)getTime{
    
    NSLog(@"getTime");
    NSDateFormatter __autoreleasing *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormat setDateFormat:@"HH:mm"];
    NSString *dateString =  [dateFormat stringFromDate:[NSDate date]];
    return  dateString;
}

-(void)getEndTime{
    
    NSLog(@"getEndTime");
    NSDateFormatter __autoreleasing *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormat setDateFormat:@"HH:mm"];
    NSString *dateString =  [dateFormat stringFromDate:[NSDate date]];
    appdelegate.appEDTime = dateString;
}

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

#pragma mark - Timer function
-(void)onTick:(NSTimer*)timer{
    
    double second = - [appdelegate.appStartTime timeIntervalSinceNow];
    int myIntSecond = (int)second;
    int h = myIntSecond / 3600;
    int m = myIntSecond / 60 - h * 60;
    int s = myIntSecond % 60;
    NSString *strHour,*strMinute,*strSecond;
    if (h <= 9) {
        strHour = [NSString stringWithFormat:@"0%d",h];
    }else{
        strHour = [NSString stringWithFormat:@"%d",h];
    }
    if (m <= 9) {
        strMinute = [NSString stringWithFormat:@"0%d",m];
    }else{
        strMinute = [NSString stringWithFormat:@"%d",m];
    }
    if (s <= 9) {
        strSecond = [NSString stringWithFormat:@"0%d",s];
    }else{
        strSecond = [NSString stringWithFormat:@"%d",s];
    }
    NSString *clockTime = [NSString stringWithFormat:@"%@:%@:%@",strHour,strMinute,strSecond];
    appdelegate.appClockTime = clockTime;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DisplayMapData" object:nil userInfo:nil];
}

#pragma mark - Button actions
- (IBAction)onYes:(id)sender {
    [self startTimer];
    [self hideRoutePopup];
    [self showTripKM];
}

- (IBAction)onNo:(id)sender {
    [self hideRoutePopup];
    [self showTripButton];
}

-(BOOL)checkFreeTripWithUserType{
    NSString *user_type = [Helper getPREF:@"user_type"];
    NSString *tripcount = [Helper getPREF:@"tripcount"];
    int count = [tripcount intValue];
    
    if ([user_type isEqualToString:@"12"]) {
        if (count >= [appdelegate.freeTrip intValue]) {
            [self freeToPaidTripPopupMessage];
            return false;
        }else{
            return true;
        }
    }else{
        return true;
    }
}

// on Private trip button action
- (IBAction)onPrivateTrip:(id)sender {
    
    NSLog(@"Clicked private trip");
    BOOL isStart = [self checkFreeTripWithUserType];
    if (isStart) {
        [self stratPrivateTrip];
    }
}

// Through beacon Private trip button action
-(void)onPrivateTripFunction{
   
     NSLog(@"onPrivateTripFunction");
    if ([appdelegate.finalTrack isEqualToString:@"1"]) {
        //[self setBeaconeConnectMSG];
        BOOL isStart = [self checkFreeTripWithUserType];
        if (isStart) {
            [self stratAutomaticTrip];
        }
    }
}

-(void)stratPrivateTrip{
    [self addTitleView2:NSLocalizedString(@"Tracking", @"")];
    isPrivate = @"1";
    appdelegate.isTripContinue = @"1";
    appdelegate.isTripAutomatic = @"0";
    [Helper setPREFStringValue:appdelegate.isTripContinue sKey:@"isTripContinue"];
    [Helper setPREFStringValue:appdelegate.isTripAutomatic sKey:@"isTripAutomatic"];
    
    [self saveTripType:isPrivate];
    [self startTimer];
    [self showTripKM];
    [self Checkbeacone];
}

-(void)stratAutomaticTrip{
    [self addTitleView2:NSLocalizedString(@"Tracking", @"")];
    isPrivate = @"0";
    appdelegate.finalTrack = @"2";
    appdelegate.isTripContinue = @"1";
    appdelegate.isTripAutomatic = @"1";
    
    [Helper setPREFStringValue:appdelegate.isTripContinue sKey:@"isTripContinue"];
    [Helper setPREFStringValue:appdelegate.isTripAutomatic sKey:@"isTripAutomatic"];
    
    [self saveTripType:isPrivate];
    [self startTimer];
    [self showTripKM];
    [self Checkbeacone];
}

- (IBAction)onBusinessTrip:(id)sender {
    
    NSLog(@"Clicked business trip");
    BOOL isStart = [self checkFreeTripWithUserType];
    if (isStart) {
        [self stratBusinessTrip];
    }
}

-(void)stratBusinessTrip{
    [self addTitleView2:NSLocalizedString(@"Tracking", @"")];
    isPrivate = @"2";
    appdelegate.isTripContinue = @"1";
    appdelegate.isTripAutomatic = @"0";
    
    [Helper setPREFStringValue:appdelegate.isTripContinue sKey:@"isTripContinue"];
    [Helper setPREFStringValue:appdelegate.isTripAutomatic sKey:@"isTripAutomatic"];
    
    [self saveTripType:isPrivate];
    [self startTimer];
    [self showTripKM];
    [self Checkbeacone];
}

-(void)startTimer{
    
    NSLog(@"Timer started");
    [appdelegate.appTimer invalidate];
    if (!appdelegate.appTimer) {
        appdelegate.appStartTime = [NSDate date];
        appdelegate.appDistance = @"";
        appdelegate.appSTTime = [self getCurrentTime];
        appdelegate.appSTDate = [self getCurrentDate];
        [self saveTripStartTime:appdelegate.appStartTime];
        [self saveTripSTTime:appdelegate.appSTTime];
        [self saveTripSTDate:appdelegate.appSTDate];
        
        appdelegate.appTimer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(onTick:) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:appdelegate.appTimer forMode:NSRunLoopCommonModes];
        [self locationStart];
    }
}

-(void)stopTimer{
    
    NSLog(@"Timer Stopped");
    appdelegate.appEDTime = [self getCurrentTime];
    [appdelegate.appTimer invalidate];
    appdelegate.appTimer = nil;
}

-(void)startTimerAfterRestart{
    
    NSLog(@"Timer Started after restart app during trip");
    if (!appdelegate.appTimer) {
        appdelegate.appStartTime = [NSDate date];
        appdelegate.appDistance = @"";
        appdelegate.appSTTime = [self getCurrentTime];
        appdelegate.appSTDate = [self getCurrentDate];
        appdelegate.appTimer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(onTick:) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:appdelegate.appTimer forMode:NSRunLoopCommonModes];
        [self locationStart ];
    }
}

- (IBAction)onFinishTrip:(id)sender{
    
    // This condition true when trip start automatic and user forse end manually
    if ([[Helper getPREF:@"type"] isEqualToString:@"0"]) {
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"beacon_disconnect_message", @"") preferredStyle:UIAlertControllerStyleAlert];
      
        UIAlertAction *yes = [UIAlertAction actionWithTitle:NSLocalizedString(@"Yes", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [appdelegate stopTripAndSave];
        }];
        
        UIAlertAction* no = [UIAlertAction actionWithTitle:NSLocalizedString(@"No", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [self dismissViewControllerAnimated:true completion:nil];
        }];
        
        [alert addAction:yes];
        [alert addAction:no];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else{
        NSLog(@"onFinishTrip");
        [self stopTimer];
        [self stopLocationUpdate];
        
        firstTimePoint = 0;
        [self addTitleView2:NSLocalizedString(@"Ready_to_track", @"")];
        NSString *km = [NSString stringWithFormat:@"%@",appdelegate.appKM];
        float fkm = [km floatValue];
        if (fkm <= 0) {
            
            [self hideTripKM];
            [appdelegate clearMapData];
            [self clearMapAnnotation];
            
            appdelegate.appStartLocation = nil;
            appdelegate.appDistance = @"";
            
            [self removeTripData];
            
            [self dismissViewControllerAnimated: YES completion: ^{
                [self viewWillAppear:true];
            }];
        }else{
            
            dispatch_async(dispatch_get_global_queue(0, 0), ^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self clearMapAnnotation];
                    [self hideTripKM];
                });
            });
            [self saveTrip:isPrivate];
        }
        [self resetHours];
    }
}

// Through beacon finish trip button action
-(void)onFinishTripFunction{
    
    NSLog(@"onFinishTripFunction");
    if ([appdelegate.finalTrack isEqualToString:@"0"]) {
       // [self setBeaconeDiscconetMSG];
        [self stopTimer];
        [self stopLocationUpdate];
        firstTimePoint = 0;
        NSString *km = [NSString stringWithFormat:@"%@",appdelegate.appKM];
        float fkm = [km floatValue];
        
        if (fkm <= 0) {

            [self hideTripKM];
            [appdelegate clearMapData];
            [self clearMapAnnotation ];
            
            appdelegate.appStartLocation = nil;
            appdelegate.appDistance = @"";
            
            [self removeTripData];
            [self dismissViewControllerAnimated: YES completion: ^{
                [self viewWillAppear:true];
            }];
        }else{
            [self hideTripKM];
            dispatch_async(dispatch_get_global_queue(0, 0), ^{
                 //[self saveTrip:@"0"];
            });
        }
        [self saveTrip:@"0"];
        
        appdelegate.finalTrack = @"2";
        appdelegate.isTripContinue = @"0";
        [Helper setPREFStringValue:appdelegate.isTripContinue sKey:@"isTripContinue"];
        
        [def setValue:@"0" forKey:@"AppTerminate"];
        [self addTitleView2:NSLocalizedString(@"Ready_to_track", @"")];
        [self resetHours];
    }
}

-(void)stopLocationUpdate{
    [appdelegate.locationManager stopUpdatingLocation];
    //[appdelegate.locationManager stopMonitoringSignificantLocationChanges];
    [appdelegate.activityManager stopActivityUpdates];
}

-(void)resetHours{
    _lblHours.text = [NSString stringWithFormat:@"00:00:00"];
    if ([strMeasure.lowercaseString isEqualToString:@"miles"] || [strMeasure.lowercaseString isEqualToString:@"mi"]){
        _lblKM.text = [NSString stringWithFormat:@"0.0 mi"];
    }else
    {
        _lblKM.text = [NSString stringWithFormat:@"0.0 km"];
    }
}

-(void)freeToPaidTripPopupMessage {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:appdelegate.freeUserMessage preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *choose = [UIAlertAction actionWithTitle:NSLocalizedString(@"Choose", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [self performSegueWithIdentifier:@"segueFreeToPaidRegister" sender:self];
    }];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"Ok", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [alert dismissViewControllerAnimated:true completion:nil];
    }];
    [alert addAction:choose];
    [alert addAction:ok];
    [self presentViewController:alert animated:true completion:nil];
}

-(void)saveTrip:(NSString *)trip_t{
    
    NSLog(@"saveTrip");
    __block NSDictionary *dict;
    __block CLLocation *myLocation;
    __block NSString *sAddress = @"",*eAddress = @"";
    __block NSString *sTime,*eTime,*Tdate,*oStart,*oend,*Dist;
    
    if (appdelegate.appLocationDataArray.count > 0) {
        
        dict = [appdelegate.appLocationDataArray objectAtIndex:0];
        CLLocationDegrees latitudeDegrees = [[dict valueForKey:@"latitude"] doubleValue];
        CLLocationDegrees longitudeDegrees = [[dict valueForKey:@"longitude"] doubleValue];
        myLocation = [[CLLocation alloc]initWithLatitude:latitudeDegrees longitude:longitudeDegrees];
        [MBProgressHUD showHUDAddedTo:self.view.window animated:YES];
        
        NSUserDefaults *def = [[NSUserDefaults standardUserDefaults] init];
        
        //[def setValue:@"YES" forKey:@"TRTPSAVE"];
        [self getAddressFromLocation:myLocation callBackData:^(NSString *sAdd) {
            sAddress = sAdd;
            
            dict = [appdelegate.appLocationDataArray objectAtIndex:appdelegate.appLocationDataArray.count - 1];
            CLLocationDegrees latitudeDegrees = [[dict valueForKey:@"latitude"] doubleValue];
            CLLocationDegrees longitudeDegrees = [[dict valueForKey:@"longitude"] doubleValue];
            myLocation = [[CLLocation alloc]initWithLatitude:latitudeDegrees longitude:longitudeDegrees];
            
            [self getAddressFromLocation:myLocation callBackData:^(NSString *sAdd) {
                eAddress = sAdd;
                
                sTime = appdelegate.appSTTime;
                eTime = appdelegate.appEDTime;
                Tdate = appdelegate.appSTDate;
                
                NSString *odometer_start = [Helper isStringIsNull:[Helper getPREF:@"odometer_start"]];
                NSString *km = [NSString stringWithFormat:@"%@",appdelegate.appKM];
                
                float odoStart = [odometer_start floatValue];
                oStart = [NSString stringWithFormat:@"%.2f",odoStart];
                
                NSData *userdata = [Helper getDataValue:@"userInfo"];
                NSDictionary *dataDict = [self dataToDicitonary:userdata];
                if (![dataDict isKindOfClass:[NSNull class]]) {
                    userInfo = [dataDict valueForKey:@"data"];
                    
                    NSDictionary *generalSetting = [userInfo valueForKey:@"general_setting"];
                    strMeasure = [generalSetting valueForKey:@"unit"];
                    if ([km.lowercaseString containsString:@"km"] || [km.lowercaseString containsString:@"miles"] || [km.lowercaseString containsString:@"mi"]) {
                        km = [km stringByReplacingOccurrencesOfString:@"km" withString:@""];
                        km = [km stringByReplacingOccurrencesOfString:@"miles" withString:@""];
                        km = [km stringByReplacingOccurrencesOfString:@"mi" withString:@""];
                    }
                    
                    float fkm = [km floatValue];
                    if ([strMeasure.lowercaseString isEqualToString:@"miles"] || [strMeasure.lowercaseString isEqualToString:@"mi"]) {
                        //strMeasure = @"mi";
                        
                        float oST = [oStart floatValue];
                        oStart = [NSString stringWithFormat:@"%.2f",oST];
                       // Dist = [NSString stringWithFormat:@"%.2f",fkm];
                        
                        //float km = fkm * 1.60934;
                        float km = fkm * 0.621371;
                        oST = oST + km;
                        oend = [NSString stringWithFormat:@"%.2f",oST];
                        Dist = [NSString stringWithFormat:@"%.2f",km];
                        
                    }else{
                        odoStart = odoStart + fkm;
                        oend = [NSString stringWithFormat:@"%.2f",odoStart];
                        Dist = [NSString stringWithFormat:@"%@",km];
                    }
                    
                    NSString *userID = [Helper getPREF:@"userID"];
                    NSDictionary *parameter;
                    NSString *strLatLong;
                    NSMutableArray *tempArray = [[NSMutableArray alloc]init];
                    for (int i = 0; i < appdelegate.appLocationDataArray.count; i++) {
                        NSDictionary *dict = [appdelegate.appLocationDataArray objectAtIndex:i];
                        double lat = [[dict valueForKey:@"latitude"]doubleValue];
                        double lon = [[dict valueForKey:@"longitude"]doubleValue];
                        strLatLong = [NSString stringWithFormat:@"%f,%f",lat,lon];
                        //strLatLong = [NSString stringWithFormat:@"%.4f,%.4f",lat,lon];
                        [tempArray addObject:strLatLong];
                    }
                    NSArray *locationArray = [[NSArray alloc]init];
                    locationArray = [tempArray mutableCopy];
                    NSString *strLocation = [locationArray componentsJoinedByString:@"|"];
                    
                    sAddress = [Helper isStringIsNull:sAddress];
                    eAddress = [Helper isStringIsNull:eAddress];
                    strLocation = [Helper isStringIsNull:strLocation];
                    
                    parameter=@{ @"id":@"",@"userid":userID, @"trip_type":trip_t, @"start_address":sAddress, @"end_address":eAddress, @"trip_date":Tdate, @"start_time":sTime, @"end_time":eTime,@"odometer_start":oStart, @"odometer_finish":oend, @"purpose_trip":@" ", @"distance":Dist, @"note":@"", @"locations":strLocation, @"locations1":strLocation, @"version":appdelegate.appVersion, @"os":@"ios", @"build":appdelegate.appBuild };
                    NSLog(@"Trip saving... %@",parameter);
                    [MBProgressHUD hideHUDForView:self.view.window animated:YES];
                    
                    [self saveOfflineTrip:parameter];
                    if(![self CheckNetworkConnection]) {
                        return;
                    }else{
                        [self callAddTripServiceWithoutPopup:parameter];
                    }
                }
            }];
        }];
    } else {
       [MBProgressHUD hideHUDForView:self.view.window animated:YES];
        
        [Helper displayAlertView:@"" message:NSLocalizedString(@"something_went_wrong_msg", @"")];
    }
}

#pragma mark - on beacon disconnect save trip without open confirm popup
/*-(void)saveTrip:(NSString *)trip_t{
    
    NSLog(@"saveTrip");
    
    NSDictionary *dict;
    NSString *sAdd,*eAdd,*sTime,*eTime,*Tdate,*oStart,*oend,*Dist;
    
    if (appdelegate.appLocationDataArray.count > 0) {
        dict = [appdelegate.appLocationDataArray objectAtIndex:0];
        sAdd = [NSString stringWithFormat:@"%@",[dict valueForKey:@"Address"]];
        dict = [appdelegate.appLocationDataArray objectAtIndex:appdelegate.appLocationDataArray.count - 1];
        eAdd = [NSString stringWithFormat:@"%@",[dict valueForKey:@"Address"]];
    }
    sTime = appdelegate.appSTTime;
    eTime = appdelegate.appEDTime;
    Tdate = appdelegate.appSTDate;
    
    NSString *odometer_start = [Helper isStringIsNull:[Helper getPREF:@"odometer_start"]];
    NSString *km = [NSString stringWithFormat:@"%@",appdelegate.appKM];
    
    float odoStart = [odometer_start floatValue];
    oStart = [NSString stringWithFormat:@"%.2f",odoStart];
    
    
    NSData *userdata = [Helper getDataValue:@"userInfo"];
    NSDictionary *dataDict = [self dataToDicitonary:userdata];
    if (![dataDict isKindOfClass:[NSNull class]]) {
        userInfo = [dataDict valueForKey:@"data"];
        
        NSDictionary *generalSetting = [userInfo valueForKey:@"general_setting"];
        strMeasure = [generalSetting valueForKey:@"unit"];
        if ([km.lowercaseString containsString:@"km"] || [km.lowercaseString containsString:@"miles"] || [km.lowercaseString containsString:@"mi"]) {
            km = [km stringByReplacingOccurrencesOfString:@"km" withString:@""];
            km = [km stringByReplacingOccurrencesOfString:@"miles" withString:@""];
            km = [km stringByReplacingOccurrencesOfString:@"mi" withString:@""];
        }
    
        float fkm = [km floatValue];
        if ([strMeasure.lowercaseString isEqualToString:@"miles"]) {
            strMeasure = @"mi";
            
            float oST = [oStart floatValue];
            oStart = [NSString stringWithFormat:@"%.2f",oST];
            Dist = [NSString stringWithFormat:@"%.2f",fkm];
            
            float km = fkm * 1.60934;
            oST = oST + km;
            oend = [NSString stringWithFormat:@"%.2f",oST];
        }else{
            odoStart = odoStart + fkm;
            oend = [NSString stringWithFormat:@"%.2f",odoStart];
            Dist = [NSString stringWithFormat:@"%@",km];
        }
        
        NSString *userID = [Helper getPREF:@"userID"];
        NSDictionary *parameter;
        NSString *strLatLong;
        NSMutableArray *tempArray = [[NSMutableArray alloc]init];
        for (int i = 0; i < appdelegate.appLocationDataArray.count; i++) {
            NSDictionary *dict = [appdelegate.appLocationDataArray objectAtIndex:i];
            double lat = [[dict valueForKey:@"latitude"]doubleValue];
            double lon = [[dict valueForKey:@"longitude"]doubleValue];
            strLatLong = [NSString stringWithFormat:@"%.4f,%.4f",lat,lon];
            [tempArray addObject:strLatLong];
        }
        
        NSArray *locationArray = [[NSArray alloc]init];
        locationArray = [tempArray mutableCopy];
        NSString *strLocation = [locationArray componentsJoinedByString:@"|"];
        //NSLog(@"locations : %@",strLocation);
        NSString *startAdd,*ebdAdd;
        if (appdelegate.appLocationDataArray.count > 0) {
            NSDictionary* dict = [appdelegate.appLocationDataArray objectAtIndex:0];
            startAdd = [NSString stringWithFormat:@"%@",[dict valueForKey:@"Address"]];
            startAdd = [Helper isStringIsNull:startAdd];
            
            dict = [appdelegate.appLocationDataArray objectAtIndex:appdelegate.appLocationDataArray.count - 1];
            ebdAdd = [NSString stringWithFormat:@"%@",[dict valueForKey:@"Address"]];
            ebdAdd = [Helper isStringIsNull:ebdAdd];
        }
        strLocation = [Helper isStringIsNull:strLocation];
        
        parameter=@{ @"id":@"",@"userid":userID, @"trip_type":trip_t, @"start_address":startAdd, @"end_address":ebdAdd, @"trip_date":Tdate, @"start_time":sTime, @"end_time":eTime,@"odometer_start":oStart, @"odometer_finish":oend, @"purpose_trip":@" ", @"distance":Dist, @"note":@"", @"locations":strLocation, @"locations1":strLocation, @"version":appdelegate.appVersion, @"os":@"ios", @"build":appdelegate.appBuild };
        NSLog(@"Trip saving... %@",parameter);
        
        [self saveOfflineTrip:parameter];
        if(![self CheckNetworkConnection]) {
            return;
        }else{
            [self callAddTripServiceWithoutPopup:parameter];
        }
    }
    else {
            [Helper displayAlertView:@"" message:NSLocalizedString(@"something_went_wrong_msg", @"")];
    }
}*/

-(void)calculateKMToMiles:(float)km{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"Miles : %f",km);
        float miles = km * 0.621371;
        _lblKM.text = [NSString stringWithFormat:@"%.2f mi",miles];
    });
}



#pragma mark - Get Car Location By Dantracker
-(void)callInitialDanttrackerApi:(NSString *)reload{
    
     NSLog(@"callInitialDanttrackerApi");
    //NSLog(@"callInitialDanttrackerApi");
    NSDictionary *dict = [userInfo valueForKey:@"data"];
    trackerID = [dict valueForKey:@"tracker_id"];
    if (trackerID.length <= 0 || trackerID == nil || trackerID == (NSString *)[NSNull null] || [trackerID isEqualToString:@""]){
        NSLog(@"tracker id not available");
    }else{
        NSDictionary *dantracker = [Helper getUserValue:appdelegate.dantracker];
        NSString *username = [NSString stringWithFormat:@"%@",[dantracker valueForKey:@"username"]];
        NSString *password = [NSString stringWithFormat:@"%@",[dantracker valueForKey:@"password"]];
        
        if(![self CheckNetworkConnection]) {
            return;
        }
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'hh:mm:ss'Z'"];
        NSString *currentDateString =[dateFormatter stringFromDate:[NSDate date]];
        
        [WebService httpGetWithCustomDelegateURLString:[NSString stringWithFormat:@"http://api.dantracker.com/trackers/%@/positions?minDateTime=%@",trackerID,currentDateString] userName:username pasword:password isDebug:true callBackData:^(NSData *data, NSError *error) {
            
            if (error == nil) {
                NSError *localError = nil;
                
                NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
                if (localError != nil) {
                    NSLog(@"dantracker : %@",localError.description);
                    //[self alertErrorwithMessage:NSLocalizedString(@"Wecouldnotfindyoucarlocation", @"")];
                }else {
                    
                    if (jsonArray.count > 0) {
                        //appdelegate.dantrackerCarLocationArray = [[NSArray alloc]init];
                        appdelegate.dantrackerCarLocationArray = [jsonArray mutableCopy];
                        NSLog(@"dantracker location found...");
                        if ([reload isEqualToString:@"1"]) {
                            [self zoomToCarLocation];
                        }
                    }else{
                        
                        // If today's car location not found then please display previous date location of car
                        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
                        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'hh:mm:ss'Z'"];
                        
                        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
                        NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
                        [offsetComponents setDay:-5]; // replace "-1" with "1" to get the datePlusOneDay
                        NSDate *dateMinusfiveDay = [gregorian dateByAddingComponents:offsetComponents toDate:[NSDate date] options:0];
                        NSString *currentDateString =[dateFormatter stringFromDate:dateMinusfiveDay];
                        
                        [WebService httpGetWithCustomDelegateURLString:[NSString stringWithFormat:@"http://api.dantracker.com/trackers/%@/positions?minDateTime=%@",trackerID,currentDateString] userName:username pasword:password isDebug:true callBackData:^(NSData *data, NSError *error) {
                            
                            if (error == nil) {
                                NSError *localError = nil;
                                
                                NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
                                if (localError != nil) {
                                    NSLog(@"dantracker : %@",localError.description);
                                    //[self alertErrorwithMessage:NSLocalizedString(@"Wecouldnotfindyoucarlocation", @"")];
                                }else {
                                    if (jsonArray.count > 0) {
                                        //appdelegate.dantrackerCarLocationArray = [[NSArray alloc]init];
                                        appdelegate.dantrackerCarLocationArray = [jsonArray mutableCopy];
                                        if ([reload isEqualToString:@"1"]) {
                                            [self zoomToCarLocation];
                                        }

                                    }else{
                                        
                                    }
                                }
                            }else{
                                //[self alertErrorwithMessage:NSLocalizedString(@"Wecouldnotfindyoucarlocation", @"")];
                            }
                        }];
                    }
                }
            }else{
                //[self alertErrorwithMessage:NSLocalizedString(@"Wecouldnotfindyoucarlocation", @"")];
            }
        }];
    }
}


// Zoom car position
-(void)zoomToCarLocation{
    
    if (appdelegate.dantrackerCarLocationArray.count > 0) {
        NSDictionary *locationDict = [appdelegate.dantrackerCarLocationArray objectAtIndex:appdelegate.dantrackerCarLocationArray.count - 1];
        double lat  = [[locationDict valueForKey:@"lat"] doubleValue];
        double lon  = [[locationDict valueForKey:@"lng"] doubleValue];
        CLLocationCoordinate2D Coordinates;
        CLLocation *loc = [[CLLocation alloc] initWithLatitude:lat longitude:lon];
        Coordinates = loc.coordinate;
        
        __block NSString *Address = @"";
        [geocoder reverseGeocodeLocation:loc completionHandler:^(NSArray *placemarks, NSError *error)
         {
             if (!(error)) {
                 CLPlacemark *placemark = [placemarks objectAtIndex:0];
                 //NSLog(@"placemark %@",placemark);
                 NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
                 Address = [[NSString alloc]initWithString:[Helper isStringIsNull:locatedAt]];
                 NSString *Area = [[NSString alloc]initWithString:[Helper isStringIsNull:placemark.locality]];
                 NSString *Country = [[NSString alloc]initWithString:[Helper isStringIsNull:placemark.country]];
                 NSString *CountryArea = [self printAddress:@"" :Area :Country];
                 Address = Address;//[self printAddress:Area :Country :CountryArea];
                 
                 _calloutView = [[CallOutAnnotationView alloc] initWithName:Address address:Address phonenumber:@"" coordinate:loc.coordinate];
                 _calloutView.contentView.frame = CGRectMake(_calloutView.contentView.frame.origin.x, _calloutView.contentView.frame.origin.y, _calloutView.contentView.frame.size.width, 300);
                 [_calloutViewArray addObject:_calloutView];
                 [_MapViewMy addAnnotation:_calloutView];
                 
                 //[NSString stringWithFormat:@"%@, %@", Area,Country]
                 //NSLog(@"%@",CountryArea);
             } else {
                 NSLog(@"Geocode failed with error %@", error);
                 NSLog(@"\nCurrent Location Not Detected\n");
             }
         }];
        
        [_MapViewMy setCenterCoordinate:Coordinates animated:YES];
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance ( loc.coordinate, 1000, 1000);
        [_MapViewMy setRegion:region animated:NO];
        
        for (int i = 0; i < _calloutViewArray.count ; i++) {
            _calloutView = [_calloutViewArray objectAtIndex:i];
            [_MapViewMy removeAnnotation:_calloutView];
        }
        /*_calloutView = [[CallOutAnnotationView alloc] initWithName:Address address:Address phonenumber:@"" coordinate:loc.coordinate];
        _calloutView.contentView.frame = CGRectMake(_calloutView.contentView.frame.origin.x, _calloutView.contentView.frame.origin.y, _calloutView.contentView.frame.size.width, 300);
        [_calloutViewArray addObject:_calloutView];
        [_MapViewMy addAnnotation:_calloutView];*/
        
        if ([carDisplayTimer isValid]) {
            [carDisplayTimer invalidate];
            carDisplayTimer = nil;
            carDisplayCounter = 0;
        }
        
        carDisplayTimer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(onCarTick:) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:carDisplayTimer forMode:NSRunLoopCommonModes];
        NSLog(@"Car display counter started for 10 second.");
        
    }else{
        
        // Zoom car data = 1 otherwise 0
        [self callInitialDanttrackerApi:@"1"];
        
    }
}

-(void)onCarTick:(NSTimer*)timer{
    carDisplayCounter = carDisplayCounter + 1;
    
    if (carDisplayCounter >= 10) {
        [carDisplayTimer invalidate];
        carDisplayTimer = nil;
        carDisplayCounter = 0;
        NSLog(@"car timer stopped");
        [_MapViewMy setCenterCoordinate:appdelegate.appUserLocation.coordinate animated:YES];
    }
}

// On Car button pressed checked tracker purchased or not if yes zoom car location
-(void)displayCarLocation{
    
    NSLog(@"displayCarLocation");
    NSData *userdata = [Helper getDataValue:@"userInfo"];
    
    if (![userdata isKindOfClass:[NSNull class]]) {
        userInfo = [[self dataToDicitonary:userdata] mutableCopy];
        NSDictionary *dict = [userInfo valueForKey:@"data"];
        trackerID = [dict valueForKey:@"tracker_id"];
        NSDictionary *paymentInfo = [dict valueForKey:@"payment_info"];
        NSString *gps_tracker = [paymentInfo valueForKey:@"gps_tracker"];
        float gpsValue = gps_tracker==(NSString *)[NSNull null]?0.0:[gps_tracker floatValue];
        if (trackerID.length <= 0)
        {
            //if (!trackerPopup) {
            if(gpsValue > 0) {
                
                UIAlertController * alertVC =[UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"You_have_purchased_TRACKER_but_need_to_configure", @"") preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* yesButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"Yes", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                    
                    NSString * storyboardName = @"Main";
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
                    UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"AddTrackerID"];
                    [self.navigationController pushViewController:vc animated:YES];
                    
                }];
                
                UIAlertAction* noButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"No", @"") style:UIAlertActionStyleDefault                                                              handler:^(UIAlertAction * action) {
                    [self dismissViewControllerAnimated:YES completion:nil];
                }];
                
                [alertVC addAction:yesButton];
                [alertVC addAction:noButton];
                [self presentViewController:alertVC animated:YES completion:nil];
                
            } else {
                
                UIAlertController * alertVC =[UIAlertController alertControllerWithTitle:NSLocalizedString(@"where_is_my_car", @"") message:NSLocalizedString(@"popup_tracker_msg_new", @"") preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* yesButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"Buy", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                    
                    [self performSegueWithIdentifier:@"segueTracker" sender:self];
                    
                }];
                
                UIAlertAction* noButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"No_thanks", @"") style:UIAlertActionStyleDefault                                                              handler:^(UIAlertAction * action) {
                    [self dismissViewControllerAnimated:YES completion:nil];
                }];
                
                [alertVC addAction:yesButton];
                [alertVC addAction:noButton];
                [self presentViewController:alertVC animated:YES completion:nil];
            }
        }else{
            [self callInitialDanttrackerApi:@"1"];
        }
    }else{
        [Helper displayAlertView:@"" message:NSLocalizedString(@"something_went_wrong_msg", @"")];
    }
}


-(void)callAddTripServiceWithoutPopup:(NSDictionary *)parameter{
    
    NSLog(@"callAddTripServiceWithoutPopup");
        if(![self CheckNetworkConnection]) {
           
            return;
        }
    
        [MBProgressHUD showHUDAddedTo:self.view.window animated:YES];
        [WebService httpPostWithCustomDelegateURLString:@".saveTrip" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error)
         {
             
             [MBProgressHUD hideHUDForView:self.view.window animated:YES];
             if (error == nil) {
                 NSError *localError = nil;
                 NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
                 //savingTrip = 0;
                 if (localError != nil) {
                     NSLog(@"%@",localError.description);
                     NSLog(@"Re Parameter : %@",parameter);
                     if (parameter != nil) {
                         NSLog(@"Network issue saved into local DB");
                     }
                 }
                 else {
                     NSDictionary *response = [parsedObject objectForKey:@"data"];
                     NSString* message = [response objectForKey:@"message"];
                     NSString* success = [response objectForKey:@"status"];
                     if ([success isEqualToString:@"Success"]) {
                         
                         [self removeLastTripFromLocalDB];
                         [appdelegate clearMapData];
                         [self clearMapAnnotation ];
                         
                         appdelegate.appStartLocation = nil;
                         appdelegate.appDistance = @"";
                         
                         [self removeTripData];
                         
                         [self dismissViewControllerAnimated: YES completion: ^{
                             [self viewWillAppear:true];
                         }];
                         NSDictionary *data = [response valueForKey:@"trip"];
                         [self calculateDistance:data];
                     } else {
                         [self removeLastTripFromLocalDB];
                         if([message isEqualToString:@"Trip distance is too small"])
                         {
                             message = NSLocalizedString(@"tripsdistancetoosmall", @"");
                         }
                         [Helper displayAlertView:@"" message:message];
                     }
                 }
             }
         }];
}

-(void)calculateDistance:(NSDictionary *)data{
    NSLog(@"Calculate Distance called : %@",data);
    NSString *locations = [data valueForKey:@"locations"];
    NSString *tID = [data valueForKey:@"id"];
    NSArray *locationArray = [locations componentsSeparatedByString:@"|"];
    
    NSString *latLong;
    NSArray *latlonArray;
    double lat,lon;
    CLLocation *oldLocation, *newLocation;
    float newDist = 0.0;
    
    for (int i = 0; i < locationArray.count; i++) {
        if (i > 0) {
            latLong = [locationArray objectAtIndex:i - 1];
            latlonArray = [latLong componentsSeparatedByString:@","];
            lat = [[latlonArray objectAtIndex:0] doubleValue];
            lon = [[latlonArray objectAtIndex:1] doubleValue];
            oldLocation = [[CLLocation alloc] initWithLatitude:lat longitude:lon];
            
            latLong = [locationArray objectAtIndex:i];
            latlonArray = [latLong componentsSeparatedByString:@","];
            lat = [[latlonArray objectAtIndex:0] doubleValue];
            lon = [[latlonArray objectAtIndex:1] doubleValue];
            newLocation = [[CLLocation alloc] initWithLatitude:lat longitude:lon];
            
            CLLocationDistance currentDistance = [newLocation distanceFromLocation:oldLocation] / 1000;
            //NSString *convertTo2Digit = [[NSString alloc] initWithFormat:@"%.2f", currentDistance];
            newDist = newDist + currentDistance;//[convertTo2Digit floatValue];
            if (newDist <= 0) {
                newDist = 0.00;
            }
        }
    }
    
    NSString *oStart,*oend,*Dist;
    
    NSString *km = [[NSString alloc] initWithFormat:@"%.2f", newDist];
    
    NSString *odometer_start = [Helper isStringIsNull:[Helper getPREF:@"odometer_start"]];
    float odoStart = [odometer_start floatValue];
    oStart = [NSString stringWithFormat:@"%.2f",odoStart];
    
    NSData *userdata = [Helper getDataValue:@"userInfo"];
    NSDictionary *dataDict = [self dataToDicitonary:userdata];
    if (![dataDict isKindOfClass:[NSNull class]]) {
        userInfo = [dataDict valueForKey:@"data"];
        
        NSDictionary *generalSetting = [userInfo valueForKey:@"general_setting"];
        strMeasure = [generalSetting valueForKey:@"unit"];
        if ([km.lowercaseString containsString:@"km"] || [km.lowercaseString containsString:@"miles"] || [km.lowercaseString containsString:@"mi"]) {
            km = [km stringByReplacingOccurrencesOfString:@"km" withString:@""];
            km = [km stringByReplacingOccurrencesOfString:@"miles" withString:@""];
            km = [km stringByReplacingOccurrencesOfString:@"mi" withString:@""];
        }
        
        float fkm = [km floatValue];
        if ([strMeasure.lowercaseString isEqualToString:@"miles"] || [strMeasure.lowercaseString isEqualToString:@"mi"]) {
            float oST = [oStart floatValue];
            oStart = [NSString stringWithFormat:@"%.2f",oST];
            float km = fkm * 0.621371;
            oST = oST + km;
            oend = [NSString stringWithFormat:@"%.2f",oST];
            Dist = [NSString stringWithFormat:@"%.2f",km];
        }else{
            odoStart = odoStart + fkm;
            oend = [NSString stringWithFormat:@"%.2f",odoStart];
            Dist = [NSString stringWithFormat:@"%@",km];
        }
    }
    NSLog(@"Total Distance : %@",Dist);
    
    NSDictionary *parameter=@{ @"id":tID,@"distance":Dist,@"odometer_start":oStart, @"odometer_finish":oend};
    NSLog(@"parameter : %@",parameter);
    [self updateDistanceOfTrip:parameter];
}


-(void)updateDistanceOfTrip:(NSDictionary *)parameter{
    [WebService httpPostWithCustomDelegateURLString:@".saveTripDistance" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error)
     {
         if (error == nil) {
             NSError *localError = nil;
             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
             if (localError != nil) {
                 NSLog(@"Distance not updated %@",localError.description);
             }
             else {
                 NSDictionary *response = [parsedObject objectForKey:@"data"];
                 NSString* success = [response objectForKey:@"status"];
                 NSLog(@"Distance updated: %@",success);
             }
         }
     }];
}

- (IBAction)CallBeaconView:(id)sender {
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    BeaconConfigureViewController* controller = [storyboard instantiateViewControllerWithIdentifier:@"BeaconConfigureView"];
    
    [self.navigationController pushViewController:controller animated:YES];
}
-(void)Checkbeacone
{
    NSString* isBeaconConnected = [Helper getPREF:@"isBeaconConnected"];
    CLRegionState currentStage = (NSInteger)[Helper getPREFNSint:@"currentStage"];
    BOOL bluth = [[NSUserDefaults standardUserDefaults] boolForKey:@"BluetoothStat"];
    if(bluth == TRUE){
        if ([isBeaconConnected isEqualToString:@"1"]) {
            if (currentStage == CLRegionStateInside)
            {
                [self setBeaconeConnectMSG];
            }else{
                [self setBeaconeDiscconetMSG];
            }
        }else{
            [self setBeaconeDiscconetMSG];
        }
    }else{
        [self setBeaconeDiscconetMSG];
    }
}

//Display Beacon Disccone Status Message on header
-(void)setBeaconeDiscconetMSG
{
    [_BtnBeconView setHidden:FALSE];
    _Lbl_beacon_status_msg.text = NSLocalizedString(@"beacone_Disconnect_msg", @"");
    _Lbl_beacon_status_msg.textColor = [UIColor whiteColor];
    _Lbl_beacon_status_msg.backgroundColor = [UIColor redColor];
    [_Lbl_beacon_status_msg setTextAlignment:NSTextAlignmentCenter];
}

//Display Beacon Connect Status Message on header
-(void)setBeaconeConnectMSG
{
    [_BtnBeconView setHidden:TRUE];
    _Lbl_beacon_status_msg.text = NSLocalizedString(@"beacone_connect_msg", @"");
    _Lbl_beacon_status_msg.textColor = [UIColor whiteColor];
    _Lbl_beacon_status_msg.backgroundColor = [UIColor colorWithRed:104.0/255.0 green:189.0/255.0 blue:69.0/255.0 alpha:1] ;
    [_Lbl_beacon_status_msg setTextAlignment:NSTextAlignmentCenter];
}

-(void)callBeaconConnectedStatusService{
    //NSLog(@"connect service status");
    NSLog(@"callBeaconConnectedStatusService");
    
    // ConnectionStatus = when becon detect set it to 1 and on exit set it to 2 otherwise 0
    if ([appdelegate.ConnectionStatus isEqualToString:@"1"])
    {
        [self Checkbeacone];
        
        appdelegate.ConnectionStatus = @"0";
        if(![self CheckNetworkConnection]) {
            return;
        }
        
        if (appdelegate.appLocationDataArray.count > 0) {
            NSDictionary *dict;
            NSString *latittude,*longitude;
            NSString *BeaconMajor,*BeaconMinor,*proximityUUID;
            
            dict = [appdelegate.appLocationDataArray objectAtIndex:0];
            latittude = [NSString stringWithFormat:@"%f",[[dict valueForKey:@"latitude"]doubleValue]];
            longitude = [NSString stringWithFormat:@"%f",[[dict valueForKey:@"longitude"]doubleValue]];
            //latittude = [NSString stringWithFormat:@"%.4f",[[dict valueForKey:@"latitude"]floatValue]];
            //longitude = [NSString stringWithFormat:@"%.4f",[[dict valueForKey:@"longitude"]floatValue]];
            
            
            // Get list of beacon connected and start monitoring.
            for (int i =0; i < appdelegate.BeaconArray.count; i++) {
                NSDictionary *beacon = [appdelegate.BeaconArray objectAtIndex:i];
                if ([appdelegate.ConnectedbeaconRegion isEqualToString:[NSString stringWithFormat:@"%d",i+1]]) {
                    BeaconMajor = [beacon valueForKey:@"major"];
                    BeaconMinor = [beacon valueForKey:@"minor"];
                    proximityUUID = [beacon valueForKey:@"proximityUUID"];
                }
            }
            
            NSDictionary *parameter=@{ @"status":@"1",@"lat":latittude, @"lng":longitude, @"proximityUUID":[NSString stringWithFormat:@"%@,%@",BeaconMajor,BeaconMinor], @"os":@"ios" };
            NSLog(@"Beacon Connected service : %@",parameter);
            [WebService httpPostWithCustomDelegateURLString:@".beaconStatus" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error)
             {
                 if (error == nil) {
                     NSError *localError = nil;
                     NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
                     
                     if (localError != nil) {
                         NSLog(@"Connect error: %@",localError.description);
                     }
                     else {
                         NSDictionary *response = [parsedObject objectForKey:@"data"];
                         //NSLog(@"response : %@",response);
                         NSString* success = [response objectForKey:@"status"];
                         NSLog(@"Succes : %@",success);
                         [appdelegate callTrackingUpdateService:@"1"];
                     }
                 }
             }];
        }else{
            [self performSelector:@selector(callBeaconStatusConnectInfo) withObject:self afterDelay:5.0];
        }
    }
}

-(void)callBeaconStatusConnectInfo{
    
    NSLog(@"callBeaconStatusConnectInfo");
    appdelegate.ConnectionStatus = @"1";
    
    [self callBeaconConnectedStatusService];
}
-(void)callBeaconStatusDisConnectInfo{
    NSLog(@"callBeaconStatusDisConnectInfo");
    appdelegate.ConnectionStatus = @"2";
    //[self setBeaconeDiscconetMSG];
    [self callBeaconDisConnectedStatusService];
}

-(void)callBeaconDisConnectedStatusService{
    
     NSLog(@"callBeaconDisConnectedStatusService");
    //NSLog(@"Disconnect service status");
    if ([appdelegate.ConnectionStatus isEqualToString:@"2"]) {
        appdelegate.ConnectionStatus = @"0";
        [self Checkbeacone];
        if(![self CheckNetworkConnection]) {
            return;
        }
        NSDictionary *dict;
        NSString *latittude,*longitude;
        NSString *BeaconMajor,*BeaconMinor,*proximityUUID;
        
        if (appdelegate.appLocationDataArray.count > 0) {
            dict = [appdelegate.appLocationDataArray objectAtIndex:appdelegate.appLocationDataArray.count - 1];
            latittude = [NSString stringWithFormat:@"%f",[[dict valueForKey:@"latitude"]doubleValue]];
            longitude = [NSString stringWithFormat:@"%f",[[dict valueForKey:@"longitude"]doubleValue]];
            //latittude = [NSString stringWithFormat:@"%.4f",[[dict valueForKey:@"latitude"]doubleValue]];
            //longitude = [NSString stringWithFormat:@"%.4f",[[dict valueForKey:@"longitude"]doubleValue]];
            
        }else{
           // latittude = [NSString stringWithFormat:@"%f",appdelegate.locationManager.location.coordinate.latitude];
           // longitude = [NSString stringWithFormat:@"%f",appdelegate.locationManager.location.coordinate.longitude];
            latittude = @"";
            longitude = @"";
        }
        
        // Get list of beacon connected and start monitoring.
        for (int i =0; i < appdelegate.BeaconArray.count; i++) {
            NSDictionary *beacon = [appdelegate.BeaconArray objectAtIndex:i];
            if ([appdelegate.ConnectedbeaconRegion isEqualToString:[NSString stringWithFormat:@"%d",i+1]]) {
                BeaconMajor = [beacon valueForKey:@"major"];
                BeaconMinor = [beacon valueForKey:@"minor"];
                proximityUUID = [beacon valueForKey:@"proximityUUID"];
            }
        }
        NSDictionary *parameter=@{ @"status":@"0",@"lat":latittude, @"lng":longitude, @"proximityUUID":[NSString stringWithFormat:@"%@,%@",BeaconMajor,BeaconMinor], @"os":@"ios" };
        
        //NSLog(@"Beacon Disconnected service : %@",parameter);
        [WebService httpPostWithCustomDelegateURLString:@".beaconStatus" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error)
         {
             if (error == nil) {
                 NSError *localError = nil;
                 NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
                 if (localError != nil) {
                     NSLog(@"Disconnect %@",localError.description);
                 }
                 else {
                     NSDictionary *response = [parsedObject objectForKey:@"data"];
                     NSString* success = [response objectForKey:@"status"];
                     NSLog(@"BeaconStatus: %@",success);
                     [appdelegate callTrackingUpdateService:@"0"];
                 }
             }
         }];
    }
}

#pragma mark - zoomIn
- (IBAction)zoomIn:(id)sender {
    
    //NSLog(@"zoomIn");
    MKUserLocation *userLocation = _MapViewMy.userLocation;
    MKCoordinateRegion region =
    MKCoordinateRegionMakeWithDistance ( userLocation.location.coordinate, 1000, 1000);
    [_MapViewMy setRegion:region animated:NO];
}

#pragma mark - Left menu gestureRecognizer
-(void)handleGestureNext:(UISwipeGestureRecognizer *)recognizer {
    //NSLog(@"Swipe Recevied");
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (void)dealloc {
     NSLog(@"dealloc");
#if DEBUG
    // Xcode8/iOS10 MKMapView bug workaround
    static NSMutableArray* unusedObjects;
    if (!unusedObjects)
        unusedObjects = [NSMutableArray new];
    
    if (_MapViewMy != nil) {
        [unusedObjects addObject:_MapViewMy];
    }
#endif
}

#pragma mark - revealLeftPane
-(IBAction)revealLeftPane:(id)sender {
    [self revealLeftPane2:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

// Save and get trip start time date
-(void)saveTripStartTime:(NSDate *)appStartTime{
    NSString *str = [self getStringFromDate:appStartTime];
    [Helper setPREFStringValue:str sKey:@"appStartTime"];
}
-(NSDate *)getTripStartTime{
    NSString *startTime = [Helper getPREF:@"appStartTime"];
    NSDate *date = [self getDateFromString:startTime];
    return date;
}

// Save and get trip sttime
-(void)saveTripSTTime:(NSString *)appSTTime{
    [Helper setPREFStringValue:appSTTime sKey:@"appSTTime"];
}
-(NSString *)getTripSTTime{
    NSString *appSTTime = [Helper getPREF:@"appSTTime"];
    return appSTTime;
}

-(void)saveTripSTDate:(NSString *)appSTDate{
    [Helper setPREFStringValue:appSTDate sKey:@"appSTDate"];
}
-(NSString *)getTripSTDate{
    NSString *appSTDate = [Helper getPREF:@"appSTDate"];
    return appSTDate;
}

// Save and get trip location array
-(void)saveTripLocationDataArray:(NSMutableArray *)locationArray{
    [Helper setLocationArrayValue:locationArray sKey:@"appLocationDataArray"];
}
-(NSMutableArray *)getTripLocationDataArray{
    return [Helper getLocationArrayValue:@"appLocationDataArray"];
}

// Save and get trip start location
-(void)saveTripStartLocation:(NSString *)appStartLocation{
    [Helper setPREFStringValue:appStartLocation sKey:@"appStartLocation"];
}
-(CLLocation *)getTripStartLocation{
    NSString *location = [Helper getPREF:@"appStartLocation"];
    NSArray *arr = [location componentsSeparatedByString:@","];
    CLLocation *LocationAtual ;
    if (arr.count > 0) {
        NSString *lat = [arr objectAtIndex:0];
        NSString *lng = [arr objectAtIndex:1];
        LocationAtual =[[CLLocation alloc] initWithLatitude:[lat doubleValue] longitude:[lng doubleValue]];
    }
    return LocationAtual;
}

// Save and get trip distance
-(void)saveTripDistance:(NSString *)appDistance{
    [Helper setPREFStringValue:appDistance sKey:@"appDistance"];
    
}
-(NSString *)getTripDistance{
    return [Helper getPREF:@"appDistance"];
}

// Save and get trip distance
-(void)saveTripType:(NSString *)type{
    [Helper setPREFStringValue:type sKey:@"type"];
    
}
-(NSString *)getTripType{
    return [Helper getPREF:@"type"];
}

-(void)removeTripData{
    [Helper delPREF:@"type"];
    [Helper delPREF:@"appDistance"];
    [Helper delPREF:@"appStartLocation"];
    [Helper delPREF:@"appLocationDataArray"];
    [Helper delPREF:@"appSTTime"];
    [Helper delPREF:@"appSTDate"];
    [Helper delPREF:@"appStartTime"];
    [Helper delPREF:@"isTripContinue"];
    [Helper delPREF:@"isTripAutomatic"];
    
    appdelegate.appStartTime = nil;
    appdelegate.appSTTime = @"";
    appdelegate.appSTDate = @"";
    appdelegate.appLocationDataArray = [[NSMutableArray alloc]init];
    appdelegate.appStartLocation = nil;
    appdelegate.appDistance = @"";
    [appdelegate.appTimer invalidate];
    
    appdelegate.isTripContinue = @"0";
    appdelegate.isTripAutomatic = @"0";
    [Helper setPREFStringValue:appdelegate.isTripAutomatic sKey:@"isTripAutomatic"];
    [Helper setPREFStringValue:appdelegate.isTripContinue sKey:@"isTripContinue"];
    firstTimePoint = 0;
    
}

-(void)SaveTripAtAppReStarttimeIfBeconDisconnect
{
    appdelegate.appStartTime = [self getTripStartTime];
    appdelegate.appSTTime = [self getTripSTTime];
    appdelegate.appSTDate = [self getTripSTDate];
    appdelegate.appLocationDataArray = [self getTripLocationDataArray];
    appdelegate.appStartLocation = [self getTripStartLocation];
    appdelegate.appDistance = [self getTripDistance];
    [appdelegate.appTimer invalidate];
   // [self setBeaconeDiscconetMSG];
    [self stopTimer];
    [appdelegate.locationManager stopUpdatingLocation];
    [appdelegate.locationManager stopMonitoringSignificantLocationChanges];
    float totalDist = [appdelegate.appDistance floatValue];
    NSString *newDist = [[NSString alloc] initWithFormat:@"%.2f", totalDist];
    appdelegate.appKM = newDist;
    
    
    NSString *km = [NSString stringWithFormat:@"%@",appdelegate.appKM];
    float fkm = [km floatValue];
    if (fkm <= 0) {
        //savingTrip = 0;
        
        [appdelegate clearMapData];
        [self clearMapAnnotation ];
        
        appdelegate.appStartLocation = nil;
        appdelegate.appDistance = @"";
        
        [self removeTripData];
        [self dismissViewControllerAnimated: YES completion: ^{
            [self viewWillAppear:true];
        }];
    }else{
        [self saveTrip:@"0"];
    }
    
    appdelegate.finalTrack = @"2";
    appdelegate.isTripContinue = @"0";
    [Helper setPREFStringValue:appdelegate.isTripContinue sKey:@"isTripContinue"];
    [def setValue:@"0" forKey:@"AppTerminate"];
    [self addTitleView2:NSLocalizedString(@"Ready_to_track", @"")];
    firstTimePoint = 0;
    _lblHours.text = [NSString stringWithFormat:@"00:00:00"];
    _lblKM.text = [NSString stringWithFormat:@"0.0 km"];
    [self hideTripKM];
    [self showTripButton];
    
}

-(void)startTripAfterRestart{
    NSLog(@"startTripAfterRestart");
    appdelegate.appStartTime = [self getTripStartTime];
    appdelegate.appSTTime = [self getTripSTTime];
    appdelegate.appSTDate = [self getTripSTDate];
    appdelegate.appLocationDataArray = [self getTripLocationDataArray];
    appdelegate.appStartLocation = [self getTripStartLocation];
    appdelegate.appDistance = [self getTripDistance];
    [appdelegate.appTimer invalidate];
    [self redrawWithClear];
    [self showTripKM];
    
    appdelegate.appTimer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(onTick:) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:appdelegate.appTimer forMode:NSRunLoopCommonModes];

    appdelegate.locationManager = [[CLLocationManager alloc] init];
    appdelegate.appLocationDataArray = [[NSMutableArray alloc]init];
    
#ifdef __IPHONE_8_0
    if(IS_OS_8_OR_LATER) {
        [appdelegate.locationManager requestWhenInUseAuthorization];
        [appdelegate.locationManager requestAlwaysAuthorization];
    }
#endif
    
    [appdelegate.locationManager startUpdatingLocation];
    //appdelegate.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    appdelegate.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    appdelegate.locAccuracy = @"0";
    appdelegate.locationManager.delegate = self;
    appdelegate.locationManager.allowsBackgroundLocationUpdates = YES;
    appdelegate.locationManager.pausesLocationUpdatesAutomatically = NO;
    
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
    if (state == UIApplicationStateBackground) {
        NSLog(@"Start significant location manager");
        [appdelegate.locationManager startMonitoringSignificantLocationChanges];
    }
    
    CLAuthorizationStatus authorizationStatus= [CLLocationManager authorizationStatus];
    if (authorizationStatus == kCLAuthorizationStatusAuthorizedAlways ||
        authorizationStatus == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [appdelegate.locationManager startUpdatingLocation];
    }
    
    if([appdelegate.locationManager respondsToSelector:@selector(setAllowsBackgroundLocationUpdates:)])
        [appdelegate.locationManager setAllowsBackgroundLocationUpdates:YES];
}

#pragma mark - Database  functions
-(void)saveOfflineTrip:(NSDictionary *)data
{
    model = [[Model alloc]init];
    model.u_id = @"";
    model.trip_id = data[@"id"];
    model.user_id = data[@"userid"];
    model.trip_type = data[@"trip_type"];
    model.start_address = data[@"start_address"];
    model.end_address = data[@"end_address"];
    model.trip_date = data[@"trip_date"];
    model.start_time = data[@"start_time"];
    model.end_time = data[@"end_time"];
    model.odometer_start = data[@"odometer_start"];
    model.odometer_finish = data[@"odometer_finish"];
    model.purpose_trip = data[@"purpose_trip"];
    model.distance = data[@"distance"];
    model.note = data[@"note"];
    model.locations = data[@"locations"];
    model.locations1 = data[@"locations1"];
    
    ModelManager *mgrObj = [ModelManager getInstance];
    [mgrObj insertData:model];
    
    //savingTrip = 0;
    [appdelegate clearMapData];
    NSLog(@"Clear clearMapData");
    [self clearMapAnnotation ];
    appdelegate.appStartLocation = nil;
    appdelegate.appDistance = @"";
    [self removeTripData];
    [self dismissViewControllerAnimated: YES completion: ^{
        [self viewWillAppear:true];
    }];
}

-(void)removeOfflineTrip:(NSString *)uniqueId{
    ModelManager *mgrObj = [ModelManager getInstance];
    [mgrObj deleteTripData:uniqueId];
}

-(void)removeLastTripFromLocalDB{
    NSMutableArray *tripArray = [[NSMutableArray alloc]init];
    ModelManager *mgrObj = [ModelManager getInstance];
    NSString *userID = [Helper getPREF:@"userID"];
    tripArray = [[mgrObj getAllLocalTrip:userID] copy];
    
    int counter = (int)tripArray.count;
    NSLog(@"Total trip %d",counter);
    NSString *u_id = @"";
    if (counter > 0) {
        NSDictionary *d = tripArray[counter - 1];
        NSLog(@"Trip to remove : %@",d);
        NSMutableDictionary *data = (NSMutableDictionary *)[d mutableCopy];
        u_id = [data valueForKey:@"u_id"];
        [self removeOfflineTrip:u_id];
    }

}

-(void)checkOfflineTripAvailable{
    ModelManager *mgrObj = [ModelManager getInstance];
    NSString *userID = [Helper getPREF:@"userID"];
    
    offlineTripArray = [[NSMutableArray alloc]init];
    offlineTripArray = [mgrObj getAllLocalTrip:userID];
    NSLog(@"Offline trip : %@",offlineTripArray);
    if (offlineTripArray.count > 0) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self callSaveOfflineTripWS];
        });
        
       /*UIAlertController  *alertUserMessage = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"tripNotStoredError", @"") preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *saveTrip = [UIAlertAction actionWithTitle:@"Save Trip" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self callSaveOfflineTripWS];
            });
            [alertUserMessage dismissViewControllerAnimated:true completion:nil];
        }];
        
        UIAlertAction *discardTrip = [UIAlertAction actionWithTitle:@"Discard Trip" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action){
            // remove all trip of current user
            [mgrObj deleteAllTripData:userID];
        }];
        [alertUserMessage addAction:saveTrip];
        [alertUserMessage addAction:discardTrip];
        [self presentViewController:alertUserMessage animated:YES completion:nil];*/
    }
}

-(void)callSaveOfflineTripWS{
    if(![self CheckNetworkConnection]) {
        return;
    }else{
        offlineCounter = 0;
        
        offlineTripArray = [[NSMutableArray alloc]init];
        ModelManager *mgrObj = [ModelManager getInstance];
        NSString *userID = [Helper getPREF:@"userID"];
        offlineTripArray = [[mgrObj getAllLocalTrip:userID] copy];
        
        int counter = (int)offlineTripArray.count;
        NSLog(@"Total trip to save %d",counter);
        NSString *u_id = @"";
        if (offlineCounter < counter) {
            
            NSDictionary *d = offlineTripArray[offlineCounter];
            NSLog(@"%@",d);
            
            NSMutableDictionary *data = (NSMutableDictionary *)[d mutableCopy];
            u_id = [data valueForKey:@"u_id"];
            [data removeObjectForKey:@"u_id"];
            [data setValue:@"" forKey:@"id"];
            
            [data setValue:appdelegate.appVersion forKey:@"version"];
            [data setValue:appdelegate.appBuild forKey:@"build"];
            [data setValue:@"ios" forKey:@"os"];
            
            NSLog(@"Data : %@",data);
            
            CLLocation *mysLocation,*myeLocation;
            __block NSString *sAddress = @"",*eAddress = @"";
            
            NSString *locations = [data valueForKey:@"locations"];
            NSArray *locationArray = [locations componentsSeparatedByString:@"!"];
            if (locationArray.count > 0) {
                
                sTempLocation = [locationArray objectAtIndex:0];
                NSArray *arr = [sTempLocation componentsSeparatedByString:@","];
                if (arr.count > 0) {
                    CLLocationDegrees latitudeDegrees = [[arr objectAtIndex:0] doubleValue];
                    CLLocationDegrees longitudeDegrees = [[arr objectAtIndex:1] doubleValue];
                    mysLocation = [[CLLocation alloc]initWithLatitude:latitudeDegrees longitude:longitudeDegrees];
                }
                
                eTempLocation = [locationArray objectAtIndex:locationArray.count - 1];
                NSArray *arr1 = [eTempLocation componentsSeparatedByString:@","];
                if (arr1.count > 0) {
                    CLLocationDegrees latitudeDegrees = [[arr1 objectAtIndex:0] doubleValue];
                    CLLocationDegrees longitudeDegrees = [[arr1 objectAtIndex:1] doubleValue];
                    myeLocation = [[CLLocation alloc]initWithLatitude:latitudeDegrees longitude:longitudeDegrees];
                }
            }
            
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [self getAddressFromLocation:mysLocation callBackData:^(NSString *sAdd) {
                sAddress = sAdd;
                [data setValue:sAddress forKey:@"start_address"];
                
                [self getAddressFromLocation:myeLocation callBackData:^(NSString *sAdd) {
                    eAddress = sAdd;
                    [data setValue:eAddress forKey:@"end_address"];
                    
                    [WebService httpPostWithCustomDelegateURLString:@".saveTrip" parameter:data isDebug:false callBackData:^(NSData *data, NSError *error)
                     {
                         [MBProgressHUD hideHUDForView:self.view animated:YES];
                         if (error == nil) {
                             NSError *localError = nil;
                             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
                             //savingTrip = 0;
                             if (localError != nil) {
                                 NSLog(@"%@",localError.description);
                             }
                             else {
                                 NSDictionary *response = [parsedObject objectForKey:@"data"];
                                 NSString* success = [response objectForKey:@"status"];
                                 if ([success isEqualToString:@"Success"]) {
                                     NSLog(@"offline trip %d saved",offlineCounter+1);
                                     [self removeOfflineTrip:u_id];
                                     offlineCounter = offlineCounter + 1;
                                     NSDictionary *data = [response valueForKey:@"trip"];
                                     [self calculateDistance:data];
                                     [self callSaveOfflineTripWS];
                                 }
                             }
                         }
                     }];
                }];
            }];
            
        }
    }
}

-(void)getAddressFromLocation:(CLLocation *)location callBackData:(void(^)(NSString *sAdd))addressReceived{
    __block NSString *fullAddress = @"";
        NSLog(@"Getting address");
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
     {
         NSString *Address = @"", *Area = @"", *Country = @"";
         if (!(error))
         {
             CLPlacemark *placemark = [placemarks objectAtIndex:placemarks.count-1];
             NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
             
             Address = [[NSString alloc]initWithString:[Helper isStringIsNull:locatedAt]];
             Area = [[NSString alloc]initWithString:[Helper isStringIsNull:placemark.locality]];
             Country = [[NSString alloc]initWithString:[Helper isStringIsNull:placemark.country]];
             fullAddress = Address;//[self printAddress:Address :Area :Country];
             //[NSString stringWithFormat:@"%@,%@,%@",Address,Area,Country]
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 addressReceived(fullAddress);
             });
         }else{
             dispatch_async(dispatch_get_main_queue(), ^{
                 addressReceived(fullAddress);
             });
         }
     }];
}

- (NSString *)printAddress:(NSString *)add_str :(NSString *)ar_str :(NSString *)ctr_str{
    NSString *return_address = @"";
    
    if (add_str.length > 0) {
        return_address = [NSString stringWithFormat:@"%@",add_str];
    }
    if (ar_str.length > 0) {
        if (add_str.length > 0) {
            return_address = [NSString stringWithFormat:@"%@, ",return_address];
        }
        return_address = [NSString stringWithFormat:@"%@%@",return_address,ar_str];
    }
    if (ctr_str.length > 0) {
        if (add_str.length > 0) {
            return_address = [NSString stringWithFormat:@"%@, ",return_address];
        }
        return_address = [NSString stringWithFormat:@"%@%@",return_address,ctr_str];
    }
    NSLog(@"Full Address : %@",return_address);
    return return_address;
}

-(void)viewWillDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DisplayMapData" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DisplayMap" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MODELVIEW DISMISS" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"RemoveMapData" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"BeaconConnection" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"BeaconConnection_Formsg" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"BeaconDisConnection" object:nil];
    //[[NSNotificationCenter defaultCenter] removeObserver:self name:@"BeaconDisConnection_Formsg" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"BeaconConnectionService" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"BeaconDisConnectionService" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"startTripAfterRestart" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"SaveTripAtAppReStarttimeIfBeconDisconnect" object:nil];
}

-(BOOL)checkTrackerID:(NSString *)tracker{
    if (tracker.length <= 0 || tracker == nil || tracker == (NSString *)[NSNull null] || [tracker isEqualToString:@""]){
        return true;
    }else{
        return false;
    }
}

@end

