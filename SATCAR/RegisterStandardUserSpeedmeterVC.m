//
//  RegisterStandardUserSpeedmeterVC.m
//  SATCAR
//
//  Created by Percept Infotech on 24/01/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import "RegisterStandardUserSpeedmeterVC.h"
#import "RegisterSplitUserViewController.h"

@interface RegisterStandardUserSpeedmeterVC ()<UITextFieldDelegate,UIAlertViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    NSArray *unitArray;
    BOOL touched,fromImage;
    UIPickerView *measurePicker;
    NSData *imageData;
    AppDelegate *appdelegate;
}
@end

@implementation RegisterStandardUserSpeedmeterVC
@synthesize userType;

- (void)viewDidLoad {
    [super viewDidLoad];
    _lblAddImage.hidden = FALSE;
    _imgCam.hidden = FALSE;
    
    _imgCam.image = [_imgCam.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [_imgCam setTintColor:[UIColor colorWithRed:0.729 green:0.733 blue:0.745 alpha:1.00]];
        _lblAddImage.text = NSLocalizedString(@"Addimage", @"");
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    _btnNext.layer.cornerRadius = 25;
    _btnSkip.layer.cornerRadius = 25;
    [_btnImage setTitle:NSLocalizedString(@"Add_image_of_speedometer", @"") forState:UIControlStateNormal];
    [_btnNext setTitle:[NSString stringWithFormat:@"%@",NSLocalizedString(@"Next", @"")] forState:UIControlStateNormal];
    [_btnSkip setTitle:[NSString stringWithFormat:@"%@",NSLocalizedString(@"Skip", @"")] forState:UIControlStateNormal];
    
    if ([userType isEqualToString:@"12"]) {
        [_btnNext setTitle:[NSString stringWithFormat:@"%@",NSLocalizedString(@"Done", @"")] forState:UIControlStateNormal];    
    }

    _lblImageDes.text = NSLocalizedString(@"Youcanaddimagefordocumentationifyouwant", @"");
    _LblSpeedMtr.text = NSLocalizedString(@"Whatdoesyourspeedmeterreadrightnow", @"");
    
    [Helper setBorderWithColorInButton:_btnImage];
    
    _imgview.layer.borderColor = [UIColor colorWithRed:0.937 green:0.941 blue:0.941 alpha:1.00].CGColor;
    _imgview.layer.borderWidth = 1.0;
    
    _txtKmMiles.text = @"KM";
    
    [_txtKmMiles addDoneOnKeyboardWithTarget:self action:@selector(onMeasureDone)];
    _IBLayoutConstraintImgHeight.constant = 0;
    touched = false;
    _lblImage.hidden = true;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected)];
    singleTap.numberOfTapsRequired = 1;
    [_imgview setUserInteractionEnabled:YES];
    [_imgview addGestureRecognizer:singleTap];

    
}
-(void)viewWillAppear:(BOOL)animated{
    [self addOnlyTitleView:@"Speedometer status"];
    unitArray = [[NSArray alloc]initWithObjects: @"KM", @"Miles", nil];
}


#pragma mark - Textfield delegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    textField.textColor =  [UIColor colorWithRed:18.0/255.0 green:172.0/255.0 blue:243.0/255.0 alpha:1.0];
    textField.layer.borderWidth = 1;
    textField.layer.borderColor =  [UIColor colorWithRed:18.0/255.0 green:172.0/255.0 blue:243.0/255.0 alpha:1.0].CGColor;
    textField.clipsToBounds = YES;
    return true;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    textField.textColor =  [UIColor blackColor];
    textField.layer.borderWidth = 0;
    textField.layer.borderColor = [UIColor clearColor].CGColor;
    textField.clipsToBounds = YES;
    return true;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if(textField == _txtKmMiles) {
        measurePicker = [[UIPickerView alloc] initWithFrame:CGRectZero];
        [measurePicker setDataSource: self];
        [measurePicker setDelegate: self];
        measurePicker.showsSelectionIndicator = YES;
        _txtKmMiles.inputView = measurePicker;
        if (![_txtKmMiles hasText]) {
            _txtKmMiles.text = [unitArray objectAtIndex:0];
        }
    }
}

#pragma mark - Picker View Delegate and Data source
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (pickerView == measurePicker) {
        return unitArray.count;
    }
    return 0;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component{
    if (pickerView == measurePicker) {
        [_txtKmMiles setText:[unitArray objectAtIndex:row]];
    }
    touched = true;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow: (NSInteger)row forComponent:(NSInteger)component{
    if (pickerView == measurePicker) {
        return [NSString stringWithFormat:@"%@",[unitArray objectAtIndex:row]];
    }else{
        return 0;
    }
}

#pragma mark - Custom functions
-(void)selectNewCamara {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = false;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:picker animated:YES completion:NULL];
}

-(void)onMeasureDone{
    if (touched) {
        touched = false;
    }else{
        _txtKmMiles.text = [unitArray objectAtIndex:0];
    }
    [_txtKmMiles resignFirstResponder];
}

-(void)goToHome{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    HomeViewController* controller = [storyboard instantiateViewControllerWithIdentifier:@"Home"];
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)tapDetected{
    NSLog(@"single Tap on imageview");
    
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"Addimage", @"") message:NSLocalizedString(@"replacePhoto", @"") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* new = [UIAlertAction actionWithTitle:NSLocalizedString(@"Takenew", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                          {
                              if ([UIImagePickerController isCameraDeviceAvailable: UIImagePickerControllerCameraDeviceRear] || [UIImagePickerController isCameraDeviceAvailable: UIImagePickerControllerCameraDeviceFront])
                              {
                                  UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                  picker.delegate = self;
                                  picker.allowsEditing = false;
                                  picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                                  [self presentViewController:picker animated:YES completion:NULL];
                              }else{
                                  [Helper displayAlertView:@"" message:@"Camara not available"];
                              }
                          }];
    
    UIAlertAction* gallery = [UIAlertAction actionWithTitle:NSLocalizedString(@"Fromgallery", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                              {
                                  /** What we write here???????? **/
                                  NSLog(@"you pressed No, thanks button");
                                  UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
                                  cameraUI.delegate = self;
                                  cameraUI.allowsEditing = NO;
                                  cameraUI.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                  [UIImagePickerController availableMediaTypesForSourceType: UIImagePickerControllerSourceTypePhotoLibrary];
                                  [self presentViewController:cameraUI animated:YES completion:nil];
                              }];
    
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [self dismissViewControllerAnimated:true completion:nil];
    }];
    
    [alert addAction:new];
    [alert addAction:gallery];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - validation
-(BOOL)isValidData{
    if (_TxtKm.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString( @"error_please_enter_speedometer", @"")];
        return false;
    }
    return true;
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    UIImage *img = info[UIImagePickerControllerOriginalImage];
    _imgview.image = img;
    _IBLayoutConstraintImgHeight.constant = 70;
    //imageData = [self compressResizeImage:info[UIImagePickerControllerOriginalImage]];
    imageData = UIImageJPEGRepresentation(info[UIImagePickerControllerEditedImage], 0.5);
    if (imageData != nil) {
        fromImage = true;
        _lblImage.hidden = FALSE;
    }
    _imgview.layer.borderColor = [UIColor clearColor].CGColor;
    _imgview.layer.borderWidth = 0;
    _imgview.backgroundColor = [UIColor clearColor];
    
    _lblAddImage.hidden = TRUE;
    _imgCam.hidden = TRUE;
    [picker dismissViewControllerAnimated:true completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:true completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Button Action
- (IBAction)NextPress:(id)sender{
    
    if ([self isValidData]) {
        
        if ([userType isEqualToString:@"10"] || [userType isEqualToString:@"12"]) {
            //NSString *sId = [Helper getPREF:@"sid"];
            NSString *userID = [Helper getPREF:@"userID"];
            
            NSDictionary *parameter;
            if (imageData != nil && imageData.length > 0) {
                parameter=@{@"user_id":userID, @"speedometer":_TxtKm.text, @"amt_upfront":@"", @"monthly_cost":@"", @"duration_of_contract":@"", @"speed_image":imageData};
            }else{
                parameter=@{@"user_id":userID, @"speedometer":_TxtKm.text, @"amt_upfront":@"", @"monthly_cost":@"", @"duration_of_contract":@"", @"speed_image":@""};
            }
            //NSLog(@"Parameter %@: ",parameter);
            [self callAddExtraInfo:parameter];
        }else{
            [self performSegueWithIdentifier:@"segueSpeedmeterToSplit" sender:self];
        }
    }
    
}

- (IBAction)onSkip:(id)sender {
    [self goToHome];
}

#pragma mark - webservice call
-(void)callAddExtraInfo:(NSDictionary *)parameter{
    
    if(![self CheckNetworkConnection]) {
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [WebService httpPostWithCustomDelegateMultipartURLString:@".userextrainfo" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (error == nil) {
            NSError *localError = nil;
            NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
            
            if (localError != nil) {
                NSLog(@"%@",localError.description);
            }
            else {
                NSDictionary *response = [parsedObject objectForKey:@"data"];
                NSString* message = [response objectForKey:@"message"];
                NSString* success = [response objectForKey:@"status"];
                if ([success isEqualToString:@"Success"]) {
                    
                    
                    NSDictionary* user = [response objectForKey:@"user"];
                    NSString *email = [user valueForKey:@"email"];
                    NSString *name = [user valueForKey:@"name"];
                    NSString *uID = [user valueForKey:@"id"];
                    NSString* registerDate = [user objectForKey:@"registerDate"];
                    //NSString* sid = [response objectForKey:@"sid"];
                    NSString* userTyp = [response objectForKey:@"user_type"];
                    appdelegate.tracker_id = [response objectForKey:@"tracker_id"];
                    
                    [Helper setPREFStringValue:uID sKey:@"userID"];
                    //[Helper setPREFStringValue:sid sKey:@"sid"];
                    [Helper setPREFStringValue:name sKey:@"userName"];
                    [Helper setPREFStringValue:email sKey:@"userEmail"];
                    [Helper setPREFStringValue:@"1" sKey:@"islogin"];
                    [Helper setPREFStringValue:userTyp sKey:@"user_type"];
                    [Helper setPREFStringValue:registerDate sKey:@"userRegisterDate"];
                    if (appdelegate.tracker_id == (NSString *)[NSNull class]) {
                        [Helper setPREFStringValue:@"" sKey:@"tracker_id"];
                    }else{
                        [Helper setPREFStringValue:appdelegate.tracker_id sKey:@"tracker_id"];
                    }
                    
                    NSDictionary *generalSetting = [response objectForKey:@"general_setting"];
                    [Helper setUserValue:generalSetting sKey:@"general_setting"];
                    [Helper setDataValue:data sKey:@"userInfo"];
                    
                    NSLog(@"user info : %@",response);
                    NSLog(@"generalSetting : %@",generalSetting);
                    
                    NSDictionary *payment_info = [response objectForKey:@"payment_info"];
                    NSString *status = [payment_info valueForKey:@"status"];
                    if (status == (NSString *)[NSNull null]) {
                        status = @"";
                        [Helper setPREFStringValue:status sKey:@"userStatus"];
                    }else{
                        [Helper setPREFStringValue:status sKey:@"userStatus"];
                    }
                    
                    [Helper setPREFStringValue:status sKey:@"userStatus"];
                    
                    
                    if ([userType isEqualToString:@"10"] || [userType isEqualToString:@"12"]) {
                        //[self performSegueWithIdentifier:@"SegueStandardConnectBeep" sender:self];
                        //[appdelegate callLocationService:uID from:@"regispterstandarduser"];
                        [appdelegate callLocationService_withTips:uID from:@"regispterstandarduser"];
                        
                        [self goToHome];
                    }else{
                        [self performSegueWithIdentifier:@"segueSpeedmeterToSplit" sender:self];
                    }
                } else {
                    NSLog(@"error :: %@",message);
                    [Helper displayAlertView:@"" message:message];
                }
            }
        }
    }];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"segueSpeedmeterToSplit"]) {
        RegisterSplitUserViewController *controller = segue.destinationViewController;
        controller.userType = userType;
        controller.speedometer = _TxtKm.text;
        controller.imageData = imageData;
    }
}

@end
