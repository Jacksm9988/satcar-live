//
//  TripTableViewCell.m
//  SATCAR
//
//  Created by Percept Infotech on 03/01/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import "TripTableViewCell.h"

@implementation TripTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _viewCreated.layer.cornerRadius = 10;
    _viewCreated.clipsToBounds = true;
    _imgUser.layer.cornerRadius = 10;
    _imgUser.clipsToBounds = true;
    _imgUser.layer.borderColor = (__bridge CGColorRef _Nullable)([UIColor whiteColor]);
    _imgUser.layer.borderWidth = 1.0;
    _viewCreated.hidden = true;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
