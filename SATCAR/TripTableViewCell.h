//
//  TripTableViewCell.h
//  SATCAR
//
//  Created by Percept Infotech on 03/01/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TripTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *viewBack;
@property (weak, nonatomic) IBOutlet UILabel *lblTripColor;
@property (weak, nonatomic) IBOutlet UILabel *lblTripDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTripTime;
@property (weak, nonatomic) IBOutlet UILabel *lblTripMeter;
@property (weak, nonatomic) IBOutlet UILabel *lblTripReim;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintReimHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintReimTop;
@property (weak, nonatomic) IBOutlet UILabel *lblCreatedName;
@property (weak, nonatomic) IBOutlet UIView *viewCreated;
@property (weak, nonatomic) IBOutlet UIImageView *imgUser;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintKMTop;
@end
