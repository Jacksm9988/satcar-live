//
//  TripViewController.h
//  SATCAR
//
//  Created by Percept Infotech on 12/20/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TripViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *tempTrips;
}
/*@property (weak, nonatomic) IBOutlet UILabel *lblBusiness;
@property (weak, nonatomic) IBOutlet UILabel *LblKm;
@property (weak, nonatomic) IBOutlet UILabel *lblstartTime1;
@property (weak, nonatomic) IBOutlet UILabel *lblstartTime2;
@property (weak, nonatomic) IBOutlet UILabel *lblEndTime1;

@property (weak, nonatomic) IBOutlet UILabel *lblEndTime2;
@property (weak, nonatomic) IBOutlet UILabel *LblAddStart;
@property (weak, nonatomic) IBOutlet UILabel *LblAddEnd;
@property (weak, nonatomic) IBOutlet UILabel *LblPurpose;
@property (weak, nonatomic) IBOutlet UIButton *BtnEdit2;
@property (weak, nonatomic) IBOutlet UILabel *lblNotes;
@property (weak, nonatomic) IBOutlet UIButton *BtnCalcel2;*/
@property (weak, nonatomic) NSString *isFromMap;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *segement2Bottom;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *segment2hight;

@property (weak, nonatomic) IBOutlet UIView *ViewSegment2;
@property (weak, nonatomic) IBOutlet UIButton *BtnPrivateSegment2;
@property (weak, nonatomic) IBOutlet UIButton *BtnBusinessSegment2;

// Top
@property (weak, nonatomic) IBOutlet UIView *ViewSegment;
@property (weak, nonatomic) IBOutlet UIButton *BtnAllSegment;
@property (weak, nonatomic) IBOutlet UIButton *BtnPrivateSegment;
@property (weak, nonatomic) IBOutlet UIButton *BtnBusinessSegment;
@property (weak, nonatomic) IBOutlet UIButton *btnPrivious;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UILabel *lblMonth;
@property (weak, nonatomic) IBOutlet UITableView *tableTrips;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintprivateWidth;
@property (weak, nonatomic) IBOutlet UIView *viewFinancialSplic;
@property (weak, nonatomic) IBOutlet UILabel *lblPrivatePercentage;
@property (weak, nonatomic) IBOutlet UILabel *lblBusinessPercentage;
@property (weak, nonatomic) IBOutlet UILabel *lblFinancialSplit;

- (IBAction)onFinancesSplit:(id)sender;
- (IBAction)onReimbursement:(id)sender;
- (IBAction)SelctSegment:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *lblMessage;
@property (weak, nonatomic) IBOutlet UIView *viewTripSplit;
@property (weak, nonatomic) IBOutlet UIButton *btnConfirm;
@property (weak, nonatomic) IBOutlet UIButton *btnEditManually;

@property (weak, nonatomic) IBOutlet UILabel *lblReimbursementTotal;
@property (weak, nonatomic) IBOutlet UILabel *lblTripMessage;

- (IBAction)onEditManually:(id)sender;
- (IBAction)onConfirm:(id)sender;
- (IBAction)onMAp:(id)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintTripPopupHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintTripPopupTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintPurposeHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintNoteHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintTripRemHeights;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintPurposeTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintNoteTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintTripTableviewBottom;
@property (weak, nonatomic) IBOutlet UIButton *btnMap;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;
@property (weak, nonatomic) IBOutlet UILabel *lblRexpense;
@property (weak, nonatomic) IBOutlet UILabel *lblRexpensetype;

@property (weak, nonatomic) IBOutlet UIView *viewReimbursement;
@property (weak, nonatomic) IBOutlet UIView *viewTripDisplay;
@property (weak, nonatomic) IBOutlet UIView *viewBackTrip;
@property (weak, nonatomic) IBOutlet UIView *viewType;
@property (weak, nonatomic) IBOutlet UIView *viewDistance;
@property (weak, nonatomic) IBOutlet UIView *viewSTime;
@property (weak, nonatomic) IBOutlet UIView *viewETime;
@property (weak, nonatomic) IBOutlet UIView *viewDuration;
@property (weak, nonatomic) IBOutlet UIView *viewDate;
@property (weak, nonatomic) IBOutlet UIView *viewEndAddress;
@property (weak, nonatomic) IBOutlet UIView *viewStartAddress;
@property (weak, nonatomic) IBOutlet UIView *viewPurpose;
@property (weak, nonatomic) IBOutlet UIView *viewNote;
@property (weak, nonatomic) IBOutlet UILabel *lblProgessIndicator;


@property (weak, nonatomic) IBOutlet UITextField *txtTypeValue;
@property (weak, nonatomic) IBOutlet UITextField *txtDistanceValue;
@property (weak, nonatomic) IBOutlet UITextField *txtStartTimeValue;
@property (weak, nonatomic) IBOutlet UITextField *txtEndTimeValue;
@property (weak, nonatomic) IBOutlet UITextField *txtDurationValue;
@property (weak, nonatomic) IBOutlet UITextField *txtDateValue;
@property (weak, nonatomic) IBOutlet UITextField *txtStartPlaceValue;
@property (weak, nonatomic) IBOutlet UITextField *txtEndValue;
@property (weak, nonatomic) IBOutlet UITextField *txtChoosePurposeValue;
@property (weak, nonatomic) IBOutlet UITextField *txtNoteValue;
@property (weak, nonatomic) IBOutlet UIButton *btnSAddress;
@property (weak, nonatomic) IBOutlet UIButton *btnEAddress;
- (IBAction)onStartAddress:(id)sender;

// Notification label
@property (weak, nonatomic) IBOutlet UILabel *lblNotification;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintNotifHeight;

@end
