//
//  AddEmailTVC.h
//  SATCAR
//
//  Created by Percept Infotech on 29/12/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddEmailTVC : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *viewBack;
@property (weak, nonatomic) IBOutlet UILabel *lblAddEmail;
@property (weak, nonatomic) IBOutlet UIButton *btnAddEmail;
@end
