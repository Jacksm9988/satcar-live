//
//  SplitOverviewViewController.h
//  SATCAR
//
//  Created by Percept Infotech on 28/12/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SplitOverviewViewController : UIViewController
{
    
}
@property (weak, nonatomic) IBOutlet UILabel *lblMonth;
@property (weak, nonatomic) IBOutlet UIButton *btnPrivious;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;

- (IBAction)onPrevious:(id)sender;
- (IBAction)onNext:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *lblTripSplit;
@property (weak, nonatomic) IBOutlet UILabel *lblTripPrivate;
@property (weak, nonatomic) IBOutlet UILabel *lblTripBusiness;
@property (weak, nonatomic) IBOutlet UILabel *lblTripPrivateValue;
@property (weak, nonatomic) IBOutlet UILabel *lblTripBusinessValue;
@property (weak, nonatomic) IBOutlet UIView *viewTripSplit;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintTripPrivateWidth;
@property (weak, nonatomic) IBOutlet UILabel *lblTripPrivatePercentage;
@property (weak, nonatomic) IBOutlet UILabel *lblTripBusinessPercentage;
- (IBAction)onTripSplit:(id)sender;


@property (weak, nonatomic) IBOutlet UILabel *lblFinancialSplit;
@property (weak, nonatomic) IBOutlet UILabel *lblFinancialPrivate;
@property (weak, nonatomic) IBOutlet UILabel *lblFinancialBusiness;
@property (weak, nonatomic) IBOutlet UILabel *lblFinancialPrivateValue;
@property (weak, nonatomic) IBOutlet UILabel *lblFinancialBusinessValue;
@property (weak, nonatomic) IBOutlet UIView *viewFinancialSplit;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintFinancialPrivateWidth;
@property (weak, nonatomic) IBOutlet UILabel *lblFinancialPrivatePercentage;
@property (weak, nonatomic) IBOutlet UILabel *lblFinancialBusinessPercentage;
- (IBAction)onFinancesSplit:(id)sender;


@property (weak, nonatomic) IBOutlet UILabel *lblTotal;
@property (weak, nonatomic) IBOutlet UILabel *lblTripTotal;
@property (weak, nonatomic) IBOutlet UILabel *lblExpenseTotal;
@property (weak, nonatomic) IBOutlet UILabel *lblTripTotalValue;
@property (weak, nonatomic) IBOutlet UILabel *lblExpenseTotalValue;

@property (weak, nonatomic) IBOutlet UILabel *lblToPay;
@property (weak, nonatomic) IBOutlet UILabel *lblToPayPrivate;
@property (weak, nonatomic) IBOutlet UILabel *lblToPayBusiness;
@property (weak, nonatomic) IBOutlet UILabel *lblToPayPrivateValue;
@property (weak, nonatomic) IBOutlet UILabel *lblToPayBusinessValue;

@property (weak, nonatomic) IBOutlet UILabel *lblMessage;
@property (weak, nonatomic) IBOutlet UIButton *btnExport;
- (IBAction)onExport:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintFinanceSplitHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintTotalHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintPayHeight;
@property (weak, nonatomic) IBOutlet UIView *viewTotal;
@property (weak, nonatomic) IBOutlet UIView *viewToPay;
@property (weak, nonatomic) IBOutlet UILabel *lblGap1;
@property (weak, nonatomic) IBOutlet UILabel *lblGap2;
@property (weak, nonatomic) IBOutlet UILabel *lblGap3;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintTotalTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintPayTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintSplitGap;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintFinanceSp;
@property (weak, nonatomic) IBOutlet UIView *viewFSplit;
@property (weak, nonatomic) IBOutlet UILabel *lblTripIndicator;
@property (weak, nonatomic) IBOutlet UILabel *lblFinanceIndicator;


@end
