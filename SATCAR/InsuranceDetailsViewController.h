//
//  InsuranceDetailsViewController.h
//  SATCAR
//
//  Created by Percept Infotech on 30/12/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InsuranceDetailsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lblInsuranceActive;

@property (weak, nonatomic) IBOutlet UILabel *lblRoadHelp;
@property (weak, nonatomic) IBOutlet UILabel *lblFreeDamage;
@property (weak, nonatomic) IBOutlet UILabel *lblInsuranceNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblDownloadInsurance;
@property (weak, nonatomic) IBOutlet UILabel *lblDownloadRed;
@property (weak, nonatomic) IBOutlet UILabel *lblDownloadGreen;

@property (weak, nonatomic) IBOutlet UITextField *txtRoadHelp;
@property (weak, nonatomic) IBOutlet UITextField *txtFreeDamage;
@property (weak, nonatomic) IBOutlet UITextField *txtInsuranceNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtInsuranceActive;
- (IBAction)onDownloadCertificate:(id)sender;
- (IBAction)onDownloadRedCard:(id)sender;
- (IBAction)onDownloadGreenCard:(id)sender;

@end
