//
//  TrackerIDViewController.m
//  SATCAR
//
//  Created by Percept Infotech on 06/02/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import "TrackerIDViewController.h"

@interface TrackerIDViewController ()
{
    AppDelegate *appdelegate;
}
@end

@implementation TrackerIDViewController
@synthesize from;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate ];
    [Helper setBorderWithColorInButton:_btnConnect];
    [Helper setBorderWithColorInButton:_btnFromReimConnect];
    [Helper setBorderWithColorInButton:_btnDisconnect];
    [Helper setBorderWithColorInText:_txtTrackerID];
    
    _btnDisconnect.hidden = true;
    
    _txtTrackerID.placeholder =NSLocalizedString(@"Tracker_ID", @"");
    _lblHEad.text =NSLocalizedString(@"InputyourtrackerID", @"");
    _lblTrackerMsg.text = NSLocalizedString(@"trackerMsg", @"");
    [_btnConnect setTitle:NSLocalizedString(@"Connect", @"") forState:UIControlStateNormal];
}

-(void)viewWillAppear:(BOOL)animated{
    [self addTitleView:@"SATCAR Tracker"];
    if ([from isEqualToString:@"Connect"]) {
        _btnConnect.hidden = true;
        _btnFromReimConnect.hidden = false;
    }else{
        _btnConnect.hidden = false;
        _btnFromReimConnect.hidden = true;
    }
    _btnFromReimConnect.hidden = true;
    _btnConnect.hidden = false;
    
    _txtTrackerID.text = [Helper getPREF:@"tracker_id"];
    if(_txtTrackerID.text.length <= 0){
        _btnDisconnect.hidden = true;
        _IBLayoutConstraintConnectBottom.constant = 30;
    }else{
        _btnDisconnect.hidden = false;
        _IBLayoutConstraintConnectBottom.constant = 90;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Textfield delegate
-(void)textFieldDidEndEditing:(UITextField *)textField{
    [textField resignFirstResponder];
}

#pragma mark - Button Action
- (IBAction)onConnect:(id)sender {
    [_txtTrackerID resignFirstResponder];
    if(!(_txtTrackerID.text.length>0) || [_txtTrackerID.text isEqualToString:@""])
    {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"PleaseEnterTrackerIdFirst", @"")];
        return;
    }
    if(![self CheckNetworkConnection]) {
        return;
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *userID = [Helper getPREF:@"userID"];
    
    
    //NSString *sid = [Helper getPREF:@"sid"];
    NSData *userdata = [Helper getDataValue:@"userInfo"];
    __block NSMutableDictionary* dataDict = [[self dataToDicitonary:userdata] mutableCopy];
    
    if (![dataDict isKindOfClass:[NSNull class]]) {
        NSMutableDictionary *userInfo = [[dataDict valueForKey:@"data"] mutableCopy];
        NSString *strMeasure;
        if (userInfo != nil) {
            NSDictionary *generalSetting = [userInfo valueForKey:@"general_setting"];
            strMeasure = [generalSetting valueForKey:@"unit"];
        }
        
        NSDictionary *parameter=@{ @"userid":userID,@"tracker_id":_txtTrackerID.text};
        //NSLog(@"parameter : %@",parameter);
        [WebService httpPostWithCustomDelegateURLString:@".connectTracker" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error)
         {
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             if (error == nil) {
                 NSError *localError = nil;
                 NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
                 
                 if (localError != nil) {
                     NSLog(@"%@",localError.description);
                 }
                 else {
                     //NSDictionary *response = [parsedObject objectForKey:@"data"];
                     NSLog(@"response : %@",parsedObject);
                     id response = [parsedObject objectForKey:@"data"];
                     
                     NSString* message = [response objectForKey:@"message"];
                     NSString* success = [response objectForKey:@"status"];
                     if ([self CheckSession:response])
                     {
                         if ([success isEqualToString:@"Success"])
                         {
                             appdelegate.tracker_id = _txtTrackerID.text;
                             
                             [dataDict removeAllObjects];
                             dataDict = [[NSMutableDictionary alloc]init];
                             [userInfo setValue:_txtTrackerID.text forKey:@"tracker_id"];
                             [Helper setPREFStringValue:_txtTrackerID.text sKey:@"tracker_id"];
                             [dataDict setValue:userInfo forKey:@"data"];
                             NSData *data = [self dicitonaryToData:dataDict];
                             [Helper setDataValue:data sKey:@"userInfo"];
                             [self.navigationController popViewControllerAnimated:true];
                             [self alertSucess:nil Message:[response objectForKey:@"message"]];
                         }else
                         {
                             [self alertErrorwithMessage:message];
                         }
                     } else
                     {
                         NSLog(@"error :: %@",message);
                         [Helper displayAlertView:@"" message:message];
                     }
                 }
             }
         }];
    }else{
        [Helper displayAlertView:@"" message:NSLocalizedString(@"something_went_wrong_msg", @"")];
    }
}


- (IBAction)onFromReimConnect:(id)sender {
}

- (IBAction)onDisconnect:(id)sender {
    [_txtTrackerID resignFirstResponder];
    if(!(_txtTrackerID.text.length>0) || [_txtTrackerID.text isEqualToString:@""])
    {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"PleaseEnterTrackerIdFirst", @"")];
        return;
    }
    if(![self CheckNetworkConnection]) {
        return;
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *userID = [Helper getPREF:@"userID"];
    
    
    //NSString *sid = [Helper getPREF:@"sid"];
    NSData *userdata = [Helper getDataValue:@"userInfo"];
    __block NSMutableDictionary* dataDict = [[self dataToDicitonary:userdata] mutableCopy];
    
    if (![dataDict isKindOfClass:[NSNull class]]) {
        NSMutableDictionary *userInfo = [[dataDict valueForKey:@"data"] mutableCopy];
        NSString *strMeasure;
        if (userInfo != nil) {
            NSDictionary *generalSetting = [userInfo valueForKey:@"general_setting"];
            strMeasure = [generalSetting valueForKey:@"unit"];
        }
        
        NSDictionary *parameter=@{ @"user_id":userID,@"tracker_id":_txtTrackerID.text};
        //NSLog(@"parameter : %@",parameter);
        [WebService httpPostWithCustomDelegateURLString:@".TrackerUpdate" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error)
         {
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             if (error == nil) {
                 NSError *localError = nil;
                 NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
                 
                 if (localError != nil) {
                     NSLog(@"%@",localError.description);
                 }
                 else {
                     //NSDictionary *response = [parsedObject objectForKey:@"data"];
                     NSLog(@"response : %@",parsedObject);
                     id response = [parsedObject objectForKey:@"data"];
                     
                     NSString* message = [response objectForKey:@"message"];
                     NSString* success = [response objectForKey:@"status"];
                     if ([self CheckSession:response])
                     {
                         if ([success isEqualToString:@"Success"])
                         {
                             appdelegate.tracker_id = @"";
                             _txtTrackerID.text = @"";
                             
                             [dataDict removeAllObjects];
                             dataDict = [[NSMutableDictionary alloc]init];
                             [userInfo setValue:_txtTrackerID.text forKey:@"tracker_id"];
                             [Helper setPREFStringValue:_txtTrackerID.text sKey:@"tracker_id"];
                             [dataDict setValue:userInfo forKey:@"data"];
                             NSData *data = [self dicitonaryToData:dataDict];
                             [Helper setDataValue:data sKey:@"userInfo"];
                             [self alertSucess:nil Message:NSLocalizedString(@"tracker_delete_success", @"")];
                             [self.navigationController popViewControllerAnimated:true];
                             //_txtTrackerID.text = @"";
                         }else
                         {
                             [self alertErrorwithMessage:message];
                         }
                     } else
                     {
                         NSLog(@"error :: %@",message);
                         [Helper displayAlertView:@"" message:message];
                     }
                 }
             }
         }];
    }else{
        [Helper displayAlertView:@"" message:NSLocalizedString(@"something_went_wrong_msg", @"")];
    }
}
@end
