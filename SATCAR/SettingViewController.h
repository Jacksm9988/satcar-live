//
//  SettingViewController.h
//  SATCAR
//
//  Created by Percept Infotech on 29/12/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableview;

@end
