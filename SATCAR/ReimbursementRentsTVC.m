//
//  ReimbursementRentsTVC.m
//  SATCAR
//
//  Created by Percept Infotech on 10/02/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import "ReimbursementRentsTVC.h"

@implementation ReimbursementRentsTVC

- (void)awakeFromNib {
    [super awakeFromNib];
    _lblFromKM.text =  NSLocalizedString(@"FromKM", @"");
    _lblTomKM.text =  NSLocalizedString(@"ToKM", @"");
    _lblYear.text =  NSLocalizedString(@"Year", @"");
    _lblAmount.text =  NSLocalizedString(@"Amount", @"");
    _lblCountry.text =  NSLocalizedString(@"Country", @"");
    _lblCurrency.text =  NSLocalizedString(@"Currency", @"");
    _lblYearVal.text = @"";
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
