//
//  ForgotPassWordViewController.h
//  SATCAR
//
//  Created by Percept Infotech on 12/16/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPassWordViewController : UIViewController<UITextFieldDelegate>
{
    NSString *Pwd;
}
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UIButton *BtnSendPassw;
- (IBAction)SendPasswordPress:(id)sender;

@end
