//
//  TPAResourceObserver.h
//  ThePerfectApp
//
//  Created by Julian Król on 19/10/15.
//  Copyright © 2015 Trifork GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TPAAppEventMessage;
@class TPAResourceObserver;

@protocol TPAResourceObserverDelegate <NSObject>

/**
 * Sends to its delegate generated warning message, additionally gives the oportunity to distinguish which observer generated it
 *
 * @param observer which generated warning message
 * @param message generated resource warning message
 */
- (void)resourceObserver:(TPAResourceObserver *)observer didReceiveResourceWarningMessage:(TPAAppEventMessage *)message;

@optional

/**
 * Asks delegate about optional dictonary of NSStings or NSData which will be used as a meta information to the CPU usage warning message send to the server
 *
 * @param observer passes object which observed memory warning situation
 */
- (NSDictionary *)customUserInfoForCPUResourceWarningMessageInResourceObserver:(TPAResourceObserver *)observer;

/**
 * Asks delegate about optional dictonary of NSStings or NSData which will be used as a meta information to the memory warning message send to the server
 * 
 * @param observer passes object which observed memory warning situation
 */
- (NSDictionary *)customUserInfoForMemoryResourceWarningMessageInResourceObserver:(TPAResourceObserver *)observer;

@end


@interface TPAResourceObserver : NSObject

/**
 * Reference to the delegate implementing TPAResourceObserverDelegate methods
 */
@property (weak, nonatomic) id <TPAResourceObserverDelegate> delegate;

/**
 * Starts watching cpu level on the background thread
 * 
 * @param warningLevel specifies cpu usage level exceeding which triggers sending message to the server (if allowed and persists over the toleration time)
 * @param timestamp defines how often measurement action will be triggered
 * @param toleranceTime time over which which warningLevel has to be exceeded to generate warning message, has to be at least twice longer than timestamp
 */
- (void)startWatchingCpuLevelWithWarningLevel:(double)warningLevel
                                    timestamp:(NSTimeInterval)timestamp
                                toleranceTime:(NSTimeInterval)toleranceTime;

/**
 * Starting listening to notification about memory warning occurred
 */
- (void)startWatchingMemoryWarnings;

@end
