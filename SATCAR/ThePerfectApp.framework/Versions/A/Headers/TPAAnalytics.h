//
//  TPAAnalitics.h
//  ThePerfectApp
//
//  Created by Julian Król on 02/10/15.
//  Copyright © 2015 Trifork GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TPAAppEventMessage.h"

@interface TPAAnalytics : NSObject

/**
 * Method used to get TPAAnalytics singleton
 *
 * @return singleton instance of Analytics
 */
+ (instancetype)sharedInstance;

/**
 * Methods starting periodical CPU measurement, not thread safe, calling it more than once has no effect
 *
 * @param timestamp indicates how often the measurement should be repeated
 * @param warningLevel value that is treated as a limit, if exceeded for the time specified by toleranceTime, resource warning message will be generated, value should be from <0,1> range
 * @param toleranceTime time for which the warningLevel has to be exceeded, tolerance time has to be at least 2 times longer than timestamp
 */
// - (void)startWatchingCpuUsageWithWarningLevel:(double)warningLevel withFrequency:(NSTimeInterval)timestamp toleranceTime:(NSTimeInterval)toleranceTime;

/**
 * Method starting observation of memory warning notification, not thread safe, calling it more than once has no effect
 */
// - (void)startWatchingMemoryWarnings;

/**
 * Method sending app event message to the server
 *
 * @param appEventMessage message that will be sent
 */
- (void)send:(TPAAppEventMessage *)appEventMessage;


@end
