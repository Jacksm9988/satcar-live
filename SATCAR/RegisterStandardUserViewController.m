//
//  RegisterStandardUserViewController.m
//  SATCAR
//
//  Created by Percept Infotech on 24/01/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import "RegisterStandardUserViewController.h"
#import "RegisterViewController.h"

@interface RegisterStandardUserViewController () {
    UIImage *selectedRadioImage,*unSelectedRadioImage,*selectedCheckImage,*unSelectedCheckImage;
    BOOL isoneMonth, is12Month, is79DKK, is1799DKK;
    int oneMonthAmount,tweleveMonthAmount;
    NSString *carBeepSelected,*GPSSelected;
    UIColor *selectedBorderColor,*unSelectedBorderColor;
    UIColor *selectedBackColor,*unSelectedBackColor;
}
@end

@implementation RegisterStandardUserViewController
@synthesize userFrom;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self addTitleView:@"Standard Korebog"];
    _btnNext.layer.cornerRadius = 25.0;
    
    selectedRadioImage = [UIImage imageNamed:@"Ic_NewCheck.png"];
    unSelectedRadioImage = [UIImage imageNamed:@"IC_NewUncheck.png"];
    selectedCheckImage = [UIImage imageNamed:@"Ic_NewCheck.png"];
    unSelectedCheckImage = [UIImage imageNamed:@"IC_NewUncheck.png"];
    isoneMonth = false;
    [self onOneMonthDuration:self];
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden = false;
    
    unSelectedBorderColor = [UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:244.0/255.0 alpha:1.00];
    selectedBorderColor = [UIColor colorWithRed:214.0/255.0 green:215.0/255.0 blue:216.0/255.0 alpha:1.00];
    
    unSelectedBackColor = [UIColor colorWithRed:253.0/255.0 green:253.0/255.0 blue:253.0/255.0 alpha:1.00];
    selectedBackColor = [UIColor colorWithRed:243.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.00];
    _viewDuration1M.layer.borderWidth = 1;
    _viewDuration12M.layer.borderWidth = 1;
    _viewAddOn79.layer.borderWidth = 1;
    _viewAddOn1799.layer.borderWidth = 1;
    
    [self onSelectDuration:1];
    _viewAddOn79.backgroundColor = unSelectedBackColor;
    _viewAddOn79.layer.borderColor = unSelectedBorderColor.CGColor;
    _viewAddOn79.backgroundColor = unSelectedBackColor;
    _viewAddOn1799.layer.borderColor = unSelectedBorderColor.CGColor;
    
    carBeepSelected = @"0";
    GPSSelected = @"0";
    if ([userFrom isEqualToString:@"standard"]) {
        
        _lblOneMonth.text = [NSString stringWithFormat:NSLocalizedString(@"1MonthDKKMonth", @""),@"59"];
        _lblTweleveMonth.text =  [NSString stringWithFormat:NSLocalizedString(@"12Month599DKK", @""),@"599",@"15"];
        oneMonthAmount = 59;
        tweleveMonthAmount = 599;
        
    }else if ([userFrom isEqualToString:@"split"]){
        
        _lblOneMonth.text = [NSString stringWithFormat:NSLocalizedString(@"1MonthDKKMonth", @""),@"99"];
        _lblTweleveMonth.text = [NSString stringWithFormat:NSLocalizedString(@"12Month599DKK", @""),@"999",@"15"];;
        oneMonthAmount = 99;
        tweleveMonthAmount = 999;
    }

    _lbldurationhead.text = [NSLocalizedString(@"Duration", @"") uppercaseString];
    _lblAddonedevice.text = NSLocalizedString(@"Wantanyaddondevices", @"");
    //_lblSatcarBeep.text = [NSString stringWithFormat:NSLocalizedString(@"79SATCARBEEP" ,@""),@"79"];
    _lblSatcarBeep.text = [NSString stringWithFormat:NSLocalizedString(@"79SATCARBEEP" ,@""),@"149"];
    //_lblSatcarGps.text = [NSString stringWithFormat:NSLocalizedString(@"1799SATCARGPSTRACKER" ,@""),@"1799"];
    _lblSatcarGps.text = [NSString stringWithFormat:NSLocalizedString(@"1799SATCARGPSTRACKER" ,@""),@"1999"];
    [_btnNext setTitle:[NSLocalizedString(@"Next", @"") uppercaseString]forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Action
- (IBAction)onOneMonthDuration:(id)sender {
    if (isoneMonth) {
        isoneMonth = false;
        is12Month = true;
        
        _imgOneMonth.image = unSelectedRadioImage;
        _imgTweleveMonth.image = selectedRadioImage;
        [self onSelectDuration:2];
        
    }else{
       
        isoneMonth = true;
        is12Month = false;
        _imgOneMonth.image = selectedRadioImage;
        _imgTweleveMonth.image = unSelectedRadioImage;
        [self onSelectDuration:1];
    }
}

- (IBAction)onTweleveMonthDuration:(id)sender
{
    if (is12Month)
    {
        
        isoneMonth = true;
        is12Month = false;
        _imgOneMonth.image = selectedRadioImage;
        _imgTweleveMonth.image = unSelectedRadioImage;
        [self onSelectDuration:1];
    }else{
       
        isoneMonth = false;
        is12Month = true;
        _imgOneMonth.image = unSelectedRadioImage;
        _imgTweleveMonth.image = selectedRadioImage;
        [self onSelectDuration:2];
    }
}

-(void)onSelectDuration:(int)selectedType{
    if (selectedType == 1) {
        
        _lblOneMonth.textColor =[UIColor colorWithRed:0.400 green:0.718 blue:0.318 alpha:1.00];
        _lblTweleveMonth.textColor =[UIColor blackColor];
        
        _viewDuration1M.backgroundColor = selectedBackColor;
        _viewDuration12M.backgroundColor = unSelectedBackColor;
        
        _viewDuration1M.layer.borderColor = selectedBorderColor.CGColor;
        _viewDuration12M.layer.borderColor = unSelectedBorderColor.CGColor;
        
    }else{
        
        _lblOneMonth.textColor =[UIColor blackColor];
        _lblTweleveMonth.textColor =[UIColor colorWithRed:0.400 green:0.718 blue:0.318 alpha:1.00];
        
        _viewDuration1M.backgroundColor = unSelectedBackColor;
        _viewDuration12M.backgroundColor = selectedBackColor;
        
        _viewDuration1M.layer.borderColor = unSelectedBorderColor.CGColor;
        _viewDuration12M.layer.borderColor = selectedBorderColor.CGColor;
    }
}

- (IBAction)on79DKK:(id)sender {
    if (is79DKK) {
        
        is79DKK = false;
        _img79DKK.image = unSelectedCheckImage;
        [self onAddOn79:2];
    }else{
         _lblSatcarBeep.textColor =[UIColor colorWithRed:0.400 green:0.718 blue:0.318 alpha:1.00];
        
        is79DKK = true;
        _img79DKK.image = selectedCheckImage;
        [self onAddOn79:1];
    }
}

- (IBAction)on1799DKK:(id)sender {
    if (is1799DKK) {
        _lblSatcarGps.textColor =[UIColor blackColor];
        
        is1799DKK = false;
        _img1799.image = unSelectedCheckImage;
        [self onAddOn1779:2];
    }else{
        _lblSatcarGps.textColor =[UIColor colorWithRed:0.400 green:0.718 blue:0.318 alpha:1.00];
        
        is1799DKK = true;
        _img1799.image = selectedCheckImage;
        [self onAddOn1779:1];
    }
}

-(void)onAddOn79:(int)selectedType{
    if (selectedType == 1) {
        _viewAddOn79.backgroundColor = selectedBackColor;
        _viewAddOn79.layer.borderColor = selectedBorderColor.CGColor;
    }else{
        _viewAddOn79.backgroundColor = unSelectedBackColor;
        _viewAddOn79.layer.borderColor = unSelectedBorderColor.CGColor;
    }
}
-(void)onAddOn1779:(int)selectedType{
    if (selectedType == 1) {
        _viewAddOn1799.backgroundColor = selectedBackColor;
        _viewAddOn1799.layer.borderColor = selectedBorderColor.CGColor;
    }else{
        _viewAddOn1799.backgroundColor = unSelectedBackColor;
        _viewAddOn1799.layer.borderColor = unSelectedBorderColor.CGColor;
    }
}

- (IBAction)onNext:(id)sender {
      [self performSegueWithIdentifier:@"segueUserStandardToRegister" sender:self];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"segueUserStandardToRegister"]) {
        float amount = 0.0,total_amount = 0.0;
        NSString *car_beep,*gps_tracker;
        if (isoneMonth) {
            amount = amount + oneMonthAmount;
            
        }else{
            amount = amount + tweleveMonthAmount;
        }
        
        if (is79DKK) {
            //amount = amount + 79;
            amount = amount + 149;
            //car_beep = @"79";
            car_beep = @"149";
            carBeepSelected = @"1";
        }else{
            car_beep = @"0";
            carBeepSelected = @"0";
        }
        
        if (is1799DKK) {
            //amount = amount + 1799;
             amount = amount + 1999;
            // gps_tracker = @"1799";
            gps_tracker = @"1999";
            GPSSelected = @"1";
        }else{
            gps_tracker = @"0";
            GPSSelected = @"0";
        }
        total_amount = amount * 100;
        RegisterViewController *controller = segue.destinationViewController;
        
        controller.amount = [NSString stringWithFormat:@"%.0f",total_amount];//[numberFormatter stringFromNumber:amount_number];
        controller.car_beep = car_beep;
        controller.gps_tracker = gps_tracker;
        controller.userFrom = userFrom;
        if ([carBeepSelected isEqualToString:@"1"] || [GPSSelected isEqualToString:@"1"] ) {
            controller.trackerSelected = @"1";
        }else{
            controller.trackerSelected = @"0";
        }
        NSLog(@"car_beep : %@",controller.car_beep);
        NSLog(@"gps_tracker : %@",controller.gps_tracker);
    }
}
@end
