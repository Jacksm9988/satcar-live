//
//  TrackersDetailsViewController.m
//  SATCAR
//
//  Created by Percept Infotech on 30/12/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import "TrackersDetailsViewController.h"
#import "UIViewController+VCHelpers.h"
#import "ECSlidingViewController.h"
#import "LeftSideViewController.h"
#import "NavigationController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "UIViewController+VCHelpers.h"


@interface TrackersDetailsViewController ()<UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate>
{
    UIPickerView *expenseTypePicker;
    NSArray *geofenceMeter;
    NSString *alarm,*geofence,*Live,*tempRange;
    AppDelegate *appdelegate;
    NSString *old_tracker_id,*new_tracker_id;
    BOOL isEdit;
}
@end

@implementation TrackersDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _viewGeoBack.layer.cornerRadius = 5.0;
    geofenceMeter = [[NSArray alloc]initWithObjects:@"300",@"500",@"1000", nil];
    isEdit = false;
    //selectedRange = @"300";
    //_txtGeoDefenceValue.text = [ NSString stringWithFormat:@"%@ meter",selectedRange];
    _lblTrackerActiveValue.text = NSLocalizedString(@"No", @"");//@"YES";
    _lblAlarmCenterValue.text = NSLocalizedString(@"Yes", @"");//@"YES"
    [self hideGeofencePopup];
    [self callGetTrackerSettingWS];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self addTitleView:NSLocalizedString(@"TrackersDetail", @"")];
    _lblTrackerSerial.text = NSLocalizedString(@"TrackerSerial", @"");
    _lblTrackerActive.text = NSLocalizedString(@"TrackerActive", @"");
    _lblAlarmCenter.text = NSLocalizedString(@"Alarm_Central", @"");
    _lblGeofence.text = NSLocalizedString(@"Geofence", @"");
    _lblGeoDefence.text = NSLocalizedString(@"geofenceDistance", @"");
    _lblLiveMode.text = NSLocalizedString(@"LiveMode", @"");
    _lblAlarm.text = NSLocalizedString(@"Alarm", @"");
    _lblLog.text = NSLocalizedString(@"Log", @"");
    [_btnOK setTitle:NSLocalizedString(@"Ok", @"") forState:UIControlStateNormal];
    [_bntCancel setTitle:NSLocalizedString(@"Cancel", @"") forState:UIControlStateNormal];
    
    UIButton *btnAdd = [UIButton buttonWithType:UIButtonTypeCustom];
    btnAdd.frame = CGRectMake(20,0,50,32);
    [btnAdd setTitle:NSLocalizedString(@"Save", @"") forState:UIControlStateNormal];
    [btnAdd setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnAdd.titleLabel.font = [UIFont systemFontOfSize:16];
    [btnAdd addTarget:self action:@selector(onSave:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* item = [[UIBarButtonItem alloc] initWithCustomView:btnAdd];
    self.navigationItem.rightBarButtonItem = item;
    
    /*UIView *Rview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 20, 20)];
    [img setImage:[UIImage imageNamed:@"img_down.png"]];
    [Rview addSubview:img];
    _txtGeoDefenceValue.rightView = Rview;
    _txtGeoDefenceValue.rightViewMode = UITextFieldViewModeAlways;*/
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField == _txtTrackerSerialValue){
        isEdit = true;
    }
}
/*#pragma mark - textfield delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if (textField == _txtGeoDefenceValue) {
        expenseTypePicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 50, 100, 150)];
        [expenseTypePicker setDataSource: self];
        [expenseTypePicker setDelegate: self];
        expenseTypePicker.showsSelectionIndicator = YES;
        _txtGeoDefenceValue.inputView = expenseTypePicker;
        if (![_txtGeoDefenceValue hasText]) {
            selectedRange = [geofenceMeter objectAtIndex:0];
            _txtGeoDefenceValue.text = [NSString stringWithFormat:@"%@ meter",selectedRange];;
        }
    }
}

#pragma mark - Picker View Data source
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
 
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return geofenceMeter.count;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component{
    selectedRange = [geofenceMeter objectAtIndex:row];
    tempRange = [geofenceMeter objectAtIndex:row];
    NSString *meter = [NSString stringWithFormat:@"%@ meter",selectedRange];
   [_txtGeoDefenceValue setText:meter];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow: (NSInteger)row forComponent:(NSInteger)component{
   return [NSString stringWithFormat:@"%@ meter",[geofenceMeter objectAtIndex:row]];
}

#pragma mark - custome  functions
-(void)showGeofencePopup{
    _viewBack.hidden = false;
    _IBLayoutConstraintPopupTop.constant = 0;
    _IBLayoutConstraintPopupHeight.constant = self.view.frame.size.height;
}*/

-(void)hideGeofencePopup{
    _viewBack.hidden = true;
    _IBLayoutConstraintPopupTop.constant = self.view.frame.size.height;
    _IBLayoutConstraintPopupHeight.constant = 0;
}


#pragma mark - Webservice call
-(void)callGetTrackerSettingWS{
    
    if(![self CheckNetworkConnection]) {
        //[self AlertNoInternet:NSLocalizedString(@"NoInternet", @"") tag:454869];
        return;
    }
 
    NSString *tracker_id = [Helper getPREF:@"tracker_id"];
    NSDictionary *parameter=@{@"tracker_id":tracker_id};
    if (tracker_id.length <= 0){
        [Helper displayAlertView:@"" message:NSLocalizedString(@"tracker_not_avbl", @"")];
        [self.navigationController popViewControllerAnimated:true];
        
    }else{
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        [WebService httpPostWithCustomDelegateURLString:@".trackerSetting" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error)
         {
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             
             if (error == nil) {
                 NSError *localError = nil;
                 NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
                 NSLog(@"Response ; %@",parsedObject);
                 
                 if (localError != nil) {
                     NSLog(@"%@",localError.description);
                 }
                 else {
                     NSDictionary *response = [parsedObject objectForKey:@"data"];
                     NSDictionary *trackerDetail = [response objectForKey:@"trackerDetail"];
                     NSString* message = [response objectForKey:@"message"];
                     NSString* success = [response objectForKey:@"status"];
                     if ([success isEqualToString:@"Success"]) {
                         //_lblTrackerSerialValue.text = [trackerDetail valueForKey:@"id"];
                         _txtTrackerSerialValue.text = [trackerDetail valueForKey:@"id"];
                         new_tracker_id = _txtTrackerSerialValue.text;
                         NSString *liveMode = [trackerDetail valueForKey:@"mode"];
                         appdelegate.selectedRange = [trackerDetail valueForKey:@"geofenceRadius"];
                         if ([liveMode isEqualToString:@"5"]){
                             
                             alarm = @"5";
                             geofence = @"0";
                             Live = @"0";
                             [_switchAlaram setOn:true];
                             [_switchGeofence setOn:false];
                             [_switchLiveMode setOn:false];
                             
                         }else if ([liveMode isEqualToString:@"3"]) {
                             
                             alarm = @"0";
                             geofence = @"3";
                             Live = @"0";
                             
                             appdelegate.selectedRange = [trackerDetail valueForKey:@"geofenceRadius"];
                             tempRange = [trackerDetail valueForKey:@"geofenceRadius"];
                             _txtGeoDefenceValue.text = [ NSString stringWithFormat:@"%@ meter",appdelegate.selectedRange];
                             [_switchGeofence setOn:true];
                             //[self showGeofencePopup];
                             
                             [_switchAlaram setOn:false];
                             [_switchLiveMode setOn:false];
                             
                         }else{
                             
                             alarm = @"0";
                             geofence = @"0";
                             Live = @"2";
                             
                             _lblTrackerActiveValue.text = NSLocalizedString(@"Yes", @"");;//@"YES";
                             [_switchLiveMode setOn:true];
                             [_switchGeofence setOn:false];
                             [_switchAlaram setOn:false];
                         }
                     } else {
                         NSLog(@"error :: %@",message);
                         [Helper displayAlertView:@"" message:NSLocalizedString(@"tracker_not_avbl", @"")];
                         [self.navigationController popViewControllerAnimated:true];
                         //                     if ([message isEqualToString:@"Your session is expired."]) {
                         //                         [self moveToLogin];
                         //                     }
                     }
                 }
             }
         }];

    }
    
   }

-(void)callSaveTrackerDetailWS:(NSDictionary *)parameter{
    if(![self CheckNetworkConnection])
    {
        //[self AlertNoInternet:NSLocalizedString(@"NoInternet", @"") tag:454869];
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [WebService httpPostWithCustomDelegateURLString:@".updatetrac_Set" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         if (error == nil) {
             NSError *localError = nil;
             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
             
             if (localError != nil) {
                 NSLog(@"%@",localError.description);
             }
             else {
                 NSDictionary *response = [parsedObject objectForKey:@"data"];
                 NSString* message = [response objectForKey:@"message"];
                 NSString* success = [response objectForKey:@"status"];
                 if ([success isEqualToString:@"Success"]) {
                     
                     if (isEdit) {
                         appdelegate.tracker_id = new_tracker_id;
                         [Helper setPREFStringValue:new_tracker_id sKey:@"tracker_id"];
                     }
                     [Helper displayAlertView:@"" message:NSLocalizedString(@"Setting_saved", nil)];
                     [self.navigationController popViewControllerAnimated:true];
                 } else {
                     NSLog(@"error :: %@",message);
                     [Helper displayAlertView:@"" message:message];
                 }
             }
         }
     }];
}


#pragma mark - button actions
- (IBAction)onSave:(id)sender {
    
    NSString *tracker_id = [Helper getPREF:@"tracker_id"];
    if (_txtTrackerSerialValue.text.length <= 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_trackid", @"")];
        return;
    }
    if (isEdit) {
        new_tracker_id = _txtTrackerSerialValue.text;
    }
    
    if ([appdelegate.selectedRange doubleValue] <= 0) {
        appdelegate.selectedRange = @"1000";
    }
    NSDictionary *parameter=@{
                              @"tracker_id":new_tracker_id,
                              @"old_tracker_id":tracker_id,
                              @"live":Live,
                              @"geofence":geofence,
                              @"alarm":alarm,
                              @"range":appdelegate.selectedRange,
                              @"is_hold":@"0"
                              };
    //NSLog(@"parameter : %@",parameter);
    [self callSaveTrackerDetailWS:parameter];
}

- (IBAction)onLog:(id)sender {
    [self performSegueWithIdentifier:@"segueTrackerLog" sender:self];
}
- (IBAction)onGeofence:(id)sender {
    if ([sender isOn]) {
        geofence = @"3";
        //[self showGeofencePopup];
        
        Live = @"0";
        _lblTrackerActiveValue.text = NSLocalizedString(@"No", @"");;
        [_switchLiveMode setOn:false];
        
        alarm = @"0";
        [_switchAlaram setOn:false];
        
        [self performSegueWithIdentifier:@"segueToGeofence" sender:self];
        
    }else{
        geofence = @"0";
       // appdelegate.selectedRange = @"";
        //[self hideGeofencePopup];
        
        if ([Live isEqualToString:@"0"] && [alarm isEqualToString:@"0"]) {
            geofence = @"3";
           // [_switchGeofence setOn:true];
            //[self showGeofencePopup];
        }
    }
}

- (IBAction)onLiveMode:(id)sender {
    
    if ([sender isOn]) {
        Live = @"2";
        _lblTrackerActiveValue.text = NSLocalizedString(@"Yes", @"");;
        
        geofence = @"0";
        appdelegate.selectedRange = @"";
       // [self hideGeofencePopup];
        [_switchGeofence setOn:false];
        
        alarm = @"0";
        [_switchAlaram setOn:false];
        
    }else{
        Live = @"0";
        _lblTrackerActiveValue.text = NSLocalizedString(@"No", @"");
        
        if ([geofence isEqualToString:@"0"] && [alarm isEqualToString:@"0"]) {
            Live = @"2";
            _lblTrackerActiveValue.text = NSLocalizedString(@"Yes", @"");
            [_switchAlaram setOn:true];
        }
    }
}

- (IBAction)onAlaram:(id)sender {
    
    if ([sender isOn]) {
        alarm = @"5";
        
        Live = @"0";
        _lblTrackerActiveValue.text = NSLocalizedString(@"No", @"");
        [_switchLiveMode setOn:false];
      
        geofence = @"0";
        appdelegate.selectedRange = @"";
        //[self hideGeofencePopup];
        [_switchGeofence setOn:false];
        
    }else{
        alarm = @"0";
        
        if ([geofence isEqualToString:@"0"] && [Live isEqualToString:@"0"]) {
            alarm = @"5";
            [_switchAlaram setOn:true];
        }
    }
}
- (IBAction)onCancel:(id)sender {
    [_txtGeoDefenceValue resignFirstResponder];
    //[self hideGeofencePopup];
    if ([Live isEqualToString:@"0"] && [alarm isEqualToString:@"0"]) {
        geofence = @"3";
        appdelegate.selectedRange = tempRange;
        [_switchGeofence setOn:true];
    }
}

- (IBAction)onOK:(id)sender {
    [_txtGeoDefenceValue resignFirstResponder];
    [_switchGeofence setOn:true];
    //[self hideGeofencePopup];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
