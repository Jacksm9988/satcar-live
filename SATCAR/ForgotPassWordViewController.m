//
//  ForgotPassWordViewController.m
//  SATCAR
//
//  Created by Percept Infotech on 12/16/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import "ForgotPassWordViewController.h"

@interface ForgotPassWordViewController ()

@end

@implementation ForgotPassWordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self addTitleView:NSLocalizedString(@"Forgotpassword", @"")];
    
    [Helper setBorderWithColorInButton:_BtnSendPassw];
    
    [_BtnSendPassw setTitle:NSLocalizedString(@"Sendpasswordreset", @"") forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Textfield delegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    textField.textColor =  [UIColor colorWithRed:18.0/255.0 green:172.0/255.0 blue:243.0/255.0 alpha:1.0];
    textField.layer.borderWidth = 1;
    textField.layer.borderColor =  [UIColor colorWithRed:18.0/255.0 green:172.0/255.0 blue:243.0/255.0 alpha:1.0].CGColor;
    textField.clipsToBounds = YES;
    return true;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    textField.textColor =  [UIColor blackColor];
    textField.layer.borderWidth = 0;
    textField.layer.borderColor = [UIColor clearColor].CGColor;
    textField.clipsToBounds = YES;
    return true;
}

#pragma mark - Button Action
- (IBAction)SendPasswordPress:(id)sender {
    [self.view endEditing:YES];
   
    if(![self CheckNetworkConnection]) {
        return;
    }
    
    if ([self isValidData]) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSDictionary *parameter=@{ @"email":_txtEmail.text };
        
        [WebService httpPostWithCustomDelegateURLString:@".forgotPassword" parameter:parameter isDebug:YES callBackData:^(NSData *data, NSError *error)
         {
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             if (error == nil) {
                 
                 NSError *localError = nil;
                 NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
                 
                 if (localError != nil) {
                     NSLog(@"%@",localError.description);
                 } else {
                     NSDictionary *response = [parsedObject objectForKey:@"data"];
                     NSString* message = [response objectForKey:@"message"];
                     NSString* success = [response objectForKey:@"status"];
                     if ([success isEqualToString:@"Success"]) {
                         [Helper displayAlertView:@"" message:NSLocalizedString(@"password_changed", @"")];
                         [self.navigationController popViewControllerAnimated:true];
                     } else {
                         NSLog(@"error :: %@",message);
                         [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Invalid_email_id", @"")];
                     }
                 }
             }
         }];
    }
}

#pragma mark - validation
-(BOOL)isValidData{
    if (_txtEmail.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_enter_email_id",nil)];
        return false;
        
    }else if (![self validateEmailWithString:_txtEmail.text]){
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Invalid_email_id",nil)];
        return false;
    }
    return true;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
@end
