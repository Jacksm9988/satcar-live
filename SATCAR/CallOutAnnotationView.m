//
//  CallOutAnnotationView.m
//  MyPlan
//
//  Created by Percept Infotech on 9/6/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import "CallOutAnnotationView.h"
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <AddressBook/AddressBook.h>

@interface CallOutAnnotationView ()
@property (nonatomic, assign) CLLocationCoordinate2D theCoordinate;
@property (nonatomic, copy) NSString *address;
-(void)drawInContext:(CGContextRef)context;
- (void)getDrawPath:(CGContextRef)context;



@end

@implementation CallOutAnnotationView
@synthesize lat=_lat,lon=_lon,altitude= _altitude;
@synthesize subtitle= _subtitle, title= _title, source=_source, image =_img;


- (id)initWithName:(NSString*)name address:(NSString*)address phonenumber:(NSString*)phonenumber coordinate:(CLLocationCoordinate2D)coordinate {
    if ((self = [super init])) {
        if ([name isKindOfClass:[NSString class]]) {
            self.title = name;
        } else {
            self.title = @"Unknown charge";
        }
        self.phonenumber = phonenumber;
        self.address = address;
        self.theCoordinate = coordinate;
       
        
        
    }
    return self;
}

- (NSString *)title {
    return _title;
}

- (NSString *)subtitle {
    return _phonenumber;
}

- (CLLocationCoordinate2D)coordinate {
    return _theCoordinate;
}
-(IBAction)OpenDirection:(float)lat lon:(float)lon
{
    UIApplication *app = [UIApplication sharedApplication];
    
    _locationManager = [CLLocationManager new];
    _locationManager.delegate = self;
    _locationManager.distanceFilter = kCLDistanceFilterNone;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [_locationManager startUpdatingLocation];

    
    NSString *coordinates = [NSString stringWithFormat:@"http://maps.google.com/maps?daddr=%f,%f&saddr=%f,%f",_locationManager.location.coordinate.latitude,_locationManager.location.coordinate.longitude,lat,lon];
    
    [app openURL:[NSURL URLWithString: coordinates]];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
   
    if(alertView.tag == 457) {
        
        if(buttonIndex == 1) {
            [self OpenDirection:_theCoordinate.latitude lon:_theCoordinate.longitude];
            
        }else if(buttonIndex == 2){
            _phonenumber = [_phonenumber stringByReplacingOccurrencesOfString:@" " withString:@""];
            NSString *phoneStr = [[NSString alloc] initWithFormat:@"tel:%@",_phonenumber];
            NSURL *phoneURL = [[NSURL alloc] initWithString:phoneStr];
            [[UIApplication sharedApplication] openURL:phoneURL];
        }
    }
}
- (void)mapItem {
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Doyouwantdirections", @"") message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"") otherButtonTitles:NSLocalizedString(@"Directions", @""),NSLocalizedString(@"Call", @""), nil];
    alert.tag = 457;
    [alert show];
}@end

