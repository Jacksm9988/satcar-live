//
//  RouteObject.h
//  SATCAR
//
//  Created by Percept Infotech on 2018-05-16.
//  Copyright © 2018 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RouteObject : UIViewController
@property(nonatomic) CLLocationCoordinate2D start;
@property(nonatomic) CLLocationCoordinate2D end;
-(void)routeWithOrigin:(CLLocationCoordinate2D)start destination:(CLLocationCoordinate2D)destination;

@end
