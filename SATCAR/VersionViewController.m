//
//  VersionViewController.m
//  SATCAR
//
//  Created by Percept Infotech on 19/04/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import "VersionViewController.h"

@interface VersionViewController (){
    NSString *parent ;
}

@end

@implementation VersionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _lblVersionValue.text = [NSString stringWithFormat:@"%@",[Helper getVersionNumber]];
    _lblBuildValue.text = [NSString stringWithFormat:@"%@",[Helper getBuildNumber]];
    _lblBuild.text = NSLocalizedString(@"Build", @"");
    parent = [Helper isStringIsNull:[Helper getPREF:@"parent"]];
    if ([parent integerValue] > 0) {
        [self.navigationController setNavigationBarHidden:NO animated:YES];
        [self addTitleView2:@"Version"];
    }else{
        [self addTitleView:@"Version"];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
