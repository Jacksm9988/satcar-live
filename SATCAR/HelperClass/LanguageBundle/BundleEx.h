//
//  BundleEx.h
//  MyPlan
//
//  Created by Percept Infotech on 15/06/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (Language)

+ (void)setLanguage:(NSString *)language;

@end
