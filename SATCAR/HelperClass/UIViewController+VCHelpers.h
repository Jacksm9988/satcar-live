//
//  AppDelegate
//  MyPlan
//
//  Created by Percept Infotech on 7/7/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>

@class AppDelegate;

@interface UIViewController (VCHelpers)<UIAlertViewDelegate>

//@property (strong, nonatomic) NSString *isBack2;
@property IBOutlet UIButton *btnAdd;
@property (nonatomic) BOOL isopen;
@property (nonatomic) BOOL isalertshow;

-(NSString *)getiPhone;
-(void)addTitleView:(NSString *)ViewTitle;
-(void)addTitleView2:(NSString *)ViewTitle;
-(void)addTitleViewHomeScreen:(NSString *)ViewTitle;

-(void)addTitleViewHelp:(NSString *)ViewTitle;
-(void)addOnlyTitleView:(NSString *)ViewTitle;
- (UIBarButtonItem *)backBarButtonItem;
-(void)removeNavigation;
-(void)showVideoMenu;
-(IBAction)revealLeftPane2:(id)sender;
- (UIColor *)colorFromHexString:(NSString *)hexString;
-(void)Logout;
-(BOOL )CheckSession:(id)WebData;
-(void)alertErrorwithMessage:(NSString *)Message;
-(NSData*)CompressData:(UIImage *)image;
-(void)alertSucess:(NSString *)Title Message:(NSString *)Message;
- (IBAction)canhaveCamPermition;
-(BOOL)CheckNetworkConnection;
-(void)removeUserData;
-(BOOL)AlertStatus:(NSDictionary *)dic;
-(void)AlertNoInternet:(NSString *)Message  tag:(int)tagvalue;
-(void)getInfoVideos;
- (void)setDailyNotification:(NSDate *)alarmdate2 repeatoption:(NSString *)repeatoption soundname:(NSString *)soundname title:(NSString *)title DicNotif:(NSDictionary*)DicNotif;
-(void)setMoodsNotifications:(NSDate *)time body:(NSString *)body dic:(NSMutableDictionary *)dic;
-(void)deleteAlarms;
-(void)showSymptomsVideoMenu;

// date formate for display
-(NSString *)ConvertToDKCurrency:(NSString *)Amount;
-(NSString *)getMonthYearDay:(NSDate*)date;
-(NSString *)getNextMonthYearDay:(NSDate*)date;
-(NSDate *)getNextMonthDate:(NSDate*)date;
-(NSDate *)getPrevoiusMonthDate:(NSDate*)date;
-(BOOL)dateComparision:(NSString *)str1 andDate2:(NSString *)str2;
-(NSString *)getCellDisplayDateFormat:(NSString*)str;
-(NSString *)getCellDisplayTimeFormat:(NSString*)str;
-(NSString *)getHeaderDisplayDateFormat:(NSDate*)date;
-(BOOL)comapreDateByMonthAndYear:(NSDate *)date registerDate:(NSString *)rDate;
-(BOOL)comapreNextDate:(NSDate *)dtTwo;
-(NSString *)getCurrentTime;
-(NSString *)getCurrentDate;
-(NSString *)getStringFromDate:(NSDate *)date;
-(NSDate *)getDateFromString:(NSString *)str;
-(NSDictionary *)dataToDicitonary:(NSData *)data;
-(NSData *)dicitonaryToData:(NSDictionary *)dict;
// on session expire move to login
-(void)moveToLogin;
-(void)setHomeScreen;
// compress image
-(NSData *)compressResizeImage:(UIImage *)image;
-(NSData *)onlyCompressImage:(UIImage *)image;
- (BOOL)validateEmailWithString:(NSString*)email;

@end
