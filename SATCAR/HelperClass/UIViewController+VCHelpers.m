//
//  UIViewController+VCHelpers.h
//  MyPlan
//
//  Created by Percept Infotech on 7/7/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import "UIViewController+VCHelpers.h"
#import "LeftSideViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "NavigationController.h"
//#import "WebServicesCalls.h"
//#import "UIImageView+WebCache.h"
//#import "DBManager.h"
/*#import <ThePerfectApp/TPAViewController.h>
#import <ThePerfectApp/ThePerfectApp.h>*/
#define IDIOM    UI_USER_INTERFACE_IDIOM()
#define IPAD     UIUserInterfaceIdiomPad

@implementation UIViewController (VCHelpers)

#pragma mark - Date related funcitons



-(NSString *)getMonthYearDay:(NSDate*)date
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];
    
    NSString *strMonth;
    int month = (int)[components month];
    if(month < 10)
    {
        strMonth = [NSString stringWithFormat:@"0%d",month];
    }else{
        strMonth = [NSString stringWithFormat:@"%d",month];
    }
    NSString *strYear = [NSString stringWithFormat:@"%d",(int)[components year]];
    NSString *dateStr = [NSString stringWithFormat:@"%@-%@",strYear,strMonth];
    return dateStr;
}

-(NSString *)getNextMonthYearDay:(NSDate*)date
{
    //NSLog(@"Current Date = %@", date);
    NSDateComponents *dateComponents = [NSDateComponents new];
    dateComponents.month = 1;
    
    //NSDate *currentDatePlus1Month = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:date options:0];
    //NSLog(@"Date = %@", currentDatePlus1Month);
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];
    
    NSString *strMonth;
    int month = (int)[components month];
    if(month < 10)
    {
        strMonth = [NSString stringWithFormat:@"0%d",month];
    }else{
        strMonth = [NSString stringWithFormat:@"%d",month];
    }
    NSString *strYear = [NSString stringWithFormat:@"%d",(int)[components year]];
    NSString *dateStr = [NSString stringWithFormat:@"%@-%@",strYear,strMonth];
    return dateStr;
}

-(NSString *)ConvertToDKCurrency:(NSString *)Amount
{
    
    NSNumber *num1 = [NSNumber numberWithDouble:[Amount doubleValue]];
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormatter setMaximumFractionDigits:2];
    [numberFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"es_US"]];
    NSString *text = [numberFormatter stringFromNumber:num1];
   /* [indCurrencyFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [indCurrencyFormatter setMaximumFractionDigits:2];
//    [indCurrencyFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"de-DE"]];
    NSString *formattedString =  [indCurrencyFormatter stringFromNumber:[NSNumber numberWithFloat:[Amount doubleValue]]];*/
    return text;
}

-(NSDate *)getNextMonthDate:(NSDate*)date
{
    //NSLog(@"Current Date = %@", date);
    NSDateComponents *dateComponents = [NSDateComponents new];
    dateComponents.month = 1;
    
    NSDate *nextDate = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:date options:0];
    //NSLog(@"Date = %@", nextDate);
    return nextDate;
}

-(NSDate *)getPrevoiusMonthDate:(NSDate*)date
{
    //NSLog(@"Current Date = %@", date);
    NSDateComponents *dateComponents = [NSDateComponents new];
    dateComponents.month = -1;
    
    NSDate *PrevoiusDate = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:date options:0];
    //NSLog(@"Date = %@", PrevoiusDate);
    return PrevoiusDate;
}

-(BOOL)comapreDateByMonthAndYear:(NSDate *)dtTwo registerDate:(NSString *)rDate{
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    NSDate *dt = [format dateFromString:rDate];
    [format setDateFormat:@"YYYY-MM-dd"];
    NSString *str1 = [format stringFromDate:dt];
    NSDate *dtOne = [format dateFromString:str1];
    
    NSLog(@"%@",dtOne);
    NSLog(@"%@",dtTwo);
    
    NSDateComponents* components1 = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth|NSCalendarUnitYear fromDate:dtOne];
    NSDateComponents* components2 = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth|NSCalendarUnitYear fromDate:dtTwo];
    
    NSString *strMonth1;
    int month1 = (int)[components1 month];
    if(month1 < 10) {
        strMonth1 = [NSString stringWithFormat:@"0%d",month1];
    }else{
        strMonth1 = [NSString stringWithFormat:@"%d",month1];
    }
    NSString *strYear1 = [NSString stringWithFormat:@"%d",(int)[components1 year]];
    NSString *dateStr1 = [NSString stringWithFormat:@"%@-%@",strYear1,strMonth1];
    
    
    NSString *strMonth2;
    int month2 = (int)[components2 month];
    if(month1 < 10) {
        strMonth2 = [NSString stringWithFormat:@"0%d",month2];
    }else{
        strMonth2 = [NSString stringWithFormat:@"%d",month2];
    }
    NSString *strYear2 = [NSString stringWithFormat:@"%d",(int)[components2 year]];
    NSString *dateStr2 = [NSString stringWithFormat:@"%@-%@",strYear2,strMonth2];
    
    if ([dateStr1 isEqualToString:dateStr2]) {
        NSLog(@"button hide");
        return false;
    }else{
        NSLog(@"button show");
        return true;
    }
}

-(BOOL)comapreNextDate:(NSDate *)dtTwo{
    
    NSDate *dtOne = [NSDate date];
    //NSLog(@"Current Date = %@", dtOne);
    NSDateComponents *dateComponents = [NSDateComponents new];
    dateComponents.month = 1;
    
    NSDate *nextDate = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:dtOne options:0];
    //NSLog(@"Date = %@", nextDate);
    
    //NSLog(@"%@",dtOne);
    //NSLog(@"%@",dtTwo);
    
    /*- (NSDate *)earlierDate:(NSDate *)anotherDate;
    - (NSDate *)laterDate:(NSDate *)anotherDate;
    - (NSComparisonResult)compare:(NSDate *)other;*/
    
    NSDateComponents* components1 = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth|NSCalendarUnitYear fromDate:dtOne];
    NSDateComponents* components2 = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth|NSCalendarUnitYear fromDate:dtTwo];
    
    NSString *strMonth1;
    int month1 = (int)[components1 month];
    if(month1 < 10) {
        strMonth1 = [NSString stringWithFormat:@"0%d",month1];
    }else{
        strMonth1 = [NSString stringWithFormat:@"%d",month1];
    }
    NSString *strYear1 = [NSString stringWithFormat:@"%d",(int)[components1 year]];
    NSString *dateStr1 = [NSString stringWithFormat:@"%@-%@",strYear1,strMonth1];
    
    
    NSString *strMonth2;
    int month2 = (int)[components2 month];
    if(month1 < 10) {
        strMonth2 = [NSString stringWithFormat:@"0%d",month2];
    }else{
        strMonth2 = [NSString stringWithFormat:@"%d",month2];
    }
    NSString *strYear2 = [NSString stringWithFormat:@"%d",(int)[components2 year]];
    NSString *dateStr2 = [NSString stringWithFormat:@"%@-%@",strYear2,strMonth2];
    
    if ([dateStr1 isEqualToString:dateStr2]) {
        //NSLog(@"button hide");
        return false;
    }else{
        //NSLog(@"button show");
        return true;
    }
}

-(BOOL)dateComparision:(NSString *)str1 andDate2:(NSString *)str2{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    BOOL isTokonValid;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSInteger desiredComponents = (NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute);
    
    NSDateComponents *firstComponents = [calendar components:desiredComponents fromDate:[dateFormat dateFromString:str1]];
    NSDateComponents *secondComponents = [calendar components:desiredComponents fromDate: [dateFormat dateFromString:str2]];
    
    NSDate *first = [calendar dateFromComponents:firstComponents];
    NSDate *second = [calendar dateFromComponents:secondComponents];
    
    NSComparisonResult result = [first compare:second];
    if (result == NSOrderedAscending) {
        NSLog(@"date1 is before date2");
        isTokonValid = YES;
    } else if (result == NSOrderedDescending) {
        NSLog(@"date1 is after date2");
        isTokonValid = NO;
    }  else {
        NSLog(@"both are same");
        isTokonValid = YES;
    }
    return isTokonValid;
}



-(NSString *)getHeaderDisplayDateFormat:(NSDate*)date{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"YYYY-MM-dd"];
    NSString *str = [dateFormat stringFromDate:date];
    [dateFormat setDateFormat:@"YYYY-MM-dd"];
    NSDate *newDate = [dateFormat dateFromString:str];
    [dateFormat setDateFormat:@"MMMM YYYY"];
    NSString *retStr = [dateFormat stringFromDate:newDate];
    return retStr;
}

-(NSString *)getCellDisplayDateFormat:(NSString*)str{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"YYYY-MM-dd"];
    NSDate *dte = [dateFormat dateFromString:str];
    //[dateFormat setDateFormat:@"dd-MMMM-YYYY"];
    [dateFormat setDateFormat:@"dd MMMM YYYY"];
    NSString *retStr = [dateFormat stringFromDate:dte];
    return retStr;
}

-(NSString *)getDisplayDateFormat:(NSString*)str{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"YYYY-MM-dd"];
    NSDate *dte = [dateFormat dateFromString:str];
    [dateFormat setDateFormat:@"dd-MMMM-YYYY"];
    NSString *retStr = [dateFormat stringFromDate:dte];
    return retStr;
}
-(NSString *)getCellDisplayTimeFormat:(NSString*)str{
    NSDateFormatter* df = [[NSDateFormatter alloc] init];
    [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [df setTimeZone:[NSTimeZone systemTimeZone]];
    [df setDateFormat:@"HH:mm:ss"];
   
    NSDate* newDate = [df dateFromString:str];
    [df setDateFormat:@"HH:mm"];
    NSString  *retStr = [df stringFromDate:newDate];
    
    return retStr;
}
-(NSString *)getCurrentTime{
    NSDateFormatter __autoreleasing *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormat setDateFormat:@"HH:mm"];
    NSString *dateString =  [dateFormat stringFromDate:[NSDate date]];
    return  dateString;
}

-(NSString *)getCurrentDate{
    NSDateFormatter __autoreleasing *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormat setDateFormat:@"dd/MM/YYYY"];
    NSString *Tdate =  [dateFormat stringFromDate:[NSDate date]];
    return Tdate;
}

-(void)moveToLogin{
    [self removeUserData];
}

-(NSString *)getStringFromDate:(NSDate *)date{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    NSString *str = [dateFormat stringFromDate:date];
    return str;
}

-(NSDate *)getDateFromString:(NSString *)str{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:str];
    return date;
}


-(NSData *)compressResizeImage:(UIImage *)image
{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 600.0;
    float maxWidth = 800.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.8;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth){
        if(imgRatio < maxRatio){
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    return imageData;//[UIImage imageWithData:imageData];
}

-(NSData *)onlyCompressImage:(UIImage *)image
{
    float compressionQuality = 0.5;//50 percent compression
    NSData *imageData = UIImageJPEGRepresentation(image, compressionQuality);
    return imageData;//[UIImage imageWithData:imageData];
}



#pragma mark - Camera Permittions

-(BOOL)CheckNetworkConnection
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        //NSLog(@"There IS NO internet connection");
        [Helper displayAlertView:@"" message:NSLocalizedString(@"err_network_not_available",nil)];
        return FALSE;
    } else {
        //NSLog(@"There IS internet connection");
        return TRUE;
    }
}

- (IBAction)canhaveCamPermition
{
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(authStatus == AVAuthorizationStatusAuthorized) {
        //[self selectNewCame];
    }
    else if(authStatus == AVAuthorizationStatusNotDetermined)     {
        //NSLog(@"%@", @"Camera access not determined. Ask for permission.");
        
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted)
         {
             if(granted) {
                // NSLog(@"Granted access to %@", AVMediaTypeVideo);
             } else   {
                 //NSLog(@"Not granted access to %@", AVMediaTypeVideo);
                 [self camDenied];
             }
         }];
    }
    else if (authStatus == AVAuthorizationStatusRestricted)     {
        // My own Helper class is used here to pop a dialog in one simple line.
        [self alertErrorwithMessage:@"You've been restricted from using the camera on this device. Without camera access this feature won't work. Please contact the device owner so they can give you access."];
    }else    {
        [self camDenied];
    }
}

-(void)setMoodsNotifications:(NSDate *)time body:(NSString *)body dic:(NSMutableDictionary *)dic
{
   /* NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar] ;
    NSDate *now = [NSDate date];
    NSDateComponents *components = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:now];
    
   // NSArray *arr = [time componentsSeparatedByString:@":"];
    //int hr = [[arr objectAtIndex:0] intValue]>12?[[arr objectAtIndex:0] intValue]-12:[[arr objectAtIndex:0] intValue];
   // [components setHour:[[arr objectAtIndex:0] intValue]];
    //[components setMinute:[[arr objectAtIndex:1] intValue]];
    //[components setSecond:00];
    
    NSDateFormatter *dtf = [[NSDateFormatter alloc]init];
    [dtf setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [dtf setDateFormat:@"hh:mm"];
    NSCalendar *calendar2 = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDate *dt = [dtf dateFromString:time];
    NSDateComponents *todayComps = [calendar2 components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:now];
    NSDateComponents *comps = [calendar2 components:(NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:dt];
     [comps setDay:components.day];
    [comps setMonth:components.month];
    [comps setYear:components.year];
   
    
    NSDate *date = [calendar dateFromComponents:comps];*/
    
    NSLog(@"Notification set : %@",time.description);
    
    UILocalNotification *notification = [[UILocalNotification alloc]init];
    notification.fireDate = time;
    [dic setObject:time forKey:@"alarmdate"];
    notification.repeatInterval = NSCalendarUnitDay;
    [notification setAlertBody:body];
    notification.userInfo = dic;
     notification.soundName = UILocalNotificationDefaultSoundName;
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
}


- (void)camDenied
{
    NSLog(@"%@", @"Denied camera access");
    
    NSString *alertText;
    NSString *alertButton;
    BOOL canOpenSettings = (&UIApplicationOpenSettingsURLString != NULL);
    
    if (canOpenSettings)
    {
        alertText = @"It looks like your privacy settings are preventing us from accessing your camera to do barcode scanning. You can fix this by doing the following:\n\n1. Touch the Go button below to open the Settings app.\n\n2. Touch Privacy.\n\n3. Turn the Camera on.\n\n4. Open this app and try again.";
        
        alertButton = @"Go";
    }
    else
    {
        alertText = @"It looks like your privacy settings are preventing us from accessing your camera to do barcode scanning. You can fix this by doing the following:\n\n1. Close this app.\n\n2. Open the Settings app.\n\n3. Scroll to the bottom and select this app in the list.\n\n4. Touch Privacy.\n\n5. Turn the Camera on.\n\n6. Open this app and try again.";
        
        alertButton = @"OK";
    }
    
    /*UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:NSLocalizedString(@"error", @"")
                          message:alertText
                          delegate:self
                          cancelButtonTitle:alertButton
                          otherButtonTitles:nil];
    alert.tag = 3491832;
    [alert show];*/
    
    
    UIAlertController *alertview = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"error", @"") message:alertText preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:alertButton style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        BOOL canOpenSettings = (UIApplicationOpenSettingsURLString != NULL);
        if (canOpenSettings)
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }];
    [alertview addAction:ok];
    [self presentViewController:alertview animated:true completion:nil];
    
}

//#pragma mark - Alertview delegate
//- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
//{
//    NSLog(@"Dismiss selected");
//    if (alertView.tag == 3491832)
//    {
//        BOOL canOpenSettings = (&UIApplicationOpenSettingsURLString != NULL);
//        if (canOpenSettings)
//            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
//    }else if (alertView.tag == 111) {
//        if (buttonIndex == 1) {
//
//            NSLog(@"OK selected");
//            [self callLogoutService];
//
//        }else{
//            NSLog(@"cancel selected");
//        }
//    }
//}
//-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//    if (alertView.tag == 111) {
//        if (buttonIndex == 1) {
//            NSLog(@"OK selected");
//        }else{
//            NSLog(@"cancel selected");
//        }
//    }
//}

-(NSData*)CompressData:(UIImage *)image
{
    
    NSData *imgData=UIImageJPEGRepresentation(image,1.0);
    float compressionRate=10;
    while (imgData.length>1024)
    {
        if (compressionRate>0.5)
        {
            compressionRate=compressionRate-0.5;
            imgData=UIImageJPEGRepresentation(image,compressionRate/10);
        }
        else
        {
            return imgData;
        }
    }
    return imgData;
}


-(void)removeNavigation
{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

-(void)alertErrorwithMessage:(NSString *)Message {
    /*UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:Message delegate:nil cancelButtonTitle:NSLocalizedString(@"Ok", @"") otherButtonTitles:nil, nil];
    //[[UIView appearance] setTintColor:[self colorFromHexString:@"#35a395"]];
    [alert show];*/
    
    UIAlertController *alertview = [UIAlertController alertControllerWithTitle:@"" message:Message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    [alertview addAction:ok];
    [self presentViewController:alertview animated:true completion:nil];
}

-(void)alertSucess:(NSString *)Title Message:(NSString *)Message
{
    /*UIAlertView *alert = [[UIAlertView alloc]initWithTitle:Title message:Message delegate:nil cancelButtonTitle:NSLocalizedString(@"Ok", @"") otherButtonTitles:nil, nil];
    //[[UIView appearance] setTintColor:[self colorFromHexString:@"#35a395"]];
    [alert show];*/
    
    UIAlertController *alertview = [UIAlertController alertControllerWithTitle:Title message:Message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertview addAction:ok];
    [self presentViewController:alertview animated:true completion:nil];
}

-(void)AlertNoInternet:(NSString *)Message  tag:(int)tagvalue{
    
    /*UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", @"") message:Message delegate:self cancelButtonTitle:NSLocalizedString(@"Retry", @"") otherButtonTitles:NSLocalizedString(@"Cancel", @""), nil];
    alert.tag = tagvalue;// 454869;
    //[[UIView appearance] setTintColor:[self colorFromHexString:@"#35a395"]];
    [alert show];*/
    
    UIAlertController *alertview = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"error", @"") message:Message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertview addAction:ok];
    [self presentViewController:alertview animated:true completion:nil];
}


-(BOOL)AlertStatus:(NSDictionary *)dic
{
    if([[[dic valueForKey:@"data"] valueForKey:@"status"] isEqualToString:@"Success"]){
        
        //[self alertSucess:[[dic valueForKey:@"data"] valueForKey:@"status"] Message:[[dic valueForKey:@"data"] valueForKey:@"message"]];
        return TRUE;
    }else
    {
        //[self alertSucess:@"Error" Message:[[dic valueForKey:@"data"] valueForKey:@"message"]];
        return FALSE;
    }

}


-(BOOL)CheckSession:(id)WebData{
    
    if([WebData isKindOfClass:[NSArray class]]){
        return TRUE;
    }else if([WebData isKindOfClass:[NSDictionary class]]){
        
        NSString *status = [WebData valueForKey:@"status"];
        if([status isEqualToString:@"Failed"])    {
            NSString *statusMsg = [WebData valueForKey:@"message"];
            if([statusMsg isEqualToString:@"Your session is expired."])    {
                [self Logout];
            }
            return FALSE;
        }else
            return TRUE;
    }else
        return false;
}


-(void)Logout{
   
    /*UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"SessionExpired", @"") message:NSLocalizedString(@"YourLoginsessionhasbeenexpired", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"Ok", @"") otherButtonTitles:nil, nil];
    alert.tag = 4568;
    [alert show];*/
    
    UIAlertController *alertview = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"SessionExpired", @"") message:NSLocalizedString(@"YourLoginsessionhasbeenexpired", @"") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"Ok", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertview addAction:ok];
    [self presentViewController:alertview animated:true completion:nil];
}

-(NSString *)getiPhone
{
    NSString *iphone;
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
    {
        if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            //iphone 5
            iphone = @"iPhone5";
        }
        else if([[UIScreen mainScreen] bounds].size.height < 568)
        {
            iphone = @"iPhone4";
            //iphone 3.5 inch screen iphone 3g,4s
        }else{
            iphone = @"iPhone6";
        }
    }
    return iphone;

}

-(void)addTitleView:(NSString *)ViewTitle
{
        // img.frame = CGRectMake(120, 5, 80, 35);
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    //self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
    
    UILabel *lbltitle = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-30, 10, 60, 30)];
    lbltitle.textColor = [UIColor colorWithRed:0.161 green:0.161 blue:0.169 alpha:1.00];
    lbltitle.text =ViewTitle;
    
    self.navigationItem.titleView=lbltitle;
    
    UIImageView *img = [[UIImageView alloc]init];
    img.image= [UIImage imageNamed:@"img_back.png"];
    
    img.frame = CGRectMake(0, 0, 30, 30);
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[img.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self.navigationController
                                                                  action:@selector(popViewControllerAnimated:)];
    //[backButton  setTintColor:[self colorFromHexString:@"#198c7a"]];
    [backButton  setTintColor:[UIColor blackColor]];
    self.navigationItem.leftBarButtonItem = backButton;
    
}

-(IBAction)showmenu:(id)sender {
    UIButton *btnMenuSlide = (UIButton *)sender;
    
    BOOL found = FALSE;
    UIView* viewvideo,*viewback;
    UIScrollView *scrollview;
    for (UIView *i in self.view.subviews){
        if([i isKindOfClass:[UIButton class]]){
            UIButton *newLbl = (UIButton *)i;
            if(newLbl.tag ==12456){
                /// Write your code
                found = TRUE;
                btnMenuSlide = newLbl;
            }
        }
        if([i isKindOfClass:[UIView class]]){
            UIView *newLbl = (UIView *)i;
            if(newLbl.tag ==124){
                /// Write your code
                found = TRUE;
                viewback = newLbl;
            }
        }
        if([i isKindOfClass:[UIView class]]){
            UIView *newLbl = (UIView *)i;
            if(newLbl.tag ==12455){
                /// Write your code
                found = TRUE;
                viewvideo = newLbl;
                for (UIView *i in viewvideo.subviews){
                    if([i isKindOfClass:[UIScrollView class]]){
                        UIScrollView *newLbl = (UIScrollView *)i;
                        if(newLbl.tag ==45582){
                            /// Write your code
                            found = TRUE;
                            scrollview = newLbl;
                        }
                    }
                }
            }
            
        }
        
        
    }
    
    CGRect msgframes =viewvideo.frame;
    CGRect msgframes2 =btnMenuSlide.frame;
    CGRect msgframes3 =viewback.frame;
    if(![[NSUserDefaults standardUserDefaults]boolForKey:@"Slidemenuon"] || [[NSUserDefaults standardUserDefaults]boolForKey:@"Slidemenuon"] == 0){
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"Slidemenuon"];
        msgframes.origin.x=self.view.frame.size.width-270;
        msgframes2.origin.x=self.view.frame.size.width-315;
        msgframes3.origin.x=0;
        
    }else{
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"Slidemenuon"];
        viewvideo.backgroundColor = [UIColor colorWithRed:0.969 green:0.969 blue:0.969 alpha:1.00];
        msgframes.origin.x=self.view.frame.size.width;
        msgframes2.origin.x=self.view.frame.size.width-46;
        msgframes3.origin.x=self.view.frame.size.width;
    }
    
    
    if(found)
    {
        viewback.frame = msgframes3;
       
        [UIView animateWithDuration:0.25 animations:^{
            viewvideo.hidden = FALSE;
            viewvideo.frame=msgframes;
            btnMenuSlide.frame=msgframes2;
            viewback.alpha = 0.6;
            // scrollview.frame=msgframes;
            // [self.view setTransform:CGAffineTransformMakeTranslation(0, -210)];
        }];
    }
    else{
        viewback.frame = msgframes3;
        [UIView animateWithDuration:0.25 animations:^{
            viewvideo.hidden = FALSE;
            viewvideo.frame=msgframes;
            btnMenuSlide.frame=msgframes2;
            
            viewback.alpha = 0;
            // scrollview.frame=msgframes;
            // [self.view setTransform:CGAffineTransformMakeTranslation(0, -210)];
        }];
    }

    
    
    
}

-(void)DispVidMenu:(id)sender 
{
   
}

- (UILabel *)alignTop:(UILabel *)lbltext {
    
    
    CGSize fontSize = [lbltext.text sizeWithFont:lbltext.font];
    double finalHeight = fontSize.height * lbltext.numberOfLines;
    double finalWidth = lbltext.frame.size.width;    //expected width of label
    CGSize theStringSize = [lbltext.text sizeWithFont:lbltext.font constrainedToSize:CGSizeMake(finalWidth, finalHeight) lineBreakMode:lbltext.lineBreakMode];
    int newLinesToPad = (finalHeight  - theStringSize.height) / fontSize.height;
    for(int i=0; i<newLinesToPad; i++)
        lbltext.text = [lbltext.text stringByAppendingString:@"\n "];
    return lbltext;
}

-(void)showVideoMenu{
   [self getInfoVideos];
}

-(void)addOnlyTitleView:(NSString *)ViewTitle
{
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    [self.navigationItem setHidesBackButton:YES animated:YES];

    UILabel *lbltitle = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-30, 10, 60, 30)];
    lbltitle.textColor = [UIColor colorWithRed:0.161 green:0.161 blue:0.169 alpha:1.00];
    lbltitle.text = ViewTitle;
    self.navigationItem.titleView=lbltitle;
    UIColor *barColor = [UIColor colorWithRed:0.961 green:0.969 blue:0.965 alpha:1.00];
    self.navigationController.navigationBar.barTintColor=barColor;
}

-(void)addTitleView2:(NSString *)ViewTitle
{
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    UILabel *lbltitle = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-30, 10, 60, 30)];
    lbltitle.textColor = [UIColor colorWithRed:0.161 green:0.161 blue:0.169 alpha:1.00];
    lbltitle.text =ViewTitle;
    self.navigationItem.titleView=lbltitle;
    
    UIButton *btnMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImageView *imgmenu = [[UIImageView alloc]initWithFrame:CGRectMake(8.5, 17.5, 25, 25)];
    imgmenu.image =  [UIImage imageNamed:@"img_menu.png"];
    
    if ( IDIOM == IPAD ) {
        imgmenu.frame = CGRectMake(8.5, 17.5, 25, 25);
    } else {
        imgmenu.frame = CGRectMake(8.5, 10, 25, 25);
    }
    
    btnMenu.frame = CGRectMake(10,0,42,60);
    btnMenu.showsTouchWhenHighlighted=YES;
    //[btnMenu setBackgroundColor:[UIColor redColor]];
    [btnMenu addSubview:imgmenu];
    [btnMenu addTarget:self action:@selector(revealLeftPane2:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* item2 = [[UIBarButtonItem alloc] initWithCustomView:btnMenu];
    
    self.navigationItem.leftBarButtonItem = item2;
    UIColor *barColor = [UIColor colorWithRed:0.961 green:0.969 blue:0.965 alpha:1.00];
    self.navigationController.navigationBar.barTintColor=barColor;
}

- (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

- (UIBarButtonItem *)backBarButtonItem {
    
    UIBarButtonItem *returnItem;
    

    UIButton *bt = [UIButton buttonWithType:UIButtonTypeCustom];
    [bt setImage:[UIImage imageNamed:@"arrow-back.png"] forState:UIControlStateNormal];
    bt.frame = CGRectMake(-10, 0, 52, 52);
    [bt addTarget:self action:@selector(go_back:) forControlEvents:UIControlEventTouchUpInside];
    if ([self.navigationController.viewControllers count]==1)
    {
        UIView *vw = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 100, 52)];

        UIButton *btnMenu = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnMenu setImage:[UIImage imageNamed:@"MenuIcone.png"] forState:UIControlStateNormal];
        btnMenu.frame = CGRectMake(20, 0, 52, 52);
        btnMenu.showsTouchWhenHighlighted=YES;
        [btnMenu addTarget:self action:@selector(revealLeftPane:) forControlEvents:UIControlEventTouchUpInside];
        //if([self.isBack2 isEqualToString:@"1"])
        [vw addSubview:btnMenu];
        returnItem=[[UIBarButtonItem alloc] initWithCustomView:vw];
    }else
    {
        returnItem =[[UIBarButtonItem alloc] initWithCustomView:bt];;
    
    
    }
        
    return returnItem;

}
#pragma mark- selectbottomsliderrow

- (void)movieFinishedCallback:(NSNotification*)aNotification
{
    
    
    [self dismissMoviePlayerViewControllerAnimated];
    // [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark- SelectRow
-(IBAction)selectRow:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    NSString *storyBoardIdentifier=@"";
    
    NSString *userType = @"";
    NSString *logout;
    userType = [Helper getPREF:@"user_type"];
    if ([userType isEqualToString:@"10"] || [userType isEqualToString:@"12"]) {
        
        switch (btn.tag) {
            case 0:
                logout = @"";
                storyBoardIdentifier=@"SeguehomeVC";
                break;
            case 1:
                storyBoardIdentifier=@"TripPath";
                break;
            case 2:
                logout = @"";
                storyBoardIdentifier=@"ExportPath";
                NSLog(@"Selected %ld",(long)btn.tag);
                break;
            case 3:
                logout = @"";
                storyBoardIdentifier=@"LocationPath";
                NSLog(@"Selected %ld",(long)btn.tag);
                break;
            case 4:
                logout = @"";
                storyBoardIdentifier=@"settingPage";
                NSLog(@"Selected %ld",(long)btn.tag);
                break;
            case 5:
                logout = @"Logout";
                storyBoardIdentifier=@"InitialAppPath";
                break;
            default:
                break;
        }
        if([storyBoardIdentifier isEqualToString:@""])
        {
            [self HideLeftmenu:nil];
        }else{
            if([storyBoardIdentifier isEqualToString:@"nochange"]) {
                [self HideLeftmenu:nil];
            }else{
                if([logout isEqualToString:@"Logout"]){
                    /*UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Logout", @"") message:NSLocalizedString(@"Areyousureyouwanttologout", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"") otherButtonTitles:NSLocalizedString(@"OK", @""), nil];
                    alert.tag = 111;
                    [alert show];*/
                    
                    UIAlertController *alertview = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Logout", @"") message:NSLocalizedString(@"Areyousureyouwanttologout", @"") preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                        NSLog(@"OK selected");
                        [self callLogoutService];
                    }];
                    
                    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    
                    [alertview addAction:cancel];
                    [alertview addAction:ok];
                    [self presentViewController:alertview animated:true completion:nil];
                    
                }else{
                    [self HideLeftmenu:nil];
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    UIViewController *newViewController = [storyboard instantiateViewControllerWithIdentifier:storyBoardIdentifier];
                    [self.slidingViewController setTopViewController:newViewController];
                    [self.slidingViewController resetTopViewAnimated:YES];
                    [[NSUserDefaults standardUserDefaults] setObject:storyBoardIdentifier forKey:@"storyBoardIdentifier"];
                }
                [self HideLeftmenu:nil];
            }
        }
    }else{
        NSString *parent = [Helper isStringIsNull:[Helper getPREF:@"parent"]];
        switch (btn.tag) {
            case 0:
                logout = @"";
                storyBoardIdentifier=@"SeguehomeVC";
                break;
            case 1:
                storyBoardIdentifier=@"TripPath";
                break;
            case 2:
                logout = @"";
                NSLog(@"Selected %ld",(long)btn.tag);
                storyBoardIdentifier=@"ExpensesPath";
                break;
            case 3:
                logout = @"";
                storyBoardIdentifier=@"ExportPath";
                NSLog(@"Selected %ld",(long)btn.tag);
                break;
            case 4:
                logout = @"";
                storyBoardIdentifier=@"LocationPath";
                NSLog(@"Selected %ld",(long)btn.tag);
                break;
            case 5:
                logout = @"";
                if ([parent integerValue] > 0) {
                    storyBoardIdentifier=@"versionPage";
                }else{
                    storyBoardIdentifier=@"settingPage";
                }
                NSLog(@"Selected %ld",(long)btn.tag);
                break;
            case 6:
                logout = @"Logout";
                storyBoardIdentifier=@"InitialAppPath";
                break;
            case 7:
                logout = @"Logout";
                storyBoardIdentifier=@"InitialAppPath";
                break;
            default:
                break;
        }
        if([storyBoardIdentifier isEqualToString:@""]) {
            [self HideLeftmenu:nil];
        }else{
            if([storyBoardIdentifier isEqualToString:@"nochange"]) {
                [self HideLeftmenu:nil];
            }else{
                if([logout isEqualToString:@"Logout"]){
                    
                    /*UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Logout", @"") message:NSLocalizedString(@"Areyousureyouwanttologout", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"") otherButtonTitles:NSLocalizedString(@"OK", @""), nil];
                    alert.tag = 111;
                    [alert show];*/
                    
                    UIAlertController *alertview = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Logout", @"") message:NSLocalizedString(@"Areyousureyouwanttologout", @"") preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                        NSLog(@"OK selected");
                        [self callLogoutService];
                    }];
                    
                    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    
                    [alertview addAction:cancel];
                    [alertview addAction:ok];
                    [self presentViewController:alertview animated:true completion:nil];
                    
                }else{
                    [self HideLeftmenu:nil];
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    UIViewController *newViewController = [storyboard instantiateViewControllerWithIdentifier:storyBoardIdentifier];
                    [self.slidingViewController setTopViewController:newViewController];
                    [self.slidingViewController resetTopViewAnimated:YES];
                    [[NSUserDefaults standardUserDefaults] setObject:storyBoardIdentifier forKey:@"storyBoardIdentifier"];
                }
                [self HideLeftmenu:nil];
            }
        }
    }
}

-(void)setHomeScreen{
    NSString *storyBoardIdentifier = @"SeguehomeVC";
    //[self HideLeftmenu:nil];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *newViewController = [storyboard instantiateViewControllerWithIdentifier:storyBoardIdentifier];
    [self.slidingViewController setTopViewController:newViewController];
    [self.slidingViewController resetTopViewAnimated:YES];
    [[NSUserDefaults standardUserDefaults] setObject:storyBoardIdentifier forKey:@"storyBoardIdentifier"];
}

-(void)beaconServiceStop{
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    [app stopBeaconDetection];
}

- (BOOL)validateEmailWithString:(NSString*)email {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+(\\+[a-z0-9-]+)?@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
    
    /*BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];*/
    
}

-(void)removeUserData{
    
    NSLog(@"remove user data");
    [Helper delPREF:@"islogin"];
    
    [Helper delPREF:@"sid"];
    [Helper delPREF:@"userID"];
    [Helper delPREF:@"user_type"];
    [Helper delPREF:@"userName"];
    [Helper delPREF:@"userEmail"];
    [Helper delPREF:@"userStatus"];
    [Helper delPREF:@"userRegisterDate"];
    
    [Helper delPREF:@"userInfo"];
    
    [Helper delPREF:@"first_name"];
    [Helper delPREF:@"last_name"];
    [Helper delPREF:@"email"];
    [Helper delPREF:@"parent"];
    
    [Helper delPREF:@"tracker_id"];
    [Helper delPREF:@"general_setting"];
    
    [Helper delPREF:@"odometer_start"];
    
    [Helper delPREF:@"major"];
    [Helper delPREF:@"minor"];
    [Helper delPREF:@"isBeaconConnected"];
    [Helper delPREF:@"beaconsArray"];
    [Helper delPREF:@"tripcount"];
    [Helper delPREF:@"locationArray"];
    
    [Helper delPREF:@"appStartTime"];
    [Helper delPREF:@"appSTTime"];
    [Helper delPREF:@"appSTDate"];
    [Helper delPREF:@"appLocationDataArray"];
    [Helper delPREF:@"appStartLocation"];
    [Helper delPREF:@"appDistance"];
    [Helper delPREF:@"type"];
    
    [Helper delPREF:@"AppBackground"];
    [Helper delPREF:@"AppTerminate"];
    [Helper delPREF:@"isTripContinue"];
    /*NSString *epay_info = @"epay_info";
    NSString *dantracker = @"dantracker";
    [Helper delPREF:epay_info];
    [Helper delPREF:dantracker];*/
    
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    app.tracker_id = @"";
    [app clearMapData];
    
    self.navigationController.navigationBar.hidden = true;
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ViewController* controller = [storyboard instantiateViewControllerWithIdentifier:@"FirstPage"];
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)deleteAlarms
{
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *eventArray = [app scheduledLocalNotifications];
    NSMutableArray *saveALarm = [[NSMutableArray alloc]init];
    NSString *userid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"user_id"]];
    for (int i=0; i<eventArray.count; i++) {
         UILocalNotification* oneEvent = [eventArray objectAtIndex:i];
        NSDictionary *dicInfo = oneEvent.userInfo;
        NSString *alarmid = [dicInfo valueForKey:@"id"];
         NSString *isFor = [dicInfo valueForKey:@"isFor"];
        
        if([isFor isEqualToString:@"Moods"]) {
        }else if([isFor isEqualToString:@"Sidas"]){
        }else{
           NSArray *arr = [alarmid componentsSeparatedByString:@","];
            if([[arr objectAtIndex:1] isEqualToString:userid]){
                [saveALarm addObject:dicInfo];
            }
        }
        [app cancelLocalNotification:oneEvent];
    }
    [[NSUserDefaults standardUserDefaults] setObject:saveALarm forKey:[NSString stringWithFormat:@"UserAlarm%@",userid]];
}

#pragma mark- HideLeftmenu

-(IBAction)HideLeftmenu:(id)sender
{
    UIViewController *controller = [UIApplication sharedApplication].keyWindow.rootViewController;
    BOOL found = FALSE;
    UIScrollView *scrollview;
    UIView *view2;
    for (UIView *i in controller.view.subviews){
        
        if([i isKindOfClass:[UIScrollView class]]){
            UIScrollView *newLbl = (UIScrollView *)i;
            if(newLbl.tag == 4558){
                /// Write your code
                found = TRUE;
                scrollview = newLbl;
            }
        }
        
        if([i isKindOfClass:[UIView class]]){
            UIView *newLbl = (UIView *)i;
            if(newLbl.tag == 45588){
                /// Write your code
                found = TRUE;
                view2 = newLbl;
            }
        }
    }
    
    if(found) {
        [UIView animateWithDuration:0.25 animations:^{
            CGRect tableFrame = CGRectMake(-self.view.frame.size.width/1.2,0, self.view.frame.size.width/1.2, self.view.frame.size.height+200);
            view2.frame =tableFrame;
            scrollview.frame =tableFrame;
        } completion:^(BOOL finished){
            for (UIView *i in scrollview.subviews){
                if([i isKindOfClass:[UIView class]]){
                    UIView *newLbl = (UIView *)i;
                    [newLbl removeFromSuperview];
                }
            }
            [scrollview removeFromSuperview];
            [view2 removeFromSuperview];
            
        }];
    }
}

- (UIBarButtonItem *)backBarButtonItem2 {
    
    UIView *vw = [[UIView alloc]init];
    vw.frame =CGRectMake(0, 0, 100, 52);
    
    UIButton *bt = [UIButton buttonWithType:UIButtonTypeCustom];
    [bt setImage:[UIImage imageNamed:@"arrow-back.png"] forState:UIControlStateNormal];
    bt.frame = CGRectMake(-10, 0, 52, 52);
    [bt addTarget:self action:@selector(go_back:) forControlEvents:UIControlEventTouchUpInside];
    [vw addSubview:bt];
    
    /*UIButton *btnMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnMenu setImage:[UIImage imageNamed:@"icon_menu.png"] forState:UIControlStateNormal];
    btnMenu.frame = CGRectMake(30, 0, 52, 52);
    btnMenu.showsTouchWhenHighlighted=YES;
    [btnMenu addTarget:self action:@selector(revealLeftPane:) forControlEvents:UIControlEventTouchUpInside];
    //if([self.isBack2 isEqualToString:@"1"])
    [vw addSubview:btnMenu];*/
    
    UIBarButtonItem *barButtonItem1 = [[UIBarButtonItem alloc] initWithCustomView:vw];
    return barButtonItem1;
}

#pragma mark- ShowLeftmenu2

-(void)ShowLeftmenu2
{
    
    int x=0,y=0,k=0;
    UIView *vw = [[UIView alloc]init];
    vw.frame =CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    vw.tag = 45588;
    CGRect scrollViewFrame = CGRectMake(-self.view.frame.size.width/1.5, 0, self.view.frame.size.width/1.2, self.view.frame.size.height);
    UIScrollView*  scrollView = [[UIScrollView alloc] initWithFrame:scrollViewFrame];
    scrollView.backgroundColor = [UIColor whiteColor];
    
    NSArray* cellTitles = [[NSArray alloc]init];
    NSString *userType = @"",*parent = @"";
    
    NSArray*   cellImages =@[@"Symptons_menu.png",@"Strategies_menu.png",@"Contacts_menu.png",@"Hope_menu.png",@"mood_ratings.png",@" ",@"quick_menu.png",@"Sharelocation_menu.png",@"Nearroom_menu.png",@" ",@"Settings_menu.png",@"Logout_menu.png"];

    parent = [Helper isStringIsNull:[Helper getPREF:@"parent"]];
    userType = [Helper getPREF:@"user_type"];
    
    if ([userType isEqualToString:@"10"] || [userType isEqualToString:@"12"]) {

        cellTitles =@[ NSLocalizedString(@"Track", @""),NSLocalizedString(@"Trips", @""),NSLocalizedString(@"Export", @""),NSLocalizedString(@"Locations", @""),NSLocalizedString(@"Settings", @""),NSLocalizedString(@"Logout", @"")];
        
        for(int i=0;i<cellTitles.count+1;i++) {
            x=0;
            UIView *viewMenu = [[UIView alloc]init];
            [viewMenu setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"backgrnd.png"]]];
            viewMenu.frame = CGRectMake(x, y, scrollViewFrame.size.width, 60);
            viewMenu.backgroundColor = [UIColor whiteColor];
            viewMenu.layer.masksToBounds = YES;
            viewMenu.tag = 4568;
            
            if(i==0) {
                k=i;
            } else {
                k=i-1;
            }
            
            UILabel *lblLine = [[UILabel alloc]init];
            if(i == 1 ||k == 2 || k == 6 || k == 9 || k == 10 || k == 11)
                lblLine.frame = CGRectMake(0, 1, scrollViewFrame.size.width, 1);
            else{
                if(i != 0)
                    lblLine.frame = CGRectMake(20, 1,scrollViewFrame.size.width-20, 1);
            }
            lblLine.backgroundColor = [UIColor colorWithRed:0.910 green:0.906 blue:0.914 alpha:1.00];
            [viewMenu addSubview:lblLine];
            
            UILabel *lbl = [[UILabel alloc]init];
            
            lbl.tag = 87458;
            lbl.font = [UIFont systemFontOfSize:17];
            lbl.textColor = [UIColor colorWithRed:0.584 green:0.592 blue:0.604 alpha:1.00];
            if(i==0) {
                
                viewMenu.frame = CGRectMake(x, y, scrollViewFrame.size.width, 110);
                viewMenu.backgroundColor =[UIColor colorWithRed:1.000 green:1.000 blue:1.000 alpha:1.00];
                
                UIImageView *imgmenu = [[UIImageView alloc]initWithFrame:CGRectMake(15, 10, 25, 25)];
                imgmenu.image =  [UIImage imageNamed:@"img_menu.png"];
                
                
                UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                [button addTarget:self action:@selector(selectRow:) forControlEvents:UIControlEventTouchUpInside];
                
                button.frame = lbl.bounds;
                button.tag = 20;
                [viewMenu addSubview:button];
                
                UIButton *btnMenu = [UIButton buttonWithType:UIButtonTypeCustom];
                btnMenu.frame = CGRectMake(15, 25, 25, 25);//CGRectMake(scrollView.frame.size.width-40,0,50, 50);
                btnMenu.showsTouchWhenHighlighted=YES;
                [btnMenu addTarget:self action:@selector(HideLeftmenu:) forControlEvents:UIControlEventTouchUpInside];
                [viewMenu addSubview:imgmenu];
                [viewMenu addSubview:btnMenu];
                
                UILabel *lblhello = [[UILabel alloc]init];
                lblhello.frame = CGRectMake(15, imgmenu.frame.origin.y + imgmenu.frame.size.height + 5 , viewMenu.frame.size.width, 20);
                lblhello.font = [UIFont boldSystemFontOfSize:16];
                lbl.frame = CGRectMake(15, lblhello.frame.origin.y + lblhello.frame.size.height+2 , viewMenu.frame.size.width, 20);
                lbl.font = [UIFont systemFontOfSize:16];
                
                lbl.textColor = [UIColor blackColor];//[UIColor colorWithRed:0.082 green:0.620 blue:0.541 alpha:1.00];
                lblhello.textColor = [UIColor blackColor];
                lblhello.text =NSLocalizedString(@"Hello", @"");
                
                NSString *fname = [Helper getPREF:@"userName"] == nil?@"":[Helper getPREF:@"userName"];
                lbl.text = [NSString stringWithFormat:@"%@",[fname capitalizedString]];
                
                lbl.textAlignment = NSTextAlignmentLeft;
                lblhello.textAlignment = NSTextAlignmentLeft;
                [viewMenu addSubview:lblhello];
                
            }
            else if(i==cellTitles.count)
            {
                viewMenu.frame = CGRectMake(x, y, scrollViewFrame.size.width, 60);
                viewMenu.backgroundColor =[UIColor colorWithRed:1.000 green:1.000 blue:1.000 alpha:1.00];
                
                UILabel *lbl1 = [[UILabel alloc]init];
                lbl1.frame = CGRectMake(20, 12, viewMenu.frame.size.width-40, 45);
                lbl1.backgroundColor = [UIColor whiteColor];
                lbl1.layer.cornerRadius = 20;
                lbl1.textColor = [UIColor colorWithRed:0.502 green:0.824 blue:0.965 alpha:1.00];
                lbl1.layer.borderWidth = 1;
                lbl1.layer.borderColor = [UIColor colorWithRed:0.502 green:0.824 blue:0.965 alpha:1.00].CGColor;
                
                lbl1.clipsToBounds = YES;
                lbl1.textAlignment = NSTextAlignmentCenter;
                lbl1.text =[cellTitles objectAtIndex:k];
                lbl1.font = [UIFont boldSystemFontOfSize:15];
                [viewMenu addSubview:lbl1];
            } else{
                
                lbl.frame = CGRectMake(20, 18, 280, 30);
                lbl.text = [cellTitles objectAtIndex:k];
                lbl.textColor =[UIColor blackColor];
            }
            
            [viewMenu addSubview:lbl];
            
            UIImageView *imgarrow = [[UIImageView alloc]init];
            imgarrow.frame =  CGRectMake(scrollViewFrame.size.width-20,25, 15, 15);
            imgarrow.image = [[UIImage imageNamed:@"img_track_arrow.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [imgarrow setTintColor:[UIColor colorWithRed:0.749 green:0.745 blue:0.757 alpha:1.00]];

            if(i==0 || k == 5 || k == 6)
            {
                
            }else
                [viewMenu addSubview:imgarrow];
            
            if(i!=0){
                UIImageView *iconimg = [[UIImageView alloc]init];
                iconimg.frame = CGRectMake(10, 10, 35, 35);
                iconimg.image = [UIImage imageNamed:[cellImages objectAtIndex:k]];
                [viewMenu addSubview:iconimg];
                
                
                UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                button.frame = CGRectMake(10, 10, 35, 35);
                [button setFrame:CGRectMake(10, 10, 35, 35)];
                button.frame = viewMenu.bounds;
                //NSLog(@"k %d",k);
                
                if (i == 1) {
                    button.tag = 0;
                }else if (i == 2){
                    button.tag = 1;
                }else if (i == 3){
                    button.tag = 2;
                }else if (i == 4){
                    button.tag = 3;
                }else if (i == 5){
                    button.tag = 4;
                }else if (i == 6){
                    button.tag = 5;
                }else if (i == 7){
                    button.tag = 6;
                }else if (i == 8){
                    button.tag = 7;
                }else if (i == 9){
                    button.tag = 8;
                }else{
                    button.tag = 9;
                }
                [button addTarget:self action:@selector(selectRow:) forControlEvents:UIControlEventTouchUpInside];
                
                [viewMenu addSubview:button];
                
            }
            [scrollView addSubview:viewMenu];
            if(i==0) {
                y=y+110;
            }
            else
                y=y+60;
        }
    }
    else {

        if ([parent integerValue] > 0) {
            cellTitles =@[ NSLocalizedString(@"Track", @""),NSLocalizedString(@"Trips", @""),NSLocalizedString(@"Finances", @""),NSLocalizedString(@"Export", @""),NSLocalizedString(@"Locations", @""),NSLocalizedString(@"Version", @""),NSLocalizedString(@"Logout", @"")];
            
        }else{
            cellTitles =@[ NSLocalizedString(@"Track", @""),NSLocalizedString(@"Trips", @""),NSLocalizedString(@"Finances", @""),NSLocalizedString(@"Export", @""),NSLocalizedString(@"Locations", @""),NSLocalizedString(@"Settings", @""),NSLocalizedString(@"Logout", @"")];
            
        }
        
        for(int i=0;i<cellTitles.count+1;i++){
            x=0;
            
            UIView *viewMenu = [[UIView alloc]init];
            [viewMenu setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"backgrnd.png"]]];
            viewMenu.frame = CGRectMake(x, y, scrollViewFrame.size.width, 60);
            viewMenu.backgroundColor = [UIColor whiteColor];
            viewMenu.layer.masksToBounds = YES;
            viewMenu.tag = 4568;
            
            if(i==0)
            {
                k=i;
            }
            else
            {
                k=i-1;
            }
            
            
            
            UILabel *lblLine = [[UILabel alloc]init];
            if(i == 1 ||k == 2 || k == 6 || k == 9 || k == 10 || k == 11)
                lblLine.frame = CGRectMake(0, 1, scrollViewFrame.size.width, 1);
            else{
                if(i != 0)
                    lblLine.frame = CGRectMake(20, 1,scrollViewFrame.size.width-20, 1);
            }
            lblLine.backgroundColor = [UIColor colorWithRed:0.910 green:0.906 blue:0.914 alpha:1.00];
            [viewMenu addSubview:lblLine];
            
            UILabel *lbl = [[UILabel alloc]init];
            
            lbl.tag = 87458;
            lbl.font = [UIFont systemFontOfSize:17];
            lbl.textColor = [UIColor colorWithRed:0.584 green:0.592 blue:0.604 alpha:1.00];
            if(i==0) {
                
                viewMenu.frame = CGRectMake(x, y, scrollViewFrame.size.width, 110);
                viewMenu.backgroundColor =[UIColor colorWithRed:1.000 green:1.000 blue:1.000 alpha:1.00];
                
                UIImageView *imgmenu = [[UIImageView alloc]initWithFrame:CGRectMake(15, 10, 25, 25)];
                imgmenu.image =  [UIImage imageNamed:@"img_menu.png"];
                
                
                UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                [button addTarget:self action:@selector(selectRow:) forControlEvents:UIControlEventTouchUpInside];
                
                button.frame = lbl.bounds;
                button.tag = 20;
                [viewMenu addSubview:button];
                
                UIButton *btnMenu = [UIButton buttonWithType:UIButtonTypeCustom];
                //[btnMenu setImage:[UIImage imageNamed:@"icon_menu.png"] forState:UIControlStateNormal];
                //[btnMenu setImage:[UIImage imageNamed:@"MenuIcone.png"] forState:UIControlStateNormal];
                btnMenu.frame = CGRectMake(15, 25, 25, 25);//CGRectMake(scrollView.frame.size.width-40,0,50, 50);
                btnMenu.showsTouchWhenHighlighted=YES;
                [btnMenu addTarget:self action:@selector(HideLeftmenu:) forControlEvents:UIControlEventTouchUpInside];
                [viewMenu addSubview:imgmenu];
                [viewMenu addSubview:btnMenu];
                
                /* UIImageView *imguser = [[UIImageView alloc]initWithFrame:CGRectMake(viewMenu.frame.size.width/2-50, viewMenu.frame.size.height/2-50, 100, 100)];
                 imguser.image = [UIImage imageNamed:@"userimg.png"];
                 imguser.layer.cornerRadius = imguser.frame.size.width/2;
                 imguser.clipsToBounds = YES;
                 [viewMenu addSubview:imguser];*/
                
                UILabel *lblhello = [[UILabel alloc]init];
                lblhello.frame = CGRectMake(15, imgmenu.frame.origin.y + imgmenu.frame.size.height + 5 , viewMenu.frame.size.width, 20);
                lblhello.font = [UIFont boldSystemFontOfSize:16];
                lbl.frame = CGRectMake(15, lblhello.frame.origin.y + lblhello.frame.size.height+2 , viewMenu.frame.size.width, 20);
                lbl.font = [UIFont systemFontOfSize:16];
                
                lbl.textColor = [UIColor blackColor];//[UIColor colorWithRed:0.082 green:0.620 blue:0.541 alpha:1.00];
                lblhello.textColor = [UIColor blackColor];
                lblhello.text =NSLocalizedString(@"Hello", @"");
                
                NSString *fname = [Helper getPREF:@"userName"] == nil?@"":[Helper getPREF:@"userName"];
                lbl.text = [NSString stringWithFormat:@"%@",[fname capitalizedString]];
                
                lbl.textAlignment = NSTextAlignmentLeft;
                lblhello.textAlignment = NSTextAlignmentLeft;
                [viewMenu addSubview:lblhello];
                
            }
            else if(i==cellTitles.count) {

                viewMenu.frame = CGRectMake(x, y, scrollViewFrame.size.width, 60);
                viewMenu.backgroundColor =[UIColor colorWithRed:1.000 green:1.000 blue:1.000 alpha:1.00];
                
                UILabel *lbl1 = [[UILabel alloc]init];
                lbl1.frame = CGRectMake(20, 12, viewMenu.frame.size.width-40, 45);
                lbl1.backgroundColor = [UIColor whiteColor];
                lbl1.layer.cornerRadius = 20;
                lbl1.textColor = [UIColor colorWithRed:0.502 green:0.824 blue:0.965 alpha:1.00];
                lbl1.layer.borderWidth = 1;
                lbl1.layer.borderColor = [UIColor colorWithRed:0.502 green:0.824 blue:0.965 alpha:1.00].CGColor;
                
                lbl1.clipsToBounds = YES;
                lbl1.textAlignment = NSTextAlignmentCenter;
                lbl1.text =[cellTitles objectAtIndex:k];
                lbl1.font = [UIFont boldSystemFontOfSize:15];
                [viewMenu addSubview:lbl1];
            }else{
                lbl.frame = CGRectMake(20, 18, 280, 30);
                lbl.text = [cellTitles objectAtIndex:k];
                lbl.textColor =[UIColor blackColor];
            }
            
            [viewMenu addSubview:lbl];
            
            UIImageView *imgarrow = [[UIImageView alloc]init];
            
            imgarrow.frame =  CGRectMake(scrollViewFrame.size.width-20,25, 15, 15);
            
            imgarrow.image = [[UIImage imageNamed:@"img_track_arrow.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [imgarrow setTintColor:[UIColor colorWithRed:0.749 green:0.745 blue:0.757 alpha:1.00]];
            if(i==0 || k == 6|| k == 7 || k == 10)
            {
                
            }else
                [viewMenu addSubview:imgarrow];
            
            if(i!=0){
                UIImageView *iconimg = [[UIImageView alloc]init];
                iconimg.frame = CGRectMake(10, 10, 35, 35);
                iconimg.image = [UIImage imageNamed:[cellImages objectAtIndex:k]];
                [viewMenu addSubview:iconimg];
                
                UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                [button addTarget:self action:@selector(selectRow:) forControlEvents:UIControlEventTouchUpInside];
                
                button.frame = viewMenu.bounds;
                button.tag = k;
                [viewMenu addSubview:button];
                
            }
            [scrollView addSubview:viewMenu];
            if(i==0)
            {
                y=y+110;
            }
            else
                y=y+60;
        }
    }
    
    scrollView.contentSize = CGSizeMake(self.view.frame.size.width/1.5 , self.view.frame.size.height + 100);
    scrollView.scrollEnabled = YES;
    scrollView.layer.shadowOpacity = 0.8;
    scrollView.layer.shadowOffset = CGSizeMake(2, 0);
    scrollView.layer.shadowRadius = 20;
    scrollView.layer.masksToBounds = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.tag = 4558;
    [scrollView reloadInputViews];
    [scrollView didMoveToSuperview];
    
    UISwipeGestureRecognizer *LeftRight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(HideLeftmenu:)];
    LeftRight.delegate=self;
    [LeftRight setDirection:UISwipeGestureRecognizerDirectionLeft];
    [vw addGestureRecognizer:LeftRight];
    
    UIViewController *controller = [UIApplication sharedApplication].keyWindow.rootViewController;
    [controller.view insertSubview:vw  atIndex:199];
    [controller.view insertSubview:scrollView aboveSubview:vw];
    
    [UIView animateWithDuration:0.25 animations:^{
        CGRect tableFrame = CGRectMake(0,0, self.view.frame.size.width/1.2, self.view.frame.size.height);
        scrollView.frame =tableFrame;
    }];

}


/*-(void)ShowLeftmenu
{
    UIView *vw = [[UIView alloc]init];
    vw.frame =CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    vw.tag = 45588;
    CGRect tableFrame = CGRectMake(-self.view.frame.size.width/1.5, 20, self.view.frame.size.width/1.2, 900);
    UITableView* tableView = [[UITableView alloc] initWithFrame:tableFrame style:UITableViewStylePlain];
    //tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    tableView.delegate = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.hidden = FALSE;
    tableView.tag = 4558;
    tableView.backgroundColor = [UIColor whiteColor];
    tableView.layer.shadowOpacity = 0.5;
    tableView.layer.shadowOffset = CGSizeMake(1, 0);
    tableView.layer.shadowRadius = 15;
    tableView.layer.masksToBounds = NO;
    tableView.dataSource = self;
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, tableView.frame.size.width-50, 20)];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor colorWithRed:0.082 green:0.620 blue:0.541 alpha:1.00];
    label.font = [label.font fontWithSize:15];
    label.text = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"Username"]==nil?@"":[NSString stringWithFormat:@"%@!",[NSUserDefaults standardUserDefaults]]];
    label.textAlignment = NSTextAlignmentCenter;
    
    
    UIImageView *imgmenu = [[UIImageView alloc]initWithFrame:CGRectMake(tableView.frame.size.width-35, 10, 25, 15)];
    imgmenu.image =  [UIImage imageNamed:@"MenuIcone.png"];
    
    
    UIButton *btnMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    //[btnMenu setImage:[UIImage imageNamed:@"icon_menu.png"] forState:UIControlStateNormal];
    //[btnMenu setImage:[UIImage imageNamed:@"MenuIcone.png"] forState:UIControlStateNormal];
    btnMenu.frame = CGRectMake(tableView.frame.size.width-40,0,40, 40);
    btnMenu.showsTouchWhenHighlighted=YES;
    [btnMenu addTarget:self action:@selector(HideLeftmenu:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UISwipeGestureRecognizer *LeftRight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(HideLeftmenu:)];
    LeftRight.delegate=self;
    [LeftRight setDirection:UISwipeGestureRecognizerDirectionLeft];
     [vw addGestureRecognizer:LeftRight];
    
    // set the nav bar's right button item
    
    
    UIView *sectionHeader = [[UIView alloc] init];
    sectionHeader.frame = CGRectMake(0, 0, tableView.frame.size.width, 40);
    sectionHeader.backgroundColor =[UIColor colorWithRed:0.969 green:0.976 blue:0.973 alpha:1.00];
    [sectionHeader addSubview:label];
    [sectionHeader addSubview:imgmenu];
    [sectionHeader addSubview:btnMenu];
    
    [tableView setTableHeaderView:sectionHeader];
    [tableView beginUpdates];
    [tableView endUpdates];
    UIViewController *controller = [UIApplication sharedApplication].keyWindow.rootViewController;
    [controller.view insertSubview:vw  atIndex:199];
    [controller.view insertSubview:tableView aboveSubview:vw];
 
    
    [UIView animateWithDuration:0.25 animations:^{
        CGRect tableFrame = CGRectMake(0,20, self.view.frame.size.width/1.2, 900);
        tableView.frame =tableFrame;
    }];
}*/

-(IBAction)revealLeftPane2:(id)sender{
    
        if(! [[NSUserDefaults standardUserDefaults] valueForKey:@"is_menushow"])
            [self ShowLeftmenu2];
        else
            [self ShowLeftmenu2];
    
}

#pragma mark - data to dictionary
-(NSDictionary *)dataToDicitonary:(NSData *)data{
    NSError *localError = nil;
    NSDictionary *response = [[NSDictionary alloc]init];
    NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
    
    if (localError != nil) {
        NSLog(@"%@",localError.description);
        return  nil;
    } else {
        response = [parsedObject objectForKey:@"data"];
        return parsedObject;
    }
}

-(NSData *)dicitonaryToData:(NSDictionary *)dict{
    //NSLog(@"dict to data : %@",dict);
    NSError *localError = nil;
    NSData *dataFromDict = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&localError];
    return dataFromDict;
}

-(void)callLogoutService{
    [MBProgressHUD showHUDAddedTo:self.view animated:true];
    
    // stop detecting beacon
    [self beaconServiceStop];
    
    NSString *userId = [Helper getPREF:@"userID"];
    NSDictionary *parameter=@{@"user_id":userId};
    
    [WebService httpPostWithCustomDelegateURLString:@".logout" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (error == nil)        {
            NSError *localError = nil;
            NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
            
            if (localError != nil) {
                NSLog(@"%@",localError.description);
            }else{
                NSDictionary *response = [parsedObject objectForKey:@"data"];
                NSLog(@"response : %@",response);
                NSString *status = [response valueForKey:@"status"];
                if ([status isEqualToString:@"Success"]) {
                    NSLog(@"Logout successfully");
                    [self HideLeftmenu:nil];
                    [self removeUserData];
                    NSString *storyBoardIdentifier=@"InitialAppPath";
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    UIViewController *newViewController = [storyboard instantiateViewControllerWithIdentifier:storyBoardIdentifier];
                    [self.slidingViewController setTopViewController:newViewController];
                    [self.slidingViewController resetTopViewAnimated:YES];
                    [[NSUserDefaults standardUserDefaults] setObject:storyBoardIdentifier forKey:@"storyBoardIdentifier"];
                }
            }
        }
    }];
}
@end
