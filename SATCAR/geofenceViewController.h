//
//  geofenceViewController.h
//  SATCAR
//
//  Created by Percept Infotech on 03/04/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "CallOutAnnotationView.h"
#import "CustomMapOverlay.h"

@interface geofenceViewController : UIViewController<MKMapViewDelegate, CustomMapDelegate>
{
    MKCircle *circle;
    MKPointAnnotation *point;
}

@property (weak, nonatomic) IBOutlet MKMapView *mapview;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (nonatomic, strong) CallOutAnnotationView *calloutView;
@property (weak, nonatomic) IBOutlet UILabel *lblRadius;

@end
