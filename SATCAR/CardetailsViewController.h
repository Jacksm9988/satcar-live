//
//  CardetailsViewController.h
//  SATCAR
//
//  Created by Percept Infotech on 30/12/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardetailsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lblRegNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblFirstRegDate;
@property (weak, nonatomic) IBOutlet UILabel *lblColor;
@property (weak, nonatomic) IBOutlet UILabel *lblVIN;
@property (weak, nonatomic) IBOutlet UILabel *lblKM;
@property (weak, nonatomic) IBOutlet UILabel *lblLeasingCo;

@property (weak, nonatomic) IBOutlet UITextField *txtRegNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtFirstRegDate;
@property (weak, nonatomic) IBOutlet UITextField *txtColor;
@property (weak, nonatomic) IBOutlet UITextField *txtVIN;
@property (weak, nonatomic) IBOutlet UITextField *txtKM;
@property (weak, nonatomic) IBOutlet UITextField *txtLeasingCo;

@end
