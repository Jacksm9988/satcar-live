//
//  ChangePasswordViewController.h
//  SATCAR
//
//  Created by Percept Infotech on 03/01/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePasswordViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *txtOldPwd;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPwd;
@property (weak, nonatomic) IBOutlet UITextField *txtConfirmPwd;
@property (weak, nonatomic) IBOutlet UIButton *BtnResetPwd;
- (IBAction)onResetPwd:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *BtnBack;
- (IBAction)onBack:(id)sender;


@end
