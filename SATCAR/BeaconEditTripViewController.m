//
//  BeaconEditTripViewController.m
//  SATCAR
//
//  Created by Percept Infotech on 02/01/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import "BeaconEditTripViewController.h"
#import "AddressSearchViewController.h"
@interface BeaconEditTripViewController ()<UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource>
{
    int selectedSegment;
    NSString *selectedPurpose,*tripID;
    UIPickerView *purposePicker;
    UIDatePicker *datePicekr,*timePicker;
    NSArray *purposeArray;
    BOOL touched,isSave;
    AppDelegate *appdelegate;
    NSDictionary *userInfo;
    NSString *strMeasure,*strLocation;
}
@end

@implementation BeaconEditTripViewController
@synthesize fromScreen,tripDataDictionary;
- (void)viewDidLoad {
    [super viewDidLoad];
    [super viewDidLoad];
    selectedSegment = 1;
    appdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    purposeArray = [[NSArray alloc]initWithObjects: NSLocalizedString(@"Customervisit", @"") , NSLocalizedString(@"Servicevisit", @""),NSLocalizedString(@"Meetingwithcustomer", @""),NSLocalizedString(@"Companyconference", @""),NSLocalizedString(@"Companyevent", @"") ,NSLocalizedString(@"Carreparation", @""),NSLocalizedString(@"Deliveryofgoods", @"") ,NSLocalizedString(@"Pickupreturngoods", @""),NSLocalizedString(@"Deliveryforthecompany", @"")
                    , NSLocalizedString(@"Pickupforthecompany", @""),NSLocalizedString(@"Other_Purpose", @"") , nil];
    
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNotification:) name:@"ManualTripAddress"
                                               object:nil];
    
    [_txtDate addPreviousNextDoneOnKeyboardWithTarget:self previousAction:@selector(onDatePrevious) nextAction:@selector(onDateNext) doneAction:@selector(onDateDone)];
    [_txtSTime addPreviousNextDoneOnKeyboardWithTarget:self previousAction:@selector(onStimePrevious) nextAction:@selector(onStimeNext) doneAction:@selector(onStimeDone)];
    [_txtETime addPreviousNextDoneOnKeyboardWithTarget:self previousAction:@selector(onEtimePrevious) nextAction:@selector(onEtimeNext) doneAction:@selector(onEtimeDone)];
    [_txtPurpose addPreviousNextDoneOnKeyboardWithTarget:self previousAction:@selector(onPurposePrevious) nextAction:@selector(onPurposeNext) doneAction:@selector(onPurposeDone)];
    [_txtDistance addPreviousNextDoneOnKeyboardWithTarget:self previousAction:@selector(onDistancePrevious) nextAction:@selector(onDistanceNext) doneAction:@selector(onDistanceDone)];
    [_txtOStart addPreviousNextDoneOnKeyboardWithTarget:self previousAction:@selector(onOStartPrevious) nextAction:@selector(onOStartNext) doneAction:@selector(onOStartDone)];
    [_txtOEnd addPreviousNextDoneOnKeyboardWithTarget:self previousAction:@selector(onOEndPrevious) nextAction:@selector(onOEndNext) doneAction:@selector(onOEndDone)];
    touched = false;
}

-(void)addTitle{
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    
    UILabel *lbltitle = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-30, 10, 60, 30)];
    lbltitle.textColor = [UIColor colorWithRed:0.161 green:0.161 blue:0.169 alpha:1.00];
    lbltitle.text =NSLocalizedString(@"ManualTripEntry", @"");
    self.navigationItem.titleView=lbltitle;

    UIImageView *img = [[UIImageView alloc]init];
    img.image= [UIImage imageNamed:@"img_back.png"];
    img.frame = CGRectMake(0, 0, 30, 30);
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[img.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(backAction:)];
    [backButton  setTintColor:[UIColor blackColor]];
    self.navigationItem.leftBarButtonItem = backButton;
}

-(void)backAction:(id)sender{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"goToHomeScreen" object:nil userInfo:nil];
}

-(void)viewWillAppear:(BOOL)animated{
    
    if ([fromScreen isEqualToString:@"address"]) {
        NSLog(@"From address");
    }else{
        NSLog(@"From Trip Edit");
        [self.navigationController setNavigationBarHidden:NO animated:YES];
        [self addTitle];
        
        UIButton *btnAdd = [UIButton buttonWithType:UIButtonTypeCustom];
        btnAdd.frame = CGRectMake(20,0,50,32);
        [btnAdd setTitle:NSLocalizedString(@"Save", @"") forState:UIControlStateNormal];
        [btnAdd setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        btnAdd.titleLabel.font = [UIFont systemFontOfSize:16];
        [btnAdd addTarget:self action:@selector(onSave:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem* item = [[UIBarButtonItem alloc] initWithCustomView:btnAdd];
        self.navigationItem.rightBarButtonItem = item;
        tripID = @"";
        
        _ViewSegment.layer.cornerRadius = 25.0;
        _ViewSegment.clipsToBounds = YES;
        _ViewSegment.layer.borderWidth = 1;
        _ViewSegment.layer.borderColor = [Helper privateColor].CGColor;
        
        [_BtnPrivateSegment setTitle:NSLocalizedString(@"ExpPrivate", @"") forState:UIControlStateNormal];
        [_BtnBusinessSegment setTitle:NSLocalizedString(@"ExpBusiness", @"") forState:UIControlStateNormal];
        _scrollview.contentSize = CGSizeMake(self.view.frame.size.width, 900);
        
        _btnSave.layer.cornerRadius = 25;
        [_btnSave setTitle:NSLocalizedString(@"Save", @"") forState:UIControlStateNormal];
        [_btnSave setBackgroundColor:[Helper privateColor]];
        
        _txtPurpose.layer.cornerRadius = 5;
        _txtPurpose.layer.borderColor = [Helper BusinessColor].CGColor;
        _txtPurpose.textColor = [Helper BusinessColor];
        _txtPurpose.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Purpospe_of_trip", @"") attributes:@{NSForegroundColorAttributeName: _txtPurpose.textColor}];
        _txtPurpose.layer.borderWidth = 1;
        _IBLayoutConstraintPurposeTop.constant = 0;
        _IBLayoutConstraintPurposeHeight.constant = 0;
        _viewPurpose.hidden = true;
        _lblTripType.text = NSLocalizedString(@"TripType", @"");
        _lblTripDetails.text = NSLocalizedString(@"TripDetails", @"");
        
        _txtSAddress.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"StartAddress", @"")];
        _txtEAddress.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"EndAddress", @"")];
        _txtDate.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Date", @"")];
        _txtSTime.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"StartTime", @"")];
        _txtETime.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"EndTime", @"")];
        _txtOStart.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"OdometerStart", @"")];
        _txtOEnd.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"OdometerFinish", @"")];
        _txtDistance.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Distance", @"")];
        _txtNotes.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Notes", @"")];
        
        NSData *userdata = [Helper getDataValue:@"userInfo"];
        NSDictionary *dataDict = [self dataToDicitonary:userdata];
        if (![dataDict isKindOfClass:[NSNull class]]) {
            userInfo = [dataDict valueForKey:@"data"];
            if (userInfo == nil) {
                // [Helper displayAlertView:@"" message:@"Data not available"];
            }
            
            NSDictionary *generalSetting = [userInfo valueForKey:@"general_setting"];
            //NSLog(@"DATA : %@",userInfo);
            strMeasure = [generalSetting valueForKey:@"unit"];
            
            if ([strMeasure.lowercaseString isEqualToString:@"miles"]) {
                strMeasure = @"mi";
            }
            _lblDistance.text =  strMeasure;
            selectedSegment = 0;
            [self onDefaultSelected];
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [self callTripService];
            });
        }else{
            [Helper displayAlertView:@"" message:NSLocalizedString(@"something_went_wrong_msg", @"")];
        }
        
        
    }
}

-(void)viewWillDisappear:(BOOL)animated{
}

- (void)receiveNotification:(NSNotification *)notification //use notification method and logic
{
    NSString *key = @"AddressValue";
    NSDictionary *dictionary = [notification userInfo];
    NSString *stringValueToUse = [dictionary valueForKey:key];
    NSString *fromFiled = [dictionary valueForKey:@"fromScreenField"];
    if ([fromFiled isEqualToString:@"SA"]) {
        
        _txtSAddress.text = stringValueToUse;
        if (_txtSAddress.text.length > 0 && _txtEAddress.text.length > 0) {
            [self getKMfromService:_txtSAddress.text end:_txtEAddress.text];
        }
    }else{
        _txtEAddress.text = stringValueToUse;
        if (_txtSAddress.text.length > 0 && _txtEAddress.text.length > 0) {
            [self getKMfromService:_txtSAddress.text end:_txtEAddress.text];
        }
    }
    NSLog(@"Address --> %@",stringValueToUse);
    fromScreen = [dictionary valueForKey:@"screen"];
}

-(void)enableEditMode{
    _txtSAddress.enabled = true;
    _txtEAddress.enabled = true;
    _txtDate.enabled = true;
    _txtSTime.enabled = true;
    _txtETime.enabled = true;
    _txtOStart.enabled = true;
    _txtOEnd.enabled = true;
    _txtDistance.enabled = true;
    _txtNotes.enabled = true;
}
-(void)disableEditMode{
    _txtSAddress.enabled = false;
    _txtEAddress.enabled = false;
    _txtDate.enabled = false;
    _txtSTime.enabled = false;
    _txtETime.enabled = false;
    _txtOStart.enabled = false;
    _txtOEnd.enabled = false;
    _txtDistance.enabled = false;
    _txtNotes.enabled = false;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - textfield delegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    textField.textColor =  [UIColor colorWithRed:18.0/255.0 green:172.0/255.0 blue:243.0/255.0 alpha:1.0];
    textField.layer.borderWidth = 1;
    textField.layer.borderColor =  [UIColor colorWithRed:18.0/255.0 green:172.0/255.0 blue:243.0/255.0 alpha:1.0].CGColor;
    textField.clipsToBounds = YES;
    return true;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    textField.textColor =  [UIColor blackColor];
    textField.layer.borderWidth = 0;
    textField.layer.borderColor = [UIColor clearColor].CGColor;
    textField.clipsToBounds = YES;
    return true;
}


-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if (textField == _txtDate) {
        
        datePicekr = [[UIDatePicker alloc] initWithFrame:CGRectZero];
        [datePicekr setDatePickerMode:UIDatePickerModeDate];
        [datePicekr addTarget:self action:@selector(onDatePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
        datePicekr.maximumDate = [NSDate date];
        _txtDate.inputView = datePicekr;
        NSDateFormatter __autoreleasing *dateFormat = [[NSDateFormatter alloc] init];
        if (![_txtDate hasText]) {
            [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
            [dateFormat setDateFormat:@"dd/MM/YYYY"];
            NSString *dateString =  [dateFormat stringFromDate:datePicekr.date];
            _txtDate.text = dateString;
        }
    }else if(textField == _txtPurpose) {
        purposePicker = [[UIPickerView alloc] initWithFrame:CGRectZero];
        [purposePicker setDataSource: self];
        [purposePicker setDelegate: self];
        purposePicker.showsSelectionIndicator = YES;
        _txtPurpose.inputView = purposePicker;
        if (![_txtPurpose hasText]) {
            _txtPurpose.text = [purposeArray objectAtIndex:0];
        }
    }else if(textField == _txtSTime) {
        
        if (_txtDate.text.length <= 0 ) {
            [Helper displayAlertView:@"" message:@"Please select date first!"];
        }else{
            timePicker = [[UIDatePicker alloc] initWithFrame:CGRectZero];
            [timePicker setDatePickerMode:UIDatePickerModeTime];
            [timePicker setLocale:[NSLocale systemLocale]];
            [timePicker addTarget:self action:@selector(onStartTimeDatePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
            _txtSTime.inputView = timePicker;
            NSDateFormatter __autoreleasing *dateFormat = [[NSDateFormatter alloc] init];
            if (![_txtSTime hasText]) {
                [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
                [dateFormat setDateFormat:@"HH:mm"];
                NSString *dateString =  [dateFormat stringFromDate:timePicker.date];
                _txtSTime.text = dateString;
            }else{
                NSString *strdate = [NSString stringWithFormat:@"%@ %@",_txtDate.text,_txtSTime.text];
                [dateFormat setDateFormat:@"dd/MM/YYYY HH:mm"];
                NSDate *date =  [dateFormat dateFromString:strdate];
                [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
                [dateFormat setDateFormat:@"HH:mm"];
                [timePicker setDate:date];
                [dateFormat setDateFormat:@"HH:mm"];
                NSString *dateString =  [dateFormat stringFromDate:timePicker.date];
                _txtSTime.text = dateString;
            }
        }
        
    }else if(textField == _txtETime) {
        
        timePicker = [[UIDatePicker alloc] initWithFrame:CGRectZero];
        [timePicker setDatePickerMode:UIDatePickerModeTime];
        [timePicker addTarget:self action:@selector(onEndTimeDatePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
        [timePicker setLocale:[NSLocale systemLocale]];
        _txtETime.inputView = timePicker;
        NSDateFormatter __autoreleasing *dateFormat = [[NSDateFormatter alloc] init];
        if (![_txtETime hasText]) {
            [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
            [dateFormat setDateFormat:@"HH:mm"];
            NSString *dateString =  [dateFormat stringFromDate:timePicker.date];
            _txtETime.text = dateString;
        }else{
            if (_txtDate.text.length <= 0 ) {
                [Helper displayAlertView:@"" message:@"Please select date first!"];
            }else{
                NSString *strdate = [NSString stringWithFormat:@"%@ %@",_txtDate.text,_txtETime.text];
                [dateFormat setDateFormat:@"dd/MM/YYYY HH:mm"];
                NSDate *date =  [dateFormat dateFromString:strdate];
                [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
                [dateFormat setDateFormat:@"HH:mm"];
                [timePicker setDate:date];
                [dateFormat setDateFormat:@"HH:mm"];
                NSString *dateString =  [dateFormat stringFromDate:timePicker.date];
                _txtETime.text = dateString;
            }
        }
    }else if (textField == _txtSAddress){
        [self.view endEditing:true];
        [self openAddressView:@"SA"];
    }else if (textField == _txtEAddress){
        [self.view endEditing:true];
        [self openAddressView:@"EA"];
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField == _txtDistance || textField == _txtOStart || textField == _txtOEnd) {
        _txtOEnd.text = [self calculateOdometer:_txtOStart.text dist:_txtDistance.text];
    }
    /*else if(textField == _txtSAddress || textField == _txtEAddress){
     if (_txtSAddress.text.length > 0 && _txtEAddress.text.length > 0) {
     [self getKMfromService:_txtSAddress.text end:_txtEAddress.text];
     }
     }*/
}

-(void)displayKMorMiles:(float)km found:(BOOL)result{
    NSLog(@"km : %f",km);
    dispatch_async(dispatch_get_main_queue(), ^{
        if (result) {
            NSString *odometer_start = _txtOStart.text;
            float oST = [odometer_start floatValue];
            _txtOStart.text = [NSString stringWithFormat:@"%.2f",oST];
            _txtDistance.text =  [NSString stringWithFormat:@"%.2f",km];
            oST = oST + km;
            _txtOEnd.text = [NSString stringWithFormat:@"%.2f",oST];
        }else{
            _txtDistance.text =  [NSString stringWithFormat:@"%.2f",km];
            _txtOEnd.text = [NSString stringWithFormat:@"%.2f",km];
            [Helper displayAlertView:@"" message:NSLocalizedString(@"route_not_found", @"")];
        }
        
    });
}

-(void)calculateMilesToKM:(float)mile found:(BOOL)result{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (result) {
            NSString *odometer_start = _txtOStart.text;
            float oST = [odometer_start floatValue];
            _txtOStart.text = [NSString stringWithFormat:@"%.2f",oST];
            _txtDistance.text = [NSString stringWithFormat:@"%.2f",mile];
            
            float km = mile * 1.60934;
            oST = oST + km;
            _txtOEnd.text = [NSString stringWithFormat:@"%.2f",oST];
            NSLog(@"km : %.2f mi : %.2f",km,mile);
        }else{
            _txtDistance.text =  [NSString stringWithFormat:@"%.2f",mile];
            _txtOEnd.text = [NSString stringWithFormat:@"%.2f",mile];
            [Helper displayAlertView:@"" message:NSLocalizedString(@"route_not_found", @"")];
        }
        
    });
}



-(void)calculateKMToMiles:(float)km{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *odometer_start = _txtOStart.text;
        float oST = [odometer_start floatValue];
        _txtOStart.text = [NSString stringWithFormat:@"%.2f",oST];
        oST = oST + km;
        _txtOEnd.text = [NSString stringWithFormat:@"%.2f",oST];
        
        float miles = km * 0.621371;
        NSLog(@"km : %.2f mi : %.2f",km,miles);
        _txtDistance.text = [NSString stringWithFormat:@"%.2f",miles];
    });
}

-(void)getKMfromService:(NSString *)startAddress end:(NSString *)endAddress{
    [MBProgressHUD showHUDAddedTo:self.view animated:true];
    NSString *directionsAPI = @"https://maps.googleapis.com/maps/api/directions/json?";
    NSString *directionsUrlString = [NSString stringWithFormat:@"%@&origin=%@&destination=%@&mode=driving&key=%@", directionsAPI,startAddress,endAddress,appdelegate.googleApiKey];
    NSString *encodedSearchString = [directionsUrlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *directionsUrl = [NSURL URLWithString:encodedSearchString];
    NSLog(@"STR : %@",directionsUrl);
    NSURLSessionDataTask *fetchDirectionsTask = [[NSURLSession sharedSession] dataTaskWithURL:directionsUrl completionHandler:                                                 ^(NSData *data, NSURLResponse *response, NSError *error)
                                                 {
                                                     [MBProgressHUD hideHUDForView:self.view animated:true];
                                                     NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                                     if(error != nil) {
                                                         [MBProgressHUD hideHUDForView:self.view animated:true];
                                                         
                                                     }else{
                                                         NSLog(@"json : %@",json);
                                                         NSArray *routesArray = [json objectForKey:@"routes"];
                                                         if ([routesArray count] > 0) {
                                                             NSDictionary *routeDict = [routesArray objectAtIndex:0];
                                                             NSLog(@"route : %@",routeDict);
                                                             
                                                             NSArray *legs = [routeDict valueForKey:@"legs"];
                                                             NSDictionary *legDict = [legs objectAtIndex:0];
                                                             NSDictionary *distance = [legDict valueForKey:@"distance"];
                                                             NSString *dist = [distance valueForKey:@"text"];
                                                             
                                                             NSRange replaceRange = [dist.lowercaseString rangeOfString:@"km"];
                                                             NSString* result;
                                                             if (replaceRange.location != NSNotFound){
                                                                 result = [dist stringByReplacingCharactersInRange:replaceRange withString:@""];
                                                                 float km = [result floatValue];
                                                                 if ([strMeasure.lowercaseString isEqualToString:@"km"]) {
                                                                     [self displayKMorMiles:km found:true];
                                                                 }else{
                                                                     [self calculateMilesToKM:km found:true];
                                                                 }
                                                             }
                                                             NSRange replaceRangeM = [dist.lowercaseString rangeOfString:@"mi"];
                                                             if (replaceRangeM.location != NSNotFound){
                                                                 result = [dist stringByReplacingCharactersInRange:replaceRangeM withString:@""];
                                                                 float km = [result floatValue];
                                                                 if ([strMeasure.lowercaseString isEqualToString:@"km"]) {
                                                                     [self displayKMorMiles:km found:true];
                                                                 }else{
                                                                     [self calculateMilesToKM:km found:true];
                                                                 }
                                                             }else{
                                                                 float km = [result floatValue];
                                                                 if ([strMeasure.lowercaseString isEqualToString:@"km"]) {
                                                                     [self displayKMorMiles:km found:true];
                                                                 }else{
                                                                     [self calculateMilesToKM:km found:true];
                                                                 }
                                                             }
                                                             dispatch_async(dispatch_get_main_queue(), ^{
                                                                 [MBProgressHUD hideHUDForView:self.view animated:true];
                                                             });
                                                         }else{
                                                             float km = 0.0;
                                                             if ([strMeasure.lowercaseString isEqualToString:@"km"]) {
                                                                 [self displayKMorMiles:km found:true];
                                                             }else{
                                                                 [self calculateMilesToKM:km found:true];
                                                             }
                                                         }
                                                     }
                                                 }];
    [fetchDirectionsTask resume];
}

#pragma mark - Picker View Data source
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (pickerView == purposePicker) {
        return  purposeArray.count;
    }
    return 0;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component{
    if (pickerView == purposePicker) {
        selectedPurpose = [purposeArray objectAtIndex:row];
        [_txtPurpose setText:selectedPurpose];
    }
    touched = true;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow: (NSInteger)row forComponent:(NSInteger)component{
    if (pickerView == purposePicker) {
        return [NSString stringWithFormat:@"%@",[purposeArray objectAtIndex:row]];
    }else{
        return 0;
    }
}

#pragma mark Keyboard delegate
-(void)onDatePrevious{
    [_txtEAddress becomeFirstResponder];
}
-(void)onDateNext{
    [_txtSTime becomeFirstResponder];
}
-(void)onDateDone{
    if (touched) {
        touched = false;
    }else{
        NSDateFormatter __autoreleasing *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
        [dateFormat setDateFormat:@"dd/MM/YYYY"];
        NSString *dateString =  [dateFormat stringFromDate:datePicekr.date];
        _txtDate.text = dateString;
    }
    [_txtDate resignFirstResponder];
}
-(void)onStimePrevious{
    [_txtDate becomeFirstResponder];
}
-(void)onStimeNext{
    [_txtETime becomeFirstResponder];
}
-(void)onStimeDone{
    if (touched) {
        touched = false;
    }else{
        NSDateFormatter __autoreleasing *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
        [dateFormat setDateFormat:@"HH:mm"];
        NSString *dateString =  [dateFormat stringFromDate:timePicker.date];
        _txtSTime.text = dateString;
        
    }
    [_txtSTime resignFirstResponder];
}
-(void)onEtimePrevious{
    [_txtSTime becomeFirstResponder];
}
-(void)onEtimeNext{
    [_txtOStart becomeFirstResponder];
}
-(void)onEtimeDone{
    if (touched) {
        touched = false;
    }else{
        NSDateFormatter __autoreleasing *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
        
        [dateFormat setDateFormat:@"HH:mm"];
        NSString *dateString =  [dateFormat stringFromDate:timePicker.date];
        _txtETime.text = dateString;
    }
    [_txtETime resignFirstResponder];
}
-(void)onPurposePrevious{
    [_txtDistance becomeFirstResponder];
}
-(void)onPurposeNext{
    [_txtNotes becomeFirstResponder];
}
-(void)onPurposeDone{
    if (touched) {
        touched = false;
    }else{
        _txtPurpose.text = [purposeArray objectAtIndex:0];
    }
    [_txtPurpose resignFirstResponder];
}
-(void)onDistancePrevious{
    _txtOEnd.text = [self calculateOdometer:_txtOStart.text dist:_txtDistance.text];
    [_txtOEnd becomeFirstResponder];
}
-(void)onDistanceNext{
    _txtOEnd.text = [self calculateOdometer:_txtOStart.text dist:_txtDistance.text];
    [_txtPurpose becomeFirstResponder];
}
-(void)onDistanceDone{
    if (touched) {
        touched = false;
    }else{
        _txtOEnd.text = [self calculateOdometer:_txtOStart.text dist:_txtDistance.text];
    }
    [_txtDistance resignFirstResponder];
}

// odometer start delegate
-(void)onOStartPrevious{
    _txtOEnd.text = [self calculateOdometer:_txtOStart.text dist:_txtDistance.text];
    [_txtETime becomeFirstResponder];
}
-(void)onOStartNext{
    _txtOEnd.text = [self calculateOdometer:_txtOStart.text dist:_txtDistance.text];
    [_txtOEnd becomeFirstResponder];
}
-(void)onOStartDone{
    if (touched) {
        touched = false;
    }else{
        _txtOEnd.text = [self calculateOdometer:_txtOStart.text dist:_txtDistance.text];
    }
    [_txtOStart resignFirstResponder];
}

// odometer end delegate
-(void)onOEndPrevious{
    _txtOEnd.text = [self calculateOdometer:_txtOStart.text dist:_txtDistance.text];
    [_txtOStart becomeFirstResponder];
}
-(void)onOEndNext{
    _txtOEnd.text = [self calculateOdometer:_txtOStart.text dist:_txtDistance.text];
    [_txtDistance becomeFirstResponder];
}
-(void)onOEndDone{
    
    if (touched) {
        touched = false;
    }else{
        _txtOEnd.text = [self calculateOdometer:_txtOStart.text dist:_txtDistance.text];
    }
    [_txtOEnd resignFirstResponder];
}


-(NSString *)calculateOdometer:(NSString *)start dist:(NSString *)distance{
    
    if (start.length != 0 && distance.length != 0) {
        NSString *kmStr = distance;
        NSDecimalNumber *price1 = [NSDecimalNumber decimalNumberWithString:start];
        NSDecimalNumber *price2 = [NSDecimalNumber decimalNumberWithString:kmStr];
        NSDecimalNumber *subtotal = [price1 decimalNumberByAdding:price2];
        NSString *total = [NSString stringWithFormat:@"%@",subtotal];
        return total;
    }else{
        return @"";
    }
}


#pragma mark - Button action
- (IBAction)onSave:(id)sender {
    if ([self isValidData]) {
        //NSString *sId = [Helper getPREF:@"sid"];
        NSString *userID = [Helper getPREF:@"userID"];
        NSDictionary *parameter;
        
        if (strLocation == nil || strLocation.length <= 0) {
            strLocation = @"";
        }
        if (selectedSegment == 0) {
            [Helper displayAlertView:@"" message:NSLocalizedString(@"error_select_Trip_Type", @"")];
            return;
        }if (selectedSegment == 1) {
            parameter=@{ @"userid":userID,@"id":tripID, @"trip_type":[NSString stringWithFormat:@"%d",selectedSegment], @"start_address":_txtSAddress.text, @"end_address":_txtEAddress.text, @"trip_date":_txtDate.text, @"start_time":_txtSTime.text, @"end_time":_txtETime.text,@"odometer_start":_txtOStart.text, @"odometer_finish":_txtOEnd.text, @"distance":_txtDistance.text, @"note":_txtNotes.text, @"locations":strLocation, @"locations1":strLocation, @"version":appdelegate.appVersion, @"os":@"ios", @"build":appdelegate.appBuild };
        }else{
            parameter=@{ @"userid":userID,@"id":tripID, @"trip_type":[NSString stringWithFormat:@"%d",selectedSegment], @"start_address":_txtSAddress.text, @"end_address":_txtEAddress.text, @"trip_date":_txtDate.text, @"start_time":_txtSTime.text, @"end_time":_txtETime.text,@"odometer_start":_txtOStart.text, @"odometer_finish":_txtOEnd.text, @"distance":_txtDistance.text, @"note":_txtNotes.text, @"purpose_trip":_txtPurpose.text , @"locations":strLocation, @"locations1":strLocation, @"version":appdelegate.appVersion, @"os":@"ios", @"build":appdelegate.appBuild};
        }
        [self callAddTripService:parameter];
    }
}

#pragma mark - Segment action
- (IBAction)SelctSegment:(id)sender{
    
    if ([sender tag] == 1) {
        [self onPrivateSelected];
    } else {
        [self onBusinessSelected];
    }
}

-(void)onDefaultSelected{
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        selectedSegment = 0;
        [_BtnPrivateSegment setBackgroundColor:[UIColor clearColor]];
        [_BtnPrivateSegment setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        _BtnPrivateSegment.layer.borderWidth = 1;
        _BtnPrivateSegment.layer.borderColor = [Helper defaultColor].CGColor;
        
        _ViewSegment.layer.borderWidth = 1;
        _ViewSegment.layer.borderColor = [Helper defaultColor].CGColor;
        
        [_BtnBusinessSegment setBackgroundColor:[UIColor clearColor]];
        _BtnBusinessSegment.layer.borderColor = [Helper defaultColor].CGColor;
        _BtnBusinessSegment.layer.borderWidth = 1;
        [_BtnBusinessSegment setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
        _IBLayoutConstraintPurposeTop.constant = 0;
        _IBLayoutConstraintPurposeHeight.constant = 0;
        _viewPurpose.hidden = true;
        [_btnSave setBackgroundColor:[Helper defaultColor]];
    }];
    
}


-(void)onPrivateSelected{
    selectedSegment = 1;
    [_BtnPrivateSegment setBackgroundColor:[Helper privateColor]];
    [_BtnPrivateSegment setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _BtnPrivateSegment.layer.borderWidth = 1;
    _BtnPrivateSegment.layer.borderColor = [Helper privateColor].CGColor;
    
    _ViewSegment.layer.borderWidth = 1;
    _ViewSegment.layer.borderColor = [Helper privateColor].CGColor;
    
    [_BtnBusinessSegment setBackgroundColor:[UIColor clearColor]];
    _BtnBusinessSegment.layer.borderColor = [Helper privateColor].CGColor;
    _BtnBusinessSegment.layer.borderWidth = 1;
    [_BtnBusinessSegment setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    
    _IBLayoutConstraintPurposeTop.constant = 0;
    _IBLayoutConstraintPurposeHeight.constant = 0;
    _viewPurpose.hidden = true;
    [_btnSave setBackgroundColor:[Helper privateColor]];
    
}

-(void)onBusinessSelected{
    selectedSegment = 2;
    
    [_BtnBusinessSegment setBackgroundColor:[Helper BusinessColor]];
    [_BtnBusinessSegment setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _BtnBusinessSegment.tintColor = [UIColor whiteColor];
    [_BtnBusinessSegment.titleLabel setTextColor:[UIColor whiteColor]];
    _BtnBusinessSegment.layer.borderWidth = 1;
    _BtnBusinessSegment.layer.borderColor = [Helper BusinessColor].CGColor;
    
    _ViewSegment.layer.borderWidth = 1;
    _ViewSegment.layer.borderColor = [Helper BusinessColor].CGColor;
    
    [_BtnPrivateSegment setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [_BtnPrivateSegment setBackgroundColor:[UIColor clearColor]];
    _BtnPrivateSegment.layer.borderColor = [Helper BusinessColor].CGColor;
    _BtnPrivateSegment.layer.borderWidth = 1;
    
    _IBLayoutConstraintPurposeTop.constant = 10;
    _IBLayoutConstraintPurposeHeight.constant = 40;
    _viewPurpose.hidden = false;
    [_btnSave setBackgroundColor:[Helper BusinessColor]];
    
}

#pragma mark - date picker
- (void)onDatePickerValueChanged:(UIDatePicker *)datePicker
{
    NSDateFormatter __autoreleasing *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormat setDateFormat:@"dd/MM/YYYY"];
    NSString *dateString =  [dateFormat stringFromDate:datePicker.date];
    _txtDate.text = dateString;
    touched = true;
}

- (void)onStartTimeDatePickerValueChanged:(UIDatePicker *)datePicker
{
    NSDateFormatter __autoreleasing *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormat setDateFormat:@"HH:mm"];
    NSString *dateString =  [dateFormat stringFromDate:timePicker.date];
    _txtSTime.text = dateString;
    touched = true;
}

- (void)onEndTimeDatePickerValueChanged:(UIDatePicker *)datePicker
{
    NSDateFormatter __autoreleasing *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormat setDateFormat:@"HH:mm"];
    NSString *dateString =  [dateFormat stringFromDate:timePicker.date];
    _txtETime.text = dateString;
    touched = true;
}

-(BOOL)isValidData{
    if (_txtSAddress.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_start_address",nil)];
        return false;
        
    }else if (_txtEAddress.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_end_address",nil)];
        return false;
        
    }else if (_txtDate.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_date",nil)];
        return false;
        
    }else if (_txtSTime.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Password_enter_start_time",nil)];
        return false;
        
    }else if (_txtETime.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Password_enter_end_time",nil)];
        return false;
        
    }else if (_txtOStart.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_odometer_start_km",nil)];
        return false;
        
    }else if (_txtOEnd.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_odometer_end_km",nil)];
        return false;
        
    }else if (_txtDistance.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_distance",nil)];
        return false;
        
    }
    //    else if (_txtNotes.text.length == 0) {
    //        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_notes",nil)];
    //        return false;
    //    }
    
    if (selectedSegment == 2) {
        if (_txtPurpose.text.length <= 0) {
            [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_business_purpose",nil)];
            return false;
        }
    }
    
    return true;
}

#pragma mark - webservice call
-(void)callTripService{
    
    if(![self CheckNetworkConnection]) {
        return;
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *userID = [Helper getPREF:@"userID"];
    NSString *user_type = [Helper getPREF:@"user_type"];
    NSString *tripDate = [self getMonthYearDay:[NSDate date]];
    
    //NSString *sid = [Helper getPREF:@"sid"];
    NSDictionary *parameter=@{
                              @"userid":userID,
                              @"trip_type":@"0",
                              @"t_date":tripDate,
                              @"limitstart":@"0"
                              };
    //NSLog(@"parameter : %@",parameter);
    
    [WebService httpPostWithCustomDelegateURLString:@".gettrips" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         if (error == nil) {
             NSError *localError = nil;
             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
             
             if (localError != nil) {
                 NSLog(@"%@",localError.description);
             }
             else {
                 //NSDictionary *response = [parsedObject objectForKey:@"data"];
                 NSLog(@"response : %@",parsedObject);
                 id response = [parsedObject objectForKey:@"data"];
                 
                 if ([self CheckSession:response]) {
                     NSMutableArray *tempTrips = [[NSMutableArray alloc]init];
                     tempTrips  = [response objectForKey:@"trips"];
                     NSLog(@"Trip Count : %d",(int)tempTrips.count);
                     
                     for (int i = 0 ; i < tempTrips.count; i++) {
                         NSDictionary *trip = [tempTrips objectAtIndex:i];
                         NSString *trip_type = [trip valueForKey:@"trip_type"];
                         if ([trip_type isEqualToString:@"0"]) {
                             tripDataDictionary = [trip mutableCopy];
                             break;
                         }
                     }
                     NSLog(@"Data : %@",tripDataDictionary);
                     if (tripDataDictionary != nil && tripDataDictionary.count != 0) {
                         tripID = [tripDataDictionary valueForKey:@"id"];
                         selectedSegment = [[tripDataDictionary valueForKey:@"trip_type"] intValue];
                         strLocation = [tripDataDictionary valueForKey:@"locations"];
                         if (selectedSegment == 0) {
                             [self onDefaultSelected];
                         }else if (selectedSegment == 1) {
                             [self onPrivateSelected];
                         }else{
                             [self onBusinessSelected];
                             _txtPurpose.text = [tripDataDictionary valueForKey:@"purpose_trip"];
                         }
                         _txtSAddress.text = [tripDataDictionary valueForKey:@"start_address"];
                         _txtEAddress.text = [tripDataDictionary valueForKey:@"end_address"];
                         
                         NSString *date = [tripDataDictionary valueForKey:@"trip_date"];
                         NSString *strdate;
                         if (date != nil) {
                             strdate = [Helper getDateFromString:date];
                             _txtDate.text = strdate;
                         }else{
                             _txtDate.text = [tripDataDictionary valueForKey:@"trip_date"];
                         }
                         _txtSTime.text = [self getCellDisplayTimeFormat:[tripDataDictionary valueForKey:@"start_time"]];
                         _txtETime.text = [self getCellDisplayTimeFormat:[tripDataDictionary valueForKey:@"end_time"]];
                         
                         double start = [[tripDataDictionary valueForKey:@"odometer_start"]doubleValue];
                         double end = [[tripDataDictionary valueForKey:@"odometer_finish"]doubleValue];
                         
                         _txtOStart.text = [NSString stringWithFormat:@"%.2f",start];
                         _txtOEnd.text = [NSString stringWithFormat:@"%.2f",end];
                         
                         NSNumber *distance = [tripDataDictionary valueForKey:@"distance"];
                         float km = [distance floatValue];
                         _txtDistance.text = [NSString stringWithFormat:@"%.2f",km];
                         _txtNotes.text = [tripDataDictionary valueForKey:@"note"];
                         [_btnSave setTitle:NSLocalizedString(@"Update", @"") forState:UIControlStateNormal];

                     }else{
                         [Helper displayAlertView:@"" message:NSLocalizedString(@"NoDataFound", @"")];
                     }
                 }else{
                     [Helper displayAlertView:@"" message:NSLocalizedString(@"NoDataFound", @"")];
                 }
             }
         }
     }];
}


-(void)callAddTripService:(NSDictionary *)parameter{
    
    if(![self CheckNetworkConnection]) {
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [WebService httpPostWithCustomDelegateURLString:@".saveTrip" parameter:parameter isDebug:true callBackData:^(NSData *data, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         if (error == nil) {
             NSError *localError = nil;
             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
             
             if (localError != nil) {
                 NSLog(@"%@",localError.description);
             }
             else {
                 NSDictionary *response = [parsedObject objectForKey:@"data"];
                 NSString* message = [response objectForKey:@"message"];
                 NSString* success = [response objectForKey:@"status"];
                 if ([success isEqualToString:@"Success"]) {
                     [self backAction:self];
                 } else {
                     NSLog(@"error :: %@",message);
                     [Helper displayAlertView:@"" message:message];
                 }
             }
         }
     }];
}

-(void)openAddressView:(NSString *)ScreenField{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AddressSearchViewController *viewController = (AddressSearchViewController *)[storyboard instantiateViewControllerWithIdentifier:@"AddressSearchViewController"];
    viewController.fromScreen = @"ManualTrip";
    viewController.fromScreenField = ScreenField;
    [self presentViewController:viewController animated:YES completion:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
@end
