//
//  BeaconTVC.h
//  SATCAR
//
//  Created by Percept Infotech on 20/04/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BeaconTVC : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnPair;
@property (weak, nonatomic) IBOutlet UIImageView *imgDetail;
@property (weak, nonatomic) IBOutlet UILabel *lblInfo;
@end
