//
//  BeaconDetailViewController.h
//  SATCAR
//
//  Created by Percept Infotech on 21/04/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BeaconDetailViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lblID;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;
@property (weak, nonatomic) IBOutlet UILabel *lblMajor;
@property (weak, nonatomic) IBOutlet UILabel *lblMinor;
@property (weak, nonatomic) IBOutlet UILabel *lblBattery;
@property (weak, nonatomic) IBOutlet UILabel *lblIDValue;
@property (weak, nonatomic) IBOutlet UILabel *lblDistanceValue;
@property (weak, nonatomic) IBOutlet UILabel *lblMajorValue;
@property (weak, nonatomic) IBOutlet UILabel *lblMinorValue;
@property (weak, nonatomic) IBOutlet UILabel *lblBatteryValue;
@property (strong, nonatomic) NSString *beaconID;
@property (weak, nonatomic) IBOutlet UIButton *btnDisconnect;
@property (strong, nonatomic) NSMutableDictionary *beaconDetail;
- (IBAction)onDisconnect:(id)sender;

@end
