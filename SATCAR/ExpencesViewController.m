//
//  ExpencesViewController.m
//  SATCAR
//
//  Created by Percept Infotech on 12/20/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import "ExpencesViewController.h"
#import "ExpenceTableViewCell.h"
#import "ExpenseEntryViewController.h"

@interface ExpencesViewController ()
{
    NSDate *currentMonth,*nextMonth;
    NSString *strCurrentDate,*userRegisterDate,*strCurrency,*parentID,*userID,*undefined_tripstotal;
    int selectedSegment,selectedIndex,removeIndex;   // 0 = all, 1 = private, 2 = business
    NSMutableArray *allTripArray,*tempTrips;
    float borderWidth;
    float pri_trips_spilt,bus_trips_spilt,pri_trips_percentage,bus_trips_percentage;
    AppDelegate *appdelegate;
    NSArray *expenseTypeArray,*expenseKeyArray;
}
@end

@implementation ExpencesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    currentMonth = [NSDate date];
    strCurrentDate = [self getMonthYearDay:currentMonth];
    _tableExpences.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _lblMessage.text = NSLocalizedString(@"NoDataFound", @"");
    _lblMessage.hidden = true;
    _viewExpenseSplit.hidden = true;
    removeIndex = 0;
    /*_viewBackTrip.layer.cornerRadius = 5.0;
    _viewBackTrip.clipsToBounds = true;*/
    
    _btnEditManually.layer.cornerRadius = 25;
    _btnEditManually.layer.borderWidth = 1;
    _btnEditManually.layer.borderColor = [UIColor colorWithRed:0.000 green:0.651 blue:0.929 alpha:1.00].CGColor;
    
    [self addTitleView2:NSLocalizedString(@"Expenses", @"")];
    UIButton *btnAdd = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnAdd setImage:[UIImage imageNamed:@"img_plus_menu"] forState:UIControlStateNormal];
    btnAdd.frame = CGRectMake(30, 0,32, 32);
    btnAdd.imageEdgeInsets = UIEdgeInsetsMake(7, 7, 7, 7);
    btnAdd.layer.cornerRadius = 10;
    btnAdd.clipsToBounds = YES;
    [btnAdd setTintColor:[UIColor whiteColor]];
    [btnAdd addTarget:self action:@selector(AddExpence) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* item = [[UIBarButtonItem alloc] initWithCustomView:btnAdd];
    self.navigationItem.rightBarButtonItem = item;
    
    _tableExpences.rowHeight = UITableViewAutomaticDimension;
    _tableExpences.estimatedRowHeight = 80;
    
    _viewNotification.hidden = true;
    _IBLayoutConstraintNotifHeight.constant = 0;
    
    
    expenseTypeArray = [[NSArray alloc]initWithObjects:NSLocalizedString(@"COM_SATCAR_EXPENSE_OPTION_SERVICE_REPAIR", @""), NSLocalizedString(@"COM_SATCAR_EXPENSE_OPTION_CIRCLE_K", @""), NSLocalizedString(@"COM_SATCAR_EXPENSE_OPTION_Q8", @""), NSLocalizedString(@"COM_SATCAR_EXPENSE_OPTION_SHELL", @""), NSLocalizedString(@"COM_SATCAR_EXPENSE_OPTION_OTHER_FUEL", @""),NSLocalizedString(@"COM_SATCAR_EXPENSE_OPTION_CARWASH", @""), NSLocalizedString(@"COM_SATCAR_EXPENSE_OPTION_FINES", @""), NSLocalizedString(@"COM_SATCAR_EXPENSE_OPTION_OTHER_WITHOUT_VAT", @""), NSLocalizedString(@"COM_SATCAR_EXPENSE_OPTION_OTHER", @""), nil];
    
   
    
    [self hideTripEditView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self initView];
    _lblProgressIndicator.backgroundColor = [Helper privateColor];
    _viewImgBorder.layer.cornerRadius = 5;
    _viewImgBorder.layer.borderWidth = 1;
    _viewImgBorder.layer.borderColor = [UIColor colorWithRed:0.902 green:0.902 blue:0.910 alpha:1.00].CGColor;
    _IBLayoutConstraintViewNoteHeight.constant = 50;
    
   [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    NSData *userdata = [Helper getDataValue:@"userInfo"];
    if (![userdata isKindOfClass:[NSNull class]]) {
        NSDictionary *dataDict = [self dataToDicitonary:userdata];
        NSDictionary *userInfo = [dataDict valueForKey:@"data"];
        if (userInfo != nil) {
            parentID = [Helper isStringIsNull:[userInfo valueForKey:@"parent"]];
        }
    }
    userID = [Helper getPREF:@"userID"];
    
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    if ([appdelegate.isRelaodTrip isEqualToString:@"1"]) {
        borderWidth = 0.5;
        selectedIndex = -1;
        [self hideTripEditView];
        
        arrTrips = [[NSMutableArray alloc]init];
        [arrTrips addObject:@""];
        [arrTrips addObject:@""];
        // set the nav bar's right button item
        
        _lblDate.text = @"October 2016";
        _tableExpences.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        
        _ViewSegment.layer.borderWidth = 1;
        _ViewSegment.layer.cornerRadius = 22;
        _ViewSegment.clipsToBounds = YES;
        _ViewSegment.layer.borderColor = [UIColor colorWithRed:0.000 green:0.647 blue:0.929 alpha:1.00].CGColor;
        
        _viewFinancialSplic.layer.cornerRadius = 20;
        _viewFinancialSplic.clipsToBounds = YES;
        
        [_BtnAllSegment setBackgroundColor:[UIColor colorWithRed:0.000 green:0.647 blue:0.929 alpha:1.00]];
        [_BtnAllSegment.titleLabel setTextColor:[UIColor whiteColor]];
        _BtnAllSegment.layer.borderWidth = 0.5;
        _BtnAllSegment.layer.borderColor = [UIColor colorWithRed:0.000 green:0.647 blue:0.929 alpha:1.00].CGColor;
        [_BtnAllSegment setTintColor: [UIColor whiteColor]];
        
        [_BtnPrivateSegment setBackgroundColor:[UIColor clearColor]];
        _BtnPrivateSegment.layer.borderColor = [UIColor colorWithRed:0.000 green:0.647 blue:0.929 alpha:1.00].CGColor;
        _BtnPrivateSegment.layer.borderWidth = 0.5;
        [_BtnPrivateSegment.titleLabel setTextColor:[UIColor grayColor]];
        [_BtnPrivateSegment setTintColor: [UIColor grayColor]];
        
        [_BtnBusinessSegment setBackgroundColor:[UIColor clearColor]];
        _BtnBusinessSegment.layer.borderColor = [UIColor colorWithRed:0.000 green:0.647 blue:0.929 alpha:1.00].CGColor;
        _BtnBusinessSegment.layer.borderWidth = 0.5;
        [_BtnBusinessSegment.titleLabel setTextColor:[UIColor grayColor]];
        [_BtnBusinessSegment setTintColor: [UIColor grayColor]];
        
        [_BtnAllSegment setTitle:NSLocalizedString(@"All", @"") forState:UIControlStateNormal];
        [_BtnPrivateSegment setTitle:NSLocalizedString(@"ExpPrivate", @"") forState:UIControlStateNormal];
        [_BtnBusinessSegment setTitle:NSLocalizedString(@"ExpBusiness", @"") forState:UIControlStateNormal];
        _lblFinancialSplit.text = NSLocalizedString(@"FINANCESSPLIT", @"");
        
        _txtEAccountValue.placeholder =  NSLocalizedString(@"ExpenseAccount", @"");
        _txtETypeValue.placeholder =  NSLocalizedString(@"ExpenseType", @"");
        _txtENameValue.placeholder =  NSLocalizedString(@"ExpenseName", @"");
        _txtEDateValue.placeholder =  NSLocalizedString(@"Date", @"");
        _txtAmountValue.placeholder =  NSLocalizedString(@"Amount", @"");
        _txtNotesValue.placeholder =  NSLocalizedString(@"Notes", @"");
        
        [_btnEditManually setTitle:NSLocalizedString(@"EditMenually", @"") forState:UIControlStateNormal];
        [_btnConfirm setTitle:NSLocalizedString(@"Close_Trip", @"") forState:UIControlStateNormal];
        
        userRegisterDate = [Helper getPREF:@"userRegisterDate"];
        //strCurrency = [Helper getPREF:@"currencyType"];
        [self initializeData];
    }else{
        appdelegate.isRelaodTrip = @"1";
    }
}

-(void)initView{
    [Helper setBorderWithColorInView:_viewEAccountValue];
    [Helper setBorderWithColorInView:_viewETypeValue];
    [Helper setBorderWithColorInView:_viewENameValue];
    [Helper setBorderWithColorInView:_viewEDateValue];
    [Helper setBorderWithColorInView:_viewAmountValue];
    [Helper setBorderWithColorInView:_viewNotesValue];
    [Helper setBorderWithColorInView:_viewImgBorder];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == _txtAmountValue){
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        
        if([string isEqualToString:@","])
        {
            textField.text = [newString stringByReplacingOccurrencesOfString:@"," withString:@"."];
            return false;
        }else{
            return true;
        }
    }else{
        return true;
    }
}

#pragma mark - Table View
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return allTripArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ExpenceTableViewCell *cell = (ExpenceTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSDictionary *tripData = [[NSDictionary alloc]init];
    tripData = [allTripArray objectAtIndex:indexPath.row];
    
    NSString *tripType = [tripData valueForKey:@"expense_account"];
    if ([tripType isEqualToString:@"1"]) {
        cell.lblTripColor.backgroundColor = [Helper privateColor];
    }else{
        cell.lblTripColor.backgroundColor = [Helper BusinessColor];
    }
    
    NSString *date = [tripData valueForKey:@"expense_date"];
    
  
    NSString *expKey = NSLocalizedString([tripData valueForKey:@"expense_type"], @"");
    NSString *expType = expKey;
    
    if (date != nil) {
        NSString *strdate = [self getCellDisplayDateFormat:date];
        cell.lblTripDate.text = [NSString stringWithFormat:@"%@",strdate];
    }
    cell.lblTripName.text = expType;
    NSString *amount = [self ConvertToDKCurrency:[tripData valueForKey:@"amount"]];
    cell.lblTripMeter.text = [NSString stringWithFormat:@"%@ %@",amount,strCurrency.uppercaseString];
    
    NSString *created_by = [tripData valueForKey:@"created_by"];
    if (![created_by isEqualToString:userID]) {
        // Display user name who created trip
        NSString *name = [tripData valueForKey:@"first_name"];
        cell.viewName.hidden = false;
        cell.lblUserName.text = name;
        cell.IBLayoutConstraintViewNameHeight.constant = 20;
    }else{
        // Hide user name view
        cell.viewName.hidden = true;
        cell.lblUserName.text = @"";
        cell.IBLayoutConstraintViewNameHeight.constant = 0;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == allTripArray.count-1) {
        NSLog(@"please update all trip data");
        //[self getAllTripData];
        
        if ((indexPath.row + 1) % 10 == 0) {
            [self getAllTripData];
        }
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedIndex = (int)indexPath.row;
    [self fillData];
    [self showTripEditView];
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        removeIndex = (int)indexPath.row;
        NSDictionary *tripData = [[NSDictionary alloc]init];
        tripData = [allTripArray objectAtIndex:indexPath.row];
        NSString *tripID = [tripData valueForKey:@"id"];
        NSDictionary *parameter=@{ @"id":tripID };
        [self callDeleteExpense:parameter];
    }
}

#pragma mark - Segment action
- (IBAction)SelctSegment:(id)sender {
    
    if([sender tag] == 1)
    {
        selectedSegment = 0;
        allTripArray = [[NSMutableArray alloc]init];
        [_tableExpences reloadData];
        [self getAllTripData];
        
        [_BtnAllSegment setBackgroundColor:[UIColor colorWithRed:0.000 green:0.647 blue:0.929 alpha:1.00]];
        [_BtnAllSegment.titleLabel setTextColor:[UIColor whiteColor]];
        _BtnAllSegment.layer.borderWidth = 0.5;
        _BtnAllSegment.layer.borderColor = [UIColor colorWithRed:0.000 green:0.647 blue:0.929 alpha:1.00].CGColor;
        [_BtnPrivateSegment setTintColor: [UIColor whiteColor]];
        
        _ViewSegment.layer.borderWidth = 1;
        _ViewSegment.layer.borderColor = [UIColor colorWithRed:0.000 green:0.647 blue:0.929 alpha:1.00].CGColor;
        
        [_BtnPrivateSegment setBackgroundColor:[UIColor clearColor]];
        _BtnPrivateSegment.layer.borderColor = [UIColor colorWithRed:0.000 green:0.647 blue:0.929 alpha:1.00].CGColor;
        _BtnPrivateSegment.layer.borderWidth = 0.5;
        [_BtnPrivateSegment.titleLabel setTextColor:[UIColor grayColor]];
        
        [_BtnBusinessSegment setBackgroundColor:[UIColor clearColor]];
        _BtnBusinessSegment.layer.borderColor = [UIColor colorWithRed:0.000 green:0.647 blue:0.929 alpha:1.00].CGColor;
        _BtnBusinessSegment.layer.borderWidth = 0.5;
        [_BtnBusinessSegment.titleLabel setTextColor:[UIColor grayColor]];
        
    }else if ([sender tag] == 2)
    {
        selectedSegment = 1;
        allTripArray = [[NSMutableArray alloc]init];
        [_tableExpences reloadData];
        [self getAllTripData];
        
        [_BtnPrivateSegment setBackgroundColor:[Helper privateColor]];
        [_BtnPrivateSegment.titleLabel setTextColor:[UIColor whiteColor]];
        _BtnPrivateSegment.titleLabel.textColor = [UIColor whiteColor];
        [_BtnPrivateSegment setTintColor: [UIColor whiteColor]];
        _BtnPrivateSegment.layer.borderWidth = 0.5;
        _BtnPrivateSegment.layer.borderColor = [Helper privateColor].CGColor;
        
        _ViewSegment.layer.borderWidth = 1;
        _ViewSegment.layer.borderColor = [Helper privateColor].CGColor;
        
        [_BtnAllSegment setBackgroundColor:[UIColor clearColor]];
        _BtnAllSegment.layer.borderColor = [Helper privateColor].CGColor;
        [_BtnAllSegment.titleLabel setTextColor:[UIColor grayColor]];
        _BtnAllSegment.layer.borderWidth = 0.5;
        
        [_BtnBusinessSegment setBackgroundColor:[UIColor clearColor]];
        _BtnBusinessSegment.layer.borderColor = [Helper privateColor].CGColor;
        _BtnBusinessSegment.layer.borderWidth = 0.5;
        [_BtnBusinessSegment.titleLabel setTextColor:[UIColor grayColor]];
    }
    else if ([sender tag] == 3)
    {
        selectedSegment = 2;
        allTripArray = [[NSMutableArray alloc]init];
        [_tableExpences reloadData];
        [self getAllTripData];
        
        [_BtnBusinessSegment setBackgroundColor:[Helper BusinessColor]];
        [_BtnBusinessSegment.titleLabel setTextColor:[UIColor whiteColor]];
        _BtnBusinessSegment.layer.borderWidth = 0.5;
        _BtnBusinessSegment.layer.borderColor = [Helper BusinessColor].CGColor;
        [_BtnBusinessSegment setTintColor: [UIColor whiteColor]];
        
        _ViewSegment.layer.borderWidth = 1;
        _ViewSegment.layer.borderColor = [Helper BusinessColor].CGColor;
        
        [_BtnPrivateSegment setBackgroundColor:[UIColor clearColor]];
        _BtnPrivateSegment.layer.borderColor = [Helper BusinessColor].CGColor;
        _BtnPrivateSegment.layer.borderWidth = 0.5;
        [_BtnPrivateSegment.titleLabel setTextColor:[UIColor grayColor]];
        
        
        [_BtnAllSegment setBackgroundColor:[UIColor clearColor]];
        _BtnAllSegment.layer.borderColor = [Helper BusinessColor].CGColor;
        _BtnAllSegment.layer.borderWidth = 0.5;
        [_BtnAllSegment.titleLabel setTextColor:[UIColor grayColor]];
    }
}

#pragma mark - Webservice call
-(void)callTripService:(NSString *)tripType date:(NSString *)tripDate limit:(NSString *)limit{
    
    _viewNotification.hidden = true;
    _IBLayoutConstraintNotifHeight.constant = 0;
    
    if(![self CheckNetworkConnection])    {
        return;
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *user_type = [Helper getPREF:@"user_type"];
    NSString *type,*date,*lmt;
    type = tripType;
    date = tripDate;
    lmt = limit;
    
    NSDictionary *parameter=@{
                              @"userid":userID,
                              @"exp_account":tripType,
                              @"e_date":tripDate,
                              @"limitstart":limit
                              };
    //NSLog(@"parameter : %@",parameter);
    [WebService httpPostWithCustomDelegateURLString:@".getexpenses" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         if (error == nil) {
             NSError *localError = nil;
             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
             
             if (localError != nil) {
                 NSLog(@"%@",localError.description);
             }
             else {
                 id response = [parsedObject objectForKey:@"data"];
                 if ([self CheckSession:response]) {
                     
                     tempTrips = [[NSMutableArray alloc]init];
                     tempTrips  = [response objectForKey:@"expenses"];
                     pri_trips_spilt = [[response objectForKey:@"private_expense"] floatValue];
                     bus_trips_spilt = [[response objectForKey:@"business_expense"] floatValue];;
                     pri_trips_percentage = [[response objectForKey:@"pri_exp_percentage"] floatValue];;
                     bus_trips_percentage = [[response objectForKey:@"bus_exp_percentage"] floatValue];;
                    
                     NSDictionary *generalSetting = [response valueForKey:@"general_setting"];
                     strCurrency = [generalSetting valueForKey:@"currency"];
                     NSString *strMeasure = [generalSetting valueForKey:@"unit"];
                     NSLog(@"Setting : %@",generalSetting);
                     
                     NSData *userdata = [Helper getDataValue:@"userInfo"];
                     NSDictionary *dataDict = [self dataToDicitonary:userdata];
                     if (![dataDict isKindOfClass:[NSNull class]]) {
                         NSDictionary *userInfo = [dataDict valueForKey:@"data"];
                         if (userInfo != nil) {
                             [userInfo setValue:generalSetting forKey:@"general_setting"];
                             //NSLog(@"DATA : %@",userInfo);
                             [dataDict setValue:userInfo forKey:@"data"];
                             NSData *data = [self dicitonaryToData:dataDict];
                             [Helper setDataValue:data sKey:@"userInfo"];
                         }
                         
                         [Helper setPREFStringValue:strCurrency sKey:@"currency"];
                         [Helper setPREFStringValue:strMeasure sKey:@"unit"];
                         [Helper setUserValue:generalSetting sKey:@"general_setting"];
                         
                         if ([user_type isEqualToString:@"11"]) {
                             undefined_tripstotal = [response objectForKey:@"expenses_notification_total"];
                             if ([undefined_tripstotal integerValue] > 0) {
                                 _viewNotification.hidden = false;
                                 _lblNotification.text = [NSString stringWithFormat:NSLocalizedString(@"Splitleasing_end_of_month_Expense_notigication_message", @"")];
                                 _IBLayoutConstraintNotifHeight.constant = 50;
                             }else{
                                 _viewNotification.hidden = true;
                                 _lblNotification.text = @"";
                                 _IBLayoutConstraintNotifHeight.constant = 0;
                             }
                         }
                         
                         [self tripSplitCalculate];
                         
                         if (tempTrips.count > 0) {
                             NSLog(@"Trips : %@",tempTrips);
                             for (int i = 0; i < tempTrips.count; i++) {
                                 [allTripArray addObject:[tempTrips objectAtIndex:i]];
                             }
                             [self tripSplitCalculate];
                             [_tableExpences reloadData];
                         }else{
                             [_tableExpences reloadData];
                         }
                         [self isDataAvailable];
                         
                         if ([parentID integerValue] > 0) {
                             _viewExpenseSplit.hidden = true;
                             _IBLayoutConstraintViewTableviewBottom.constant = 10;
                         }else{
                             _viewExpenseSplit.hidden = false;
                             _IBLayoutConstraintViewTableviewBottom.constant = 130;
                         }
                     }else{
                         [Helper displayAlertView:@"" message:NSLocalizedString(@"something_went_wrong_msg", @"")];
                     }
                 } else {
                 }
             }
         }else{
             UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"request_time_out", @"") preferredStyle:UIAlertControllerStyleAlert];
             
             UIAlertAction *retry = [UIAlertAction actionWithTitle:NSLocalizedString(@"Retry", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                 [self callTripService:type date:date limit:lmt];
             }];
             UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                 [alert dismissViewControllerAnimated:true completion:nil];
             }];
             [alert addAction:retry];
             [alert addAction:cancel];
             [self presentViewController:alert animated:true completion:nil];
         }
     }];
}

- (void)callDeleteExpense:(NSDictionary *)parameter{
    [self.view endEditing:YES];
    
    if(![self CheckNetworkConnection]) {
        return;
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //NSLog(@"parameter : %@",parameter);
    
    [WebService httpPostWithCustomDelegateURLString:@".deleteExpense" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         if (error == nil) {
             
             
             NSError *localError = nil;
             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
             
             if (localError != nil) {
                 NSLog(@"%@",localError.description);
             }
             else {
                 NSDictionary *response = [parsedObject objectForKey:@"data"];
                 NSString* message = [response objectForKey:@"message"];
                 NSString* success = [response objectForKey:@"status"];
                 NSLog(@"Response : %@",response);
                 if ([success isEqualToString:@"Success"]) {
                     tempTrips = [[NSMutableArray alloc]init];
                     allTripArray = [[NSMutableArray alloc]init];
                     [_tableExpences reloadData];
                     
                     [self checkDataAvailable:currentMonth];
                     [self checkNextDataAvailable:currentMonth];
                     _lblDate.text = [self getHeaderDisplayDateFormat:currentMonth];//strCurrentDate;
                     [self getAllTripData ];
                     //[allTripArray removeObjectAtIndex:removeIndex];
                    // [_tableExpences reloadData];
                 } else {
                     NSLog(@"error :: %@",message);
                     [Helper displayAlertView:@"" message:message];
                 }
             }
         }
     }];
}

#pragma mark - Button action

- (IBAction)onFinancesSplit:(id)sender {
    [self performSegueWithIdentifier:@"ExpenseToSplit" sender:self];
}

- (IBAction)onPreviousDate:(id)sender {
    
    NSDate *prev = [self getPrevoiusMonthDate:currentMonth];
    currentMonth = prev;
     [self checkDataAvailable:currentMonth];
    strCurrentDate = [self getMonthYearDay:currentMonth];
    _lblDate.text = [self getHeaderDisplayDateFormat:currentMonth];
    allTripArray = [[NSMutableArray alloc]init];
    [_tableExpences reloadData];
    [self getAllTripData];
    
}

- (IBAction)onNextDate:(id)sender {

    NSDate *next = [self getNextMonthDate:currentMonth];
    currentMonth = next;
    [self checkNextDataAvailable:currentMonth];
    strCurrentDate = [self getMonthYearDay:currentMonth];
    _lblDate.text = [self getHeaderDisplayDateFormat:currentMonth];
    allTripArray = [[NSMutableArray alloc]init];
    [_tableExpences reloadData];
    [self getAllTripData];
}

- (IBAction)onEditManually:(id)sender {
    self.navigationController.navigationBar.hidden = false;
    [self hideTripEditView];
    [self performSegueWithIdentifier:@"ExpenseToEditManualEntry" sender:self];
}

- (IBAction)onConfirm:(id)sender {
   // selectedSegment = -1;
    [self hideTripEditView];
}

#pragma mark - custome funcitons
-(void)initializeData{
    tempTrips = [[NSMutableArray alloc]init];
    allTripArray = [[NSMutableArray alloc]init];
    selectedSegment = 0;
    [_tableExpences reloadData];
    [self checkDataAvailable:currentMonth];
    [self checkNextDataAvailable:currentMonth];
    _lblDate.text = [self getHeaderDisplayDateFormat:currentMonth];//strCurrentDate;
    [self getAllTripData ];
}

-(void)checkDataAvailable:(NSDate *)date{
    
    BOOL previousAvailable = [self comapreDateByMonthAndYear:date registerDate:userRegisterDate];
    [self tempNext:date];
    _btnPrivious.hidden = true;
    if (previousAvailable) {
        _btnPrivious.hidden = false;
    }else{
        _btnPrivious.hidden = true;
    }
}

-(void)checkNextDataAvailable:(NSDate *)date{
    
    BOOL nextAvailable = [self comapreNextDate:date];
    [self tempPrevious:date];
    _btnNext.hidden = true;
    if (nextAvailable) {
        _btnNext.hidden = false;
    }else{
        _btnNext.hidden = true;
    }
}

-(void)tempPrevious:(NSDate *)date{
    
    BOOL previousAvailable = [self comapreDateByMonthAndYear:date registerDate:userRegisterDate];
    _btnPrivious.hidden = true;
    if (previousAvailable) {
        _btnPrivious.hidden = false;
    }else{
        _btnPrivious.hidden = true;
    }
}
-(void)tempNext:(NSDate *)date{
    BOOL nextAvailable = [self comapreNextDate:date];
    _btnNext.hidden = true;
    if (nextAvailable) {
        _btnNext.hidden = false;
    }else{
        _btnNext.hidden = true;
    }
}


-(void)hideTripEditView{
    _viewTripDisplay.hidden = true;
    self.navigationController.navigationBar.hidden = false;
    [UIView animateWithDuration:5.0 delay:1.0 options:UIViewAnimationOptionTransitionNone animations:^{
        _IBLayoutConstraintTripPopupTop.constant = self.view.frame.size.height;
        _IBLayoutConstraintTripPopupHeight.constant = 0;
    }completion:nil];
}
-(void)showTripEditView{
    
    [UIView animateWithDuration:5.0 delay:1.0 options:UIViewAnimationOptionTransitionNone animations:^{
        self.navigationController.navigationBar.hidden = true;
        _viewTripDisplay.hidden = false;
        _IBLayoutConstraintTripPopupTop.constant = 0;
        _IBLayoutConstraintTripPopupHeight.constant = self.view.frame.size.height;
    }completion:nil];
    
}
-(void)fillData{
    NSDictionary *tripData = [allTripArray objectAtIndex:selectedIndex];
    
    
    /*_lblETypeValue.text = [NSString stringWithFormat:@"%@",[tripData valueForKey:@"expense_type"]];
    _lblENameValue.text = [NSString stringWithFormat:@"%@",[tripData valueForKey:@"name"]];
    _lblEDateValue.text = [NSString stringWithFormat:@"%@",[Helper getDateFromString:[tripData valueForKey:@"expense_date"]]];*/
    
    _txtETypeValue.text = NSLocalizedString([tripData valueForKey:@"expense_type"], @"");
    _txtENameValue.text = [NSString stringWithFormat:@"%@",[tripData valueForKey:@"name"]];
    _txtEDateValue.text = [NSString stringWithFormat:@"%@",[Helper getDateFromString:[tripData valueForKey:@"expense_date"]]];
    
    
    NSString *amount = [self ConvertToDKCurrency:[tripData valueForKey:@"amount"]];
    /*_lblAmountValue.text = [NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@ %@",amount,strCurrency]];
    _lblNotesValue.text = [NSString stringWithFormat:@"%@",[tripData valueForKey:@"note"]];*/
    
    _txtAmountValue.text = [NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@ %@",amount,strCurrency]];
    _txtNotesValue.text = [NSString stringWithFormat:@"%@",[tripData valueForKey:@"note"]];
    
    _IBLayoutConstraintViewNoteHeight.constant = _txtNotesValue.frame.size.height + 33;
    
    NSString *imgUrl = [tripData valueForKey:@"image_doc"];
    if (imgUrl.length > 0) {
        NSString *url = [NSString stringWithFormat:@"%@%@",[WebService getExpenseImageUrl],imgUrl];
        [_imgBusiness sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"camera_96.png"]];
        _imgBusiness.backgroundColor = [UIColor clearColor];
        //_IBLayoutConstraintImageHeight.constant = 60;
        _IBLayoutConstraintViewImgHeight.constant = 150;
        _IBLayoutConstraintViewImgTop.constant = 10;
    }else{
        _IBLayoutConstraintImageHeight.constant = 0;
        _IBLayoutConstraintViewImgTop.constant = 0;
        _imgBusiness.image = [UIImage imageNamed:@"camera_96.png"];
        _IBLayoutConstraintViewImgHeight.constant = 0;
    }
    
    NSString *tripType = [tripData valueForKey:@"expense_account"];
    UIColor *privateColor = [Helper privateColor];
    UIColor *businessColor = [Helper BusinessColor];
    
    if ([tripType isEqualToString:@"1"]) {
        _txtEAccountValue.text = @"Private trip";
        //_lblEAccountValue.textColor = privateColor;
        _btnEditManually.layer.borderColor = privateColor.CGColor;
       // _btnConfirm.layer.borderColor = privateColor.CGColor;
        [_btnEditManually setTitleColor:privateColor forState:UIControlStateNormal];
        //[_btnConfirm setTitleColor:privateColor forState:UIControlStateNormal];
        _lblTripmessage.backgroundColor = privateColor;
    }else{
        _txtEAccountValue.text = @"Business trip";
        //_lblEAccountValue.textColor = businessColor;
        _btnEditManually.layer.borderColor = businessColor.CGColor;
       // _btnConfirm.layer.borderColor = businessColor.CGColor;
        [_btnEditManually setTitleColor:businessColor forState:UIControlStateNormal];
       // [_btnConfirm setTitleColor:businessColor forState:UIControlStateNormal];
        _lblTripmessage.backgroundColor = businessColor;
    }
}

-(void)AddExpence
{
    [self performSegueWithIdentifier:@"ExpenseToEntry" sender:self];
}

-(void)getAllTripData{
    NSString *count = [NSString stringWithFormat:@"%d",(int)allTripArray.count];
    NSString *trip = [NSString stringWithFormat:@"%d",selectedSegment];
    [self callTripService:trip date:strCurrentDate limit:count];
}

-(void) tripSplitCalculate {
    NSLog(@"calulate");
    double totalDistance =0.0;
    double privateTripTotalDistace =0.0;
    privateTripTotalDistace = pri_trips_spilt * 100;
    totalDistance = (pri_trips_spilt + bus_trips_spilt);
    float progress = (float)(privateTripTotalDistace/totalDistance);
    if (progress < 0 || isnan(progress)) {
        progress = 0;
        _IBLayoutConstraintprivateWidth.constant = 0;
        
        NSString *PTPstr = [NSString stringWithFormat:@"%.2f",progress];
        NSString *BTPstr = [NSString stringWithFormat:@"%.2f",(100-progress)];
        
        double BprivateTripTotalDistace = bus_trips_spilt * 100;
        double BtotalDistance = (pri_trips_spilt + bus_trips_spilt);
        float Bprogress = (float)(BprivateTripTotalDistace/BtotalDistance);
        if (Bprogress < 0 || isnan(Bprogress)){
            Bprogress = 0;
            NSString *BTPstr = [NSString stringWithFormat:@"%.2f",Bprogress];
            _lblPrivatePercentage.text = [NSString stringWithFormat:@"Private (%@%%)",PTPstr];
            _lblBusinessPercentage.text = [NSString stringWithFormat:@"Business (%@%%)",BTPstr];
            
            _lblPrivatePercentage.text = [NSString stringWithFormat:NSLocalizedString(@"PrivatePerc" ,@""),PTPstr];
            _lblBusinessPercentage.text = [NSString stringWithFormat:NSLocalizedString(@"BusinessPerc" ,@""),BTPstr];
        }else{
            _lblPrivatePercentage.text = [NSString stringWithFormat:@"Private (%@%%)",PTPstr];
            _lblBusinessPercentage.text = [NSString stringWithFormat:@"Business (%@%%)",BTPstr];
            
            _lblPrivatePercentage.text = [NSString stringWithFormat:NSLocalizedString(@"PrivatePerc" ,@""),PTPstr];
            _lblBusinessPercentage.text = [NSString stringWithFormat:NSLocalizedString(@"BusinessPerc" ,@""),BTPstr];
        }
        
    }else{
        float screenWidth = (float)self.view.frame.size.width - 40;
        float width = (screenWidth * progress ) / 100;
        _IBLayoutConstraintprivateWidth.constant = width;
        
        NSString *PTPstr = [NSString stringWithFormat:@"%.2f",progress];
        NSString *BTPstr = [NSString stringWithFormat:@"%.2f",(100-progress)];
        
        _lblPrivatePercentage.text = [NSString stringWithFormat:@"Private (%@%%)",PTPstr];
        _lblBusinessPercentage.text = [NSString stringWithFormat:@"Business (%@%%)",BTPstr];
        
        _lblPrivatePercentage.text = [NSString stringWithFormat:NSLocalizedString(@"PrivatePerc" ,@""),PTPstr];
        _lblBusinessPercentage.text = [NSString stringWithFormat:NSLocalizedString(@"BusinessPerc" ,@""),BTPstr];
    }
}

-(void)isDataAvailable{
    if (allTripArray.count <= 0) {
        _lblMessage.hidden = false;
        _viewExpenseSplit.hidden = true;
    }else{
        _lblMessage.hidden = true;
        _viewExpenseSplit.hidden = false;
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"ExpenseToEditManualEntry"]) {
        ExpenseEntryViewController *controller = segue.destinationViewController;
        NSDictionary *tripData = [allTripArray objectAtIndex:selectedIndex];
        controller.tripDataDictionary = tripData;
        controller.fromScreen = @"TripEdit";
    }
}


@end
