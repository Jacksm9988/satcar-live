//
//  ExportViewController.m
//  SATCAR
//
//  Created by Percept Infotech on 29/12/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import "ExportViewController.h"
#import "UIViewController+VCHelpers.h"
#import "ECSlidingViewController.h"
#import "LeftSideViewController.h"
#import "NavigationController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "UIViewController+VCHelpers.h"
#import "EmailTVC.h"
#import "AddEmailTVC.h"

@interface ExportViewController ()<UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource>
{
    BOOL isPSelectAll, isPType, isPDate, isPStartTime, isPEndTime, isPDistance, isPOStart, isPOEnd, isPSAddress, isPEAddress, isPSCoordinate, isPECoordinate, isPReimbursement, isPNote;
    int isBSelectAll, isBAccountTpe, isBDate, isBName, isBAmount, isBCurrency, isBNotes, isBZip,isDateFrom,selectedSegment;
    int isOSelectAll, isOTripSplit, isOFinanceSplit, isOTtotal;
    UIImage *selectedImage,*unSelectedImage;
    NSMutableArray *emailArray;
    UIDatePicker *datePicker;
    UIPickerView *exportTypePicker;
    NSArray *exportTypeArray;
    NSString *userType;
    NSString *export_type,*strSelectedPrivateExport,*strSelectedBusinessExport,*selectedFields,*strSelectedOverviewExport;
    NSMutableArray *selectedPrivateExport,*selectedBusinessExport,*selectedOverviewExport;
    NSMutableArray *tempSelectedPrivateExport,*tempSelectedBusinessExport,*tempSelectedOverviewExport;
    int tblHeight;
    EmailTVC *emailCell;
    BOOL touched;
    NSString *firstEmail;
}
@end

@implementation ExportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    userType = [Helper getPREF:@"user_type"];
    if ([userType isEqualToString:@"10"] || [userType isEqualToString:@"12"]) {
        _viewPrivateReimbursement.hidden = false;
        _ViewSegment.hidden = true;
        _IBLayoutConstraintPrivateReimbursementTop.constant = 5;
        _IBLayoutConstraintPrivateReimbursementHeight.constant = 30;
        _imgPReimbursement.hidden = false;
        selectedSegment = 2;
        selectedPrivateExport = [[NSMutableArray alloc]initWithObjects:@"trip_type",@"trip_date",@"start_time",@"end_time",@"distance",@"odometer_start",@"odometer_finish",@"start_address",@"end_address",@"srart_point",@"destination_point",@"reimbursement",@"notetrip", nil];
    }else{
        _ViewSegment.hidden = false;
        _viewPrivateReimbursement.hidden = true;
        _IBLayoutConstraintPrivateReimbursementTop.constant = 0;
        _IBLayoutConstraintPrivateReimbursementHeight.constant = 0;
        selectedSegment = 1;
        _imgPReimbursement.hidden = true;
        selectedPrivateExport = [[NSMutableArray alloc]initWithObjects:@"trip_type",@"trip_date",@"start_time",@"end_time",@"distance",@"odometer_start",@"odometer_finish",@"start_address",@"end_address",@"srart_point",@"destination_point",@"notetrip", nil];
        
    }

    exportTypeArray = [[NSArray alloc]initWithObjects:@"PDF",@"EXCEL",@"CSV",nil];
    selectedBusinessExport = [[NSMutableArray alloc]initWithObjects:@"expense_account",@"expense_date",@"name",@"amount",@"currency",@"note",@"imagezip", nil];
    selectedOverviewExport = [[NSMutableArray alloc]initWithObjects:@"trip_split",@"finance_split",@"total_split", nil];
    
    tempSelectedPrivateExport = [[NSMutableArray alloc]init];
    tempSelectedBusinessExport = [[NSMutableArray alloc]init];
    tempSelectedOverviewExport = [[NSMutableArray alloc]init];
    
    selectedFields = @"";
    export_type = @"all";
    touched = false;
    [self hideAll];
}

-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self addTitleView2:NSLocalizedString(@"Export", @"")];
    UIButton *btnAdd = [UIButton buttonWithType:UIButtonTypeCustom];
    btnAdd.frame = CGRectMake(20,0,50,32);
    [btnAdd setTitle:NSLocalizedString(@"Send", @"") forState:UIControlStateNormal];
    [btnAdd setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnAdd.titleLabel.font = [UIFont systemFontOfSize:16];
    [btnAdd addTarget:self action:@selector(onExport:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* item = [[UIBarButtonItem alloc] initWithCustomView:btnAdd];
    self.navigationItem.rightBarButtonItem = item;
    isDateFrom = 0;   // 1 = txtFromDate , 2 = txtTodate
    _tableview.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _btnExport.layer.cornerRadius = 25;
    touched = false;
    _ViewSegment.layer.borderWidth = 1;
    _ViewSegment.layer.cornerRadius = 22;
    _ViewSegment.clipsToBounds = YES;
    _ViewSegment.layer.borderColor = [UIColor colorWithRed:0.000 green:0.647 blue:0.929 alpha:1.00].CGColor;
    
    [_BtnAllSegment setBackgroundColor:[UIColor colorWithRed:0.000 green:0.647 blue:0.929 alpha:1.00]];
    [_BtnAllSegment.titleLabel setTextColor:[UIColor whiteColor]];
    _BtnAllSegment.layer.borderWidth = 0.5;
    _BtnAllSegment.layer.borderColor = [UIColor colorWithRed:0.000 green:0.647 blue:0.929 alpha:1.00].CGColor;
    [_BtnAllSegment setTintColor: [UIColor whiteColor]];
    
    [_BtnPrivateSegment setBackgroundColor:[UIColor clearColor]];
    _BtnPrivateSegment.layer.borderColor = [UIColor colorWithRed:0.000 green:0.647 blue:0.929 alpha:1.00].CGColor;
    _BtnPrivateSegment.layer.borderWidth = 0.5;
    [_BtnPrivateSegment.titleLabel setTextColor:[UIColor grayColor]];
    [_BtnPrivateSegment setTintColor: [UIColor grayColor]];
    
    [_BtnBusinessSegment setBackgroundColor:[UIColor clearColor]];
    _BtnBusinessSegment.layer.borderColor = [UIColor colorWithRed:0.000 green:0.647 blue:0.929 alpha:1.00].CGColor;
    _BtnBusinessSegment.layer.borderWidth = 0.5;
    [_BtnBusinessSegment.titleLabel setTextColor:[UIColor grayColor]];
    [_BtnBusinessSegment setTintColor: [UIColor grayColor]];
    
    [_BtnOverviewSegment setBackgroundColor:[UIColor clearColor]];
    _BtnOverviewSegment.layer.borderColor = [UIColor colorWithRed:0.000 green:0.647 blue:0.929 alpha:1.00].CGColor;
    _BtnOverviewSegment.layer.borderWidth = 0.5;
    [_BtnOverviewSegment.titleLabel setTextColor:[UIColor grayColor]];
    [_BtnOverviewSegment setTintColor: [UIColor grayColor]];
    
    _viewPrivateReimbursement.layer.borderWidth = 1;
    _viewPrivateReimbursement.layer.cornerRadius = 22;
    _viewPrivateReimbursement.clipsToBounds = YES;
    _viewPrivateReimbursement.layer.borderColor = [UIColor colorWithRed:0.000 green:0.647 blue:0.929 alpha:1.00].CGColor;
    
    [_BtnPrivateReimbursement setBackgroundColor:[UIColor colorWithRed:0.000 green:0.647 blue:0.929 alpha:1.00]];
    [_BtnPrivateReimbursement.titleLabel setTextColor:[UIColor whiteColor]];
    _BtnPrivateReimbursement.layer.borderWidth = 0.5;
    _BtnPrivateReimbursement.layer.borderColor = [UIColor colorWithRed:0.000 green:0.647 blue:0.929 alpha:1.00].CGColor;
    [_BtnPrivateReimbursement setTintColor: [UIColor whiteColor]];
    
    [_BtnAllSegment setTitle:NSLocalizedString(@"All", @"") forState:UIControlStateNormal];
    [_BtnPrivateSegment setTitle:NSLocalizedString(@"Trips", @"") forState:UIControlStateNormal];
    [_BtnPrivateReimbursement setTitle:NSLocalizedString(@"Trips", @"") forState:UIControlStateNormal];
    [_BtnBusinessSegment setTitle:NSLocalizedString(@"economy", @"") forState:UIControlStateNormal];
    [_BtnOverviewSegment setTitle:NSLocalizedString(@"Overview", @"") forState:UIControlStateNormal];
    [_btnExport setTitle:NSLocalizedString(@"SendEmail", @"") forState:UIControlStateNormal];
    
    /*UIView *Rview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 30, 40)];
    UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(5, 10, 20, 20)];
    [img setImage:[UIImage imageNamed:@"img_down.png"]];
    [Rview addSubview:img];
    _txtFormate.rightView = Rview;
    _txtFormate.rightViewMode = UITextFieldViewModeAlways;*/
    
    _lblFormate.text = NSLocalizedString(@"selectFormate", @"");
    _lblBIncludeExpo.text = NSLocalizedString(@"IncludeInExport", @"");
    
    _lblWhatDoExport.text = NSLocalizedString(@"WhatDoYouWantToExport", @"");
    _lblPeriod.text = NSLocalizedString(@"Period", @"");
    _lblPeriodFrom.text = NSLocalizedString(@"From", @"");
    //_txtPeriodFromValue.text = NSLocalizedString(@"To", @"");
    _lblPeriodTo.text = NSLocalizedString(@"To", @"");
    //_txtPeriodToValue.text = NSLocalizedString(@"", @"");
    _lblPSelectAll.text = NSLocalizedString(@"SelectAll", @"");
    _lblPType.text = NSLocalizedString(@"TypeOftrip", @"");
    _lblPDate.text = NSLocalizedString(@"Date", @"");
    _lblPStartTime.text = NSLocalizedString(@"Start_time", @"");
    _lblPEndTime.text = NSLocalizedString(@"End_time", @"");
    _lblPDistance.text = NSLocalizedString(@"Distance", @"");
    _lblPOStart.text = NSLocalizedString(@"Odometers_start", @"");
    _lblPOEnd.text = NSLocalizedString(@"Odometer_end", @"");
    _lblPSAddress.text = NSLocalizedString(@"StartAddress", @"");
    _lblPEAddress.text = NSLocalizedString(@"EndAddress", @"");
    _lblPSCoordinate.text = NSLocalizedString(@"StartCoordinate", @"");
    _lblPECoordinate.text = NSLocalizedString(@"EndCoordinate", @"");
    _lblPNote.text =  NSLocalizedString(@"Note", @"");
    _lblBIncludeExpo.text = NSLocalizedString(@"IncludeInExport", @"");
    _lblBSelectAll.text = NSLocalizedString(@"SelectAll", @"");
    _lblBAccountTpe.text = NSLocalizedString(@"Account", @"");
    _lblBDate.text = NSLocalizedString(@"Date", @"");
    _lblBName.text = NSLocalizedString(@"Name", @"");
    _lblBAmount.text = NSLocalizedString(@"Amount", @"");
    _lblBCurrency.text = NSLocalizedString(@"Currency", @"");
    _lblBNotes.text = NSLocalizedString(@"Note", @"");
    _lblBZip.text = NSLocalizedString(@"ZipImageDoc", @"");
    _lblOSelectAll.text = NSLocalizedString(@"SelectAll", @"");
    _lblOTripSplit.text = NSLocalizedString(@"trip_split", @"");
    _lblOFinancialSplit.text = NSLocalizedString(@"finance_split", @"");
    _lblOTotals.text = NSLocalizedString(@"trip_totals", @"");
    _lblAdditional.text = NSLocalizedString(@"AdditionalImageSendTo", @"");
    
    _txtPeriodFromValue.placeholder = NSLocalizedString(@"Date", @"");
    _txtPeriodToValue.placeholder = NSLocalizedString(@"Date", @"");
    _txtFormate.placeholder = NSLocalizedString(@"currentpassword", @"");
    _txtFormate.text = [exportTypeArray objectAtIndex:0];
    selectedImage = [UIImage imageNamed:@"withcheck"];
    unSelectedImage = [UIImage imageNamed:@"withoutcheck"];
    emailArray = [[NSMutableArray alloc]initWithObjects:@"", nil];
    
    tblHeight = 150;
    _IBLayoutConstraintTblHeight.constant = tblHeight;
    
    userType = [Helper getPREF:@"user_type"];
    if ([userType isEqualToString:@"10"] || [userType isEqualToString:@"12"]) {
        _viewPrivateExport.hidden = false;
        _lblPrivateBottom.hidden = false;
        _IBLayoutConstraintTableviewTop.constant = 576;
        
        //_IBLayoutConstraintPrivateExportViewTop.constant = 10;
        //_IBLayoutConstraintPrivateExportViewHeight.constant = 506;
    }else{
        _viewPrivateExport.hidden = true;
        _lblPrivateBottom.hidden = true;
        _IBLayoutConstraintTableviewTop.constant = 10;
        //_IBLayoutConstraintPrivateExportViewTop.constant = 0;
        //_IBLayoutConstraintPrivateExportViewHeight.constant = 0;
    }
    
    /*_IBLayoutConstraintBusinessExportViewTop.constant = 0;
    _IBLayoutConstraintBusinessExportViewHeight.constant = 0;
    _IBLayoutConstraintOverviewViewTop.constant = 0;
    _IBLayoutConstraintOverviewViewHeight.constant = 0;*/
    _viewBusinessExport.hidden = true;
    _viewOverviewExport.hidden = true;
    
    _lblBusinessBottom.hidden = true;
    _lblOverviewBottom.hidden = true;
    
    [_txtPeriodFromValue addPreviousNextDoneOnKeyboardWithTarget:self previousAction:@selector(onPeriodFromPrevious) nextAction:@selector(onPeriodFromNext) doneAction:@selector(onPeriodFromDone)];
    
    [_txtPeriodToValue addPreviousNextDoneOnKeyboardWithTarget:self previousAction:@selector(onPeriodToPrevious) nextAction:@selector(onPeriodToNext) doneAction:@selector(onPeriodToDone)];
    
    [_txtFormate addPreviousNextDoneOnKeyboardWithTarget:self previousAction:@selector(onFormatPrevious) nextAction:@selector(onFormatNext) doneAction:@selector(onFormatDone)];
    
    NSData *userdata = [Helper getDataValue:@"userInfo"];
    NSDictionary * dict = [self dataToDicitonary:userdata];
    if (dict != nil) {
       // NSLog(@"DATA : %@",dict);
        NSDictionary *userInfo = [dict valueForKey:@"data"];
        firstEmail = [[userInfo valueForKey:@"user"]valueForKey:@"email"];
    }
    [self initializePrivateExport];
    [self initializeBusibessExport];
    [self initializeOverviewExport];
}

-(void)hideAll{
    _IBLayoutConstraintTableviewTop.constant = 10;
    _viewPrivateExport.hidden = true;
    _viewBusinessExport.hidden = true;
    _viewOverviewExport.hidden = true;
    
    _lblPrivateBottom.hidden = true;
    _lblBusinessBottom.hidden = true;
    _lblOverviewBottom.hidden = true;
}

#pragma mark - Table View
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return emailArray.count + 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < emailArray.count) {
        EmailTVC *cell = (EmailTVC *)[tableView dequeueReusableCellWithIdentifier:@"cellEamil"];
        emailCell = cell;
        if (indexPath.row == 0) {
            cell.txtEmail.text = firstEmail;
        }
        return cell;
    }else{
        AddEmailTVC *cell = (AddEmailTVC *)[tableView dequeueReusableCellWithIdentifier:@"cellAddEamil"];
        [cell.btnAddEmail addTarget:self action:@selector(onAddEmail:) forControlEvents:UIControlEventTouchUpInside];
        cell.lblAddEmail.text = NSLocalizedString(@"AddAnotherEmail", @"");
        
        return cell;
    }
}

#pragma mark - textfield delegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    textField.textColor =  [UIColor colorWithRed:18.0/255.0 green:172.0/255.0 blue:243.0/255.0 alpha:1.0];
    textField.layer.borderWidth = 1;
    textField.layer.borderColor =  [UIColor colorWithRed:18.0/255.0 green:172.0/255.0 blue:243.0/255.0 alpha:1.0].CGColor;
    textField.clipsToBounds = YES;
    return true;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    textField.textColor =  [UIColor blackColor];
    textField.layer.borderWidth = 0;
    textField.layer.borderColor = [UIColor clearColor].CGColor;
    textField.clipsToBounds = YES;
    return true;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if (textField == _txtPeriodFromValue) {
        
        datePicker = [[UIDatePicker alloc] initWithFrame:CGRectZero];
        [datePicker setDatePickerMode:UIDatePickerModeDate];
        [datePicker addTarget:self action:@selector(onDatePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
        datePicker.maximumDate = [NSDate date];
        _txtPeriodFromValue.inputView = datePicker;
        isDateFrom = 1;
        if (![_txtPeriodFromValue hasText]) {
            NSDateFormatter __autoreleasing *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
            [dateFormat setDateFormat:@"dd/MM/YYYY"];
            NSString *dateString =  [dateFormat stringFromDate:datePicker.date];
            _txtPeriodFromValue.text = dateString;
        }
    }else if (textField == _txtPeriodToValue){
        datePicker = [[UIDatePicker alloc] initWithFrame:CGRectZero];
        [datePicker setDatePickerMode:UIDatePickerModeDate];
        [datePicker addTarget:self action:@selector(onDatePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
        datePicker.maximumDate = [NSDate date];
        _txtPeriodToValue.inputView = datePicker;
        isDateFrom = 2;
        if (![_txtPeriodToValue hasText]) {
            NSDateFormatter __autoreleasing *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
            [dateFormat setDateFormat:@"dd/MM/YYYY"];
            NSString *dateString =  [dateFormat stringFromDate:datePicker.date];
            _txtPeriodToValue.text = dateString;
        }
    }else if(textField == _txtFormate) {
        exportTypePicker = [[UIPickerView alloc] initWithFrame:CGRectZero];
        [exportTypePicker setDataSource: self];
        [exportTypePicker setDelegate: self];
        exportTypePicker.showsSelectionIndicator = YES;
        _txtFormate.inputView = exportTypePicker;
        if (![_txtFormate hasText]) {
            _txtFormate.text = [exportTypeArray objectAtIndex:0];
        }
    }
}

#pragma mark - Textfield keyboard delegate
#pragma mark Keyboard delegate
-(void)onPeriodFromPrevious{
    [_txtFormate becomeFirstResponder];
}
-(void)onPeriodFromNext{
    [_txtPeriodToValue becomeFirstResponder];
}
-(void)onPeriodFromDone{
    if (touched) {
        touched = false;
    }else{
        isDateFrom = 1;
        NSDateFormatter __autoreleasing *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
        [dateFormat setDateFormat:@"dd/MM/YYYY"];
        NSString *dateString =  [dateFormat stringFromDate:datePicker.date];
        _txtPeriodFromValue.text = dateString;
    }
    [_txtPeriodFromValue resignFirstResponder];
}

-(void)onPeriodToPrevious{
    [_txtPeriodFromValue becomeFirstResponder];
}
-(void)onPeriodToNext{
    [_txtFormate becomeFirstResponder];
}
-(void)onPeriodToDone{
    if (touched) {
        touched = false;
    }else{
        isDateFrom = 2;
        NSDateFormatter __autoreleasing *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
        [dateFormat setDateFormat:@"dd/MM/YYYY"];
        NSString *dateString =  [dateFormat stringFromDate:datePicker.date];
        _txtPeriodToValue.text = dateString;
        
    }
    [_txtPeriodToValue resignFirstResponder];
}

-(void)onFormatPrevious{
    [_txtPeriodToValue becomeFirstResponder];
}
-(void)onFormatNext{
    [_txtPeriodFromValue becomeFirstResponder];
}
-(void)onFormatDone{
    if (touched) {
        touched = false;
    }else{
        _txtFormate.text = [exportTypeArray objectAtIndex:0];
    }
    [_txtFormate resignFirstResponder];
}

#pragma mark - date picker
- (void)onDatePickerValueChanged:(UIDatePicker *)datePickers
{
    NSDateFormatter __autoreleasing *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormat setDateFormat:@"dd/MM/YYYY"];
    NSString *dateString =  [dateFormat stringFromDate:datePickers.date];
    if (isDateFrom == 1) {
        _txtPeriodFromValue.text = dateString;
    }else{
        _txtPeriodToValue.text = dateString;
    }
    touched = true;
}

#pragma mark - Picker View Data source
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
     return exportTypeArray.count;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component{
     [_txtFormate setText:[exportTypeArray objectAtIndex:row]];
    touched = true;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow: (NSInteger)row forComponent:(NSInteger)component{
     return [NSString stringWithFormat:@"%@",[exportTypeArray objectAtIndex:row]];
}

#pragma mark - checkbox functions
-(void)initializePrivateExport{
    [self DeSelectAllPrivateExport];
}

-(void)initializeBusibessExport{
    [self DeSelectAllBusibessExport];
}

-(void)initializeOverviewExport{
    [self DeSelectAllOverviewExport];
}

-(void)SelectAllPrivateExport{
    isPSelectAll = 1; isPType = 1; isPDate = 1; isPStartTime = 1; isPEndTime = 1; isPDistance = 1; isPOStart = 1; isPOEnd = 1; isPSAddress = 1; isPEAddress = 1; isPSCoordinate = 1; isPECoordinate = 1; isPReimbursement = 1; isPNote = 1;
    
    _imgPSelectAll.image = selectedImage;
    _imgPType.image = selectedImage;
    _imgPDate.image = selectedImage;
    _imgPStartTime.image = selectedImage;
    _imgPEndTime.image = selectedImage;
    _imgPDistance.image = selectedImage;
    _imgPOStart.image = selectedImage;
    _imgPOEnd.image = selectedImage;
    _imgPSAddress.image = selectedImage;
    _imgPEAddress.image = selectedImage;
    _imgPSCoordinate.image = selectedImage;
    _imgPECoordinate.image = selectedImage;
    _imgPReimbursement.image = selectedImage;
    _imgPNote.image = selectedImage;
    
    if ([userType isEqualToString:@"10"] || [userType isEqualToString:@"12"]) {
         strSelectedPrivateExport = [NSString stringWithFormat:@"trip_type,trip_date,start_time,end_time,distance,odometer_start,odometer_finish,start_address,end_address,srart_point,destination_point,reimbursement,notetrip"];
    }else{
        strSelectedPrivateExport = [NSString stringWithFormat:@"trip_type,trip_date,start_time,end_time,distance,odometer_start,odometer_finish,start_address,end_address,srart_point,destination_point,notetrip"];
    }
    tempSelectedPrivateExport = [selectedPrivateExport mutableCopy];
}

-(void)SelectAllBusibessExport{
    isBSelectAll = 1; isBAccountTpe = 1; isBDate = 1; isBName = 1; isBAmount = 1; isBCurrency = 1; isBNotes = 1; isBZip = 1;
    
    _imgBSelectAll.image = selectedImage;
    _imgBAccountTpe.image = selectedImage;
    _imgBDate.image = selectedImage;
    _imgBName.image = selectedImage;
    _imgBAmount.image = selectedImage;
    _imgBCurrency.image = selectedImage;
    _imgBNotes.image = selectedImage;
    _imgBZip.image = selectedImage;
    strSelectedBusinessExport = [NSString stringWithFormat:@"expense_account,expense_date,name,amount,currency,note,imagezip"];
    tempSelectedBusinessExport = [selectedBusinessExport mutableCopy];
}

-(void)SelectAllOverviewExport{
    isOSelectAll = 1; isOTripSplit = 1; isOFinanceSplit = 1; isOTtotal = 1;
    
    _imgOSelectAll.image = selectedImage;
    _imgOTripSplit.image = selectedImage;
    _imgOFinancialSplit.image = selectedImage;
    _imgOTotals.image = selectedImage;
    strSelectedOverviewExport = [NSString stringWithFormat:@"trip_split,finance_split,total_split"];
    tempSelectedOverviewExport = [selectedOverviewExport mutableCopy];
}


-(void)DeSelectAllPrivateExport{
    isPSelectAll = 0; isPType = 0; isPDate = 0; isPStartTime = 0; isPEndTime = 0; isPDistance = 0; isPOStart = 0; isPOEnd = 0; isPSAddress = 0; isPEAddress = 0; isPSCoordinate = 0; isPECoordinate = 0; isPReimbursement = 0; isPNote = 0;
    
    _imgPSelectAll.image = unSelectedImage;
    _imgPType.image = unSelectedImage;
    _imgPDate.image = unSelectedImage;
    _imgPStartTime.image = unSelectedImage;
    _imgPEndTime.image = unSelectedImage;
    _imgPDistance.image = unSelectedImage;
    _imgPOStart.image = unSelectedImage;
    _imgPOEnd.image = unSelectedImage;
    _imgPSAddress.image = unSelectedImage;
    _imgPEAddress.image = unSelectedImage;
    _imgPSCoordinate.image = unSelectedImage;
    _imgPECoordinate.image = unSelectedImage;
    _imgPReimbursement.image = unSelectedImage;
    _imgPNote.image = unSelectedImage;
    strSelectedPrivateExport = @"";
    [tempSelectedPrivateExport removeAllObjects];
}

-(void)DeSelectAllBusibessExport{
    isBSelectAll = 0; isBAccountTpe = 0; isBDate = 0; isBName = 0; isBAmount = 0; isBCurrency = 0; isBNotes = 0; isBZip = 0;
    
    _imgBSelectAll.image = unSelectedImage;
    _imgBAccountTpe.image = unSelectedImage;
    _imgBDate.image = unSelectedImage;
    _imgBName.image = unSelectedImage;
    _imgBAmount.image = unSelectedImage;
    _imgBCurrency.image = unSelectedImage;
    _imgBNotes.image = unSelectedImage;
    _imgBZip.image = unSelectedImage;
    strSelectedBusinessExport = @"";
    [tempSelectedBusinessExport removeAllObjects];
}

-(void)DeSelectAllOverviewExport{
    isOSelectAll = 0; isOTripSplit = 0; isOFinanceSplit = 0; isOTtotal = 0;
    
    _imgOSelectAll.image = unSelectedImage;
    _imgOTripSplit.image = unSelectedImage;
    _imgOFinancialSplit.image = unSelectedImage;
    _imgOTotals.image = unSelectedImage;
    strSelectedOverviewExport = @"";
    [tempSelectedOverviewExport removeAllObjects];
}

#pragma mark - Custom functions
-(BOOL)isValidData{
    
    BOOL isTokonValid = [self dateComparision:_txtPeriodFromValue.text andDate2:_txtPeriodToValue.text];
    
    if (_txtPeriodFromValue.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_select_fromdate",nil)];
        return false;
        
    }else if (_txtPeriodToValue.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_select_todate",nil)];
        return false;
        
    }else if (_txtFormate.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_select_format",nil)];
        return false;
    }else{
        if (isTokonValid) {
            NSLog(@"Valid");
            return true;
        }else{
            NSLog(@"Not valid");
            [Helper displayAlertView:@"" message:NSLocalizedString(@"date_message", @"")];
            return false;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)onSend:(id)sender {
    if ([self isValidData]){
        
    }
}

- (IBAction)onAddEmail:(id)sender {
    if (emailArray.count <= 2) {
        [emailArray addObject:@""];
        tblHeight = 150 + ((int)(emailArray.count - 1) * 70);
        _IBLayoutConstraintTblHeight.constant = tblHeight;
        [_tableview reloadData];
    }
}


#pragma mark - private export aciton

- (IBAction)onPSelectAll:(id)sender {
    if (isPSelectAll) {
        [self DeSelectAllPrivateExport];
    }else{
        [self SelectAllPrivateExport];
    }
    
}
- (IBAction)onPType:(id)sender{
    if (isPType) {
        isPType = 0;
        _imgPType.image = unSelectedImage;
    }else{
        isPType = 1;
        _imgPType.image = selectedImage;
    }
    [self PrivateValueAddRemove:[selectedPrivateExport objectAtIndex:0]];
}
- (IBAction)onPDate:(id)sender{
    if (isPDate) {
        isPDate = 0;
        _imgPDate.image = unSelectedImage;
    }else{
        isPDate = 1;
        _imgPDate.image = selectedImage;
    }
    [self PrivateValueAddRemove:[selectedPrivateExport objectAtIndex:1]];
    
}
- (IBAction)onPStartTime:(id)sender{
    if (isPStartTime) {
        isPStartTime = 0;
        _imgPStartTime.image = unSelectedImage;
        
    }else{
        isPStartTime = 1;
        _imgPStartTime.image = selectedImage;
    }
    [self PrivateValueAddRemove:[selectedPrivateExport objectAtIndex:2]];
}
- (IBAction)onPEndTime:(id)sender{
    
    if (isPEndTime) {
        isPEndTime = 0;
        _imgPEndTime.image = unSelectedImage;
        
    }else{
        isPEndTime = 1;
        _imgPEndTime.image = selectedImage;
    }
    [self PrivateValueAddRemove:[selectedPrivateExport objectAtIndex:3]];
}
- (IBAction)onPDistance:(id)sender{
    
    if (isPDistance) {
        isPDistance = 0;
        _imgPDistance.image = unSelectedImage;
        
    }else{
        isPDistance = 1;
        _imgPDistance.image = selectedImage;
    }
    [self PrivateValueAddRemove:[selectedPrivateExport objectAtIndex:4]];
}
- (IBAction)onPOStart:(id)sender{
    
    if (isPOStart) {
        isPOStart = 0;
        _imgPOStart.image = unSelectedImage;
        
    }else{
        isPOStart = 1;
        _imgPOStart.image = selectedImage;
    }
    [self PrivateValueAddRemove:[selectedPrivateExport objectAtIndex:5]];
}
- (IBAction)onPOEnd:(id)sender{
    
    if (isPOEnd) {
        isPOEnd = 0;
        _imgPOEnd.image = unSelectedImage;
    }else{
        isPOEnd = 1;
        _imgPOEnd.image = selectedImage;
    }
    [self PrivateValueAddRemove:[selectedPrivateExport objectAtIndex:6]];
}
- (IBAction)onPSAddress:(id)sender{
    
    if (isPSAddress) {
        isPType = 0;
        _imgPSAddress.image = unSelectedImage;
    }else{
        isPSAddress = 1;
        _imgPSAddress.image = selectedImage;
    }
    [self PrivateValueAddRemove:[selectedPrivateExport objectAtIndex:7]];
}
- (IBAction)onPEAddress:(id)sender{
    
    if (isPEAddress) {
        isPEAddress = 0;
        _imgPEAddress.image = unSelectedImage;
    }else{
        isPEAddress = 1;
        _imgPEAddress.image = selectedImage;
    }
    [self PrivateValueAddRemove:[selectedPrivateExport objectAtIndex:8]];
}
- (IBAction)onPSCoordinate:(id)sender{
    
    if (isPSCoordinate) {
        isPSCoordinate = 0;
        _imgPSCoordinate.image = unSelectedImage;
    }else{
        isPSCoordinate = 1;
        _imgPSCoordinate.image = selectedImage;
    }
    [self PrivateValueAddRemove:[selectedPrivateExport objectAtIndex:9]];
}
- (IBAction)onPECoordinate:(id)sender{
    
    if (isPECoordinate) {
        isPECoordinate = 0;
        _imgPECoordinate.image = unSelectedImage;
    }else{
        isPECoordinate = 1;
        _imgPECoordinate.image = selectedImage;
    }
    [self PrivateValueAddRemove:[selectedPrivateExport objectAtIndex:10]];
}

- (IBAction)onReimbursement:(id)sender {
    if (isPReimbursement) {
        isPReimbursement = 0;
        _imgPReimbursement.image = unSelectedImage;
    }else{
        isPReimbursement = 1;
        _imgPReimbursement.image = selectedImage;
    }
    [self PrivateValueAddRemove:[selectedPrivateExport objectAtIndex:11]];
}

- (IBAction)onPNote:(id)sender {
    if (isPNote) {
        isPNote = 0;
        _imgPNote.image = unSelectedImage;
    }else{
        isPNote = 1;
        _imgPNote.image = selectedImage;
    }
    
    if ([userType isEqualToString:@"10"] || [userType isEqualToString:@"12"]) {
        [self PrivateValueAddRemove:[selectedPrivateExport objectAtIndex:12]];
    }else{
        [self PrivateValueAddRemove:[selectedPrivateExport objectAtIndex:11]];
    }
}

-(void)PrivateValueAddRemove:(NSString *)val{
    if ([tempSelectedPrivateExport containsObject:val]) {
        NSLog(@"Removed : %@",val);
        [tempSelectedPrivateExport removeObject:val];
    }else{
        NSLog(@"Added : %@",val);
        [tempSelectedPrivateExport addObject:val];
    }
    
}

#pragma mark - Business export aciton

- (IBAction)onBSelectAll:(id)sender{
    if (isBSelectAll) {
        [self DeSelectAllBusibessExport];
    }else{
        [self SelectAllBusibessExport];
    }
}
- (IBAction)onBAccountTpe:(id)sender{
    if (isBAccountTpe) {
        isBAccountTpe = 0;
        _imgBAccountTpe.image = unSelectedImage;
    }else{
        isBAccountTpe = 1;
        _imgBAccountTpe.image = selectedImage;
    }
    [self BusinessValueAddRemove:[selectedBusinessExport objectAtIndex:0]];
}
- (IBAction)onBDate:(id)sender{
    
    if (isBDate) {
        isBDate = 0;
        _imgBDate.image = unSelectedImage;
    }else{
        isBDate = 1;
        _imgBDate.image = selectedImage;
    }
    [self BusinessValueAddRemove:[selectedBusinessExport objectAtIndex:1]];
    
}
- (IBAction)onBName:(id)sender{
    if (isBName) {
        isBName = 0;
        _imgBName.image = unSelectedImage;
    }else{
        isBName = 1;
        _imgBName.image = selectedImage;
    }
    [self BusinessValueAddRemove:[selectedBusinessExport objectAtIndex:2]];
}
- (IBAction)onBAmount:(id)sender{
    
    if (isBAmount) {
        isBAmount = 0;
        _imgBAmount.image = unSelectedImage;
    }else{
        isBAmount = 1;
        _imgBAmount.image = selectedImage;
    }
    [self BusinessValueAddRemove:[selectedBusinessExport objectAtIndex:3]];
}
- (IBAction)onBCurrency:(id)sender{
    
    if (isBCurrency) {
        isBCurrency = 0;
        _imgBCurrency.image = unSelectedImage;
    }else{
        isBCurrency = 1;
        _imgBCurrency.image = selectedImage;
    }
    [self BusinessValueAddRemove:[selectedBusinessExport objectAtIndex:4]];
}
- (IBAction)onBNotes:(id)sender{
    if (isBNotes) {
        isBNotes = 0;
        _imgBNotes.image = unSelectedImage;
    }else{
        isBNotes = 1;
        _imgBNotes.image = selectedImage;
    }
    [self BusinessValueAddRemove:[selectedBusinessExport objectAtIndex:5]];
}
- (IBAction)onBZip:(id)sender{
    if (isBZip) {
        isBZip = 0;
        _imgBZip.image = unSelectedImage;
    }else{
        isBZip = 1;
        _imgBZip.image = selectedImage;
    }
    [self BusinessValueAddRemove:[selectedBusinessExport objectAtIndex:6]];
}

-(void)BusinessValueAddRemove:(NSString *)val{
    if ([tempSelectedBusinessExport containsObject:val]) {
        NSLog(@"Removed : %@",val);
        [tempSelectedBusinessExport removeObject:val];
    }else{
        NSLog(@"Added : %@",val);
        [tempSelectedBusinessExport addObject:val];
    }
}

#pragma mark - Overview export aciton

-(void)onOSelectAll:(id)sender{
    if (isOSelectAll) {
        [self DeSelectAllOverviewExport];
    }else{
        [self SelectAllOverviewExport];
    }
}
-(void)onOTripSplit:(id)sender{
    if (isOTripSplit) {
        isOTripSplit = 0;
        _imgOTripSplit.image = unSelectedImage;
    }else{
        isOTripSplit = 1;
        _imgOTripSplit.image = selectedImage;
    }
    [self overviewValueAddRemove:[selectedOverviewExport objectAtIndex:0]];
}

-(void)onOFinancialSplit:(id)sender{
    if (isOFinanceSplit) {
        isOFinanceSplit = 0;
        _imgOFinancialSplit.image = unSelectedImage;
    }else{
        isOFinanceSplit = 1;
        _imgOFinancialSplit.image = selectedImage;
    }
    [self overviewValueAddRemove:[selectedOverviewExport objectAtIndex:1]];
}
-(void)onOTotals:(id)sender{
    if (isOTtotal) {
        isOTtotal = 0;
        _imgOTotals.image = unSelectedImage;
    }else{
        isOTtotal = 1;
        _imgOTotals.image = selectedImage;
    }
    [self overviewValueAddRemove:[selectedOverviewExport objectAtIndex:2]];
}

-(void)overviewValueAddRemove:(NSString *)val{
    if ([tempSelectedOverviewExport containsObject:val]) {
        NSLog(@"Removed : %@",val);
        [tempSelectedOverviewExport removeObject:val];
    }else{
        NSLog(@"Added : %@",val);
        [tempSelectedOverviewExport addObject:val];
    }
}

-(void)DateComaparFunc{
    
    BOOL isTokonValid = [self dateComparision:_txtPeriodFromValue.text andDate2:_txtPeriodToValue.text];
    if (isTokonValid) {
        NSLog(@"Valid");
    }else{
        NSLog(@"Not valid");
        [Helper displayAlertView:@"" message:NSLocalizedString(@"date_message", @"")];
    }
}


#pragma mark - button action
- (IBAction)onExport:(id)sender {
    if ([self isValidData]){
        //[self DateComaparFunc];
        
        if (selectedSegment == 1) {
            // all selected
            NSString *string1 = [selectedPrivateExport componentsJoinedByString:@","];
            NSString *string2 = [selectedBusinessExport componentsJoinedByString:@","];
            NSString *string3 = [selectedOverviewExport componentsJoinedByString:@","];
            selectedFields = [NSString stringWithFormat:@"%@,%@,%@",string1,string2,string3];
            
        }else if (selectedSegment == 2) {
            // private selected
            if (tempSelectedPrivateExport.count > 0) {
                NSString *string1 = [tempSelectedPrivateExport componentsJoinedByString:@","];
                selectedFields = [NSString stringWithFormat:@"%@",string1];
            }else{
                [Helper displayAlertView:@"" message:NSLocalizedString(@"error_select_private_fields",nil)];
                return;
            }
            
        }else if (selectedSegment == 3){
            //business selected
            if (tempSelectedBusinessExport.count > 0) {
                NSString *string1 = [tempSelectedBusinessExport componentsJoinedByString:@","];
                selectedFields = [NSString stringWithFormat:@"%@",string1];
            }else{
                [Helper displayAlertView:@"" message:NSLocalizedString(@"error_select_private_fields",nil)];
                return;
            }
        }else if (selectedSegment == 4){
            //business selected
            if (tempSelectedOverviewExport.count > 0) {
                NSString *string1 = [tempSelectedOverviewExport componentsJoinedByString:@","];
                selectedFields = [NSString stringWithFormat:@"%@",string1];
            }else{
                //[Helper displayAlertView:@"" message:NSLocalizedString(@"error_select_overview_fields",nil)];
                //return;
            }
        }
        
        NSString *emailStr = @"";
        for (int i = 0; i < emailArray.count; i++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
            emailCell = [_tableview cellForRowAtIndexPath:indexPath];
            if (emailCell.txtEmail.text.length > 0) {
                if (i == emailArray.count - 1) {
                    emailStr = [emailStr stringByAppendingString:[NSString stringWithFormat:@"%@",emailCell.txtEmail.text]];
                }else{
                    emailStr = [emailStr stringByAppendingString:[NSString stringWithFormat:@"%@,",emailCell.txtEmail.text]];
                }
            }
        }
        if ([emailStr length] > 0) {
            NSString *lastChar = [emailStr substringFromIndex:[emailStr length] - 1];
            if ([lastChar isEqualToString:@","]) {
                emailStr = [emailStr substringToIndex:[emailStr length] - 1];
            }
            NSLog(@"Email : %@",emailStr);
            
            NSString *userID = [Helper getPREF:@"userID"];
            NSDictionary *parameter=@{
                                      @"userid":userID,
                                      @"export_type":export_type.lowercaseString,
                                      @"from_date":_txtPeriodFromValue.text,
                                      @"to_date":_txtPeriodToValue.text,
                                      @"export_formate":_txtFormate.text.lowercaseString,
                                      @"selected_field":selectedFields,
                                      @"email":emailStr
                                      };
            //NSLog(@"parameter : %@",parameter);
            [self callSendExportDataWS:parameter];
            
        }else{
           [Helper displayAlertView:@"" message:NSLocalizedString(@"error_enter_email_id",nil)];
        }
    }
}

#pragma mark - Webservice call
-(void)callSendExportDataWS:(NSDictionary *)parameter{
    if(![self CheckNetworkConnection]){
       return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [WebService httpPostWithCustomDelegateURLString:@".export" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error)
     {
          [MBProgressHUD hideHUDForView:self.view animated:YES];
         if (error == nil) {
            
             
             NSError *localError = nil;
             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
             
             if (localError != nil) {
                 NSLog(@"%@",localError.description);
             }
             else {
                 NSDictionary *response = [parsedObject objectForKey:@"data"];
                 NSString* message = [response objectForKey:@"message"];
                 NSString* success = [response objectForKey:@"status"];
                 if ([success isEqualToString:@"Success"]) {
                     [Helper displayAlertView:@"" message:NSLocalizedString(message.lowercaseString, "")];
                 } else {
                     NSLog(@"error :: %@",message);
                     [Helper displayAlertView:@"" message:message];
                 }
             }
         }
     }];
}

#pragma mark - segment action
- (IBAction)SelctSegment:(id)sender{
    if([sender tag] == 1) {
        selectedSegment = 1;
        export_type = @"all";
        [_BtnAllSegment setBackgroundColor:[UIColor colorWithRed:0.000 green:0.647 blue:0.929 alpha:1.00]];
        [_BtnAllSegment.titleLabel setTextColor:[UIColor whiteColor]];
        
        [_BtnPrivateSegment setBackgroundColor:[UIColor clearColor]];
        [_BtnPrivateSegment.titleLabel setTextColor:[UIColor grayColor]];
        
        [_BtnBusinessSegment setBackgroundColor:[UIColor clearColor]];
        [_BtnBusinessSegment.titleLabel setTextColor:[UIColor grayColor]];
        
        [_BtnOverviewSegment setBackgroundColor:[UIColor clearColor]];
        [_BtnOverviewSegment.titleLabel setTextColor:[UIColor grayColor]];
        
        /*_IBLayoutConstraintPrivateExportViewTop.constant = 0;
        _IBLayoutConstraintPrivateExportViewHeight.constant = 0;
        _IBLayoutConstraintBusinessExportViewTop.constant = 0;
        _IBLayoutConstraintBusinessExportViewHeight.constant = 0;
        _IBLayoutConstraintOverviewViewTop.constant = 0;
        _IBLayoutConstraintOverviewViewHeight.constant = 0;*/
        
        [self hideAll];
        
    }else if ([sender tag] == 2) {
        selectedSegment = 2;
        export_type = @"trips";
        [_BtnPrivateSegment setBackgroundColor:[UIColor colorWithRed:0.000 green:0.647 blue:0.929 alpha:1.00]];
        [_BtnPrivateSegment.titleLabel setTextColor:[UIColor whiteColor]];
        [_BtnPrivateSegment setTintColor: [UIColor whiteColor]];
        
        [_BtnAllSegment setBackgroundColor:[UIColor clearColor]];
        [_BtnAllSegment.titleLabel setTextColor:[UIColor grayColor]];
        
        [_BtnBusinessSegment setBackgroundColor:[UIColor clearColor]];
        [_BtnBusinessSegment.titleLabel setTextColor:[UIColor grayColor]];
        
        [_BtnOverviewSegment setBackgroundColor:[UIColor clearColor]];
        [_BtnOverviewSegment.titleLabel setTextColor:[UIColor grayColor]];
        
        // show private export view
        /*_IBLayoutConstraintPrivateExportViewTop.constant = 10;
        _IBLayoutConstraintPrivateExportViewHeight.constant = 476;
        _IBLayoutConstraintBusinessExportViewTop.constant = 0;
        _IBLayoutConstraintBusinessExportViewHeight.constant = 0;
        _IBLayoutConstraintOverviewViewTop.constant = 0;
        _IBLayoutConstraintOverviewViewHeight.constant = 0;*/
        
        _viewPrivateExport.hidden = false;
        _viewBusinessExport.hidden = true;
        _viewOverviewExport.hidden = true;
        
        if ([userType isEqualToString:@"10"] || [userType isEqualToString:@"12"]) {
            _IBLayoutConstraintTableviewTop.constant = 576;
        }else{
            _IBLayoutConstraintTableviewTop.constant = 546;
        }
        _lblPrivateBottom.hidden = false;
        _lblBusinessBottom.hidden = true;
        _lblOverviewBottom.hidden = true;

    }
    else if ([sender tag] == 3) {
        selectedSegment = 3;
        export_type = @"expense";
        [_BtnBusinessSegment setBackgroundColor:[UIColor colorWithRed:0.000 green:0.647 blue:0.929 alpha:1.00]];
        [_BtnBusinessSegment.titleLabel setTextColor:[UIColor whiteColor]];
        [_BtnBusinessSegment setTintColor: [UIColor whiteColor]];
        
        [_BtnPrivateSegment setBackgroundColor:[UIColor clearColor]];
        [_BtnPrivateSegment.titleLabel setTextColor:[UIColor grayColor]];
        
        [_BtnAllSegment setBackgroundColor:[UIColor clearColor]];
        [_BtnAllSegment.titleLabel setTextColor:[UIColor grayColor]];
        
        [_BtnOverviewSegment setBackgroundColor:[UIColor clearColor]];
        [_BtnOverviewSegment.titleLabel setTextColor:[UIColor grayColor]];
        
        // show business export view
        /*_IBLayoutConstraintPrivateExportViewTop.constant = 0;
        _IBLayoutConstraintPrivateExportViewHeight.constant = 0;
        _IBLayoutConstraintBusinessExportViewTop.constant = 10;
        _IBLayoutConstraintBusinessExportViewHeight.constant = 336;
        _IBLayoutConstraintOverviewViewTop.constant = 0;
        _IBLayoutConstraintOverviewViewHeight.constant = 0;*/
        
        _viewPrivateExport.hidden = true;
        _viewBusinessExport.hidden = false;
        _viewOverviewExport.hidden = true;
        _IBLayoutConstraintTableviewTop.constant = 356;
        
        _lblPrivateBottom.hidden = true;
        _lblBusinessBottom.hidden = false;
        _lblOverviewBottom.hidden = true;
        
    }else if ([sender tag] == 4) {
        selectedSegment = 4;
        export_type = @"overview";
        [_BtnOverviewSegment setBackgroundColor:[UIColor colorWithRed:0.000 green:0.647 blue:0.929 alpha:1.00]];
        [_BtnOverviewSegment.titleLabel setTextColor:[UIColor whiteColor]];
        [_BtnOverviewSegment setTintColor: [UIColor whiteColor]];
        
        [_BtnPrivateSegment setBackgroundColor:[UIColor clearColor]];
        [_BtnPrivateSegment.titleLabel setTextColor:[UIColor grayColor]];
        
        [_BtnBusinessSegment setBackgroundColor:[UIColor clearColor]];
        [_BtnBusinessSegment.titleLabel setTextColor:[UIColor grayColor]];
        
        [_BtnAllSegment setBackgroundColor:[UIColor clearColor]];
        [_BtnAllSegment.titleLabel setTextColor:[UIColor grayColor]];
        
        // show business export view
       /* _IBLayoutConstraintPrivateExportViewTop.constant = 0;
        _IBLayoutConstraintPrivateExportViewHeight.constant = 0;
        _IBLayoutConstraintBusinessExportViewTop.constant = 0;
        _IBLayoutConstraintBusinessExportViewHeight.constant = 0;
        _IBLayoutConstraintOverviewViewTop.constant = 10;
        _IBLayoutConstraintOverviewViewHeight.constant = 196;*/
        
        _viewPrivateExport.hidden = true;
        _viewBusinessExport.hidden = true;
        _viewOverviewExport.hidden = false;
        _IBLayoutConstraintTableviewTop.constant = 216;
        
        _lblPrivateBottom.hidden = true;
        _lblBusinessBottom.hidden = true;
        _lblOverviewBottom.hidden = false;
    }
}

@end
