//
//  EmailTVC.m
//  SATCAR
//
//  Created by Percept Infotech on 29/12/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import "EmailTVC.h"

@implementation EmailTVC
@synthesize viewBack,txtEmail;
- (void)awakeFromNib {
    [super awakeFromNib];
    viewBack.layer.cornerRadius = 5;
    viewBack.layer.borderWidth = 1;
    viewBack.layer.borderColor = [UIColor colorWithRed:0.902 green:0.902 blue:0.910 alpha:1.00].CGColor;
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 15)];
    txtEmail.leftView = paddingView;
    txtEmail.leftViewMode = UITextFieldViewModeAlways;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
