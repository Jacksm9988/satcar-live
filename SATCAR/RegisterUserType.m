//
//  RegisterUserType.m
//  SATCAR
//
//  Created by Percept Infotech on 23/01/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import "RegisterUserType.h"
#import "RegisterViewController.h"
#import "RegisterStandardUserViewController.h"

@interface RegisterUserType () {
    UIColor *bluecolor;
    float btnRadius;
    NSString *status;
    int selectedType;
    UIImage *selectedImage,*unSelectedImage;
    UIColor *selectedBorderColor,*unSelectedBorderColor;
    UIColor *selectedBackColor,*unSelectedBackColor;
    
}
@end

@implementation RegisterUserType

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addTitleView:NSLocalizedString(@"Whattypeofuserareyou", @"")];
    selectedType = 0;
    [Helper setBorderWithColorInButton:_btnNext];
    
    _lblFreeMsg.text = NSLocalizedString(@"Afreeusergivesyoujust30free", @"");
    _LblstandardMsg.text = NSLocalizedString(@"Standardkorebogisforusers", @"");
    _lblSplitMsg.text = NSLocalizedString(@"Splitleasingisforusers", @"");
}

-(void)viewWillAppear:(BOOL)animated{
    selectedImage = [UIImage imageNamed:@"Ic_NewCheck.png"];;
    unSelectedImage = [UIImage imageNamed:@"IC_NewUncheck.png"];
    [self allUnselected];
    
    selectedType = 1;
    [self onSelectUserType];
    
    self.navigationController.navigationBarHidden = false;
    NSData *userdata = [Helper getDataValue:@"userInfo"];
    if (userdata != nil) {
        NSDictionary *dataDict = [self dataToDicitonary:userdata];
        if (![dataDict isKindOfClass:[NSNull class]]) {
            NSDictionary *userInfo = [dataDict valueForKey:@"data"];
            if (![userInfo isKindOfClass:[NSNull class]]) {
                NSDictionary *payment_info = [userInfo objectForKey:@"payment_info"];
                if (![payment_info isKindOfClass:[NSNull class]]) {
                    status = [payment_info valueForKey:@"status"];
                    if (status == (NSString *)[NSNull null]) {
                        status = @"";
                        [Helper setPREFStringValue:status sKey:@"userStatus"];
                    }else{
                        [Helper setPREFStringValue:status sKey:@"userStatus"];
                    }
                    
                    if (![status.lowercaseString isEqualToString:@"paid"]) {
                        _IBLayoutConstraintSatndarTop.constant = 20;
                        _viewFree.hidden = true;
                    }else{
                        _IBLayoutConstraintviewFreeHeight.constant = 170;
                        _viewFree.hidden = false;
                    }
                }else{
                    [Helper displayAlertView:@"" message:NSLocalizedString(@"payment_detail_not_found", @"")];
                }
            }else{
                [Helper displayAlertView:@"" message:NSLocalizedString(@"user_detail_not_found", @"")];
            }
        }else{
            [Helper displayAlertView:@"" message:NSLocalizedString(@"something_went_wrong_msg", @"")];
        }
    }else{
        _IBLayoutConstraintSatndarTop.constant = 170;
         _viewFree.hidden = false;
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    if ([status.lowercaseString isEqualToString:@"pending"]) {
        self.navigationController.navigationBarHidden = true;
    }else{
        self.navigationController.navigationBarHidden = false;
    }
}

#pragma mark - Custom functions
-(void)allUnselected{
    
    unSelectedBorderColor = [UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:244.0/255.0 alpha:1.00];
    selectedBorderColor = [UIColor colorWithRed:214.0/255.0 green:215.0/255.0 blue:216.0/255.0 alpha:1.00];
    
    unSelectedBackColor = [UIColor colorWithRed:253.0/255.0 green:253.0/255.0 blue:253.0/255.0 alpha:1.00];
    selectedBackColor = [UIColor colorWithRed:243.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.00];
    
    _viewFreeBack.layer.borderWidth = 1;
    _viewStandardBack.layer.borderWidth = 1;
    _viewSplitBack.layer.borderWidth = 1;
    
    _viewFreeBack.layer.borderColor = unSelectedBorderColor.CGColor;
    _viewStandardBack.layer.borderColor = unSelectedBorderColor.CGColor;
    _viewSplitBack.layer.borderColor = unSelectedBorderColor.CGColor;
    
    _viewFreeBack.backgroundColor = unSelectedBackColor;
    _viewStandardBack.backgroundColor = unSelectedBackColor;
    _viewSplitBack.backgroundColor = unSelectedBackColor;
    
    _imgFree.image = unSelectedImage;
    _imgStandard.image = unSelectedImage;
    _imgvSplit.image = unSelectedImage;
    
    _lblFreePrice.textColor = [UIColor blackColor];
    _lblStandardPrice.textColor = [UIColor blackColor];
    _lblSplitPrice.textColor = [UIColor blackColor];
}

-(void)onSelectUserType{
    if (selectedType == 1) {
        
        _imgFree.image = selectedImage;
        _imgStandard.image = unSelectedImage;
        _imgvSplit.image = unSelectedImage;
        
        _lblFreePrice.textColor = [Helper greenColor];
        _lblStandardPrice.textColor = [UIColor blackColor];
        _lblSplitPrice.textColor = [UIColor blackColor];
        
        _viewFreeBack.backgroundColor = selectedBackColor;
        _viewStandardBack.backgroundColor = unSelectedBackColor;
        _viewSplitBack.backgroundColor = unSelectedBackColor;
        
        _viewFreeBack.layer.borderColor = selectedBorderColor.CGColor;
        _viewStandardBack.layer.borderColor = unSelectedBorderColor.CGColor;
        _viewSplitBack.layer.borderColor = unSelectedBorderColor.CGColor;
        
        
    }else if (selectedType == 2){
        _imgFree.image = unSelectedImage;
        _imgStandard.image = selectedImage;
        _imgvSplit.image = unSelectedImage;
        
        _lblFreePrice.textColor = [UIColor blackColor];
        _lblStandardPrice.textColor = [Helper greenColor];
        _lblSplitPrice.textColor = [UIColor blackColor];
        
        _viewFreeBack.backgroundColor = unSelectedBackColor;
        _viewStandardBack.backgroundColor = selectedBackColor;
        _viewSplitBack.backgroundColor = unSelectedBackColor;
        
        _viewFreeBack.layer.borderColor = unSelectedBorderColor.CGColor;
        _viewStandardBack.layer.borderColor = selectedBorderColor.CGColor;
        _viewSplitBack.layer.borderColor = unSelectedBorderColor.CGColor;
    }else{
        
        _imgFree.image = unSelectedImage;
        _imgStandard.image = unSelectedImage;
        _imgvSplit.image = selectedImage;
        
        _lblFreePrice.textColor = [UIColor blackColor];
        _lblStandardPrice.textColor = [UIColor blackColor];
        _lblSplitPrice.textColor = [Helper greenColor];
        
        _viewFreeBack.backgroundColor = unSelectedBackColor;
        _viewStandardBack.backgroundColor = unSelectedBackColor;
        _viewSplitBack.backgroundColor = selectedBackColor;
        
        _viewFreeBack.layer.borderColor = unSelectedBorderColor.CGColor;
        _viewStandardBack.layer.borderColor = unSelectedBorderColor.CGColor;
        _viewSplitBack.layer.borderColor = selectedBorderColor.CGColor;    }
}

#pragma mark - Button actions
- (IBAction)onFreeUserPress:(id)sender{
    selectedType = 1;
    [self onSelectUserType];
}

- (IBAction)onStandarUserPress:(id)sender{
    selectedType = 2;
    [self onSelectUserType];
}

- (IBAction)onSplitleasingPress:(id)sender{
    selectedType = 3;
    [self onSelectUserType];
}

- (IBAction)onNext:(id)sender {
    if (selectedType == 1) {
        [self performSegueWithIdentifier:@"segueUserRegister" sender:self];
    }else if (selectedType == 2){
        [self performSegueWithIdentifier:@"segueRegisterStandard" sender:self];
    }else if (selectedType == 3){
        [self performSegueWithIdentifier:@"segueRegisterSplit" sender:self];
    }else{
        [Helper displayAlertView:@"" message:NSLocalizedString(@"user_type_msg", @"")];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"segueUserRegister"]) {
        RegisterViewController *controller = segue.destinationViewController;
        controller.userFrom = @"free";
    }else if([segue.identifier isEqualToString:@"segueRegisterStandard"]) {
        RegisterStandardUserViewController *controller = segue.destinationViewController;
        controller.userFrom = @"standard";
    }else{
        RegisterStandardUserViewController *controller = segue.destinationViewController;
        controller.userFrom = @"split";
    }
}
@end
