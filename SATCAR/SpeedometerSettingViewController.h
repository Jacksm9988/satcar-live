//
//  SpeedometerSettingViewController.h
//  SATCAR
//
//  Created by Percept Infotech on 24/02/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpeedometerSettingViewController : UIViewController<UIAlertViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate>
{
    NSString *current_speedometer,*initial_speedometer,*speed_image;
    NSData *imageData;
}
@property (weak, nonatomic) IBOutlet UILabel *LblinitialSpeed;
@property (weak, nonatomic) IBOutlet UILabel *LblCurrentSpeed;
@property (weak, nonatomic) IBOutlet UILabel *LblImageDocument;
@property (weak, nonatomic) IBOutlet UITextField *txtInitialSpeed;
@property (weak, nonatomic) IBOutlet UITextField *txtCurrentSpeed;
- (IBAction)onImageDoc:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnAddImage;
@property (weak, nonatomic) IBOutlet UIImageView *imgSpeed;

@end
