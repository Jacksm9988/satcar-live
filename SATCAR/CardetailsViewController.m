//
//  CardetailsViewController.m
//  SATCAR
//
//  Created by Percept Infotech on 30/12/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import "CardetailsViewController.h"
#import "UIViewController+VCHelpers.h"
#import "ECSlidingViewController.h"
#import "LeftSideViewController.h"
#import "NavigationController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "UIViewController+VCHelpers.h"

@interface CardetailsViewController ()<UITextFieldDelegate>

@end

@implementation CardetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [self addTitleView:NSLocalizedString(@"Cardetails", @"")];
    
    _lblRegNumber.text = NSLocalizedString(@"RegNumber", @"");
    _lblFirstRegDate.text = NSLocalizedString(@"FirstRegDate", @"");
    _lblColor.text = NSLocalizedString(@"Color", @"");
    
    _lblVIN.text = NSLocalizedString(@"VIN", @"");
    _lblKM.text = NSLocalizedString(@"Kilometer", @"");
    _lblLeasingCo.text = NSLocalizedString(@"LeasingCompany", @"");
    [self getCarDetail];
}

- (void)getCarDetail{
    
    NSString *tracker_id = [Helper getPREF:@"tracker_id"];
    NSDictionary *parameter=@{ @"tracker_id":tracker_id };
    //NSLog(@"parameter : %@",parameter);
    [self callGetCarDetailWS:parameter];
}

-(void)callGetCarDetailWS:(NSDictionary *)parameter{
    if(![self CheckNetworkConnection])
    {
        //[self AlertNoInternet:NSLocalizedString(@"NoInternet", @"") tag:454869];
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [WebService httpPostWithCustomDelegateURLStringForCar:@".getCarDetail" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         if (error == nil) {
             
             NSError *localError = nil;
             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
             
             if (localError != nil) {
                 NSLog(@"%@",localError.description);
             }
             else {
                 NSLog(@"Response : %@",parsedObject);
                 NSDictionary *response = [parsedObject objectForKey:@"data"];
                 NSDictionary *cardetail = [response objectForKey:@"CarDetail"];
                 NSString* message = [response objectForKey:@"message"];
                 NSString* success = [response objectForKey:@"status"];
                 if ([success isEqualToString:@"Success"]) {
                     _txtKM.text = [cardetail valueForKey:@"car_km"];
                     _txtRegNumber.text = [cardetail valueForKey:@"registration_number"];
                     _txtFirstRegDate.text = [cardetail valueForKey:@"carfirst_registration"];
                     _txtColor.text = [cardetail valueForKey:@"color"];
                     _txtVIN.text = [cardetail valueForKey:@"car_vin"];
                     _txtLeasingCo.text = [cardetail valueForKey:@"company"];
                 } else {
                     NSLog(@"error :: %@",message);
                     //[Helper displayAlertView:@"" message:message];
                 }
             }
         }
     }];
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    textField.textColor =  [UIColor colorWithRed:18.0/255.0 green:172.0/255.0 blue:243.0/255.0 alpha:1.0];
    textField.layer.borderWidth = 1;
    textField.layer.borderColor =  [UIColor colorWithRed:18.0/255.0 green:172.0/255.0 blue:243.0/255.0 alpha:1.0].CGColor;
    textField.clipsToBounds = YES;
    return true;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    textField.textColor =  [UIColor blackColor];
    textField.layer.borderWidth = 0;
    textField.layer.borderColor = [UIColor clearColor].CGColor;
    textField.clipsToBounds = YES;
    return true;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
