//
//  GeneralSettingViewController.h
//  SATCAR
//
//  Created by Percept Infotech on 30/12/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GeneralSettingViewController : UIViewController
@property (weak, nonatomic) IBOutlet UISwitch *SwitchEmailNotif;
@property (weak, nonatomic) IBOutlet UITextField *txtMeasure;
@property (weak, nonatomic) IBOutlet UITextField *txtCurrency;
@property (weak, nonatomic) IBOutlet UILabel *lblMeasure;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrency;
- (IBAction)onEmailNotif:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblemail;
@property (weak, nonatomic) IBOutlet UILabel *lblLanguage;
@property (weak, nonatomic) IBOutlet UITextField *txtLanguage;
@property (weak, nonatomic) IBOutlet UILabel *lblNotification;
@property (weak, nonatomic) IBOutlet UISwitch *switchNotification;
- (IBAction)onNotification:(id)sender;

@end
