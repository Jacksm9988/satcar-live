//
//  TracerDetailsTVC.h
//  SATCAR
//
//  Created by Percept Infotech on 13/01/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TracerDetailsTVC : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTripColor;
@property (weak, nonatomic) IBOutlet UILabel *lblLatitude;
@property (weak, nonatomic) IBOutlet UILabel *lblLongitude;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;

@end
