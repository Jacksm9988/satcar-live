//
//  TrackersDetailsViewController.h
//  SATCAR
//
//  Created by Percept Infotech on 30/12/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrackersDetailsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lblTrackerSerial;
@property (weak, nonatomic) IBOutlet UILabel *lblTrackerActive;
@property (weak, nonatomic) IBOutlet UILabel *lblAlarmCenter;
@property (weak, nonatomic) IBOutlet UILabel *lblGeofence;
@property (weak, nonatomic) IBOutlet UILabel *lblLiveMode;
@property (weak, nonatomic) IBOutlet UILabel *lblAlarm;
@property (weak, nonatomic) IBOutlet UILabel *lblLog;

@property (weak, nonatomic) IBOutlet UISwitch *switchLiveMode;
@property (weak, nonatomic) IBOutlet UISwitch *switchGeofence;
@property (weak, nonatomic) IBOutlet UISwitch *switchAlaram;
- (IBAction)onLog:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblTrackerSerialValue;
@property (weak, nonatomic) IBOutlet UILabel *lblTrackerActiveValue;
@property (weak, nonatomic) IBOutlet UILabel *lblAlarmCenterValue;
- (IBAction)onGeofence:(id)sender;
- (IBAction)onLiveMode:(id)sender;
- (IBAction)onAlaram:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintPopupHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintPopupTop;
@property (weak, nonatomic) IBOutlet UILabel *lblGeoDefence;
@property (weak, nonatomic) IBOutlet UITextField *txtGeoDefenceValue;
@property (weak, nonatomic) IBOutlet UIButton *btnOK;
@property (weak, nonatomic) IBOutlet UIButton *bntCancel;
- (IBAction)onCancel:(id)sender;
- (IBAction)onOK:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewBack;
@property (weak, nonatomic) IBOutlet UIView *viewGeoBack;
@property (weak, nonatomic) IBOutlet UITextField *txtTrackerSerialValue;

@end
