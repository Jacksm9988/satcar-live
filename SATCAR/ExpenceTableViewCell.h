//
//  ExpenceTableViewCell.h
//  SATCAR
//
//  Created by Percept Infotech on 04/01/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExpenceTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *viewBack;
@property (weak, nonatomic) IBOutlet UILabel *lblTripColor;
@property (weak, nonatomic) IBOutlet UILabel *lblTripDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTripName;
@property (weak, nonatomic) IBOutlet UILabel *lblTripMeter;
@property (weak, nonatomic) IBOutlet UIView *viewName;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintViewNameHeight;
@property (weak, nonatomic) IBOutlet UIImageView *imgUser;

@end
