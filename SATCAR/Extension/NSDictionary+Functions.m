//
//  NSDictionary+Functions.m
//  CameraTest
//  Copyright © 2017 Affectiva. All rights reserved.
//

#import "NSDictionary+Functions.h"

@implementation NSDictionary (Functions)
-(NSString *)getDictStringValue:(NSString *)key{
    
    if (![[self objectForKey:key] isKindOfClass:[NSNull class]]) {
        return [self valueForKey:key];
    }else{
        return @"";
    }
}

- (id)safeObjectForKey:(id)aKey {
    NSObject *object = self[aKey];
    
    if (object == [NSNull null]) {
        return nil;
    }
    
    return object;
}

-(NSDictionary *)getDictionaryValue:(NSString *)key{
    NSDictionary *data = [[NSDictionary alloc]init];
    if (![[self objectForKey:key] isKindOfClass:[NSNull class]]) {
        data = [[self valueForKey:key] mutableCopy];
    }
    return data;
}
@end
