//
//  NSDictionary+Functions.h
//  CameraTest
//
//  Copyright © 2017 Affectiva. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Functions)
-(NSString *)getDictStringValue:(NSString *)key;
-(NSDictionary *)getDictionaryValue:(NSString *)key;
- (id)safeObjectForKey:(id)aKey;
@end
