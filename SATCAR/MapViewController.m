//
//  MapViewController.m
//  SATCAR
//
//  Created by Percept Infotech on 25/01/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import "MapViewController.h"
#import <GoogleMapsBase/GoogleMapsBase.h>
#import "CustomeAnnotation.h"
#import "CallOutAnnotationView.h"
#import "RouteObject.h"

@interface MapViewController ()<MKMapViewDelegate> {
    NSArray *snapArray;
    int counter,snapLimit;
    BOOL firstPoint;
    BOOL loaded;
    int start,stop,called;
    MKPolyline *routeLine;
    AppDelegate *appdelegate;
    RouteObject *route;
    NSString *saved_locations;
}
@property (nonatomic, strong) CallOutAnnotationView *calloutView;
@property (nonatomic, strong) NSArray *trackedLocations;

@end

@implementation MapViewController
@synthesize mapview,locationDataDictionary;

- (void)viewDidLoad {
    [super viewDidLoad];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    mapview.delegate = self;
    [self addTitleView:NSLocalizedString(@"Location", @"")];
    snapLimit = 10;
    loaded = false;
    appdelegate.isRelaodTrip = @"0";
    start = 0;
    stop = 0;
}

-(void)viewWillAppear:(BOOL)animated{
    _IBLayoutConstraintMapBottom.constant = 0;
    [self snapToRoaDSelected ];
    start = 0;
    stop = 0;
    called = 0;
    if ([_fromScreen isEqualToString:@"TripMap"]) {
        _btnOrignalTracked.hidden = false;
        _btnSnapToRoad.hidden = false;
        _IBLayoutConstraintMapBottom.constant = 50;
        mapview.delegate = self;
        [self callGetLocationService];
    }else{
        _btnOrignalTracked.hidden = true;
        _btnSnapToRoad.hidden = true;
        NSString *address = [NSString stringWithFormat:@"%@,%@,%@",[locationDataDictionary valueForKey:@"address"],[locationDataDictionary valueForKey:@"zip_and_city"],[locationDataDictionary valueForKey:@"country"]];
        [self getLocationFromAddressString:address];
    }
}

-(CLLocationCoordinate2D) getLocationFromAddressString: (NSString*) addressStr {
    
    double latitude = 0, longitude = 0;
    
    // Deprecated
    //NSString *esc_addr =  [addressStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
    NSString *encodedString = [addressStr stringByAddingPercentEncodingWithAllowedCharacters:set];
    NSString *GapiKey = appdelegate.googleApiKey;
    //NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", encodedString];
    NSString *req = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?address=%@+CA&key=%@", encodedString,GapiKey];
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    if (result) {
        NSScanner *scanner = [NSScanner scannerWithString:result];
        if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
            [scanner scanDouble:&latitude];
            if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
                [scanner scanDouble:&longitude];
            }
        }
    }
    
    CLLocationCoordinate2D center;
    center.latitude=latitude;
    center.longitude = longitude;
    
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    CLLocationCoordinate2D myCoordinate;
    myCoordinate.latitude=center.latitude;
    myCoordinate.longitude= center.longitude;
    annotation.coordinate = myCoordinate;
    annotation.title = [locationDataDictionary valueForKey:@"address"];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [mapview addAnnotation:annotation];
    });

    MKCoordinateSpan span = {.latitudeDelta =  .1, .longitudeDelta =  .1};
    MKCoordinateRegion region = {center, span};
    [mapview setRegion:region animated:YES];
    return center;
}

#pragma mark - mapview delegate
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation{
}

-(void)mapView:(MKMapView *)mapView didAddOverlayRenderers:(NSArray<MKOverlayRenderer *> *)renderers{
}

-(void)newRoute:(NSString *)location{
    
    NSArray *locationArray = [location componentsSeparatedByString:@"|"];
    
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);     // Create the semaphore
    
    __block NSMutableArray *routes = [[NSMutableArray alloc] init];         // Arrays to store MKRoute objects and MKAnnotationPoint objects, so then we can draw them on the map
    __block NSMutableArray *annotations = [[NSMutableArray alloc] init];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        MKPlacemark *sourcePlacemark;
        for (int i = 0; i < locationArray.count; i++) {
            
            if (i > 19) {
                NSLog(@"I = %d",i);
                if (i % 20 == 0) {
                    NSLog(@"Draw");
                    NSString *latLong = [locationArray objectAtIndex:i];
                    NSArray *latlonArray = [latLong componentsSeparatedByString:@","];
                    
                    double lat = [[latlonArray objectAtIndex:0] doubleValue];
                    double lon = [[latlonArray objectAtIndex:1] doubleValue];
                    
                    CLLocationCoordinate2D sourceCoordinate = CLLocationCoordinate2DMake(lat, lon);
                    sourcePlacemark = [[MKPlacemark alloc] initWithCoordinate:sourceCoordinate addressDictionary:nil];
                    
                    
                    latLong = [locationArray objectAtIndex:i-20];
                    latlonArray = [latLong componentsSeparatedByString:@","];
                    
                    lat = [[latlonArray objectAtIndex:0] doubleValue];
                    lon = [[latlonArray objectAtIndex:1] doubleValue];
                    
                    CLLocationCoordinate2D destCoordinate = CLLocationCoordinate2DMake(lat, lon);
                    MKPlacemark *destPlacemark = [[MKPlacemark alloc] initWithCoordinate:destCoordinate addressDictionary:nil];
                    
                    MKDirectionsRequest *directionsRequest = [MKDirectionsRequest new];
                    [directionsRequest setTransportType:MKDirectionsTransportTypeAutomobile];
                    [directionsRequest setSource:[[MKMapItem alloc] initWithPlacemark:sourcePlacemark]];
                    [directionsRequest setDestination:[[MKMapItem alloc] initWithPlacemark:destPlacemark]];
                    [directionsRequest setRequestsAlternateRoutes:NO];
                    
                    MKDirections *direction = [[MKDirections alloc] initWithRequest:directionsRequest];
                    
                    // For each object in the locations array, we request that route from its origin and its destination
                    [direction calculateDirectionsWithCompletionHandler: ^(MKDirectionsResponse *response, NSError *error) {
                        if (error) {
                            NSLog(@"There was an error getting your directions");
                            return;
                        }
                        
                        
                        NSString *latLong = [locationArray objectAtIndex:locationArray.count - 1];
                        NSArray *latlonArray = [latLong componentsSeparatedByString:@","];
                        
                        double lat = [[latlonArray objectAtIndex:0] doubleValue];
                        double lon = [[latlonArray objectAtIndex:1] doubleValue];
                        CLLocation *loc = [[CLLocation alloc] initWithLatitude:lat longitude:lon];
                        
                        MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
                        CLLocationCoordinate2D myCoordinate;
                        myCoordinate.latitude=loc.coordinate.latitude;
                        myCoordinate.longitude= loc.coordinate.longitude;
                        annotation.coordinate = myCoordinate;
                        annotation.title = [locationDataDictionary valueForKey:@"address"];
                        
                        MKRoute *route = [response.routes firstObject];
                        [routes addObject:route];
                        [annotations addObject:annotation];
                        
                        dispatch_semaphore_signal(semaphore);    // Send the signal that one semaphore is ready to consume
                    }];
                }
                
            }
            
        }
    });
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        for (int i = 0; i < locationArray.count; i++) {
            dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);    // Wait until one semaphore is ready to consume
            
            dispatch_async(dispatch_get_main_queue(), ^{    // For each element, dispatch to the main queue to draw route and annotation corresponding to that location
                MKRoute *route = routes[i];
                [mapview addOverlay:route.polyline];
                [mapview addAnnotation:annotations[i]];
            });
        }
       
    });
}

// orignal lat long from apple and converted into google snap to road api
-(void)displayRouteFromTrackedLocations:(NSString *)locations{
    
    NSString *start_address = [NSString stringWithFormat:@"%@",[locationDataDictionary valueForKey:@"start_address"]];
    NSString *end_address = [NSString stringWithFormat:@"%@",[locationDataDictionary valueForKey:@"end_address"]];
    //NSString *locations1 = [NSString stringWithFormat:@"%@",[locationDataDictionary valueForKey:@"locations1"]];
    //NSLog(@"Location : %@",locations);
    NSLog(@"Data : %@",locationDataDictionary);
    
    NSArray *locationArray = [locations componentsSeparatedByString:@"|"];
    CLLocationCoordinate2D Coordinates[[locationArray count]];
    
    CLLocationCoordinate2D CoordinatesAll[[locationArray count]];
    for (int i = 0; i < locationArray.count; i++) {
        NSString *latLong = [locationArray objectAtIndex:i];
        NSArray *latlonArray = [latLong componentsSeparatedByString:@","];
       
        double lat = [[latlonArray objectAtIndex:0] doubleValue];
        double lon = [[latlonArray objectAtIndex:1] doubleValue];
        CLLocation *loc = [[CLLocation alloc] initWithLatitude:lat longitude:lon];
        Coordinates[i] = loc.coordinate;
        CoordinatesAll[i] = loc.coordinate;
        
        if (i == 0) {
            _calloutView = [[CallOutAnnotationView alloc] initWithName:start_address address:@"" phonenumber:@"" coordinate:Coordinates[i]];
            _calloutView.contentView.frame = CGRectMake(_calloutView.contentView.frame.origin.x, _calloutView.contentView.frame.origin.y, 10, 10);
             _calloutView.title = start_address;
            [mapview addAnnotation:_calloutView];
            
        }else if(i == locationArray.count - 1){
            CustomeAnnotation *point=[[CustomeAnnotation alloc] initWithLocation:Coordinates[i]];
            point.title =end_address;
            [mapview addAnnotation:point];
        }
        if (i > 0) {
            //NSLog(@"i = %d",i);
            CLLocationCoordinate2D Coordinates[2];
            Coordinates[0] = loc.coordinate;
            CLLocation *start = loc;
            latLong = [locationArray objectAtIndex:i-1];
            latlonArray = [latLong componentsSeparatedByString:@","];
            
            lat = [[latlonArray objectAtIndex:0] doubleValue];
            lon = [[latlonArray objectAtIndex:1] doubleValue];
            loc = [[CLLocation alloc] initWithLatitude:lat longitude:lon];
            CLLocation *end = loc;
            Coordinates[1] = loc.coordinate;
            
            /*CLLocationDistance currentDistance = [end distanceFromLocation:start];
            NSLog(@"current distance : %f",currentDistance);
            if (currentDistance > 500) {
                NSLog(@"Too much distance without location data");
                [self drawManualRouteIfLatLongNotAvailable:start end:end];
                //break;
            }else{
                MKPolyline *polyLine = [MKPolyline polylineWithCoordinates:Coordinates count: 2];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [mapview addOverlay:polyLine];
                });
            }*/
            MKPolyline *polyLine = [MKPolyline polylineWithCoordinates:Coordinates count: 2];
            dispatch_async(dispatch_get_main_queue(), ^{
                [mapview addOverlay:polyLine];
            });
        }
    }

    MKPolyline *polyLine = [MKPolyline polylineWithCoordinates:CoordinatesAll count:locationArray.count];
    //[mapview addOverlay:polyLine];
    MKCoordinateRegion r = MKCoordinateRegionForMapRect([polyLine boundingMapRect]);
    r.span.latitudeDelta += 0.015;
    r.span.longitudeDelta += 0.015;
    [mapview setRegion:r animated:YES];
}

// orignal lat long from apple
-(void)displayRouteFromTrackedLocationsByAppleMap:(NSString *)locations{
    
    NSString *start_address = [NSString stringWithFormat:@"%@",[locationDataDictionary valueForKey:@"start_address"]];
    NSString *end_address = [NSString stringWithFormat:@"%@",[locationDataDictionary valueForKey:@"end_address"]];
    NSString *locations1 = [NSString stringWithFormat:@"%@",[locationDataDictionary valueForKey:@"locations1"]];
    NSLog(@"Location : %@",locations);
    NSLog(@"locations1 : %@",locations1);
    
    NSArray *locationArray = [locations componentsSeparatedByString:@"|"];
    CLLocationCoordinate2D Coordinates[[locationArray count]];
    
    CLLocationCoordinate2D CoordinatesAll[[locationArray count]];
    for (int i = 0; i < locationArray.count; i++) {
        NSString *latLong = [locationArray objectAtIndex:i];
        NSArray *latlonArray = [latLong componentsSeparatedByString:@","];
        
        double lat = [[latlonArray objectAtIndex:0] doubleValue];
        double lon = [[latlonArray objectAtIndex:1] doubleValue];
        CLLocation *loc = [[CLLocation alloc] initWithLatitude:lat longitude:lon];
        Coordinates[i] = loc.coordinate;
        CoordinatesAll[i] = loc.coordinate;
        
        if (i == 0) {
            _calloutView = [[CallOutAnnotationView alloc] initWithName:start_address address:@"" phonenumber:@"" coordinate:Coordinates[i]];
            _calloutView.contentView.frame = CGRectMake(_calloutView.contentView.frame.origin.x, _calloutView.contentView.frame.origin.y, 10, 10);
            _calloutView.title = start_address;
            [mapview addAnnotation:_calloutView];
            
        }else if(i == locationArray.count - 1){
            CustomeAnnotation *point=[[CustomeAnnotation alloc] initWithLocation:Coordinates[i]];
            point.title =end_address;
            [mapview addAnnotation:point];
        }
        if (i > 0) {
            //NSLog(@"i = %d",i);
            CLLocationCoordinate2D Coordinates[2];
            Coordinates[0] = loc.coordinate;
            CLLocation *start = loc;
            latLong = [locationArray objectAtIndex:i-1];
            latlonArray = [latLong componentsSeparatedByString:@","];
            
            lat = [[latlonArray objectAtIndex:0] doubleValue];
            lon = [[latlonArray objectAtIndex:1] doubleValue];
            loc = [[CLLocation alloc] initWithLatitude:lat longitude:lon];
            CLLocation *end = loc;
            Coordinates[1] = loc.coordinate;
            
            /*CLLocationDistance currentDistance = [end distanceFromLocation:start];
             NSLog(@"current distance : %f",currentDistance);
             if (currentDistance > 500) {
             NSLog(@"Too much distance without location data");
             [self drawManualRouteIfLatLongNotAvailable:start end:end];
             //break;
             }else{
             MKPolyline *polyLine = [MKPolyline polylineWithCoordinates:Coordinates count: 2];
             dispatch_async(dispatch_get_main_queue(), ^{
             [mapview addOverlay:polyLine];
             });
             }*/
            MKPolyline *polyLine = [MKPolyline polylineWithCoordinates:Coordinates count: 2];
            dispatch_async(dispatch_get_main_queue(), ^{
                [mapview addOverlay:polyLine];
            });
        }
    }
    
    MKPolyline *polyLine = [MKPolyline polylineWithCoordinates:CoordinatesAll count:locationArray.count];
    //[mapview addOverlay:polyLine];
    MKCoordinateRegion r = MKCoordinateRegionForMapRect([polyLine boundingMapRect]);
    r.span.latitudeDelta += 0.005;
    r.span.longitudeDelta += 0.005;
    [mapview setRegion:r animated:YES];
}


#pragma mark - Mapview delegate
-(MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay {
    
    if ([overlay isKindOfClass:MKPolyline.class]) {
        MKPolylineRenderer *lineView = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
        lineView.strokeColor = [[UIColor alloc] initWithRed:0.0f green:174.0f/255.0f blue:239.0f/255.0f alpha:1];
        lineView.lineWidth = 3.5;
        return lineView;
        
    }else if ([overlay isKindOfClass:MKPolygon.class]){
        MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
        renderer.strokeColor = [UIColor magentaColor];
        renderer.lineWidth = 3.5;
        return renderer;
    }else{
        return nil;
    }
}

-(void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated{
}

// Deprecated
/*- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id<MKOverlay>)overlay {
    if ([overlay isKindOfClass:MKPolyline.class]) {
        MKPolylineView *lineView = [[MKPolylineView alloc] initWithOverlay:overlay];
        [lineView setStrokeColor:[UIColor colorWithRed:0.0f green:174.0f/255.0f blue:239.0f/255.0f alpha:1]];
        return lineView;
    } else if ([overlay isKindOfClass:MKPolygon.class]) {
        MKPolygonView *polygonView = [[MKPolygonView alloc] initWithOverlay:overlay];
        [polygonView setStrokeColor:[UIColor magentaColor]];
        return polygonView;
    }
    return nil;
}*/

-(void)drawRoute{
    
    NSString *startAddress = [locationDataDictionary valueForKey:@"srart_point"];
    NSString *endAddress = [locationDataDictionary valueForKey:@"destination_point"];
    NSArray *startArray = [startAddress componentsSeparatedByString:@","];
    NSArray *endtArray = [endAddress componentsSeparatedByString:@","];
    if (startArray.count > 1 && endtArray.count > 1) {
        double slat = [[startArray objectAtIndex:0]doubleValue];
        double slon = [[startArray objectAtIndex:1]doubleValue];
        double elat = [[endtArray objectAtIndex:0]doubleValue];
        double elon = [[endtArray objectAtIndex:1]doubleValue];
        
        MKDirectionsRequest *directionsRequest = [[MKDirectionsRequest alloc] init];
        
        CLLocationCoordinate2D sourceCoordinate = CLLocationCoordinate2DMake(slat, slon);
        MKPlacemark *sourcePlacemark = [[MKPlacemark alloc] initWithCoordinate:sourceCoordinate addressDictionary:nil];
        [directionsRequest setSource:[[MKMapItem alloc] initWithPlacemark:sourcePlacemark]];
        
        CLLocationCoordinate2D destinationCoordinate = CLLocationCoordinate2DMake(elat, elon);
        MKPlacemark *destinationPlacemark = [[MKPlacemark alloc] initWithCoordinate:destinationCoordinate addressDictionary:nil];
        [directionsRequest setDestination:[[MKMapItem alloc] initWithPlacemark:destinationPlacemark]];
        
        MKDirections *directions = [[MKDirections alloc] initWithRequest:directionsRequest];
        
        // Requesting route information from Apple Map services
        [directions calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse *response, NSError *error) {
            if (error) {
                //NSLog(@"Cannot calculate directions: %@",[error localizedDescription]);
            } else {
                // Displaying the route on the map
                MKRoute *route = [response.routes firstObject];
                [mapview addOverlay:route.polyline];
            }
        }];
    }
}

-(void)drawMapWithDestination{
    
    NSString *startAddress,*endAddress;
    NSArray *startArray,*endtArray;
    
    startAddress = [locationDataDictionary valueForKey:@"srart_point"];
    endAddress = [locationDataDictionary valueForKey:@"destination_point"];
    
    startArray = [startAddress componentsSeparatedByString:@","];
    endtArray = [endAddress componentsSeparatedByString:@","];
    
    double slat,slon,elat,elon;
    if (startArray.count > 1 && endtArray.count > 1) {
        slat = [[startArray objectAtIndex:0]doubleValue];
        slon = [[startArray objectAtIndex:1]doubleValue];
        elat = [[endtArray objectAtIndex:0]doubleValue];
        elon = [[endtArray objectAtIndex:1]doubleValue];
    }else{
        [Helper displayAlertView:@"" message:@"Location not found"];
        return;
    }
    
     MKPlacemark *source = [[MKPlacemark alloc]initWithCoordinate:CLLocationCoordinate2DMake(slat, slon) addressDictionary:[NSDictionary dictionaryWithObjectsAndKeys:@"",@"", nil] ];
     MKPlacemark *destination = [[MKPlacemark alloc]initWithCoordinate:CLLocationCoordinate2DMake(elat, elon) addressDictionary:[NSDictionary dictionaryWithObjectsAndKeys:@"",@"", nil] ];
    MKMapItem *srcMapItem = [[MKMapItem alloc]initWithPlacemark:source];
    [srcMapItem setName:[locationDataDictionary valueForKey:@"start_address"]];
    
    MKMapItem *distMapItem = [[MKMapItem alloc]initWithPlacemark:destination];
    [distMapItem setName:[locationDataDictionary valueForKey:@"end_address"]];
    
    _calloutView = [[CallOutAnnotationView alloc] initWithName:@"" address:[locationDataDictionary valueForKey:@"start_address"] phonenumber:@"" coordinate:source.location.coordinate];
    _calloutView.contentView.frame = CGRectMake(_calloutView.contentView.frame.origin.x, _calloutView.contentView.frame.origin.y, 10, 10);
    _calloutView.title = [locationDataDictionary valueForKey:@"start_address"] ;
    [mapview addAnnotation:_calloutView];
    
    CustomeAnnotation *point=[[CustomeAnnotation alloc] initWithLocation:destination.location.coordinate];
    point.title =[locationDataDictionary valueForKey:@"start_address"];
    [mapview addAnnotation:point];
    
    MKDirectionsRequest *request = [[MKDirectionsRequest alloc]init];
    [request setSource:srcMapItem];
    [request setDestination:distMapItem];
    [request setTransportType:MKDirectionsTransportTypeAutomobile];
    
    MKDirections *direction = [[MKDirections alloc]initWithRequest:request];
    
    [direction calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse *response, NSError *error) {
        
        NSArray *arrRoutes = [response routes];
        [arrRoutes enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            MKRoute *rout = obj;
            MKPolyline *line = [rout polyline];
            [mapview addOverlay:line];
            NSArray *steps = [rout steps];
            [steps enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                NSLog(@"Rout Instruction : %@",[obj instructions]);
                //NSLog(@"Rout Distance : %f",[obj distance]);
            }];
            
            MKMapRect rect = [rout.polyline boundingMapRect];
            [mapview setRegion:MKCoordinateRegionForMapRect(rect)];
        }];
    }];
}

-(void)drawManualRouteIfLatLongNotAvailable:(CLLocation *)start end:(CLLocation *)end{
    
    NSLog(@"Manual draw");
    MKPlacemark *source = [[MKPlacemark alloc]initWithCoordinate:CLLocationCoordinate2DMake(start.coordinate.latitude, end.coordinate.longitude) addressDictionary:[NSDictionary dictionaryWithObjectsAndKeys:@"",@"", nil] ];
    MKPlacemark *destination = [[MKPlacemark alloc]initWithCoordinate:CLLocationCoordinate2DMake(end.coordinate.latitude, end.coordinate.longitude) addressDictionary:[NSDictionary dictionaryWithObjectsAndKeys:@"",@"", nil] ];
    
    MKMapItem *srcMapItem = [[MKMapItem alloc]initWithPlacemark:source];
    MKMapItem *distMapItem = [[MKMapItem alloc]initWithPlacemark:destination];
    
    MKDirectionsRequest *request = [[MKDirectionsRequest alloc]init];
    [request setSource:srcMapItem];
    [request setDestination:distMapItem];
    [request setTransportType:MKDirectionsTransportTypeAutomobile];
    
    MKDirections *direction = [[MKDirections alloc]initWithRequest:request];
    
    [direction calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse *response, NSError *error) {
        
        NSArray *arrRoutes = [response routes];
        [arrRoutes enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            MKRoute *rout = obj;
            MKPolyline *line = [rout polyline];
            [mapview addOverlay:line];
            NSArray *steps = [rout steps];
            [steps enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                NSLog(@"Route Instruction : %@",[obj instructions]);
                //NSLog(@"Rout Distance : %f",[obj distance]);
            }];
        }];
    }];
}


-(void)zoomToPolyLine: (MKMapView*)map polyline: (MKPolyline*)polyline animated: (BOOL)animated{
    [mapview setRegion:MKCoordinateRegionForMapRect(polyline.boundingMapRect) animated:YES];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView1 viewForAnnotation:(id <MKAnnotation>)annotation{
    
    if([annotation isKindOfClass:[CallOutAnnotationView class]]) {
        static NSString *annotationViewReuseIdentifier = @"annotationViewReuseIdentifier";
        MKAnnotationView *annotationView = (MKAnnotationView *)[mapView1 dequeueReusableAnnotationViewWithIdentifier:annotationViewReuseIdentifier];
        
        if (annotationView == nil) {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationViewReuseIdentifier];
        }
        UIGraphicsEndImageContext();
        annotationView.image =  [UIImage imageNamed:@"start_route"];
        annotationView.centerOffset = CGPointMake(0, - annotationView.frame.size.height / 2 + 5);
        annotationView.annotation = annotation;
        annotationView.canShowCallout = YES;
        return annotationView;
    }else if ([annotation isKindOfClass:[CustomeAnnotation class]]) {
        static NSString *annotationViewReuseIdentifier = @"annotationViewReuseIdentifier1";
        MKAnnotationView *annotationView = (MKAnnotationView *)[mapView1 dequeueReusableAnnotationViewWithIdentifier:annotationViewReuseIdentifier];
        
        if (annotationView == nil) {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationViewReuseIdentifier];
        }
        UIGraphicsEndImageContext();
        annotationView.image =  [UIImage imageNamed:@"end_route"];
        annotationView.centerOffset = CGPointMake(0, - annotationView.frame.size.height / 2 + 5);
        annotationView.annotation = annotation;
        annotationView.canShowCallout = YES;
        return annotationView;
        
    }else{
        return nil;
    }
}

#pragma mark - Webservice call

-(void)callGetLocationService{
    NSString *tripID = [NSString stringWithFormat:@"%@",[locationDataDictionary valueForKey:@"id"]];
    NSDictionary *parameter=@{ @"id":tripID };
    [self callLocationService:parameter];
}

#pragma mark - webservice call

-(void)callLocationService:(NSDictionary *)parameter{
    
    if(![self CheckNetworkConnection]) {
        return;
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [WebService httpPostWithCustomDelegateURLString:@".gettripmap" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         if (error == nil) {
             NSError *localError = nil;
             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
             
             if (localError != nil) {
                 NSLog(@"%@",localError.description);
             }
             else {
                 //NSDictionary *response = [parsedObject objectForKey:@"data"];
                 //NSLog(@"response : %@",parsedObject);
                 id response = [parsedObject objectForKey:@"data"];
                 if ([self CheckSession:response]) {
                     NSString *locations  = [response objectForKey:@"locations"];
                     if (locations.length <= 0) {
                         //[self getPolyline];
                         // Display direction from apple map
                         _IBLayoutConstraintMapBottom.constant = 0;
                         _btnOrignalTracked.hidden = true;
                         _btnSnapToRoad.hidden = true;
                         [self drawMapWithDestination];
                     }else{
                         saved_locations = locations;
                         _IBLayoutConstraintMapBottom.constant = 50;
                         _btnOrignalTracked.hidden = false;
                         _btnSnapToRoad.hidden = false;
                         [self displayRouteFromTrackedLocations:locations];
                     }
                    }else{
                     //[self moveToLogin ];
                 }
             }
         }
     }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


// Not using this function
- (void)getPolyline{
    
    NSString *destinationString = [NSString stringWithFormat:@"%@",[locationDataDictionary valueForKey:@"destination_point"]];
    NSString *originString = [NSString stringWithFormat:@"%@",[locationDataDictionary valueForKey:@"srart_point"]];
    NSString *directionsAPI = @"https://maps.googleapis.com/maps/api/directions/json?";
    
    NSString *start_address = [NSString stringWithFormat:@"%@",[locationDataDictionary valueForKey:@"start_address"]];
    NSString *end_address = [NSString stringWithFormat:@"%@",[locationDataDictionary valueForKey:@"end_address"]];
    
    if (originString.length <= 0) {
        originString = start_address;
    }
    
    if (destinationString.length <= 0) {
        destinationString = end_address;
    }
    
    NSString *directionsUrlString = [NSString stringWithFormat:@"%@&origin=%@&destination=%@&mode=driving&key=%@", directionsAPI,originString,destinationString,appdelegate.googleApiKey];
    
    directionsUrlString = [NSString stringWithFormat:@"%@&origin=Den Blå Planet&destination=Denmark&mode=driving&key=%@", directionsAPI,appdelegate.googleApiKey];
    
    NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
    NSString *encodedString = [directionsUrlString stringByAddingPercentEncodingWithAllowedCharacters:set];
    
    NSURL *directionsUrl = [NSURL URLWithString:encodedString];
    //NSLog(@"STR : %@",directionsUrl);
    
    NSURLSessionDataTask *fetchDirectionsTask = [[NSURLSession sharedSession] dataTaskWithURL:directionsUrl completionHandler:                                                 ^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        if(error) {
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            NSArray *routesArray = [json objectForKey:@"routes"];
            if ([routesArray count] > 0) {
                NSDictionary *routeDict = [routesArray objectAtIndex:0];
                //NSLog(@"route : %@",routeDict);
                
                NSArray *legs = [routeDict valueForKey:@"legs"];
                NSDictionary *legDict = [legs objectAtIndex:0];
                NSArray *steps = [legDict valueForKey:@"steps"];
                NSDictionary *stepsDict;
                CLLocationCoordinate2D Coordinates[[steps count]];
                                                             
                for (int i = 0; i < steps.count; i++) {
                    stepsDict = [steps objectAtIndex:i];
                    
                    NSDictionary *Start_location = [stepsDict objectForKey:@"start_location"];
                    NSDictionary *End_location = [stepsDict objectForKey:@"end_location"];
                    
                    double location_lat = [[Start_location valueForKey:@"lat"] doubleValue];
                    double location_lng = [[Start_location valueForKey:@"lng"] doubleValue];
                    
                    double Elocation_lat = [[End_location valueForKey:@"lat"] doubleValue];
                    double Elocation_lng = [[End_location valueForKey:@"lng"] doubleValue];
                    
                    if (i == 0) {
                        
                        CLLocation *loc = [[CLLocation alloc] initWithLatitude:location_lat longitude:location_lng];
                        Coordinates[i] = loc.coordinate;
                        _calloutView = [[CallOutAnnotationView alloc] initWithName:start_address address:@"" phonenumber:@"" coordinate:Coordinates[i]];
                        _calloutView.contentView.frame = CGRectMake(_calloutView.contentView.frame.origin.x, _calloutView.contentView.frame.origin.y, 10, 10);
                        _calloutView.title = start_address;
                        [mapview addAnnotation:_calloutView];
                        
                    }else if (i == steps.count - 1){
                        
                        CLLocation *loc = [[CLLocation alloc] initWithLatitude:location_lat longitude:location_lng];
                        Coordinates[i] = loc.coordinate;
                        CustomeAnnotation *point=[[CustomeAnnotation alloc] initWithLocation:Coordinates[i]];
                        point.title =end_address;
                        [mapview addAnnotation:point];
                    }
                    
                    if (i > 0) {
                        
                        CLLocationCoordinate2D Coordinates[2];
                        CLLocation *loc = [[CLLocation alloc] initWithLatitude:location_lat longitude:location_lng];
                        Coordinates[0] = loc.coordinate;
                        
                        NSDictionary *second = [steps objectAtIndex:i - 1];
                        NSDictionary *Start_location = [second objectForKey:@"start_location"];
                        
                        Elocation_lat = [[Start_location valueForKey:@"lat"]doubleValue];
                        Elocation_lng = [[Start_location valueForKey:@"lng"]doubleValue];
                        loc = [[CLLocation alloc] initWithLatitude:Elocation_lat longitude:Elocation_lng];
                        Coordinates[1] = loc.coordinate;
                        
                        MKPolyline *polyLine = [MKPolyline polylineWithCoordinates:Coordinates count: 2];
                        [mapview addOverlay:polyLine];
                    }
                    
                }
                MKMapRect zoomRect = MKMapRectNull;
                for (id <MKAnnotation> annotation in mapview.annotations)
                {
                    MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
                    MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.0, 0.0);
                    zoomRect = MKMapRectUnion(zoomRect, pointRect);
                }
                [mapview setVisibleMapRect:zoomRect edgePadding:(UIEdgeInsetsMake(40, 20, 20, 20)) animated:true];
                
            }else{
                [Helper displayAlertView:@"" message:@"Direction not found."];
            }
            
        });
    }];
    [fetchDirectionsTask resume];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)onGoogle:(id)sender {
    [self clearMapAnnotation];
    [self snapToRoaDSelected ];
    [self displayRouteFromTrackedLocations:saved_locations];
}
- (IBAction)oniOS:(id)sender {
    [self clearMapAnnotation];
    [self orignalTrackedCalled ];
    NSString *location = [NSString stringWithFormat:@"%@",[locationDataDictionary valueForKey:@"locations1"]];
      if (location.length <= 0) {
          [Helper displayAlertView:@"" message:@"Trip is older then current version of application"];
      }else{
          [self displayRouteFromTrackedLocations:location];
      }
}

-(void)snapToRoaDSelected{
    _btnSnapToRoad.layer.borderWidth = 1.0;
    _btnSnapToRoad.layer.borderColor  = [UIColor whiteColor].CGColor;
    _btnSnapToRoad.backgroundColor = [Helper defaultColor];
    [_btnSnapToRoad setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    _btnOrignalTracked.layer.borderWidth = 1.0;
    _btnOrignalTracked.layer.borderColor  = [UIColor whiteColor].CGColor;
    _btnOrignalTracked.backgroundColor = [UIColor whiteColor];
    [_btnOrignalTracked setTitleColor:[Helper defaultColor] forState:UIControlStateNormal];
}

-(void)orignalTrackedCalled{
    _btnOrignalTracked.layer.borderWidth = 1.0;
    _btnOrignalTracked.layer.borderColor  = [UIColor whiteColor].CGColor;
    _btnOrignalTracked.backgroundColor = [Helper defaultColor];
    [_btnOrignalTracked setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    _btnSnapToRoad.layer.borderWidth = 1.0;
    _btnSnapToRoad.layer.borderColor  = [UIColor whiteColor].CGColor;
    _btnSnapToRoad.backgroundColor = [UIColor whiteColor];
    [_btnSnapToRoad setTitleColor:[Helper defaultColor] forState:UIControlStateNormal];
}

-(void)clearMapAnnotation
{
        NSLog(@"clearMapAnnotation");
        if (mapview.annotations.count > 0) {
            [mapview removeAnnotations:mapview.annotations];
        }
        if (mapview.overlays.count > 0) {
            [mapview removeAnnotations:mapview.annotations];
            [mapview removeOverlays:mapview.overlays];
        }
}
@end
