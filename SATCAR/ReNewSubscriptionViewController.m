//
//  ReNewSubscriptionViewController.m
//  SATCAR
//
//  Created by Percept Infotech on 09/02/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import "ReNewSubscriptionViewController.h"

@interface ReNewSubscriptionViewController () {
    UIColor *bluecolor;
    float btnRadius;
}
@end

@implementation ReNewSubscriptionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    bluecolor = [UIColor colorWithRed:0.000 green:0.651 blue:0.929 alpha:1.00];
    btnRadius = 25.0;
    
    _btnRenew.layer.cornerRadius = btnRadius;
    _btnRenew.layer.borderWidth = 1;
    _btnRenew.layer.borderColor = bluecolor.CGColor;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onReNewSubscription:(id)sender {
    
    [self performSegueWithIdentifier:@"segueReNewToRegister" sender:self];
}
@end
