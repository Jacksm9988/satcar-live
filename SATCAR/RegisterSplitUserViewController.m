//
//  RegisterSplitUserViewController.m
//  SATCAR
//
//  Created by Percept Infotech on 31/01/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import "RegisterSplitUserViewController.h"
#import "HomeViewController.h"
@interface RegisterSplitUserViewController ()<UITextFieldDelegate>{
    AppDelegate *appdelegate;
}

@end

@implementation RegisterSplitUserViewController
@synthesize speedometer,userType,imageData;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addTitleView:@"Splitleasing financials"];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _btnNext.layer.cornerRadius = 25;
    [_btnNext setTitle:[NSString stringWithFormat:@"%@",NSLocalizedString(@"Next", @"")] forState:UIControlStateNormal];
    _lblSplitMCost.text = NSLocalizedString(@"Thefollowinginformationisneededtocalciulatethemonthlycostofyouraplitleasing", @"");
    _LblAmoutPaid.text = NSLocalizedString(@"Amountpaidupfronttoleasingcompany", @"");
    _LblMonthlyCost.text = NSLocalizedString(@"Monthlycost", @"");
    _LblMonths.text = [NSLocalizedString(@"Months", @"") uppercaseString];
    _LblContract.text = NSLocalizedString(@"Durationofcontract", @"");
    
}

#pragma mark - Textfield delegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    textField.textColor =  [UIColor colorWithRed:18.0/255.0 green:172.0/255.0 blue:243.0/255.0 alpha:1.0];
    textField.layer.borderWidth = 1;
    textField.layer.borderColor =  [UIColor colorWithRed:18.0/255.0 green:172.0/255.0 blue:243.0/255.0 alpha:1.0].CGColor;
    textField.clipsToBounds = YES;
    return true;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    textField.textColor =  [UIColor blackColor];
    textField.layer.borderWidth = 0;
    textField.layer.borderColor = [UIColor clearColor].CGColor;
    textField.clipsToBounds = YES;
    return true;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Button Action
- (IBAction)onNext:(id)sender {
    if ([self isValidData]) {
        NSString *userID = [Helper getPREF:@"userID"];
        
        NSDictionary *parameter;
        if (imageData != nil && imageData.length > 0) {
            parameter=@{@"user_id":userID, @"speedometer":speedometer, @"amt_upfront":_txtAmount.text, @"monthly_cost":_txtMonthly.text, @"duration_of_contract":_txtDuration.text, @"speed_image":imageData};
        }else{
            parameter=@{@"user_id":userID, @"speedometer":speedometer, @"amt_upfront":_txtAmount.text, @"monthly_cost":_txtMonthly.text, @"duration_of_contract":_txtDuration.text, @"speed_image":@""};
        }
        //NSLog(@"Parameter %@: ",parameter);
        [self callAddExtraInfo:parameter];
    }
}

#pragma mark - validation
-(BOOL)isValidData{
    if (_txtAmount.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_amount",nil)];
        return false;
        
    }else if (_txtMonthly.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_please_enter_month_cost",nil)];
        return false;
        
    }else if (_txtDuration.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_please_enter_contract",nil)];
        return false;
    }
    return true;
}

#pragma mark - webservice call
-(void)callAddExtraInfo:(NSDictionary *)parameter{
    
    if(![self CheckNetworkConnection]) {
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [WebService httpPostWithCustomDelegateMultipartURLString:@".userextrainfo" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (error == nil) {
            NSError *localError = nil;
            NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
            
            if (localError != nil) {
                NSLog(@"%@",localError.description);
            }
            else {
                NSDictionary *response = [parsedObject objectForKey:@"data"];
                NSLog(@"response : %@",response);
                NSString* message = [response objectForKey:@"message"];
                NSString* success = [response objectForKey:@"status"];
                if ([success isEqualToString:@"Success"]) {
                    
                    NSDictionary* user = [response objectForKey:@"user"];
                    NSString *email = [user valueForKey:@"email"];
                    NSString *name = [user valueForKey:@"name"];
                    NSString *uID = [user valueForKey:@"id"];
                    NSString* registerDate = [user objectForKey:@"registerDate"];
                    //NSString* sid = [response objectForKey:@"sid"];
                    NSString* userTyp = [response objectForKey:@"user_type"];
                    appdelegate.tracker_id = [response objectForKey:@"tracker_id"];
                    
                    [Helper setPREFStringValue:uID sKey:@"userID"];
                    //[Helper setPREFStringValue:sid sKey:@"sid"];
                    [Helper setPREFStringValue:name sKey:@"userName"];
                    [Helper setPREFStringValue:email sKey:@"userEmail"];
                    [Helper setPREFStringValue:@"1" sKey:@"islogin"];
                    [Helper setPREFStringValue:userTyp sKey:@"user_type"];
                    [Helper setPREFStringValue:registerDate sKey:@"userRegisterDate"];
                    if (appdelegate.tracker_id == (NSString *)[NSNull class]) {
                        [Helper setPREFStringValue:@"" sKey:@"tracker_id"];
                    }else{
                        [Helper setPREFStringValue:appdelegate.tracker_id sKey:@"tracker_id"];
                    }
                    
                    
                    NSDictionary *generalSetting = [response objectForKey:@"general_setting"];
                    [Helper setUserValue:generalSetting sKey:@"general_setting"];
                    [Helper setDataValue:data sKey:@"userInfo"];
                    
                    NSLog(@"user info : %@",response);
                    NSLog(@"generalSetting : %@",generalSetting);
                    
                    NSDictionary *payment_info = [response objectForKey:@"payment_info"];
                    NSString *status = [payment_info valueForKey:@"status"];
                    if (status == (NSString *)[NSNull null]) {
                        status = @"";
                        [Helper setPREFStringValue:status sKey:@"userStatus"];
                    }else{
                        [Helper setPREFStringValue:status sKey:@"userStatus"];
                    }

                    [Helper setPREFStringValue:status sKey:@"userStatus"];
                    
                    //[self performSegueWithIdentifier:@"SegueSplitConnectBeep" sender:self];
                    
                    // go to home
                   // [appdelegate callLocationService:uID from:@"regisptersplituser"];
                    [appdelegate callLocationService_withTips:uID from:@"regispterstandarduser"];
                    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    HomeViewController* controller = [storyboard instantiateViewControllerWithIdentifier:@"Home"];
                    [self.navigationController pushViewController:controller animated:YES];
                    
                    
                } else {
                    NSLog(@"error :: %@",message);
                    [Helper displayAlertView:@"" message:message];
                }
            }
        }
    }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
