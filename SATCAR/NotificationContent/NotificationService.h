//
//  NotificationService+NotificationService.h
//  SATCAR
//
//  Created by Percept Infotech on 2018-02-19.
//  Copyright © 2018 Percept Infotech. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>
@interface NotificationService : UNNotificationServiceExtension
@end
