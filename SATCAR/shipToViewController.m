//
//  shipToViewController.m
//  SATCAR
//
//  Created by Percept Infotech on 23/02/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import "shipToViewController.h"
#import <GooglePlaces/GooglePlaces.h>
#import "ePayLib.h"
#import "ePayParameter.h"
#import "RegisterViewController.h"

@interface shipToViewController ()<UITextFieldDelegate,GMSAutocompleteViewControllerDelegate>
{
    NSString *ordernum;
    NSMutableDictionary *dataDict;
}
@end

@implementation shipToViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _btnPurchase.layer.cornerRadius = 25;
    _btnPurchase.layer.borderWidth = 1;
    _btnPurchase.layer.borderColor =[UIColor colorWithRed:0.000 green:0.651 blue:0.929 alpha:1.00].CGColor;
    [_btnPurchase setTitle:NSLocalizedString(@"Purchase", @"") forState:UIControlStateNormal];
    
    _txtFname.placeholder = NSLocalizedString(@"FirstName", @"");
    _txtLname.placeholder = NSLocalizedString(@"LastName", @"");
    _txtAddress.placeholder = NSLocalizedString(@"Address", @"");
    _txtPostcode.placeholder = NSLocalizedString(@"Postcode", @"");
    _txtCity.placeholder = NSLocalizedString(@"City", @"");
    _txtCountry.placeholder = NSLocalizedString(@"Country", @"");
}

-(void)viewWillAppear:(BOOL)animated{
    ordernum = @"";
    [self addTitleView:NSLocalizedString(@"ShipTo", @"")];
}

#pragma mark - Custom functions
-(BOOL)isValidData{
    
    if (_txtFname.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_name",nil)];
        return false;
        
    }else if (_txtLname.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_lname",nil)];
        return false;
        
    }else if (_txtAddress.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_address",nil)];
        return false;
        
    }/*else if (_txtHouseNumber.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_address",nil)];
        return false;
        
    }*/else if (_txtPostcode.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_zipcode",nil)];
        return false;
        
    }else if (_txtCity.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_city",nil)];
        return false;
        
    }else if (_txtCountry.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_country",nil)];
        return false;
    }
    return true;
}

#pragma mark - textfield delegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    textField.textColor =  [UIColor colorWithRed:18.0/255.0 green:172.0/255.0 blue:243.0/255.0 alpha:1.0];
    textField.layer.borderWidth = 1;
    textField.layer.borderColor =  [UIColor colorWithRed:18.0/255.0 green:172.0/255.0 blue:243.0/255.0 alpha:1.0].CGColor;
    textField.clipsToBounds = YES;
    return true;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    textField.textColor =  [UIColor blackColor];
    textField.layer.borderWidth = 0;
    textField.layer.borderColor = [UIColor clearColor].CGColor;
    textField.clipsToBounds = YES;
    return true;
}



-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField == _txtAddress){
        
        if (_txtAddress.text.length <= 0) {
            GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
            acController.delegate = self;
            GMSAutocompleteFilter *filter = [[GMSAutocompleteFilter alloc]init];
            filter.type = kGMSPlacesAutocompleteTypeFilterNoFilter;
            //filter.country = @"DK";
            acController.autocompleteFilter = filter;
            [self presentViewController:acController animated:YES completion:nil];
        }
       // [self.view endEditing:true];
        
    }
}

#pragma mark - button action
- (IBAction)onPurchase:(id)sender {
    
    if ([self isValidData]) {
        [self callGetOrderNumberService];
    }
    
    /*if ([self isValidData]) {
        NSString *userID = [Helper getPREF:@"userID"];
        NSDictionary *parameter = @{ @"userid":userID, @"name":_txtFname.text, @"lname":_txtLname.text, @"address":_txtAddress.text, @"zip_and_city":_txtPostcode.text, @"city":_txtCity.text, @"country":_txtCountry.text};
        [self.view endEditing:true];
        [self callShipDeviceService:parameter];
    }*/
}

#pragma mark - webservice call
-(void)callGetOrderNumberService{
    
    if(![self CheckNetworkConnection]) {
        return;
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [WebService httpPostWithCustomDelegateMultipartURLString:@".getOrdernumber" parameter:nil isDebug:false callBackData:^(NSData *data, NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (error == nil) {
            NSError *localError = nil;
            NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
            
            if (localError != nil) {
                NSLog(@"%@",localError.description);
            }
            else {
                NSDictionary *response = [parsedObject objectForKey:@"data"];
                NSLog(@"Ship to response : %@",response);
                
                NSString* success = [response objectForKey:@"status"];
                if ([success isEqualToString:@"Success"]) {
                    ordernum =  [response objectForKey:@"ordernum"];
                    if (![ordernum isEqual:[NSNull null]]) {
                        dataDict = [[NSMutableDictionary alloc]init];
                        [dataDict setValue:_txtFname.text forKey:@"first_name"];
                        [dataDict setValue:_txtLname.text forKey:@"last_name"];
                        [dataDict setValue:_txtAddress.text forKey:@"address"];
                        //[dataDict setValue:_txtHouseNumber.text forKey:@"house_number"];
                        [dataDict setValue:_txtPostcode.text forKey:@"post_number"];
                        [dataDict setValue:_txtCity.text forKey:@"city"];
                        [dataDict setValue:_txtCountry.text forKey:@"country"];
                        [self performSegueWithIdentifier:@"segueShipToPayment" sender:self];
                    }
                }
            }
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Handle the user's selection.
- (void)viewController:(GMSAutocompleteViewController *)viewController didAutocompleteWithPlace:(GMSPlace *)place {
    [self dismissViewControllerAnimated:YES completion:nil];
    // Do something with the selected place.
    NSLog(@"Place name %@", place.name);
    NSLog(@"Place address %@", place.formattedAddress);
    NSLog(@"Place attributions %@", place.attributions.string);
    NSString *placeAddress = [NSString stringWithFormat:@"%@,",place.name];
    for (int i = 0; i < place.addressComponents.count; i++) {
        GMSAddressComponent *add = [place.addressComponents objectAtIndex:i];
       // NSLog(@"type : %@",add.type);
        //NSLog(@"type : %@",add.name);
        if ([add.type containsString:@"locality"]) {
            _txtCity.text = add.name;
        }else if ([add.type containsString:@"country"]) {
            _txtCountry.text = add.name;
        }else if ([add.type containsString:@"postal_code"]) {
            _txtPostcode.text = add.name;
        }
        placeAddress = [NSString stringWithFormat:@"%@",place.name];
    }
    NSLog(@"Address : %@",placeAddress);
    _txtAddress.text = placeAddress;
}

- (void)viewController:(GMSAutocompleteViewController *)viewController didFailAutocompleteWithError:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:nil];
    // TODO: handle the error.
    NSLog(@"Error: %@", [error description]);
}

// User canceled the operation.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

// Turn the network activity indicator on and off again.
- (void)didRequestAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)didUpdateAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    RegisterViewController *controller = segue.destinationViewController;
    controller.userFrom = @"shipTracking";
    //controller.amount = @"179900";
    controller.amount = @"199900";
    controller.ordernum = ordernum;
    controller.shipDict = dataDict;
}


@end
