//
//  LocationEditViewController.h
//  SATCAR
//
//  Created by Percept Infotech on 16/01/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationEditViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextField *txtAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtCity;
@property (weak, nonatomic) IBOutlet UITextField *txtZip;
@property (weak, nonatomic) IBOutlet UITextField *txtCountry;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
- (IBAction)onSave:(id)sender;
//@property (weak, nonatomic) IBOutlet UITextField *txtHouseNumber;

@property(nonatomic,strong)NSDictionary *locationDataDictionary;
@property(nonatomic,strong)NSString *fromScreen;

@end
