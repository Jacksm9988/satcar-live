//
//  VersionViewController.h
//  SATCAR
//
//  Created by Percept Infotech on 19/04/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VersionViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lblVersion;
@property (weak, nonatomic) IBOutlet UILabel *lblBuild;
@property (weak, nonatomic) IBOutlet UILabel *lblVersionValue;
@property (weak, nonatomic) IBOutlet UILabel *lblBuildValue;
@end
