//
//  RegisterViewController.m
//  SATCAR
//
//  Created by Percept Infotech on 12/7/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import "RegisterViewController.h"
#import <GooglePlaces/GooglePlaces.h>
#import "ePayLib.h"
#import "ePayParameter.h"
#import "RegisterStandardUserSpeedmeterVC.h"

@interface RegisterViewController ()<UITextFieldDelegate,GMSAutocompleteViewControllerDelegate> {
    NSString *subscribe_newsletter;
    NSString *userType,*orderNumber,*userID;
    AppDelegate* myAppDelegate;
    ePayLib* epaylib;
    BOOL googleAddress;
}
@end

@implementation RegisterViewController
@synthesize userFrom,amount,car_beep,gps_tracker;
@synthesize activityIndicatorView;

- (void)viewDidLoad {
    [super viewDidLoad];
    googleAddress = false;
}

-(void)viewWillAppear:(BOOL)animated
{
    if (googleAddress) {
        googleAddress = false;
    }else{
        [self addTitleView:NSLocalizedString( @"Createnewuser", @"")];
        [self.navigationController setNavigationBarHidden:NO animated:YES];
        myAppDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        subscribe_newsletter = @"0";
        
        [Helper setBorderWithColorInButton:_btnRegister];
        [_btnRegister setTitle:NSLocalizedString(@"Register", @"") forState:UIControlStateNormal];
        
        _txtFname.placeholder = NSLocalizedString(@"FirstName", @"");
        _txtLname.placeholder = NSLocalizedString(@"LastName", @"");
        _txtEmail.placeholder = NSLocalizedString(@"email", @"");
        _txtPwd.placeholder = NSLocalizedString(@"Password", @"");
        _txtAddress.placeholder = NSLocalizedString(@"Address", @"");
        //_txtHouseNumber.placeholder = NSLocalizedString(@"HouseNo", @"");
        _txtpostcode.placeholder = NSLocalizedString(@"Postcode", @"");
        _txtcity.placeholder = NSLocalizedString(@"City", @"");
        _txtcountry.placeholder = NSLocalizedString(@"Country", @"");
        _lblNote.text = NSLocalizedString(@"Iwanttoreceivenewsletter", @"");
        if ([userFrom isEqualToString:@"free"]) {
            userType = @"12";
        }else if ([userFrom isEqualToString:@"standard"]){
            userType = @"10";
        }else{
            userType = @"11";
        }
        [self hideShowAddressFields];
        
        if (![userFrom isEqualToString:@"free"]) {
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(event:) name:PaymentAcceptedNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(event:) name:PaymentLoadingAcceptPageNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(event:) name:PaymentWindowCancelledNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(event:) name:PaymentWindowLoadedNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(event:) name:PaymentWindowLoadingNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(event:) name:ErrorOccurredNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(event:) name:NetworkActivityNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(event:) name:NetworkNoActivityNotification object:nil];
        }
        
        NSDictionary *dataDict,*dict,*userInfo;
        NSData *userdata = [Helper getDataValue:@"userInfo"];
        if (userdata != nil) {
            dataDict = [self dataToDicitonary:userdata];
            if (![dataDict isKindOfClass:[NSNull class]]) {
                dict = [dataDict valueForKey:@"data"];
                userInfo = [dict valueForKey:@"user"];
                userID = [userInfo valueForKey:@"id"];
                NSDictionary *payment_info = [dict objectForKey:@"payment_info"];
                userType = [payment_info valueForKey:@"user_type"];
            }else{
                [Helper displayAlertView:@"" message:NSLocalizedString(@"something_went_wrong_msg", @"")];
            }
        }
        
        if ([userFrom isEqualToString:@"shipTracking"]) {
            NSLog(@"From shipTracking");
            [self hideAll];
            orderNumber = _ordernum;
           // googleAddress = true
            [self initPayment:orderNumber];
        }else if ([userFrom isEqualToString:@"PendingPayment"]){
            NSLog(@"From Pending payment");
            NSData *userdata = [Helper getDataValue:@"userInfo"];
            if (userdata != nil) {
                NSDictionary *payment_info = [dict objectForKey:@"payment_info"];
                NSString *status = [payment_info valueForKey:@"status"];
                if (status == (NSString *)[NSNull null]) {
                    status = @"";
                    [Helper setPREFStringValue:status sKey:@"userStatus"];
                }else{
                    [Helper setPREFStringValue:status sKey:@"userStatus"];
                }
                userType = [payment_info valueForKey:@"user_type"];
                NSString *amt = [payment_info valueForKey:@"amount"];
                if (amt == (NSString *)[NSNull null]) {
                    amount = @"0.0";
                }else{
                    amount = [NSString stringWithFormat:@"%@",amt];
                }
                
                if ([status.lowercaseString isEqualToString:@"pending"]) {
                    [self hideAll];
                    orderNumber = [dict valueForKey:@"ordernumber"];
                    NSLog(@"order number : %@",orderNumber);
                    float total_amount = 0.0;
                    total_amount = [amount doubleValue] * 100 ;
                    amount = [NSString stringWithFormat:@"%.0f",total_amount];
                    NSLog(@"amount : %@",amount);
                    [self initPayment:orderNumber];
                }
            }
        }else{
            NSLog(@"From Else");
            NSData *userdata = [Helper getDataValue:@"userInfo"];
            if (userdata != nil) {
                NSDictionary *payment_info = [dict objectForKey:@"payment_info"];
                NSString *status = [payment_info valueForKey:@"status"];
                userType = [payment_info valueForKey:@"user_type"];
                if (status == (NSString *)[NSNull null]) {
                    status = @"";
                    [Helper setPREFStringValue:status sKey:@"userStatus"];
                }else{
                    [Helper setPREFStringValue:status sKey:@"userStatus"];
                }
                
                if ([status.lowercaseString isEqualToString:@"pending"]) {
                    [self hideAll];
                    orderNumber = [dict valueForKey:@"ordernumber"];
                    NSLog(@"order number : %@",orderNumber);
                    [self initPayment:orderNumber];
                }
            }
        }
    }
}

#pragma mark - Validation
-(BOOL)isValidData{
    if (_txtFname.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_first_name",nil)];
        return false;
        
    }else if (_txtLname.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_last_name",nil)];
        return false;
        
    }else if (_txtEmail.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_enter_email_id",nil)];
        return false;
        
    }else if (![self validateEmailWithString:_txtEmail.text]){
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Invalid_email_id",nil)];
        return false;
        
    }else if (_txtPwd.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_your_password",nil)];
        return false;
        
    }
    
    if ([_trackerSelected isEqualToString:@"1"]){
        if (_txtAddress.text.length == 0) {
            [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_address",nil)];
            return false;
            
        }else if (_txtpostcode.text.length == 0) {
            [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_zipcode",nil)];
            return false;
            
        }else if (_txtcity.text.length == 0) {
            [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_city",nil)];
            return false;
            
        }else if (_txtcountry.text.length == 0) {
            [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_country",nil)];
            return false;
        }
    }
    return true;
}

#pragma mark - Custom functions
-(void)hideAll{
    _txtFname.hidden = true;
    _txtLname.hidden = true;
    _txtEmail.hidden = true;
    _txtPwd.hidden = true;
    _lblNote.hidden = true;
    _btnRegister.hidden = true;
    _switchNews.hidden = true;
}

-(void)hideShowAddressFields{
    if (![_trackerSelected isEqualToString:@"1"]) {
        _IBLayoutConstraintAddressTop.constant = 0;
        _IBLayoutConstraintAddressHeight.constant = 0;
        _txtAddress.hidden = true;
        _txtpostcode.hidden = true;
        _txtcity.hidden = true;
        _txtcountry.hidden = true;
       // _txtHouseNumber.hidden = true;
        _txtAddress.text = @"";
        _txtpostcode.text = @"";
        _txtcity.text = @"";
        _txtcountry.text = @"";
        _IBLayoutConstraintNoteTop.constant = 20;
    }else{
        _IBLayoutConstraintAddressTop.constant = 10;
        _IBLayoutConstraintAddressHeight.constant = 50;
        _txtAddress.hidden = false;
        _txtpostcode.hidden = false;
        _txtcity.hidden = false;
        _txtcountry.hidden = false;
        //_txtHouseNumber.hidden = false;
        _IBLayoutConstraintNoteTop.constant = 270;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Textfield delegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    textField.textColor =  [UIColor colorWithRed:18.0/255.0 green:172.0/255.0 blue:243.0/255.0 alpha:1.0];
    textField.layer.borderWidth = 1;
    textField.layer.borderColor =  [UIColor colorWithRed:18.0/255.0 green:172.0/255.0 blue:243.0/255.0 alpha:1.0].CGColor;
    textField.clipsToBounds = YES;
    return true;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    textField.textColor =  [UIColor blackColor];
    textField.layer.borderWidth = 0;
    textField.layer.borderColor = [UIColor clearColor].CGColor;
    textField.clipsToBounds = YES;
    return true;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    textField.layer.borderColor = [UIColor colorWithRed:18.0/255.0 green:172.0/255.0 blue:243.0/255.0 alpha:1.0].CGColor;
    textField.clipsToBounds = YES;
    
    if (textField == _txtAddress){
        
        if (_txtAddress.text.length <= 0) {
            googleAddress = true;
            [self.view endEditing:true];
            GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
            acController.delegate = self;
            GMSAutocompleteFilter *filter = [[GMSAutocompleteFilter alloc]init];
            filter.type = kGMSPlacesAutocompleteTypeFilterNoFilter;
            //filter.country = @"DK";
            acController.autocompleteFilter = filter;
            [self presentViewController:acController animated:YES completion:nil];
        }
        
    }
}


#pragma mark - Button actions
- (IBAction)RegisterPress:(id)sender {
    [self.view endEditing:YES];
    
    if(![self CheckNetworkConnection]) {
        return;
    }
    
    if (amount == nil) {
        amount = @"";
    }if (car_beep == nil) {
        car_beep = @"";
    }if (gps_tracker == nil) {
        gps_tracker = @"";
    }
    
    if ([self isValidData]) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSDictionary *parameter=@{
                                  @"user_type":userType,
                                  @"amount":[NSString stringWithFormat:@"%.0f",[amount doubleValue]/100],
                                  @"car_beep":car_beep,
                                  @"gps_tracker":gps_tracker,
                                  @"first_name":_txtFname.text,
                                  @"last_name":_txtLname.text,
                                  @"email":_txtEmail.text,
                                  @"password":_txtPwd.text,
                                  @"subscribe_newsletter":subscribe_newsletter,
                                  @"address":_txtAddress.text,
                                  @"post_number":_txtpostcode.text,
                                  @"city":_txtcity.text,
                                  @"country":_txtcountry.text,
                                  @"device_uuid":myAppDelegate.strDeviceToken,
                                  @"os":@"ios",
                                  };
         NSLog(@"register data : %@",parameter);
        [WebService httpPostWithCustomDelegateURLString:@".register" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error)
         {
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             if (error == nil) {
                 
                 
                 NSError *localError = nil;
                 NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
                 
                 if (localError != nil) {
                     NSLog(@"%@",localError.description);
                 }
                 else {
                     NSDictionary *response = [parsedObject objectForKey:@"data"];
                     NSString* message = [response objectForKey:@"message"];
                     NSString* success = [response objectForKey:@"status"];
                     NSLog(@"Response : %@",response);
                     if ([success isEqualToString:@"Success"]) {
                         myAppDelegate.appLogin = @"1";
                         NSDictionary* user = [response objectForKey:@"user"];
                         NSString *eml = [user valueForKey:@"email"];
                         NSString *name = [user valueForKey:@"name"];
                         NSString *uID = [user valueForKey:@"id"];
                         NSString* registerDate = [user objectForKey:@"registerDate"];
                         //NSString* sid = [response objectForKey:@"sid"];
                         myAppDelegate.tracker_id = [response objectForKey:@"tracker_id"];
                         
                         [Helper setPREFStringValue:uID sKey:@"userID"];
                         //[Helper setPREFStringValue:sid sKey:@"sid"];
                         [Helper setPREFStringValue:name sKey:@"userName"];
                         [Helper setPREFStringValue:eml sKey:@"userEmail"];
                         [Helper setPREFStringValue:@"1" sKey:@"islogin"];
                         [Helper setPREFStringValue:userType sKey:@"user_type"];
                         [Helper setPREFStringValue:registerDate sKey:@"userRegisterDate"];
                         
                         if (myAppDelegate.tracker_id == (NSString *)[NSNull class] || myAppDelegate.tracker_id.length <= 0) {
                             [Helper setPREFStringValue:@"" sKey:@"tracker_id"];
                         }else{
                             [Helper setPREFStringValue:myAppDelegate.tracker_id sKey:@"tracker_id"];
                         }
                         
                         NSString* parent = [Helper isStringIsNull:[response objectForKey:@"parent"]];
                         if (parent == (NSString *)[NSNull class]) {
                             [Helper setPREFStringValue:@"0" sKey:@"parent"];
                         }else{
                             [Helper setPREFStringValue:parent sKey:@"parent"];
                         }
                         
                         
                         
                         NSDictionary *generalSetting = [response objectForKey:@"general_setting"];
                         NSMutableDictionary *generalSetting2 = [[NSMutableDictionary alloc]init];
                         NSArray *allk = [generalSetting allKeys];
                         for (int i=0; i<allk.count; i++) {
                             NSString *st = [allk objectAtIndex:i];
                             NSString *val =[generalSetting valueForKey:st];
                             if (val == (NSString *)[NSNull null] || val == nil) {
                                 if([st isEqualToString:@"notification"])
                                 {
                                     [generalSetting2 setObject:@"0" forKey:st];
                                 }else if([st isEqualToString:@"trip_notification"])
                                 {
                                     [generalSetting2 setObject:@"0" forKey:st];
                                 }else{
                                     [generalSetting2 setObject:@"" forKey:st];
                                 }
                                 
                             }else{
                                 [generalSetting2 setObject:val forKey:st];
                             }
                         }
                         
                         [Helper setUserValue:generalSetting2 sKey:@"general_setting"];
                        
                             myAppDelegate.appTrackNotification = [generalSetting2 valueForKey:@"trip_notification"];
                             [Helper setPREFID:myAppDelegate.appTrackNotification :@"trip_notification"];
                        
                         
                         
                         NSDictionary *temp = [parsedObject mutableCopy];
                         NSDictionary *tempRes = [response mutableCopy];
                         orderNumber = [response valueForKey:@"ordernumber"];
                         if (orderNumber == (NSString *)[NSNull null]) {
                             orderNumber = @"0";
                         }
                         [tempRes setValue:orderNumber forKey:@"ordernumber"];
                         [temp setValue:tempRes forKey:@"data"];
                        
                         NSData *data = [self dicitonaryToData:temp];
                         if (data != nil) {
                             [Helper setDataValue:data sKey:@"userInfo"];
                         }else{
                            // [Helper displayAlertView:@"" message:@"Data not available"];
                         }
                         
                         [myAppDelegate callGetCredentialsService:uID];
                         
                         NSLog(@"user info : %@",temp);
                         NSLog(@"generalSetting : %@",generalSetting2);
                         if ([userType isEqualToString:@"12"]) {
                             [self performSegueWithIdentifier:@"segueRegisterToSpeedmeter" sender:self];
                         }else{
                             [self initPayment:orderNumber];
                         }
                         
                         
                     } else {
                         NSLog(@"error :: %@",message);
                         [Helper displayAlertView:@"" message:message];
                     }
                 }
             }
         }];
    }
}


#pragma mark - epay payment
- (void)initPayment:(NSString *)orderno {
    // Declare your unique OrderId
    NSString *orderId = [NSString stringWithFormat:@"%@", orderno];
    NSDate *date = [NSDate date];
    NSTimeInterval ti = [date timeIntervalSince1970];
    NSTimeInterval timeInterval=ti;
    orderId = [NSString stringWithFormat:@"%.0f",timeInterval];
    
    // Init the ePay Lib with parameters and the view to add it to.
    epaylib = [[ePayLib alloc] initWithView:self.view];
    
    // Add parameters to the array
    NSDictionary *epay_info = [Helper getUserValue:myAppDelegate.epay_info];
    NSString *merchantNumber = [NSString stringWithFormat:@"%@",[epay_info valueForKey:@"merchantnumber"]];
    if (merchantNumber.length > 0) {
        [epaylib.parameters addObject:[ePayParameter key:@"merchantnumber" value:merchantNumber]];
        [epaylib.parameters addObject:[ePayParameter key:@"currency" value:@"DKK"]];
        [epaylib.parameters addObject:[ePayParameter key:@"amount" value:amount]];
        [epaylib.parameters addObject:[ePayParameter key:@"orderid" value:orderId]];
        [epaylib.parameters addObject:[ePayParameter key:@"paymentcollection" value:@"0"]];
        [epaylib.parameters addObject:[ePayParameter key:@"lockpaymentcollection" value:@"0"]];
        [epaylib.parameters addObject:[ePayParameter key:@"language" value:@"0"]];
        [epaylib.parameters addObject:[ePayParameter key:@"encoding" value:@"UTF-8"]];
        [epaylib.parameters addObject:[ePayParameter key:@"instantcapture" value:@"1"]];
        [epaylib.parameters addObject:[ePayParameter key:@"instantcallback" value:@"1"]];
        [epaylib.parameters addObject:[ePayParameter key:@"ordertext" value:@"Payment Successfull"]];
        [epaylib.parameters addObject:[ePayParameter key:@"description" value:@"SATCAR registeration payment"]];
        
         if ([userFrom isEqualToString:@"shipTracking"]) {
             [epaylib.parameters addObject:[ePayParameter key:@"subscription" value:@"0"]];
         }else
             [epaylib.parameters addObject:[ePayParameter key:@"subscription" value:@"1"]];
        [epaylib.parameters addObject:[ePayParameter key:@"declinetext" value:@"Payment is declined"]];
        // Set the hash key
        //[epaylib setHashKey:@"MartinBilgrau2013"];
        
        // Show/hide the Cancel button
        [epaylib setDisplayCancelButton:NO];
        self.navigationController.navigationBar.hidden = true;
        // Load the payment window
        [epaylib loadPaymentWindow];
    }else{
        [Helper displayAlertView:@"" message:NSLocalizedString(@"Merchant_info_not_avaialble", @"")];
    }
}

- (void)event:(NSNotification*)notification {
    // Here we handle all events sent from the ePay Lib
    
    if ([[notification name] isEqualToString:PaymentAcceptedNotification]) {
        NSLog(@"EVENT: PaymentAcceptedNotification");
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        for (ePayParameter *item in [notification object]) {
            [dict setValue:item.value forKey:item.key];
        }
        if (dict.allKeys.count > 0) {
            
            if ([userFrom isEqualToString:@"shipTracking"]) {
                    [dict setValue:userID forKey:@"user_id"];
                    [dict setValue:userType forKey:@"user_type"];
                    [dict setValue:[_shipDict valueForKey:@"first_name"] forKey:@"first_name"];
                    [dict setValue:[_shipDict valueForKey:@"last_name"] forKey:@"last_name"];
                    [dict setValue:[_shipDict valueForKey:@"address"] forKey:@"address"];
                    [dict setValue:[_shipDict valueForKey:@"post_number"] forKey:@"post_number"];
                    [dict setValue:[_shipDict valueForKey:@"city"] forKey:@"city"];
                    [dict setValue:[_shipDict valueForKey:@"country"] forKey:@"country"];
            }else{
            userID = [Helper getPREF:@"userID"];
            [dict setValue:userID forKey:@"user_id"];
            }
            NSLog(@"makePayment data : %@",dict);
            [WebService httpPostWithCustomDelegateURLString:@".makepayment" parameter:dict isDebug:false callBackData:^(NSData *data, NSError *error)
             {
                 self.navigationController.navigationBar.hidden = false;
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                 if (error == nil) {
                     
                     
                     NSError *localError = nil;
                     NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
                     NSLog(@"response : %@",parsedObject);
                     if (localError != nil) {
                         NSLog(@"%@",localError.description);
                     }
                     else {
                         NSDictionary *response = [parsedObject objectForKey:@"data"];
                         
                         NSString* message = [response objectForKey:@"message"];
                         NSString* success = [response objectForKey:@"status"];
                        
                         if ([success isEqualToString:@"Success"]) {
                             myAppDelegate.appLogin = @"1";
                             if ([userFrom isEqualToString:@"shipTracking"]) {
                                 [Helper setDataValue:data sKey:@"userInfo"];
                                 UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                 HomeViewController* controller = [storyboard instantiateViewControllerWithIdentifier:@"Home"];
                                 [self.navigationController pushViewController:controller animated:YES];
                             }else{
                                 [myAppDelegate callGetCredentialsService:userID];
                                 [Helper displayAlertView:@"" message:NSLocalizedString(@"Payment_success", @"")];
                                 [self performSegueWithIdentifier:@"segueRegisterToSpeedmeter" sender:self];
                             }
                             
                         } else {
                             NSLog(@"error :: %@",message);
                             [Helper displayAlertView:@"" message:message];
                         }
                     }
                 }
             }];
        }
    }
    else if ([[notification name] isEqualToString:PaymentLoadingAcceptPageNotification]) {
        NSLog(@"EVENT: PaymentLoadingAcceptPageNotification");
    }
    else if ([[notification name] isEqualToString:PaymentWindowCancelledNotification]) {
        NSLog(@"EVENT: PaymentWindowCancelledNotification");
        self.navigationController.navigationBar.hidden = false;
    }
    else if ([[notification name] isEqualToString:PaymentWindowLoadingNotification]) {
        NSLog(@"EVENT: PaymentWindowLoadingNotification");
        // Display a loading indicator while loading the payment window
        //[activityIndicatorView startAnimating];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    else if ([[notification name] isEqualToString:PaymentWindowLoadedNotification]) {
        NSLog(@"EVENT: PaymentWindowLoadedNotification");
        
        // Stop our loading indicator when the payment window is loaded
        //[activityIndicatorView stopAnimating];
        [MBProgressHUD hideHUDForView:self.view animated:true];
    }
    else if ([[notification name] isEqualToString:ErrorOccurredNotification]) {
        // Display error object if we get a error notification
        NSLog(@"EVENT: ErrorOccurredNotification - %@", [notification object]);
    }
    else if ([[notification name] isEqualToString:NetworkActivityNotification]) {
        // Display network indicator in the statusbar
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    }
    else if ([[notification name] isEqualToString:NetworkNoActivityNotification]) {
        // Hide network indicator in the statusbar
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }
}

- (IBAction)SwitchChange:(id)sender {
    if([sender isOn]){
        subscribe_newsletter = @"1";
    }else{
        subscribe_newsletter = @"0";
    }
}

// Handle the user's selection.
- (void)viewController:(GMSAutocompleteViewController *)viewController didAutocompleteWithPlace:(GMSPlace *)place {
    [self dismissViewControllerAnimated:YES completion:nil];
    // Do something with the selected place.
    NSLog(@"Place name %@", place.name);
    NSLog(@"Place address %@", place.formattedAddress);
    NSLog(@"Place attributions %@", place.attributions.string);
    NSString *placeAddress = [NSString stringWithFormat:@"%@,",place.name];
    for (int i = 0; i < place.addressComponents.count; i++) {
        GMSAddressComponent *add = [place.addressComponents objectAtIndex:i];
        //NSLog(@"type : %@",add.type);
        //NSLog(@"type : %@",add.name);
        if ([add.type containsString:@"locality"]) {
            _txtcity.text = add.name;
        }else if ([add.type containsString:@"country"]) {
            _txtcountry.text = add.name;
        }else if ([add.type containsString:@"postal_code"]) {
            _txtpostcode.text = add.name;
        }
        placeAddress = [NSString stringWithFormat:@"%@",place.name];
    }
    NSLog(@"Address : %@",placeAddress);
    _txtAddress.text = placeAddress;
}

- (void)viewController:(GMSAutocompleteViewController *)viewController didFailAutocompleteWithError:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:nil];
    // TODO: handle the error.
    NSLog(@"Error: %@", [error description]);
}

// User canceled the operation.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

// Turn the network activity indicator on and off again.
- (void)didRequestAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)didUpdateAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"segueRegisterToSpeedmeter"]) {
        RegisterStandardUserSpeedmeterVC *controller = segue.destinationViewController;
        controller.userType = userType;
    }
}


@end
