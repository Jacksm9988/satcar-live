
//
//  BeaconConfigureViewController.m
//  SATCAR
//
//  Created by Percept Infotech on 11/07/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import "BeaconConfigureViewController.h"
#import "BeaconTVC.h"
#import "BeaconDetailViewController.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import <CoreLocation/CoreLocation.h>
#import <KontaktSDK/KTKDeviceConnection.h>
#import <KontaktSDK/KTKCloudClient.h>

@interface BeaconConfigureViewController ()<KTKBeaconManagerDelegate,CBCentralManagerDelegate,CLLocationManagerDelegate,KTKDevicesManagerDelegate>{
    
    BOOL isBeaconLoaded,isAnimating;
    AppDelegate *appdelegate;
    NSMutableArray *localBeaconsArray;
    
    BOOL isBluetoothOn,isStart,isIndicatorAdded;
    NSMutableDictionary *sendDataDict;
    UIActivityIndicatorView *activityIndicator;
    CLBeacon *currentBeacon;
    KTKDevice *device;
    KTKNearbyDevice *nearDevice;
    NSArray *cloudBeaconArray;
    KTKDeviceConfiguration *deviceConfiguration;
}

@property (nonatomic, strong) NSArray *beaconsArray;
@property (nonatomic, strong) NSArray *discoverBeaconsArray;
@property (weak, nonatomic) IBOutlet UIButton *btnStart;
@property (weak, nonatomic) IBOutlet UIView *viewActivity;
@property (nonatomic,strong) CLLocationManager *locationManager;
@property (strong, nonatomic) KTKDevicesManager *devicesManager;
@property (strong, nonatomic) KTKCloudClient *kontaktCloud;
@property (strong, nonatomic) CBCentralManager *bluetoothManager;
@end

@implementation BeaconConfigureViewController
@synthesize KTKbeaconManager,KTKregion;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    localBeaconsArray = [[NSMutableArray alloc]init];
    isBluetoothOn = false;
    isStart = false;
    appdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    isIndicatorAdded = false;
    self.tableview.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    _kontaktCloud = [[KTKCloudClient alloc]init];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshItems) name:UIApplicationDidBecomeActiveNotification object:nil];
    [_swtchBluetooth addTarget:self action:@selector(changed:) forControlEvents:UIControlEventValueChanged];

}

-(void)viewWillAppear:(BOOL)animated{
    //[self fetchDevicesFromCloud];
    isAnimating =false;
    [self addTitleView:NSLocalizedString(@"satcar_beep", @"")];
    localBeaconsArray = [[NSMutableArray alloc]init];
    self.beaconsArray = [[NSMutableArray alloc]init];
    [_tableview reloadData];
    [_btnStart setTitle:@"Nil" forState:UIControlStateNormal];
    if(!_bluetoothManager) {
        _bluetoothManager = [[CBCentralManager alloc] initWithDelegate:self queue:dispatch_get_main_queue()] ;
    }
    [self centralManagerDidUpdateState:_bluetoothManager]; // Show initial state*/
    [self callGetBeaconList];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return [NSString stringWithFormat:NSLocalizedString(@"MY_satcar_beep", @"")];
    }else{
        return [NSString stringWithFormat:NSLocalizedString(@"Other_devices", @"")];
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 50.0f)];
    view.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:247/255.0 blue:253/255.0 alpha:1.0];
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 150, 49)];
    [lbl setFont:[UIFont fontWithName:@"SF UI Text Regular" size:18]];
    [lbl setTextColor:[UIColor darkGrayColor]];
    lbl.backgroundColor = [UIColor clearColor];
    if (section == 0) {
        [lbl setText:NSLocalizedString(@"MY_satcar_beep", @"")];
    }else{
        [lbl setText:NSLocalizedString(@"Other_devices", @"")];
    }
    [view addSubview:lbl];
    
    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, 49, self.view.bounds.size.width, 1)];
    [line setBackgroundColor:[UIColor lightGrayColor]];
    [view addSubview:line];
    if (section == 1) {
        
        if (!isIndicatorAdded) {
            //Create and add the Activity Indicator to splashView
        }
        
    }
    return view;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section == 0) {
        if (localBeaconsArray.count > 0) {
            [MBProgressHUD hideHUDForView:self.view animated:true];
            return [localBeaconsArray count];
        }else{
            return 1;
        }
        
    }else{
        if (self.beaconsArray.count > 0) {
            [MBProgressHUD hideHUDForView:self.view animated:true];
        }else{
            [activityIndicator stopAnimating];
        }
        return [self.beaconsArray count];
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    BeaconTVC *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    NSString *title = @"";
    if (indexPath.section == 0) {
        if (localBeaconsArray.count == 0) {
            cell.lblInfo.text = [NSString stringWithFormat:NSLocalizedString(@"No_device_configured", @"")];
            cell.btnPair.hidden = true;
            cell.imgDetail.hidden = true;
            cell.lblInfo.hidden = false;
            cell.lblTitle.hidden = true;
        }else{
            if (localBeaconsArray.count > indexPath.row){
                NSDictionary *beacon = [localBeaconsArray objectAtIndex:indexPath.row];
                //title = [NSString stringWithFormat:@"%@",[beacon valueForKey:@"proximityUUID"]];
                title = [NSString stringWithFormat:@"%@",[beacon valueForKey:@"name"]];
                cell.lblTitle.text = [NSString stringWithFormat:@"SATCAR-BEEP-%@",title];
                cell.btnPair.hidden = true;
                cell.imgDetail.hidden = false;
                cell.lblInfo.hidden = true;
                cell.lblTitle.hidden = false;
            }else{
                cell.lblInfo.text = [NSString stringWithFormat:NSLocalizedString(@"No_device_configured", @"")];
                cell.btnPair.hidden = true;
                cell.imgDetail.hidden = true;
                cell.lblInfo.hidden = false;
                cell.lblTitle.hidden = true;
            }
        }
    }else{
        
        if (self.beaconsArray.count > indexPath.row){
            id beacon = [self.beaconsArray objectAtIndex:indexPath.row];
        
        if ([beacon isKindOfClass:[CLBeacon class]]){
            CLBeacon *cBeacon = (CLBeacon *)beacon;
            
            device = [self findCloudDevice:cBeacon];
            NSDictionary *data = [self getBatteryLavel:device];
            
            title = [NSString stringWithFormat:@"%@",cBeacon.proximityUUID ];
            if (data != nil) {
                NSString *name = [data valueForKey:@"name"];
                if (name.length <= 0) {
                    cell.lblTitle.text = [NSString stringWithFormat:@"SATCAR-BEEP-%@",[title substringFromIndex: [title length] - 5]];
                }else{
                    cell.lblTitle.text = [NSString stringWithFormat:@"SATCAR-BEEP-%@",[data valueForKey:@"name"]];
                }
            }else{
                cell.lblTitle.text = [NSString stringWithFormat:@"SATCAR-BEEP-%@",[title substringFromIndex: [title length] - 5]];
            }
            
            cell.btnPair.hidden = false;
            cell.imgDetail.hidden = true;
            cell.lblInfo.hidden = true;
            cell.btnPair.tag = indexPath.row;
            cell.lblTitle.hidden = false;
            [cell.btnPair addTarget:self action:@selector(onBeaconConfigure:) forControlEvents:UIControlEventTouchUpInside];
            
        }
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [_tableview deselectRowAtIndexPath:indexPath animated:true];
    sendDataDict = [[NSMutableDictionary alloc]init];
    NSString *title;
    
    if (indexPath.section == 0) {
        if (localBeaconsArray.count > 0) {
            NSDictionary *beacon = [localBeaconsArray objectAtIndex:indexPath.row];
            NSString *ttl = [NSString stringWithFormat:@"%@",[beacon valueForKey:@"proximityUUID"]];
            title = [NSString stringWithFormat:@"SATCAR-BEEP-%@",[ttl substringFromIndex: [ttl length] - 5]];
            
            [sendDataDict setValue:title forKey:@"navTitle"];
            [sendDataDict setValue:[beacon valueForKey:@"proximityUUID"] forKey:@"id"];
            [sendDataDict setValue:[beacon valueForKey:@"id"] forKey:@"beaconID"];
            [sendDataDict setValue:[beacon valueForKey:@"major"] forKey:@"major"];
            [sendDataDict setValue:[beacon valueForKey:@"minor"] forKey:@"minor"];
            [sendDataDict setValue:[beacon valueForKey:@"measuredPower"] forKey:@"measuredPower"];
            [sendDataDict setValue:[beacon valueForKey:@"rssi"] forKey:@"rssi"];
            [sendDataDict setValue:@"1" forKey:@"connectStatus"];
            
            [self performSegueWithIdentifier:@"segueBeaconDetail" sender:self];
        }
    }else{
        
        id beacon = [self.beaconsArray objectAtIndex:indexPath.row];
        CLBeacon *cBeacon = (CLBeacon *)beacon;
        
        NSString *ttl = [NSString stringWithFormat:@"%@",cBeacon.proximityUUID ];
        title = [NSString stringWithFormat:@"SATCAR-BEEP-%@",[ttl substringFromIndex: [ttl length] - 5]];
        [sendDataDict setValue:title forKey:@"navTitle"];
        [sendDataDict setValue:ttl forKey:@"id"];
        [sendDataDict setValue:@"" forKey:@"beaconID"];
        [sendDataDict setValue:cBeacon.major forKey:@"major"];
        [sendDataDict setValue:cBeacon.minor forKey:@"minor"];
        
        device = [self findCloudDevice:cBeacon];
        if (device == nil) {
            [sendDataDict setValue:@"" forKey:@"rssi"];
            [sendDataDict setValue:@"" forKey:@"measuredPower"];
        }else{
            NSDictionary *data = [self getBatteryLavel:device];
            [sendDataDict setValue:[data valueForKey:@"bLevel"] forKey:@"rssi"];
            [sendDataDict setValue:[data valueForKey:@"dist"] forKey:@"measuredPower"];
        }
        [sendDataDict setValue:@"0" forKey:@"connectStatus"];
        [self performSegueWithIdentifier:@"segueBeaconDetail" sender:self];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        if (localBeaconsArray.count == 0) {
            return 70;
        }else{
            return 60;
        }
    }
    return 60;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50;
}

#pragma mark - button action
- (IBAction)onBeaconConfigure:(UIButton *)sender {
    int tag = (int)sender.tag;
    id beacon = [self.beaconsArray objectAtIndex:tag];
    if ([beacon isKindOfClass:[CLBeacon class]]){
        CLBeacon *cBeacon = (CLBeacon *)beacon;
        currentBeacon = (CLBeacon *)beacon;
        NSString *userID = [Helper getPREF:@"userID"];
        //NSString *rssi = [NSString stringWithFormat:@"%ld",(long)cBeacon.rssi];
        
        device = [self findCloudDevice:cBeacon];
        if (device == nil) {
            [Helper displayAlertView:@"" message:NSLocalizedString(@"Error_while_fetching_devices", @"")];
            [self.navigationController popViewControllerAnimated:true];
        }else{
            NSDictionary *data = [self getBatteryLavel:device];
            NSString *name = [data valueForKey:@"name"];
            if (name == nil) {
                name = @"";
            }
            
            if (device != nil) {
                NSDictionary *parameter=@{ @"userid":userID, @"name":name, @"major":cBeacon.major, @"minor":cBeacon.minor, @"rssi":[data valueForKey:@"bLevel"], @"measuredPower":[data valueForKey:@"dist"],@"proximityUUID":cBeacon.proximityUUID, @"macAddress":device.macAddress};
                //NSLog(@"Parameter : %@",parameter);
                [self callConnectBeep:parameter];
            }else{
                [Helper displayAlertView:@"" message:NSLocalizedString(@"Error_while_fetching_devices", @"")];
            }
        }
        
    }
}

-(KTKDevice *)findCloudDevice:(CLBeacon *)cBeacon{
    //NSLog(@"findCloudDevice");
    device = nil;
    NSString *major = [NSString stringWithFormat:@"%@",cBeacon.major];
    NSString *minor = [NSString stringWithFormat:@"%@",cBeacon.minor];
    NSString *proximityUUID = [NSString stringWithFormat:@"%@",cBeacon.proximityUUID];
    NSString *macAddress;
    if (cloudBeaconArray.count > 0) {
        for (int i = 0; i < cloudBeaconArray.count; i++) {
            device = [cloudBeaconArray objectAtIndex:i];
            deviceConfiguration = [device configuration];
            NSString *cMajor = [NSString stringWithFormat:@"%@",deviceConfiguration.major];
            NSString *cMinor = [NSString stringWithFormat:@"%@",deviceConfiguration.minor];
            NSString *cProximityUUID = [NSString stringWithFormat:@"%@",deviceConfiguration.proximityUUID];
            if ([cMajor isEqualToString:major] && [cMinor isEqualToString:minor] && [cProximityUUID isEqualToString:proximityUUID]) {
                macAddress = device.macAddress;
                break;
            }
        }
        return device;
    }else{
        return device;
    }
}

-(NSDictionary *)getBatteryLavel:(KTKDevice *)ktdevice{
    //NSLog(@"getBatteryLavel");
    NSString *batteryLavel = @"";
    NSString *distance = @"0",*deviceID = @"";
    NSDictionary *param;
    if (_discoverBeaconsArray.count > 0) {
        for (int i = 0; i < _discoverBeaconsArray.count; i++) {
            nearDevice = [_discoverBeaconsArray objectAtIndex:i];
            if ([nearDevice.uniqueID isEqualToString:ktdevice.uniqueID]) {
                deviceID = nearDevice.uniqueID;
                batteryLavel = [NSString stringWithFormat:@"%lu",(unsigned long)nearDevice.batteryLevel];
                double dist = pow(nearDevice.transmissionPower, [nearDevice.RSSI doubleValue]);
                if (dist < 0) {
                    dist = 0;
                }
                distance = [NSString stringWithFormat:@"%.2f",dist];
            }
        }
    }else{
        batteryLavel = @"0";
        distance = @"0";
        //nearDevice.uniqueID = @"";
    }
    param = @{@"bLevel" : batteryLavel,@"dist" : distance,@"name" : deviceID};
    return param;
}

-(void)fetchDevicesFromCloud{
    
    /*if(![self CheckNetworkConnection]) {
        return;
    }*/
    if (!isAnimating) {
        [MBProgressHUD showHUDAddedTo:self.view animated:true];
    }
    
    [_kontaktCloud getObjects:[KTKDevice self] completion:^(KTKKontaktResponse * _Nullable response, NSError * _Nullable error) {
        NSString *title,*message;
        [MBProgressHUD hideHUDForView:self.view animated:true];
        
        NSDictionary *ktkError = KTKCloudErrorFromError(error);
        if (ktkError != nil) {
            title = NSLocalizedString(@"Error_while_fetching_devices", @"");
            message = ktkError.debugDescription;
            NSLog(@"Title : %@",title);
            NSLog(@"message : %@",message);
            [Helper displayAlertView:@"" message:message];
            [self.navigationController popViewControllerAnimated:true];
        } else{
            NSLog(@"Fetched %@ device(s) from API",response);
           // if([response isKindOfClass:[NSArray class]])
           // {
                cloudBeaconArray = [response objects];
                NSLog(@"Fetched %d device(s) from API",(int)cloudBeaconArray.count);
                if (cloudBeaconArray.count > 0) {
                    [_tableview reloadData];
                }else{
                    [Helper displayAlertView:@"" message:NSLocalizedString(@"Error_while_fetching_devices", @"")];
                    [self.navigationController popViewControllerAnimated:true];
                }
           // }
            
        }
    }];
    
}

-(void)initializeBeacon{
    
    KTKbeaconManager = [[KTKBeaconManager alloc] initWithDelegate:self];
    _devicesManager  = [[KTKDevicesManager alloc]initWithDelegate:self];
    
    [KTKbeaconManager requestLocationAlwaysAuthorization];
    if ([KTKbeaconManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [KTKbeaconManager requestLocationAlwaysAuthorization];
    }else{
        [KTKbeaconManager requestLocationAlwaysAuthorization];
        [KTKbeaconManager requestLocationWhenInUseAuthorization];
    }
    
    if ([KTKBeaconManager isRangingAvailable]) {
        if ([KTKBeaconManager locationAuthorizationStatus] == kCLAuthorizationStatusAuthorizedAlways || [KTKBeaconManager locationAuthorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) {
            NSUUID *proximityUUID = [[NSUUID alloc] initWithUUIDString:@"f7826da6-4fa2-4e98-8024-bc5b71e0893e"];
            
            // Create region instance
            KTKregion = [[KTKBeaconRegion alloc] initWithProximityUUID: proximityUUID identifier:appdelegate.beaconRegionName];
            
            // Start Monitoring
            //[self.beaconManager startMonitoringForRegion: region];
            
            // You can also start ranging ...
            //[KTKbeaconManager startRangingBeaconsInRegion: KTKregion];
            [self startRangingBeacons];
            
            
        }
    }
    [_devicesManager startDevicesDiscoveryWithInterval:1.0];
}

-(void)startRangingBeacons{
    appdelegate.currentStage = CLRegionStateUnknown;
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined){
        [KTKbeaconManager requestLocationAlwaysAuthorization];
        [KTKbeaconManager requestLocationWhenInUseAuthorization];
        NSLog(@"kCLAuthorizationStatusNotDetermined");
    }else if([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied){
        [MBProgressHUD hideHUDForView:self.view animated:true];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"Denied_access_location", @"")                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }else if([CLLocationManager authorizationStatus] == kCLAuthorizationStatusRestricted){
        [MBProgressHUD hideHUDForView:self.view animated:true];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"Restrict_location_access", @"") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }else if([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse){
        NSLog(@"Started ranging beacon......");
        [KTKbeaconManager startRangingBeaconsInRegion: KTKregion];
        [_devicesManager startDevicesDiscoveryWithInterval:1.0];
        
    }
}



// Deprecated 1.4
/*- (void)devicesManager:(KTKDevicesManager*)manager didDiscoverDevices:(NSArray <KTKNearbyDevice*>* _Nullable)devices{
    NSLog(@"DevicesManager Discover : %@",devices);
    _discoverBeaconsArray = [[NSArray alloc]init];
    _discoverBeaconsArray = [devices mutableCopy];
}

- (void)devicesManagerDidFailToStartDiscovery:(KTKDevicesManager*)manager withError:(NSError* _Nullable)error{
    NSLog(@"Failed Discover : %@",error.description);
    //[Helper displayAlertView:@"" message:error.description];
}*/

- (void)devicesManager:(KTKDevicesManager *)manager didDiscoverDevices:(NSArray<KTKNearbyDevice *> *)devices{
    //NSLog(@"DevicesManager Discover : %@",devices);
    _discoverBeaconsArray = [[NSArray alloc]init];
    _discoverBeaconsArray = [devices mutableCopy];
}

- (void)devicesManagerDidFailToStartDiscovery:(KTKDevicesManager *)manager withError:(NSError *)error{
    NSLog(@"Failed Discover : %@",error.description);
}

- (void)beaconManager:(KTKBeaconManager*)manager didRangeBeacons:(NSArray <CLBeacon *>*)beacons inRegion:(__kindof KTKBeaconRegion*)regions{
    //NSLog(@"becaons :%@",beacons);
    self.beaconsArray = [[NSArray alloc]init];
    self.beaconsArray = [beacons mutableCopy];
    [_tableview reloadData];
}

- (void)beaconManager:(KTKBeaconManager*)manager didChangeLocationAuthorizationStatus:(CLAuthorizationStatus)status{
    if (status == kCLAuthorizationStatusAuthorizedAlways || status == kCLAuthorizationStatusAuthorizedWhenInUse ) {
        [KTKbeaconManager startRangingBeaconsInRegion: KTKregion];
        [_devicesManager startDevicesDiscoveryWithInterval:1.0];
    }else{
        [KTKbeaconManager stopRangingBeaconsInAllRegions];
        [_devicesManager stopDevicesDiscovery];
    }
}



#pragma mark - webservice call
-(void)callConnectBeep:(NSDictionary *)detail{
    
    if(![self CheckNetworkConnection]) {
        return;
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [WebService httpPostWithCustomDelegateURLString:@".connectBeep" parameter:detail isDebug:false callBackData:^(NSData *data, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         if (error == nil) {
             NSError *localError = nil;
             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
             
             if (localError != nil) {
                 NSLog(@"%@",localError.description);
             }
             else {
                 NSLog(@"response : %@",parsedObject);
                 id response = [parsedObject objectForKey:@"data"];
                 NSString *status = [response valueForKey:@"status"];
                 if ([status.lowercaseString isEqualToString:@"success"]) {
                     
                     [Helper setPREFStringValue:[detail valueForKey:@"major"] sKey:@"major"];
                     [Helper setPREFStringValue:[detail valueForKey:@"minor"] sKey:@"minor"];
                     [Helper setPREFStringValue:@"1" sKey:@"isBeaconConnected"];
                     [Helper displayAlertView:@"" message:NSLocalizedString(@"beacon_message", @"")];
                     [KTKbeaconManager stopRangingBeaconsInAllRegions];
                     [_devicesManager stopDevicesDiscovery];
                     [self callGetBeaconList2];
                     [self.navigationController popViewControllerAnimated:true];
                 }else{
                     [Helper displayAlertView:@"" message:NSLocalizedString(@"Please_try_other_device", @"")];
                 }
             }
         }
     }];
}

- (void)callGetBeaconList{
    NSLog(@"Service : .getBeep");
    [self.view endEditing:YES];
    
    if(![self CheckNetworkConnection]) {
        return;
    }
    isAnimating = true;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *userID = [Helper getPREF:@"userID"];
    NSDictionary *parameter=@{ @"userid":userID };
    NSLog(@"register data : %@",parameter);
    
    [WebService httpPostWithCustomDelegateURLString:@".getBeep" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         if (error == nil) {
             appdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
             isAnimating = false;
             
             NSError *localError = nil;
             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
             
             if (localError != nil) {
                 NSLog(@"%@",localError.description);
             }
             else {
                 NSDictionary *response = [parsedObject objectForKey:@"data"];
                 
                 NSString* success = [response objectForKey:@"status"];
                 NSLog(@"Response : %@",response);
                 if ([success isEqualToString:@"Success"]) {
                     
                     [self fetchDevicesFromCloud];
                     //[self performSelector:@selector(fetchDevicesFromCloud) withObject:nil afterDelay:1];
                     localBeaconsArray = [[NSMutableArray alloc]init];
                     localBeaconsArray = [response valueForKey:@"beep"];
                     if (localBeaconsArray.count > 0) {
                         [Helper setPREFStringValue:@"1" sKey:@"isBeaconConnected"];
                     }
                     [Helper setUserArrayValue:localBeaconsArray sKey:@"beaconsArray"];
                     //[appdelegate performSelectorOnMainThread:@selector(beaconDetection) withObject:nil waitUntilDone:YES];
                     //[appdelegate performSelectorInBackground:@selector(beaconDetection) withObject:nil];
                     
                     
                    //[appdelegate beaconDetection];
                     
                    
                     
                     //[self changeBeaconStatus];
                     
                 } else {
                     [self fetchDevicesFromCloud];
                     //[self performSelector:@selector(fetchDevicesFromCloud) withObject:nil afterDelay:1];
                     [_tableview reloadData];
                     //[Helper displayAlertView:@"" message:NSLocalizedString(@"No_device_configured", @"")];
                 }
             }
         }
     }];
}


- (void)callGetBeaconList2{
    NSLog(@"Service : .getBeep");
    [self.view endEditing:YES];
    
    if(![self CheckNetworkConnection]) {
        return;
    }
    isAnimating = true;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *userID = [Helper getPREF:@"userID"];
    NSDictionary *parameter=@{ @"userid":userID };
    NSLog(@"register data : %@",parameter);
    
    [WebService httpPostWithCustomDelegateURLString:@".getBeep" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         if (error == nil) {
             appdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
             isAnimating = false;
             
             NSError *localError = nil;
             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
             
             if (localError != nil) {
                 NSLog(@"%@",localError.description);
             }
             else {
                 NSDictionary *response = [parsedObject objectForKey:@"data"];
                 
                 NSString* success = [response objectForKey:@"status"];
                 NSLog(@"Response : %@",response);
                 if ([success isEqualToString:@"Success"]) {
                     
                     [self fetchDevicesFromCloud];
                     //[self performSelector:@selector(fetchDevicesFromCloud) withObject:nil afterDelay:1];
                     localBeaconsArray = [[NSMutableArray alloc]init];
                     localBeaconsArray = [response valueForKey:@"beep"];
                     if (localBeaconsArray.count > 0) {
                         [Helper setPREFStringValue:@"1" sKey:@"isBeaconConnected"];
                     }
                     [Helper setUserArrayValue:localBeaconsArray sKey:@"beaconsArray"];
                     //[appdelegate performSelectorOnMainThread:@selector(beaconDetection) withObject:nil waitUntilDone:YES];
                     //[appdelegate performSelectorInBackground:@selector(beaconDetection) withObject:nil];
                     
                     
                     [appdelegate beaconDetection];
                     
                     
                     
                     //[self changeBeaconStatus];
                     
                 } else {
                     [self fetchDevicesFromCloud];
                     //[self performSelector:@selector(fetchDevicesFromCloud) withObject:nil afterDelay:1];
                     [_tableview reloadData];
                     //[Helper displayAlertView:@"" message:NSLocalizedString(@"No_device_configured", @"")];
                 }
             }
         }
     }];
}


-(void)changeBeaconStatus{
    // Status 0= ,1=connected, 2=started monitoring
    for (int i = 0; i < localBeaconsArray.count; i++) {
        NSDictionary *data = [localBeaconsArray objectAtIndex:i];
        NSMutableDictionary *dataCopy = [data mutableCopy];
        if (![Helper containsKey:@"Status" data:data]) {
            [dataCopy setValue:@"1" forKey:@"Status"];
        }
        [localBeaconsArray replaceObjectAtIndex:i withObject:dataCopy];
    }
    [Helper setUserArrayValue:localBeaconsArray sKey:@"beaconsArray"];
}

#pragma mark - central manager delegate
- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    NSString *stateString = nil;
   
    switch(_bluetoothManager.state) {
        case CBCentralManagerStateResetting:
            isBluetoothOn = false;
            [_swtchBluetooth setOn:false animated:true];
            [[NSUserDefaults standardUserDefaults] setBool:isBluetoothOn forKey:@"BluetoothStat"];
            [_btnStart setTitle:@"stop" forState:UIControlStateNormal];
            break;
            
        case CBCentralManagerStateUnsupported:
        case CBCentralManagerStatePoweredOff:
            stateString = @"Bluetooth is currently powered off.";
            
            isBluetoothOn = false;
            [_swtchBluetooth setOn:false animated:true];
            [[NSUserDefaults standardUserDefaults] setBool:isBluetoothOn forKey:@"BluetoothStat"];
            [_btnStart setTitle:@"stop" forState:UIControlStateNormal];
            _beaconsArray  = [[NSMutableArray alloc]init];
            [_tableview reloadData];
            break;
            
        case CBCentralManagerStatePoweredOn:
            stateString = @"Bluetooth is currently powered on and available to use.";
            
            isBluetoothOn = true;
            [[NSUserDefaults standardUserDefaults] setBool:isBluetoothOn forKey:@"BluetoothStat"];
            [_swtchBluetooth setOn:true animated:true];
            [_btnStart setTitle:@"start" forState:UIControlStateNormal];
            [self initializeBeacon];
            
            break;
        default:
            stateString = @"State unknown, update imminent.";
            isBluetoothOn = false;
            
            [_swtchBluetooth setOn:false animated:true];
            [_btnStart setTitle:@"stop" forState:UIControlStateNormal];
            [[NSUserDefaults standardUserDefaults] setBool:isBluetoothOn forKey:@"BluetoothStat"];
            _beaconsArray  = [[NSMutableArray alloc]init];
            [_tableview reloadData];
            break;
    }
    NSLog(@"Bluetooth State : %@",stateString);
}

-(void)changed:(id)sender{
    if ([sender isOn]) {
        
        isBluetoothOn = true;
        /*if (_bluetoothManager.state != CBCentralManagerStatePoweredOn) {
            NSURL *url = [NSURL URLWithString:@"prefs:root=Bluetooth"];
            if ([[UIApplication sharedApplication] canOpenURL:url]) {   //Pre iOS 10
                [[UIApplication sharedApplication] openURL:url];
            } else  {   //iOS 10
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-Prefs:root=Bluetooth"]];
            }
           // [[UIApplication sharedApplication] openURL: [NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }
        // [[NSUserDefaults standardUserDefaults] setBool:isBluetoothOn forKey:@"BluetoothStat"];
        [_btnStart setTitle:@"start" forState:UIControlStateNormal];
        [activityIndicator startAnimating];*/
        
        
    } else {
        
        isBluetoothOn = false;
       /* NSURL *url = [NSURL URLWithString:@"prefs:root=Bluetooth"];
        if ([[UIApplication sharedApplication] canOpenURL:url]) {   //Pre iOS 10
            [[UIApplication sharedApplication] openURL:url];
        } else  {   //iOS 10
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-Prefs:root=Bluetooth"]];
        }
        // [[UIApplication sharedApplication] openURL: [NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        //[[NSUserDefaults standardUserDefaults] setBool:isBluetoothOn forKey:@"BluetoothStat"];
        [_btnStart setTitle:@"stop" forState:UIControlStateNormal];
        [activityIndicator stopAnimating];*/
    }
   
}

-(void)refreshItems{
    NSLog(@"b state : %d",(int)_bluetoothManager.state);
    
    if (_bluetoothManager.state == CBCentralManagerStatePoweredOn) {
        [_swtchBluetooth setOn:true animated:true];
        [_btnStart setTitle:@"start" forState:UIControlStateNormal];
        [activityIndicator startAnimating];
        [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:@"BluetoothStat"];
    }else{
        [_swtchBluetooth setOn:false animated:true];
        [_btnStart setTitle:@"stop" forState:UIControlStateNormal];
        [activityIndicator stopAnimating];
        [[NSUserDefaults standardUserDefaults] setBool:FALSE forKey:@"BluetoothStat"];
    }
}
- (IBAction)onStart:(id)sender {
    if (!isStart) {
        isStart = true;
    }else{
        isStart = false;
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    BeaconDetailViewController *vc = segue.destinationViewController;
    vc.beaconDetail = sendDataDict;
}

-(void)viewDidDisappear:(BOOL)animated{
    [KTKbeaconManager stopRangingBeaconsInRegion:KTKregion];
   [_devicesManager stopDevicesDiscovery];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
