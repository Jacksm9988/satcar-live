//
//  LocationListViewController.h
//  SATCAR
//
//  Created by Percept Infotech on 16/01/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationListViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableview;


@property (weak, nonatomic) IBOutlet UIView *viewLocationDisplay;
@property (weak, nonatomic) IBOutlet UIView *viewBackLocation;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintLocationPopupHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintLocationPopupTop;
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;

@property (weak, nonatomic) IBOutlet UILabel *lblLocationpmessage;
@property (weak, nonatomic) IBOutlet UITextField *txtNameValue;
@property (weak, nonatomic) IBOutlet UITextField *txtAddressValue;
@property (weak, nonatomic) IBOutlet UITextField *txtZipValue;
@property (weak, nonatomic) IBOutlet UITextField *txtCountryValue;
//@property (weak, nonatomic) IBOutlet UITextField *txtHouseNumberValue;

@property (weak, nonatomic) IBOutlet UIButton *btnConfirm;
@property (weak, nonatomic) IBOutlet UIButton *btnEditManually;
- (IBAction)onEditManually:(id)sender;
- (IBAction)onConfirm:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnMap;
- (IBAction)onMap:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewName;
@property (weak, nonatomic) IBOutlet UIView *viewAddress;
@property (weak, nonatomic) IBOutlet UIView *viewZip;
@property (weak, nonatomic) IBOutlet UIView *viewCountry;
@property (weak, nonatomic) IBOutlet UIView *viewHousenumber;

@end
