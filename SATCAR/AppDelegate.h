//
//  AppDelegate.h
//  SATCAR
//
//  Created by Percept Infotech on 12/7/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import <AVFoundation/AVFoundation.h>
#import <CoreAudio/CoreAudioTypes.h>
#import <AudioToolbox/AudioToolbox.h>
#import <CoreLocation/CoreLocation.h>
#import <KontaktSDK/KontaktSDK.h>
#import "BeaconEditTripViewController.h"
#import <CoreMotion/CoreMotion.h>

#define kTimeoutUserInteraction 1800

@import GooglePlaces;

@interface AppDelegate : UIResponder
{
    NSString *WebServiceUrl,*SelectNote,*SelectWeb,*SelectPriority,*oldPwd,*Newpwd,*Email;
       UIAlertView *alertSync;
    BOOL isalert;
    AVAudioPlayer* audioPlayer;
    NSTimer *idleTimer;
    NSTimer *popupTimer;

}
@property (strong, nonatomic)NSString *SelectNote,*SelectWeb,*oldPwd,*Newpwd,*Email;

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic)  NSString *WebServiceUrl,*SelectPriority;
@property (nonatomic)   BOOL networkReachable;
@property(nonatomic)BOOL isAppStartWithoutNet;
@property(nonatomic)BOOL internetActive;
@property(nonatomic)BOOL hostActive;
-(void)callLocationService:(NSString *)userID from:(NSString *)from undefine_trips_count:(int)undefine_trips_count;
-(void)callLocationService_withTips:(NSString *)userID from:(NSString *)from;
@property (strong, nonatomic) NSTimer *appTimer;
@property (strong, nonatomic) NSTimer *exitTimer;
@property (strong, nonatomic) NSDate *appStartTime;
@property (strong, nonatomic) NSString *appClockTime;
@property (strong, nonatomic) NSString *appKM;
@property (strong, nonatomic) NSMutableArray *appLocationDataArray;
@property (strong, nonatomic) NSString *appFirstLaunch;
@property (strong, nonatomic) NSString *appSTTime;
@property (strong, nonatomic) NSString *appSTDate;
@property (strong, nonatomic) NSString *appDistance;
@property (strong, nonatomic) NSString *appEDTime;
@property (strong, nonatomic) NSString *appLogin;
@property (strong, nonatomic) NSString *appLaunch;
-(void)clearMapData;
- (BOOL)connected;
@property (strong, nonatomic) NSMutableDictionary *appTempSendDataDict;
@property (strong, nonatomic) NSString *appTempFromScreen;
@property (strong, nonatomic) CLLocation *appStartLocation;
@property (strong, nonatomic) CLLocation *appUserLocation;
@property (strong, nonatomic) CLLocation *appOldLocation;

@property (strong, nonatomic) NSString *epay_info;
@property (strong, nonatomic) NSString *dantracker;
@property (strong, nonatomic) NSString *freeUserMessage;

//beacon related properties

@property (strong, nonatomic) NSString *BeaconProximityUUID;
@property (strong, nonatomic) NSString *BeaconMajor;
@property (strong, nonatomic) NSString *BeaconMinor;
@property (strong, nonatomic) NSString *isBeaconConnected;
@property (nonatomic) CLRegionState currentStage;
@property (nonatomic) CLRegionState *PreviousStage;
//@property (nonatomic) NSString *BeaconStage;
@property (nonatomic) NSString *BeaconNotifConnected;
@property (nonatomic) NSString *BeaconNotifDisconnected;
@property (nonatomic) NSString *ConnectedbeaconRegion;
@property (nonatomic) NSString *ConnectionStatus;
@property (nonatomic) NSString *strDeviceToken;
@property (nonatomic) NSString *tracker_id;
@property (nonatomic) NSString *selectedRange;
@property (nonatomic, strong) NSMutableArray *BeaconArray;
@property (nonatomic, strong) KTKBeaconRegion *region22;
-(void)callGetCredentialsService:(NSString *)uID;
-(void)beaconDetection;
-(void)stopBeaconDetection;
-(void)callTrackingUpdateService:(NSString *)status;
@property (strong, nonatomic) KTKBeaconManager *KTKbeaconManager;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) CLLocationManager *anotherLocationManager;
@property (strong, nonatomic) NSString *finalTrack;

@property (strong, nonatomic) NSDate *beaconDateFirst;
@property (strong, nonatomic) NSDate *beaconDateSecond;

// for trip reload
@property (strong, nonatomic) NSString *isRelaodTrip;
@property (strong, nonatomic) NSString *beaconRegionName;
@property int exitTimerCount;
@property(nonatomic,strong) UIApplication *app;
@property(nonatomic,strong) NSString *appState;
@property(nonatomic,strong) NSString *appLanguage;
@property(nonatomic,strong) NSString *appTrackNotification;
@property(nonatomic,strong) NSString *googleApiKey;

@property(nonatomic,strong) NSString *appVersion;
@property(nonatomic,strong) NSString *appBuild;
@property(nonatomic,strong) NSString *freeTrip;

@property(nonatomic,strong) NSArray *dantrackerCarLocationArray;
@property(nonatomic,strong) NSString *isTripContinue;
@property(nonatomic,strong) NSString *isTripAutomatic;
@property(nonatomic,strong) NSString *beaconWarningMessage;

@property(nonatomic,strong) CMMotionActivityManager *activityManager;
@property(nonatomic,strong) CMMotionActivity *activity;
@property(nonatomic,strong) NSString *locAccuracy;

@property(nonatomic,strong) NSString *locationCounter;

-(void)setApplicationLanguage;
-(void)stopTripAndSave;
@end

