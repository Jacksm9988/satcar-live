//
//  ChangePasswordViewController.m
//  SATCAR
//
//  Created by Percept Infotech on 03/01/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "UIViewController+VCHelpers.h"
@interface ChangePasswordViewController ()<UITextFieldDelegate>

@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self addTitleView:NSLocalizedString(@"ChangePassword", @"")];
    
    _BtnResetPwd.layer.cornerRadius = 25;
    _BtnResetPwd.layer.borderWidth = 1;
    _BtnResetPwd.layer.borderColor = [UIColor colorWithRed:0.000 green:0.651 blue:0.929 alpha:1.00].CGColor;
    [_BtnResetPwd setTitle:NSLocalizedString(@"ChangePassword", @"") forState:UIControlStateNormal];
    
    _txtOldPwd.placeholder = NSLocalizedString(@"currentpassword", @"");
    _txtOldPwd.layer.cornerRadius = 5;
    _txtOldPwd.layer.borderWidth = 1;
    _txtOldPwd.layer.borderColor = [UIColor colorWithRed:0.902 green:0.902 blue:0.910 alpha:1.00].CGColor;
    
    _txtNewPwd.placeholder = NSLocalizedString(@"newpassword", @"");
    _txtNewPwd.layer.cornerRadius = 5;
    _txtNewPwd.layer.borderWidth = 1;
    _txtNewPwd.layer.borderColor = [UIColor colorWithRed:0.902 green:0.902 blue:0.910 alpha:1.00].CGColor;

    _txtConfirmPwd.placeholder =NSLocalizedString(@"Confirmpassword", @"");
    _txtConfirmPwd.layer.cornerRadius = 5;
    _txtConfirmPwd.layer.borderWidth = 1;
    _txtConfirmPwd.layer.borderColor = [UIColor colorWithRed:0.902 green:0.902 blue:0.910 alpha:1.00].CGColor;
}


-(BOOL)isValidData{
    if (_txtOldPwd.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_old_password",nil)];
        return false;
        
    }else if (_txtNewPwd.text.length == 0){
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_new_password",nil)];
        return false;
        
    }else if (_txtConfirmPwd.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_confirm_password",nil)];
        return false;
        
    }else if (_txtConfirmPwd.text != _txtNewPwd.text) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Password_and_Confirm_password_are_not_same",nil)];
        return false;
    }
    return true;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    textField.textColor =  [UIColor colorWithRed:18.0/255.0 green:172.0/255.0 blue:243.0/255.0 alpha:1.0];
    textField.layer.borderWidth = 1;
    textField.layer.borderColor =  [UIColor colorWithRed:18.0/255.0 green:172.0/255.0 blue:243.0/255.0 alpha:1.0].CGColor;
    textField.clipsToBounds = YES;
    return true;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    textField.textColor =  [UIColor blackColor];
    textField.layer.borderWidth = 0;
    textField.layer.borderColor = [UIColor clearColor].CGColor;
    textField.clipsToBounds = YES;
    return true;
}

#pragma mark - Button actions
- (IBAction)onResetPwd:(id)sender{
    
    if(![self CheckNetworkConnection]) {
        return;
    }
    
    if ([self isValidData]) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSString *userID = [Helper getPREF:@"userID"];
        NSDictionary *parameter=@{
                                  @"userid":userID,
                                  @"password":_txtNewPwd.text,
                                  @"oldpass":_txtOldPwd.text
                                  };
        
        [WebService httpPostWithCustomDelegateURLString:@".profile" parameter:parameter isDebug:YES callBackData:^(NSData *data, NSError *error)
         {
             if (error == nil) {
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                 
                 NSError *localError = nil;
                 NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
                 
                 if (localError != nil) {
                     NSLog(@"%@",localError.description);
                 }
                 else {
                     NSDictionary *response = [parsedObject objectForKey:@"data"];
                     NSString* message = [response objectForKey:@"message"];
                     NSString* success = [response objectForKey:@"status"];
                     if ([success isEqualToString:@"Success"]) {
                         
                         /*NSDictionary* user = [response objectForKey:@"user"];
                         NSString *email = [user valueForKey:@"email"];
                         NSString *name = [user valueForKey:@"name"];
                         NSString *uID = [user valueForKey:@"id"];
                         //NSString* sid = [response objectForKey:@"sid"];
                         
                         [Helper setPREFStringValue:uID sKey:@"userID"];
                         //[Helper setPREFStringValue:sid sKey:@"sid"];
                         [Helper setPREFStringValue:name sKey:@"userName"];
                         [Helper setPREFStringValue:email sKey:@"userEmail"];
                         [Helper setPREFStringValue:@"1" sKey:@"islogin"];
                         [Helper setDataValue:data sKey:@"userInfo"];
                         
                         NSDictionary *payment_info = [response objectForKey:@"payment_info"];
                         NSString *status = [payment_info valueForKey:@"status"];
                         if (status == (NSString *)[NSNull null]) {
                             status = @"";
                             [Helper setPREFStringValue:status sKey:@"userStatus"];
                         }else{
                             [Helper setPREFStringValue:status sKey:@"userStatus"];
                         }

                         [Helper setPREFStringValue:status sKey:@"userStatus"];*/
                         
                         [Helper displayAlertView:@"" message:message];
                         [self.navigationController popViewControllerAnimated:true];
                     }
                     else {
                         NSLog(@"error :: %@",message);
                         [Helper displayAlertView:@"" message:message];
                     }
                 }
             }
         }];
    }else{
        
    }
}

- (IBAction)onBack:(id)sender{
    [self.navigationController popViewControllerAnimated:true];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
