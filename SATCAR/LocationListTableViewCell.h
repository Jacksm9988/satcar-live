//
//  LocationListTableViewCell.h
//  SATCAR
//
//  Created by Percept Infotech on 16/01/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *viewBack;
@property (weak, nonatomic) IBOutlet UILabel *lblNameValue;
@end
