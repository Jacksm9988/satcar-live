//
//  LoginViewController.m
//  SATCAR
//
//  Created by Percept Infotech on 12/7/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import "LoginViewController.h"
#import "HomeViewController.h"
#import "RegisterPayVC.h"

@interface LoginViewController () {
    AppDelegate* appdelegate;
    NSDictionary *resForPayment;
}
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self addTitleView:@""];
    appdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
}

-(void)viewWillAppear:(BOOL)animated{
    
    _txtEmail.placeholder =NSLocalizedString(@"Email", @"");
    _txtPassword.placeholder =NSLocalizedString(@"Password", @"");
    [_BtnLogin setTitle:NSLocalizedString(@"Login", @"") forState:UIControlStateNormal];
    [_btnForgotPassword setTitle:NSLocalizedString(@"Forgotpassword?", @"") forState:UIControlStateNormal];
    
    [Helper setBorderWithColorInText:_txtEmail];
    [Helper setBorderWithColorInText:_txtPassword];
    [Helper setBorderWithColorInButton:_BtnLogin];
}

-(void)viewDidDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"RemoveMapData" object:nil];
}

#pragma mark - validation
-(BOOL)isValidData{
    if (_txtEmail.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_enter_email_id",nil)];
        return false;
        
    }else if (![self validateEmailWithString:_txtEmail.text]){
      [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Invalid_email_id",nil)];
      return false;
      
    }else if (_txtPassword.text.length == 0) {
          [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_your_password",nil)];
          return false;
    }
    return true;
}

#pragma mark - Textfield delegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    textField.textColor =  [UIColor colorWithRed:18.0/255.0 green:172.0/255.0 blue:243.0/255.0 alpha:1.0];
    textField.layer.borderWidth = 1;
    textField.layer.borderColor =  [UIColor colorWithRed:18.0/255.0 green:172.0/255.0 blue:243.0/255.0 alpha:1.0].CGColor;
    textField.clipsToBounds = YES;
    return true;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    textField.textColor =  [UIColor blackColor];
    textField.layer.borderWidth = 0;
    textField.layer.borderColor = [UIColor clearColor].CGColor;
    textField.clipsToBounds = YES;
    return true;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Action
- (IBAction)LoginPress:(id)sender {
    [self.view endEditing:YES];
    
    if(![self CheckNetworkConnection]) {
        return;
    }
    
    if ([self isValidData]) {
        
        if (appdelegate.strDeviceToken == nil) {
            appdelegate.strDeviceToken = @"";
        }
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSDictionary *parameter=@{
                                  @"username":_txtEmail.text,
                                  @"password":_txtPassword.text,
                                  @"device_uuid":appdelegate.strDeviceToken,
                                  @"os":@"ios"
                                  };
        //@"device_uuid":appdelegate.strDeviceToken,
        [WebService httpPostWithCustomDelegateURLString:@".login" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error)
        {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
             if (error == nil) {
                 NSError *localError = nil;
                 NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
                 
                 if (localError != nil) {
                     NSLog(@"%@",localError.description);
                     
                 }
                 else {
                     NSDictionary *response = [parsedObject objectForKey:@"data"];
                     NSString* message = [response objectForKey:@"message"];
                     NSString* success = [response objectForKey:@"status"];
                     
                     if ([success isEqualToString:@"Success"]) {
                         
                         // Save user value
                         appdelegate.appLogin = @"1";
                         NSDictionary* user = [response objectForKey:@"user"];
                         NSString *uID = [user valueForKey:@"id"];
                         NSNumber *userID = [user valueForKey:@"id"];
                         
                         if ([userID integerValue] == 0) {
                             [Helper displayAlertView:@"" message:NSLocalizedString(@"Please_contact_admin", @"")];
                             return;
                         }else{
                             NSString *email = [user valueForKey:@"email"];
                             NSString *name = [user valueForKey:@"name"];
                             NSString* registerDate = [user objectForKey:@"registerDate"];
                             //NSString* sid = [response objectForKey:@"sid"];
                             NSString* userType = [response objectForKey:@"user_type"];
                             NSString* parent = [Helper isStringIsNull:[response objectForKey:@"parent"]];
                             appdelegate.tracker_id = [response objectForKey:@"tracker_id"];
                             
                             if (parent == (NSString *)[NSNull class]) {
                                 [Helper setPREFStringValue:@"0" sKey:@"parent"];
                             }else{
                                 [Helper setPREFStringValue:parent sKey:@"parent"];
                             }
                             [Helper setPREFStringValue:parent sKey:@"parent"];
                             [Helper setPREFStringValue:uID sKey:@"userID"];
                             //[Helper setPREFStringValue:sid sKey:@"sid"];
                             [Helper setPREFStringValue:name sKey:@"userName"];
                             [Helper setPREFStringValue:email sKey:@"userEmail"];
                             [Helper setPREFStringValue:@"1" sKey:@"islogin"];
                             [Helper setPREFStringValue:userType sKey:@"user_type"];
                             [Helper setPREFStringValue:registerDate sKey:@"userRegisterDate"];
                             
                             if (appdelegate.tracker_id == (NSString *)[NSNull class]) {
                                 [Helper setPREFStringValue:@"" sKey:@"tracker_id"];
                             }else{
                                 [Helper setPREFStringValue:appdelegate.tracker_id sKey:@"tracker_id"];
                             }
                             
                             // Save general settings
                             NSDictionary *generalSetting = [response objectForKey:@"general_setting"];
                             NSMutableDictionary *generalSetting2 = [[NSMutableDictionary alloc]init];
                             NSArray *allk = [generalSetting allKeys];
                             for (int i=0; i<allk.count; i++) {
                                 NSString *st = [allk objectAtIndex:i];
                                 NSString *val =[generalSetting valueForKey:st];
                                 if (val == (NSString *)[NSNull null] || val == nil) {
                                     if([st isEqualToString:@"notification"])
                                     {
                                         [generalSetting2 setObject:@"0" forKey:st];
                                     }else if([st isEqualToString:@"trip_notification"])
                                     {
                                         [generalSetting2 setObject:@"0" forKey:st];
                                     }else{
                                         [generalSetting2 setObject:@"" forKey:st];
                                     }
                                     
                                 }else{
                                     [generalSetting2 setObject:val forKey:st];
                                 }
                             }
                             
                             if ([[generalSetting2 valueForKey:@"language"] isEqualToString:@"da-DK"]) {
                                 [Helper setPREFStringValue:@"da-DK" sKey:@"language"];
                             }else{
                                 [Helper setPREFStringValue:@"en-GB" sKey:@"language"];
                             }
                             [appdelegate setApplicationLanguage];
                            
                                 appdelegate.appTrackNotification = [generalSetting2 valueForKey:@"trip_notification"];
                                 [Helper setPREFID:appdelegate.appTrackNotification :@"trip_notification"];
                            
                            
                             
                             [Helper setUserValue:generalSetting2 sKey:@"general_setting"];
                             [Helper setDataValue:data sKey:@"userInfo"];
                             NSLog(@"user info : %@",response);
                             [appdelegate setApplicationLanguage];
                             
                             NSDictionary *payment_info = [response objectForKey:@"payment_info"];
                             NSString *status = [payment_info valueForKey:@"status"];
                             if (status == (NSString *)[NSNull null]) {
                                 status = @"";
                                 [Helper setPREFStringValue:status sKey:@"userStatus"];
                             }else{
                                 [Helper setPREFStringValue:status sKey:@"userStatus"];
                             }
                             [appdelegate callGetCredentialsService:uID];
                             
                             if ([uID isEqualToString:@"12"]) {
                                 UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                 UIViewController *newViewController = [storyboard instantiateViewControllerWithIdentifier:@"SeguehomeVC"];
                                 [self.slidingViewController setTopViewController:newViewController];
                                 [self.slidingViewController resetTopViewAnimated:YES];
                                 [[NSUserDefaults standardUserDefaults] setObject:@"SeguehomeVC" forKey:@"storyBoardIdentifier"];
                             }else{
                                 if (![status.lowercaseString isEqualToString:@"paid"] && status.length != 0) {
                                     //[self performSegueWithIdentifier:@"segueReNew" sender:self];
                                     resForPayment = response;
                                     [self performSegueWithIdentifier:@"segueProcessToPayFromLogin" sender:self];
                                     
                                 }else{
                                     UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                     UIViewController *newViewController = [storyboard instantiateViewControllerWithIdentifier:@"SeguehomeVC"];
                                     [self.slidingViewController setTopViewController:newViewController];
                                     [self.slidingViewController resetTopViewAnimated:YES];
                                     [[NSUserDefaults standardUserDefaults] setObject:@"SeguehomeVC" forKey:@"storyBoardIdentifier"];
                                 }
                             }
                         }
                         
                     } else {
                         NSLog(@"error :: %@",message);
                         [Helper displayAlertView:@"" message:message];
                     }
                 }
             }else{
                 [self reCallServiceDueToNetworkIssue];
             }
         }];
    }
}

- (IBAction)ForgotPassWPress:(id)sender {
    [self.view endEditing:YES];
    [self performSegueWithIdentifier:@"ForgotPass" sender:self];
}

- (IBAction)BackPress:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)reCallServiceDueToNetworkIssue{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"request_time_out", @"") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *retry = [UIAlertAction actionWithTitle:NSLocalizedString(@"Retry", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [self LoginPress:self];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [alert dismissViewControllerAnimated:true completion:nil];
    }];
    [alert addAction:retry];
    [alert addAction:cancel];
    [self presentViewController:alert animated:true completion:nil];
}

#pragma mark - Navigation

//// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    if ([segue.identifier isEqualToString:@"segueProcessToPayFromLogin"]) {
//        RegisterPayVC *controller = [storyboard instantiateViewControllerWithIdentifier:@"RegisterPayVC"];
//        controller.userInfo = resForPayment;
//    }
//}

@end
