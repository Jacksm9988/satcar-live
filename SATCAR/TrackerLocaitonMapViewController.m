//
//  TrackerLocaitonMapViewController.m
//  SATCAR
//
//  Created by Percept Infotech on 13/01/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import "TrackerLocaitonMapViewController.h"
@interface TrackerLocaitonMapViewController ()
@end

@implementation TrackerLocaitonMapViewController
@synthesize strLat,strLon;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self addTitleView:NSLocalizedString(@"Your_car_was_here", @"")];
    [self displayMap];
}

-(void)displayMap{
    CLLocationCoordinate2D annotationCoord;
    
    MKPointAnnotation *annotationPoint = [[MKPointAnnotation alloc] init];
    annotationCoord.latitude = [strLat doubleValue ];
    annotationCoord.longitude = [strLon doubleValue];
    
    annotationPoint.coordinate = annotationCoord;
    
    MKCoordinateRegion myRegion;
    MKCoordinateSpan span;
    
    span.latitudeDelta=0.1;
    span.longitudeDelta=0.1;
    
    myRegion.center=annotationCoord;
    myRegion.span=span;
    [self.mapview setRegion:myRegion animated:YES];
    [self.mapview addAnnotation:annotationPoint];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
