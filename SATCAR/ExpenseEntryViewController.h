//
//  ExpenseEntryViewController.h
//  SATCAR
//
//  Created by Percept Infotech on 28/12/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExpenseEntryViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>
{
    
}

@property (weak, nonatomic) IBOutlet UILabel *lblExpenseDetails;
@property (weak, nonatomic) IBOutlet UILabel *lblExpenseAccount;
@property (weak, nonatomic) IBOutlet UIView *ViewSegment;
@property (weak, nonatomic) IBOutlet UIButton *BtnPrivateSegment;
@property (weak, nonatomic) IBOutlet UIButton *BtnBusinessSegment;
- (IBAction)SelctSegment:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *txtEName;
@property (weak, nonatomic) IBOutlet UITextField *txtEType;
@property (weak, nonatomic) IBOutlet UITextField *txtDate;
@property (weak, nonatomic) IBOutlet UITextField *txtAmount;
@property (weak, nonatomic) IBOutlet UITextField *txtCurrency;
@property (weak, nonatomic) IBOutlet UITextField *txtNotes;
@property (weak, nonatomic) IBOutlet UITextField *txtUserName;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
- (IBAction)onSave:(id)sender;
@property (weak, nonatomic) IBOutlet UICollectionView *collectioview;
@property (weak, nonatomic) IBOutlet UILabel *lblUploadDesign;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintImageTop;
@property (weak, nonatomic) IBOutlet UILabel *lblVat;
@property (weak, nonatomic) IBOutlet UISwitch *SwitchVat;
- (IBAction)ChangeSwitch:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtvat;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *Vat_hight;

@property(nonatomic,strong)NSDictionary *tripDataDictionary;
@property(nonatomic,strong)NSString *fromScreen;

@end
