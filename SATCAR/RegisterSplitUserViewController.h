//
//  RegisterSplitUserViewController.h
//  SATCAR
//
//  Created by Percept Infotech on 31/01/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterSplitUserViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property(nonatomic,strong)NSString *speedometer;
@property (weak, nonatomic) IBOutlet UITextField *txtAmount;
@property (weak, nonatomic) IBOutlet UITextField *txtMonthly;
@property (weak, nonatomic) IBOutlet UITextField *txtDuration;
@property (weak, nonatomic) IBOutlet UILabel *lblSplitMCost;
@property (weak, nonatomic) IBOutlet NSString *userType;
@property (weak, nonatomic) IBOutlet UILabel *LblAmoutPaid;
@property (weak, nonatomic) IBOutlet UILabel *LblMonthlyCost;
@property (weak, nonatomic) IBOutlet UILabel *LblContract;
@property (weak, nonatomic) IBOutlet UILabel *LblMonths;
@property (nonatomic, strong) NSData *imageData;
@end
