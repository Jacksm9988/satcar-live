//
//  ImageCropAndRotateViewController.m
//  MyPlan
//
//  Created by Percept Infotech on 9/26/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import "ImageCropAndRotateViewController.h"
#import "UIImageView+WebCache.h"
//#import "UIImageView+Networking.h"
#import <unistd.h>
#import "UIView+RNActivityView.h"
#import "SDWebImageDownloader.h"
#import "TOCropViewController.h"
#import "WebServicesCalls.h"
#import "NavigationController.h"
#import "UIViewController+VCHelpers.h"

@interface ImageCropAndRotateViewController ()<TOCropViewControllerDelegate>
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, assign) TOCropViewCroppingStyle croppingStyle; //The cropping style
@property (nonatomic, assign) CGRect croppedFrame;
@property (nonatomic, assign) NSInteger angle;
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
@property (nonatomic, strong) UIPopoverController *activityPopoverController;
#pragma clang diagnostic pop

- (void)showCropViewController;


- (void)layoutImageView;
- (void)didTapImageView;

- (void)updateImageViewWithImage:(UIImage *)image fromCropViewController:(TOCropViewController *)cropViewController;

@end

@implementation ImageCropAndRotateViewController
@synthesize image,isFrom;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    TOCropViewController *cropController = [[TOCropViewController alloc] initWithCroppingStyle:self.croppingStyle image:image];
    cropController.delegate = self;
    self.imageView = [[UIImageView alloc] init];
    self.imageView.userInteractionEnabled = YES;
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    _imageView.image = image;
    [self.view addSubview:self.imageView];
     self.imageView.hidden = YES;
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapImageView)];
    [self.imageView addGestureRecognizer:tapRecognizer];
    
    
    self.croppingStyle = TOCropViewCroppingStyleDefault;
    
   /* UIImagePickerController *standardPicker = [[UIImagePickerController alloc] init];
    standardPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    standardPicker.allowsEditing = NO;
    standardPicker.delegate = self;
    [self presentViewController:standardPicker animated:YES completion:nil];*/
    
    [self presentViewController:cropController animated:YES completion:nil];
   /* [picker dismissViewControllerAnimated:YES completion:^{
    
    }];*/

    
}


#pragma mark - Gesture Recognizer -
- (void)didTapImageView
{
    //When tapping the image view, restore the image to the previous cropping state
    
    TOCropViewController *cropController = [[TOCropViewController alloc] initWithCroppingStyle:self.croppingStyle image:self.image];
    cropController.delegate = self;
    CGRect viewFrame = [self.view convertRect:self.imageView.frame toView:self.navigationController.view];
    [cropController presentAnimatedFromParentViewController:self
                                                  fromImage:self.imageView.image
                                                   fromView:nil
                                                  fromFrame:viewFrame
                                                      angle:self.angle
                                               toImageFrame:self.croppedFrame
                                                      setup:^{ self.imageView.hidden = YES; }
                                                 completion:nil];
}

#pragma mark - Cropper Delegate -
-(void)cropViewController:(TOCropViewController *)cropViewController didFinishCancelled:(BOOL)cancelled
{
    if(cancelled)
    {
        //[[NSUserDefaults standardUserDefaults] setObject:imgData forKey:@"tmpAvtarImage"];
        [cropViewController dismissViewControllerAnimated:YES completion:^{
            [self.navigationController popViewControllerAnimated:YES];
        }];
        
    }
}


- (void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image2 withRect:(CGRect)cropRect angle:(NSInteger)angle
{
    
    /*UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Are you sure to use this image as an profile image" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alert.tag = 142;
    [alert show];*/
    
    self.croppedFrame = cropRect;
    self.angle = angle;
    [self updateImageViewWithImage:image2 fromCropViewController:cropViewController];
}

- (void)cropViewController:(TOCropViewController *)cropViewController didCropToCircularImage:(UIImage *)image2 withRect:(CGRect)cropRect angle:(NSInteger)angle
{
    self.croppedFrame = cropRect;
    self.angle = angle;
    [self updateImageViewWithImage:image2 fromCropViewController:cropViewController];
}

- (void)updateImageViewWithImage:(UIImage *)image2 fromCropViewController:(TOCropViewController *)cropViewController
{
    self.imageView.image = image2;
    [self layoutImageView];
    
    self.navigationItem.rightBarButtonItem.enabled = YES;
    
    if (cropViewController.croppingStyle != TOCropViewCroppingStyleCircular) {
        self.imageView.hidden = YES;
        [cropViewController dismissAnimatedFromParentViewController:self
                                                   withCroppedImage:image2
                                                             toView:self.imageView
                                                            toFrame:CGRectZero
                                                              setup:^{
                                                                  [self layoutImageView];
                                                              }
                                                         completion:
         ^{
             self.imageView.hidden = YES;
             imgData = UIImagePNGRepresentation(self.imageView.image);
             [self SaveProImage];
         }];
    }
    else {
        self.imageView.hidden = YES;
        [cropViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    }
}

-(UIImage *)compressImage:(UIImage *)image2
{
    float actualHeight = image2.size.height;
    float actualWidth = image2.size.width;
    float maxHeight = 600.0;
    float maxWidth = 800.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth){
        if(imgRatio < maxRatio){
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image2 drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithData:imageData];
}

#pragma mark - Image Layout -
- (void)layoutImageView
{
    if (self.imageView.image == nil)
        return;
    
    CGFloat padding = 20.0f;
    
    CGRect viewFrame = self.view.bounds;
    viewFrame.size.width -= (padding * 2.0f);
    viewFrame.size.height -= ((padding * 2.0f));
    
    CGRect imageFrame = CGRectZero;
    imageFrame.size = self.imageView.image.size;
    
    if (self.imageView.image.size.width > viewFrame.size.width ||
        self.imageView.image.size.height > viewFrame.size.height)
    {
        CGFloat scale = MIN(viewFrame.size.width / imageFrame.size.width, viewFrame.size.height / imageFrame.size.height);
        imageFrame.size.width *= scale;
        imageFrame.size.height *= scale;
        imageFrame.origin.x = (CGRectGetWidth(self.view.bounds) - imageFrame.size.width) * 0.5f;
        imageFrame.origin.y = (CGRectGetHeight(self.view.bounds) - imageFrame.size.height) * 0.5f;
        
        self.imageView.frame = imageFrame;
        
    }
    else {
        self.imageView.frame = imageFrame;
        self.imageView.center = (CGPoint){CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds)};
    }
    
    //UIImage *img23 = [self compressImage:self.imageView.image];
    // [imgarr addObject:UIImagePNGRepresentation(img23)];
    
   
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    if(alertView.tag == 4548785)
    {
        if(buttonIndex == 0)
        {
            [self SaveProImage];
        }
    }
}



#pragma mark - SaveProImage
-(void)SaveProImage
{
    
    if([isFrom isEqualToString:@"signup"])
    {
        [[NSUserDefaults standardUserDefaults] setObject:imgData forKey:@"tmpAvtarImage"];
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    [self.view endEditing:YES];
    
    if(![self CheckNetworkConnection])
    {
        [self AlertNoInternet:NSLocalizedString(@"NoInternet", @"") tag:4548785];
        return;
    }
    
    [self.navigationController.view showActivityViewWithLabel:@"Loading"];
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    NSString *name =[[NSUserDefaults standardUserDefaults] valueForKey:@"Username"];
    NSArray *arr = [name componentsSeparatedByString:@" "];
    NSString* Fname =arr.count>0?[arr objectAtIndex:0]:@"";
    NSString* Lname =arr.count>1?[arr objectAtIndex:1]:@"";
    NSString* Email =  [[NSUserDefaults standardUserDefaults] valueForKey:@"email"];
    NSString *sid =  [[NSUserDefaults standardUserDefaults] valueForKey:@"sid"];
    NSString *sname =   [[NSUserDefaults standardUserDefaults]valueForKey:@"sname"];
    
    Email =  [[NSUserDefaults standardUserDefaults] valueForKey:@"email"];
    
    [parameters setValue:sid  forKey:@"sid"];
    [parameters setValue:sname forKey:@"sname"];
    [parameters setValue:@"" forKey:@"password"];
    [parameters setValue:Email  forKey:@"email"];
    [parameters setValue:Fname forKey:@"first_name"];
    [parameters setValue:Lname forKey:@"last_name"];
     imgData = UIImageJPEGRepresentation([UIImage imageWithData:imgData], 0.4);
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSDictionary *dic = [WebServicesCalls CallWebsevice:@"profile" TaskdataDictionary:parameters Imagedata:imgData ImagePeraname:@"profile_image" VideoData:nil VideoPeraname:nil AudioData:nil AudioPeraname:nil];
        dispatch_sync(dispatch_get_main_queue(), ^{
            
            NSLog(@"Return Dic %@",dic);
            id returndata = [dic valueForKey:@"data"];
            if([self CheckSession:returndata])
            {
                if([[[dic valueForKey:@"data"] valueForKey:@"status"] isEqualToString:@"Success"]){
                    
                    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@ %@",Fname,Lname] forKey:@"Username"];
                    [[NSUserDefaults standardUserDefaults] setObject:Email forKey:@"email"];
                    [[NSUserDefaults standardUserDefaults] setObject:imgData forKey:@"AvatarImage"];
                    
                    //_UserAvtarimage.image = [UIImage imageWithData:imgData];
                    
                    // symptomsArr = [dic valueForKey:@"data"];
                    //[_tableview reloadData];
                    [self alertSucess:[[dic valueForKey:@"data"] valueForKey:@"status"] Message:[[dic valueForKey:@"data"] valueForKey:@"message"]];
                    
                }else
                {
                    [self alertSucess:NSLocalizedString(@"error", @"") Message:[[dic valueForKey:@"data"] valueForKey:@"message"]];
                }
                
            }
           
            
            [self.navigationController.view hideActivityViewWithAfterDelay:0.5];
            [self.navigationController popViewControllerAnimated:YES];
            
        });
    });
    
    
    
    
    
    //InspirationArr = [dic valueForKey:@"data"];
    //    NSString *status = [datadic valueForKey:@"status"];
    
    
    
    
    //[self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark - Bar Button Items -
- (void)showCropViewController
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Crop Image"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action) {
                                                              self.croppingStyle = TOCropViewCroppingStyleDefault;
                                                              
                                                              UIImagePickerController *standardPicker = [[UIImagePickerController alloc] init];
                                                              standardPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                                              standardPicker.allowsEditing = NO;
                                                              standardPicker.delegate = self;
                                                              [self presentViewController:standardPicker animated:YES completion:nil];
                                                          }];
    
    UIAlertAction *profileAction = [UIAlertAction actionWithTitle:@"Make Profile Picture"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action) {
                                                              self.croppingStyle = TOCropViewCroppingStyleCircular;
                                                              
                                                              UIImagePickerController *profilePicker = [[UIImagePickerController alloc] init];
                                                              profilePicker.modalPresentationStyle = UIModalPresentationPopover;
                                                              profilePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                                              profilePicker.allowsEditing = NO;
                                                              profilePicker.delegate = self;
                                                              profilePicker.preferredContentSize = CGSizeMake(512,512);
                                                              profilePicker.popoverPresentationController.barButtonItem = self.navigationItem.leftBarButtonItem;
                                                              [self presentViewController:profilePicker animated:YES completion:nil];
                                                          }];
    
    [alertController addAction:defaultAction];
    [alertController addAction:profileAction];
    [alertController setModalPresentationStyle:UIModalPresentationPopover];
    
    UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
    popPresenter.barButtonItem = self.navigationItem.leftBarButtonItem;
    [self presentViewController:alertController animated:YES completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
