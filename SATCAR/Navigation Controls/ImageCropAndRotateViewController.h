//
//  ImageCropAndRotateViewController.h
//  MyPlan
//
//  Created by Percept Infotech on 9/26/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageCropAndRotateViewController : UIViewController<UIAlertViewDelegate>
{
    UIImage *image;
    NSString *isFrom;
    NSData *imgData;
}
@property (strong, nonatomic)UIImage *image;
@property (strong, nonatomic) NSString *isFrom;

@end
