//
//  NavigationController
//  MyPlan
//
//  Created by Percept Infotech on 7/7/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavigationController : UINavigationController
{
UIImage *storedShadowImage;


}
@property(nonatomic,strong)UIImageView *img;
-(void)addShadowImage;
-(void)hideImg;
@end