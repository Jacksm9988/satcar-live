//
//  InsuranceDetailsViewController.m
//  SATCAR
//
//  Created by Percept Infotech on 30/12/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import "InsuranceDetailsViewController.h"
#import "UIViewController+VCHelpers.h"
#import "ECSlidingViewController.h"
#import "LeftSideViewController.h"
#import "NavigationController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "UIViewController+VCHelpers.h"

@interface InsuranceDetailsViewController ()<UIDocumentInteractionControllerDelegate>{
    NSString *certificateUrl,*created_by,*registration_number,*greencardUrl;
    UIDocumentInteractionController *documentInteractionController;
}

@end

@implementation InsuranceDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addTitleView:NSLocalizedString(@"InsuranceDetails", @"")];
    
    _lblInsuranceActive.text = NSLocalizedString(@"InsuranceActive", @"");
    _lblRoadHelp.text = NSLocalizedString(@"RoadhelpActive", @"");
    _lblFreeDamage.text = NSLocalizedString(@"FreeDamageActive", @"");
    _lblInsuranceNumber.text = NSLocalizedString(@"InsuranceNumber", @"");
    
    _lblDownloadInsurance.text = NSLocalizedString(@"DownloadInsuranceCertificate", @"");
    _lblDownloadGreen.text = NSLocalizedString(@"DownloadGreenCard", @"");
    _lblDownloadRed.text = NSLocalizedString(@"DownloadRedCard", @"");
    [self getInsuranceDetail];
    //_lblPost.text = NSLocalizedString(@"InsuranceActive", @"");

}

-(void)viewWillAppear:(BOOL)animated{
    
}

- (void)getInsuranceDetail{
    
    NSString *tracker_id = [Helper getPREF:@"tracker_id"];
    NSDictionary *parameter=@{ @"tracker_id":tracker_id };
    //NSLog(@"parameter : %@",parameter);
    [self callGetCarUserInsuranceDetailWS:parameter];
}

-(void)callGetCarUserInsuranceDetailWS:(NSDictionary *)parameter{
    if(![self CheckNetworkConnection])
    {
        //[self AlertNoInternet:NSLocalizedString(@"NoInternet", @"") tag:454869];
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [WebService httpPostWithCustomDelegateURLStringForCar:@".getCarUserInsuranceDetail" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         if (error == nil) {
             
             NSError *localError = nil;
             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
             
             if (localError != nil) {
                 NSLog(@"%@",localError.description);
             }
             else {
                 NSLog(@"Response : %@",parsedObject);
                 NSDictionary *response = [parsedObject objectForKey:@"data"];
                 NSDictionary *cardetail = [response objectForKey:@"CarInsuranceDetail"];
                 NSString* message = [response objectForKey:@"message"];
                 NSString* success = [response objectForKey:@"status"];
                 if ([success isEqualToString:@"Success"]) {
                     
                     int insuranceActive = [[cardetail valueForKey:@"insurance_active"]intValue];
                     if (insuranceActive <= 0) {
                         _txtInsuranceActive.text = NSLocalizedString(@"No", @"");
                     }else{
                         _txtInsuranceActive.text = NSLocalizedString(@"Yes", @"");
                     }
                     
                     int roadHelp = [[cardetail valueForKey:@"roadhelp_active"]intValue];
                     if (roadHelp <= 0) {
                         _txtRoadHelp.text = NSLocalizedString(@"No", @"");
                     }else{
                         _txtRoadHelp.text = NSLocalizedString(@"Yes", @"");
                     }
                     
                     int freeDamage = [[cardetail valueForKey:@"free_damage_active"]intValue];
                     if (freeDamage <= 0) {
                         _txtFreeDamage.text = NSLocalizedString(@"No", @"");
                     }else{
                         _txtFreeDamage.text = NSLocalizedString(@"Yes", @"");
                     }
                     
                     certificateUrl = [cardetail valueForKey:@"download_certificate"];
                     greencardUrl = [cardetail valueForKey:@"greencard"];
                     created_by = [cardetail valueForKey:@"created_by"];
                     registration_number = [cardetail valueForKey:@"registration_number"];
                     
                 } else {
                     NSLog(@"error :: %@",message);
                     //[Helper displayAlertView:@"" message:message];
                 }
             }
         }
     }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onDownloadCertificate:(id)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:true];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = paths[0];
    
    NSURL *documentsDirectoryPath = [NSURL fileURLWithPath:[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject]];
    NSURL *URL =[documentsDirectoryPath URLByAppendingPathComponent:@"certificate.pdf"];
    NSString *strURL = [documentsDirectory stringByAppendingPathComponent:@"certificate.pdf"];
    
    NSURL *url = [NSURL URLWithString:certificateUrl];
    NSData *data = [[NSData alloc] initWithContentsOfURL:url];
    [data writeToFile:strURL atomically:YES];
    [MBProgressHUD hideHUDForView:self.view animated:true];
    
    if (URL) {
        // Initialize Document Interaction Controller
        documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:URL];
        
        // Configure Document Interaction Controller
        [documentInteractionController setDelegate:self];
        
        // Preview PDF
        [documentInteractionController presentPreviewAnimated:YES];
    }
}

- (UIViewController *) documentInteractionControllerViewControllerForPreview: (UIDocumentInteractionController *) controller {
    return self;
}

- (IBAction)onDownloadRedCard:(id)sender {
    [self performSegueWithIdentifier:@"segueRedCard" sender:self];
}

- (IBAction)onDownloadGreenCard:(id)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:true];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = paths[0];
    
    NSURL *documentsDirectoryPath = [NSURL fileURLWithPath:[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject]];
    NSURL *URL =[documentsDirectoryPath URLByAppendingPathComponent:@"greenCardcertificate.pdf"];
    NSString *strURL = [documentsDirectory stringByAppendingPathComponent:@"greenCardcertificate.pdf"];
    
    NSURL *url = [NSURL URLWithString:greencardUrl];
    NSData *data = [[NSData alloc] initWithContentsOfURL:url];
    [data writeToFile:strURL atomically:YES];
    [MBProgressHUD hideHUDForView:self.view animated:true];
    
    if (URL) {
        // Initialize Document Interaction Controller
        documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:URL];
        
        // Configure Document Interaction Controller
        [documentInteractionController setDelegate:self];
        
        // Preview PDF
        [documentInteractionController presentPreviewAnimated:YES];
    }
}
@end
