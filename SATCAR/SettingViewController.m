//
//  SettingViewController.m
//  SATCAR
//
//  Created by Percept Infotech on 29/12/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import "SettingViewController.h"
#import "UIViewController+VCHelpers.h"
#import "ECSlidingViewController.h"
#import "LeftSideViewController.h"
#import "NavigationController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "UIViewController+VCHelpers.h"
#import "SettingTVC.h"
@interface SettingViewController ()
{
    NSMutableArray *menuArray;
    NSString *userType;
    AppDelegate *appdelegate;
}
@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

-(void)viewWillAppear:(BOOL)animated{
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    userType = [Helper getPREF:@"user_type"];
    
    
    if ([userType isEqualToString:@"11"])
    {
        menuArray = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"GeneralSetting", @""),NSLocalizedString(@"MyPersonalDetails", @""),NSLocalizedString(@"TrackerDetails", @""),NSLocalizedString(@"Split_details", @""),NSLocalizedString(@"Add_split_user", @""),NSLocalizedString(@"Connect_Add_One", @""),NSLocalizedString(@"speedometer", @""),NSLocalizedString(@"Version", @""), nil];
        
    }else if([userType isEqualToString:@"12"]){
        
        menuArray = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"General_settings", @""),NSLocalizedString(@"MyPersonalDetails", @""),NSLocalizedString(@"TrackerDetails", @""),NSLocalizedString(@"Reimbursement_rates", @""),NSLocalizedString(@"Speedodometer", @""),NSLocalizedString(@"Version", @""), nil];
    }else
    {
        menuArray = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"General_settings", @""),NSLocalizedString(@"MyPersonalDetails", @""),NSLocalizedString(@"TrackerDetails", @""),NSLocalizedString(@"Connect_Add_One", @""),NSLocalizedString(@"Reimbursement_rates", @""),NSLocalizedString(@"Speedodometer", @""),NSLocalizedString(@"Version", @""), nil];
    }
    _tableview.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    [_tableview reloadData];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self addTitleView2:NSLocalizedString(@"Settings", @"")];
}

#pragma mark - Table View
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return menuArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SettingTVC *cell = (SettingTVC *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.lblMenu.text = [menuArray objectAtIndex:indexPath.row];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        [self performSegueWithIdentifier:@"segueGeneralSetting" sender:self];
        
    }else if (indexPath.row == 1){
        [self performSegueWithIdentifier:@"SegueMyDetails" sender:self];
        
    }else if (indexPath.row == 2){
        [self performSegueWithIdentifier:@"SegueTrackerDetails" sender:self];
        
    }else if (indexPath.row == 3){
        if ([userType isEqualToString:@"10"]){
            [self performSegueWithIdentifier:@"segueConnectAddOne" sender:self];
        }else if([userType isEqualToString:@"12"]){
            [self performSegueWithIdentifier:@"segueReimbursementRate" sender:self];
        }else{
            [self performSegueWithIdentifier:@"SegueSplitLeasingDetail" sender:self];
        }
    }else if (indexPath.row == 4){
        if ([userType isEqualToString:@"10"]){
            [self performSegueWithIdentifier:@"segueReimbursementRate" sender:self];
        }else if([userType isEqualToString:@"12"]){
            [self performSegueWithIdentifier:@"SegueSpeedodometer" sender:self];
        }else{
            [self performSegueWithIdentifier:@"segueAddUser" sender:self];
        }
    }else if (indexPath.row == 5){
        if ([userType isEqualToString:@"10"]){
            [self performSegueWithIdentifier:@"SegueSpeedodometer" sender:self];
        }else if([userType isEqualToString:@"12"]){
            [self performSegueWithIdentifier:@"segueVersionDetail" sender:self];
        }else{
            [self performSegueWithIdentifier:@"segueConnectAddOne" sender:self];
        }
        
    }else if (indexPath.row == 6){
        if([userType isEqualToString:@"11"]){
            [self performSegueWithIdentifier:@"SegueSpeedodometer" sender:self];
        }else{
            [self performSegueWithIdentifier:@"segueVersionDetail" sender:self];
        }
    }else{
        [self performSegueWithIdentifier:@"segueVersionDetail" sender:self];
    }
}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//
//    if (indexPath.row == 0) {
//        [self performSegueWithIdentifier:@"segueGeneralSetting" sender:self];
//
//    }else if (indexPath.row == 1){
//        [self performSegueWithIdentifier:@"SegueMyDetails" sender:self];
//
//    }else if (indexPath.row == 2){
//        [self performSegueWithIdentifier:@"SegueTrackerDetails" sender:self];
//
//    }else if (indexPath.row == 3){
//        if ([userType isEqualToString:@"10"]){
//            [self performSegueWithIdentifier:@"segueConnectAddOne" sender:self];
//        }else if([userType isEqualToString:@"12"]){
//            [self performSegueWithIdentifier:@"segueReimbursementRate" sender:self];
//        }else{
//            [self performSegueWithIdentifier:@"SegueSplitLeasingDetail" sender:self];
//        }
//
//    }else if (indexPath.row == 4){
//        if ([userType isEqualToString:@"10"]){
//            [self performSegueWithIdentifier:@"segueReimbursementRate" sender:self];
//        }else if([userType isEqualToString:@"12"]){
//            [self performSegueWithIdentifier:@"SegueSpeedodometer" sender:self];
//        }else{
//            [self performSegueWithIdentifier:@"segueConnectAddOne" sender:self];
//        }
//    }else if (indexPath.row == 5){
//        if([userType isEqualToString:@"12"]){
//            [self performSegueWithIdentifier:@"segueVersionDetail" sender:self];
//        }else{
//            [self performSegueWithIdentifier:@"SegueSpeedodometer" sender:self];
//        }
//
//    }else if (indexPath.row == 6){
//        [self performSegueWithIdentifier:@"segueVersionDetail" sender:self];
//
//    }
//}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
