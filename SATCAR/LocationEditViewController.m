//
//  LocationEditViewController.m
//  SATCAR
//
//  Created by Percept Infotech on 16/01/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import "LocationEditViewController.h"
#import <GooglePlaces/GooglePlaces.h>
#import "AddressSearchViewController.h"

@interface LocationEditViewController ()<UITextFieldDelegate,GMSAutocompleteViewControllerDelegate>//,GMSAutocompleteTableDataSourceDelegate
{
    NSString *locationID;
    GMSAutocompleteViewController *acController;
    GMSPlace *place2;
}

@end

@implementation LocationEditViewController
@synthesize fromScreen,locationDataDictionary;

- (void)viewDidLoad {
    [super viewDidLoad];
    locationID = @"";
    acController = [[GMSAutocompleteViewController alloc] init];
    _btnSave.layer.cornerRadius = 25;
    _btnSave.layer.borderWidth = 1;
    _btnSave.layer.borderColor = [UIColor colorWithRed:0.000 green:0.651 blue:0.929 alpha:1.00].CGColor;
    //place2 = [[GMSPlace alloc]init];
    _lblTitle.text = NSLocalizedString(@"LocationDetails", @"");
    _txtName.placeholder = NSLocalizedString(@"Name", @"");
    _txtAddress.placeholder = NSLocalizedString(@"Address", @"");
    _txtZip.placeholder = NSLocalizedString(@"Zip", @"");
    _txtCity.placeholder = NSLocalizedString(@"City", @"");
    _txtCountry.placeholder = NSLocalizedString(@"Country", @"");
    [_btnSave setTitle:NSLocalizedString(@"Save", @"") forState:UIControlStateNormal];
    
    locationID = @"";
    if ([fromScreen isEqualToString:@"address"]) {
        
    }else if ([fromScreen isEqualToString:@"LocationEdit"]) {
        [self addTitleView:NSLocalizedString(@"edit_location", @"")];
        NSLog(@"Data : %@",locationDataDictionary);
        
        locationID = [locationDataDictionary valueForKey:@"id"];
        _txtName.text = [locationDataDictionary valueForKey:@"name"];
        _txtAddress.text = [locationDataDictionary valueForKey:@"address"];
        //_txtHouseNumber.text = [locationDataDictionary valueForKey:@"house_number"];
        NSString *zip_city = [locationDataDictionary valueForKey:@"zip_and_city"];
        NSArray *zipCityarray = [zip_city componentsSeparatedByString:@" "];
        if (zipCityarray.count > 0) {
            _txtZip.text = [zipCityarray objectAtIndex:0];
            _txtCity.text = [zipCityarray objectAtIndex:1];
        }
        _txtCountry.text = [locationDataDictionary valueForKey:@"country"];
    }else{
        [self addTitleView:NSLocalizedString(@"add_location", @"")];
        
        
    }
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNotification:) name:@"LocationAddAddress"
                                               object:nil];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}



#pragma mark - Custom functions
-(BOOL)isValidData{
    
    if (_txtName.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_name",nil)];
        return false;
        
    }else if (_txtAddress.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_address",nil)];
        return false;
        
    }/*else if (_txtHouseNumber.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_address",nil)];
        return false;
        
    }*/else if (_txtZip.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_zipcode",nil)];
        return false;
        
    }else if (_txtCity.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_city",nil)];
        return false;
        
    }else if (_txtCountry.text.length == 0) {
        [Helper displayAlertView:@"" message:NSLocalizedString(@"error_Please_enter_country",nil)];
        return false;
    }
    return true;
}


#pragma mark - Button actions
- (IBAction)onSave:(id)sender {
    if ([self isValidData]) {
        NSString *userID = [Helper getPREF:@"userID"];
        NSDictionary *parameter;
        if ([fromScreen isEqualToString:@"LocationEdit"]) {
            parameter = @{ @"id":locationID ,@"userid":userID, @"name":_txtName.text, @"address":[Helper isStringIsNull:_txtAddress.text], @"zip_and_city":_txtZip.text, @"city":_txtCity.text, @"country":_txtCountry.text};
        }else{
            parameter = @{ @"userid":userID, @"name":_txtName.text, @"address":_txtAddress.text, @"zip_and_city":_txtZip.text, @"city":_txtCity.text, @"country":_txtCountry.text};
        }
        [self.view endEditing:true];
        [self callAddLocationService:parameter];
    }
}

#pragma mark - webservice call
-(void)callAddLocationService:(NSDictionary *)parameter{
    
    if(![self CheckNetworkConnection]) {
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [WebService httpPostWithCustomDelegateMultipartURLString:@".saveLocation" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (error == nil) {
            NSError *localError = nil;
            NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
            
            if (localError != nil) {
                NSLog(@"%@",localError.description);
            }
            else {
                NSDictionary *response = [parsedObject objectForKey:@"data"];
                NSString* message = [response objectForKey:@"message"];
                NSString* success = [response objectForKey:@"status"];
                if ([success isEqualToString:@"Success"]) {
                    //[Helper displayAlertView:@"" message:message];
                    [self.navigationController popViewControllerAnimated:true];
                } else {
                    NSLog(@"error :: %@",message);
                    [Helper displayAlertView:@"" message:message];
                    if ([message isEqualToString:@"Your session is expired."]) {
                        [self moveToLogin];
                    }
                }
            }
        }
    }];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - custome funcitons
- (void)receiveNotification:(NSNotification *)notification //use notification method and logic
{
    NSString *key = @"AddressValue";
    NSDictionary *dictionary = [notification userInfo];
    NSString *stringValueToUse = [dictionary valueForKey:key];
    _txtAddress.text = stringValueToUse;
    NSLog(@"Address --> %@",stringValueToUse);
    fromScreen = [dictionary valueForKey:@"screen"];
}

#pragma mark - textfield delegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    textField.textColor =  [UIColor colorWithRed:18.0/255.0 green:172.0/255.0 blue:243.0/255.0 alpha:1.0];
    textField.layer.borderWidth = 1;
    textField.layer.borderColor =  [UIColor colorWithRed:18.0/255.0 green:172.0/255.0 blue:243.0/255.0 alpha:1.0].CGColor;
    textField.clipsToBounds = YES;
    return true;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    textField.textColor =  [UIColor blackColor];
    textField.layer.borderWidth = 0;
    textField.layer.borderColor = [UIColor clearColor].CGColor;
    textField.clipsToBounds = YES;
    return true;
}


-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField == _txtAddress){
        //[self.view endEditing:true];
        if (_txtAddress.text.length <= 0) {
            acController.delegate = self;
            GMSAutocompleteFilter *filter = [[GMSAutocompleteFilter alloc]init];
            filter.type = kGMSPlacesAutocompleteTypeFilterNoFilter;
            //filter.country = @"DK";
            acController.autocompleteFilter = filter;
            acController.delegate = self;
            [self presentViewController:acController animated:YES completion:nil];
        }
    }
}

-(void)openAddressView:(NSString *)ScreenField{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AddressSearchViewController *viewController = (AddressSearchViewController *)[storyboard instantiateViewControllerWithIdentifier:@"AddressSearchViewController"];
    if ([fromScreen isEqualToString:@"LocationEdit"]) {
        viewController.fromScreen = @"EditLocation";
    }else{
        viewController.fromScreen = @"AddLocation";
    }
    
    viewController.fromScreenField = ScreenField;
    [self presentViewController:viewController animated:YES completion:nil];
}

// Handle the user's selection.
- (void)viewController:(GMSAutocompleteViewController *)viewController didAutocompleteWithPlace:(GMSPlace *)place {
    [self dismissViewControllerAnimated:YES completion:nil];
    // Do something with the selected place.
    place2 = place;
    
    NSLog(@"Place name %@", place.name);
    NSLog(@"Place address %@", place.formattedAddress);
    NSLog(@"Place attributions %@", place.attributions.string);
    NSString *placeAddress = [NSString stringWithFormat:@"%@,",place.name];
    for (int i = 0; i < place.addressComponents.count; i++) {
        GMSAddressComponent *add = [place.addressComponents objectAtIndex:i];
        //NSLog(@"type : %@",add.type);
        //NSLog(@"type : %@",add.name);
        if ([add.type containsString:@"locality"]) {
            _txtCity.text = add.name;
        }else if ([add.type containsString:@"country"]) {
            _txtCountry.text = add.name;
        }else if ([add.type containsString:@"postal_code"]) {
            _txtZip.text = add.name;
        }
        placeAddress = [NSString stringWithFormat:@"%@",place.name];
   }
   NSLog(@"Address : %@",placeAddress);
    _txtAddress.text = placeAddress;
}

- (void)viewController:(GMSAutocompleteViewController *)viewController didFailAutocompleteWithError:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:nil];
    // TODO: handle the error.
    NSLog(@"Error: %@", [error description]);
}

// User canceled the operation.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    
    _txtAddress.text = [NSString stringWithFormat:@"%@",place2.name];
    [self dismissViewControllerAnimated:YES completion:nil];
}

// Turn the network activity indicator on and off again.
- (void)didRequestAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    /*[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    UIView *Views = viewController.view;
    NSArray *Aar2 = [[Views subviews][0] subviews];
    UIView *View2 = Aar2[1];
    UIView *View3 = [View2 subviews][2];
    UISearchBar *Search = [[View3 subviews] firstObject];
    Search.text = @"Your text";
    [Search.delegate searchBar:Search textDidChange:_txtAddress.text];*/
}

- (void)didUpdateAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
