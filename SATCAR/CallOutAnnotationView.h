//
//  CallOutAnnotationView.h
//  MyPlan
//
//  Created by Percept Infotech on 9/6/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface CallOutAnnotationView : NSObject <MKAnnotation,UIAlertViewDelegate,CLLocationManagerDelegate>
@property (nonatomic,retain)UIView *contentView;
@property (nonatomic) CGFloat lat;
@property (nonatomic) CGFloat lon;
@property (nonatomic) CGFloat altitude;
@property (nonatomic,  copy) NSString * title;
@property (nonatomic,  copy) NSString * phonenumber;
@property (nonatomic, copy) NSString * subtitle;
@property (nonatomic,retain) NSString *source;
@property (nonatomic,retain) UIImage  *image;
@property (strong, nonatomic) CLLocationManager *locationManager;
- (id)initWithName:(NSString*)name address:(NSString*)address phonenumber:(NSString*)phonenumber coordinate:(CLLocationCoordinate2D)coordinate;
- (void)mapItem;

@end
