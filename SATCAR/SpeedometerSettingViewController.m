//
//  SpeedometerSettingViewController.m
//  SATCAR
//
//  Created by Percept Infotech on 24/02/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import "SpeedometerSettingViewController.h"

@interface SpeedometerSettingViewController ()

@end

@implementation SpeedometerSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _LblinitialSpeed.text = NSLocalizedString(@"Initial_Speedometer", @"");
    _LblCurrentSpeed.text = NSLocalizedString(@"Current_Speedometer", @"");
    _LblImageDocument.text = NSLocalizedString(@"Image_documentation", @"");
    [_txtInitialSpeed setEnabled:FALSE];
    [_txtCurrentSpeed setEnabled:FALSE];
    [_btnAddImage setEnabled:FALSE];
    
    [self addTitleView:NSLocalizedString(@"Speedometer", @"")];
    [self getSpeedometervalue];
}
#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    //UIImage *img23 = info[UIImagePickerControllerOriginalImage];
    imageData = [self compressResizeImage:info[UIImagePickerControllerOriginalImage]];
    imageData = UIImageJPEGRepresentation([UIImage imageWithData:imageData], 0.5);
    _imgSpeed.image = [UIImage imageWithData:imageData];
    [picker dismissViewControllerAnimated:true completion:nil];
    
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected)];
    singleTap.numberOfTapsRequired = 1;
    [_imgSpeed setUserInteractionEnabled:YES];
    [_imgSpeed addGestureRecognizer:singleTap];
    
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:true completion:nil];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSData *userdata = [Helper getDataValue:@"userInfo"];
    NSDictionary* dataDict = [self dataToDicitonary:userdata];
    if (![dataDict isKindOfClass:[NSNull class]]) {
        NSDictionary *userInfo = [dataDict valueForKey:@"data"];
        NSString *strMeasure;
        if (userInfo != nil) {
            NSDictionary *generalSetting = [userInfo valueForKey:@"general_setting"];
            strMeasure = [generalSetting valueForKey:@"unit"];
        }
        if ([strMeasure isEqualToString:@"miles"]) {
            strMeasure = @"mi";
        }
        textField.text = [NSString stringWithFormat:@"%@ %@",textField.text,strMeasure];
    }else{
        [Helper displayAlertView:@"" message:NSLocalizedString(@"something_went_wrong_msg", @"")];
    }
    
    return TRUE;
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    
    NSData *userdata = [Helper getDataValue:@"userInfo"];
    NSDictionary* dataDict = [self dataToDicitonary:userdata];
    if (![dataDict isKindOfClass:[NSNull class]]) {
        NSDictionary *userInfo = [dataDict valueForKey:@"data"];
        NSString *strMeasure;
        if (userInfo != nil) {
            NSDictionary *generalSetting = [userInfo valueForKey:@"general_setting"];
            strMeasure = [generalSetting valueForKey:@"unit"];
        }
        if ([strMeasure isEqualToString:@"miles"]) {
            strMeasure = @"mi";
        }
        if(textField.text.length>0)
        {
            if([_txtInitialSpeed.text rangeOfString:strMeasure].location == NSNotFound)
            {
                textField.text = [NSString stringWithFormat:@"%@ %@",textField.text,strMeasure];
            }
        }else{
            textField.text = @"0 km";
        }
    }else{
        [Helper displayAlertView:@"" message:NSLocalizedString(@"something_went_wrong_msg", @"")];
    }
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    textField.text = @"";
    
   return TRUE;
}


-(IBAction)saveSpeedometer:(id)sender
{
    [self.view endEditing:YES];
    if([_txtInitialSpeed.text isEqualToString:@"0 km"] || [_txtInitialSpeed.text isEqualToString:@"km"] || [_txtInitialSpeed.text isEqualToString:@""])
    {
        [self alertErrorwithMessage:NSLocalizedString(@"PleaseEnterInitialMeterValue", @"")];
        return;
    }
    
    if(![self CheckNetworkConnection]) {
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *userID = [Helper getPREF:@"userID"];
    
    
    //NSString *sid = [Helper getPREF:@"sid"];
    NSData *userdata = [Helper getDataValue:@"userInfo"];
    NSDictionary* dataDict = [self dataToDicitonary:userdata];
    
    NSDictionary *userInfo = [dataDict valueForKey:@"data"];
    NSString *strMeasure;
    if (userInfo != nil) {
        NSDictionary *generalSetting = [userInfo valueForKey:@"general_setting"];
        strMeasure = [generalSetting valueForKey:@"unit"];
    }
    if ([strMeasure isEqualToString:@"miles"]) {
        strMeasure = @"mi";
    }
    
    if([_txtInitialSpeed.text rangeOfString:strMeasure].location == NSNotFound)
    {
        //categoryString does not contains Holiday
        _txtInitialSpeed.text =  [_txtInitialSpeed.text stringByReplacingOccurrencesOfString:strMeasure withString:@""];
    }
    else
    {
        
    }
    __block NSString *strmiles = strMeasure.lowercaseString;
    NSDictionary *parameter;
        if (imageData != nil && imageData.length > 0)
        {
            
            parameter=@{
                        @"userid":userID,
                        @"speedometer":_txtInitialSpeed.text,
                        @"speed_image":imageData,
                        @"unit":strmiles,
                        };
        }
        else{
            parameter=@{
                        @"userid":userID,
                        @"speedometer":_txtInitialSpeed.text,
                        @"speed_image":@"",
                        @"unit":strmiles,
                        };
        }
    //NSLog(@"parameter : %@",parameter);
    [WebService httpPostWithCustomDelegateMultipartURLString:@".savespeedometer" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         if (error == nil) {
             NSError *localError = nil;
             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
             
             if (localError != nil) {
                 NSLog(@"%@",localError.description);
             }
             else {
                 //NSDictionary *response = [parsedObject objectForKey:@"data"];
                 NSLog(@"response : %@",parsedObject);
                    id response = [parsedObject objectForKey:@"data"];
                
                     NSString* message = [response objectForKey:@"message"];
                     NSString* success = [response objectForKey:@"status"];
                    if ([self CheckSession:response])
                    {
                        if ([success isEqualToString:@"Success"])
                        {
                            [self alertSucess:nil Message:[response objectForKey:@"message"]];
                        }else
                        {
                            [self alertErrorwithMessage:message];
                        }
                     } else
                     {
                         NSLog(@"error :: %@",message);
                         [Helper displayAlertView:@"" message:message];
                     }
             }
         }
     }];
    

}
-(void)getSpeedometervalue
{
    
    if(![self CheckNetworkConnection]) {
        return;
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *userID = [Helper getPREF:@"userID"];
    NSData *userdata = [Helper getDataValue:@"userInfo"];
    NSDictionary* dataDict = [self dataToDicitonary:userdata];
    NSDictionary *userInfo = [dataDict valueForKey:@"data"];
    NSString *strMeasure;
    if (userInfo != nil) {
        NSDictionary *generalSetting = [userInfo valueForKey:@"general_setting"];
        strMeasure = [generalSetting valueForKey:@"unit"];
    }
    
    __block NSString *strmiles = strMeasure.lowercaseString;
    
    NSDictionary *parameter=@{
                              @"userid":userID,
                              @"unit":strmiles,
                              };
    //NSLog(@"parameter : %@",parameter);
    [WebService httpPostWithCustomDelegateURLString:@".getSpeedometer" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         if (error == nil) {
             NSError *localError = nil;
             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
             
             if (localError != nil) {
                 NSLog(@"%@",localError.description);
             }
             else {
                 //NSDictionary *response = [parsedObject objectForKey:@"data"];
                 NSLog(@"response : %@",parsedObject);
                 id response = [parsedObject objectForKey:@"data"];
                 if ([self CheckSession:response]) {
                     current_speedometer = [response objectForKey:@"current_speedometer"];
                     initial_speedometer= [response objectForKey:@"initial_speedometer"];
                     
                     if ([current_speedometer isKindOfClass:[NSNull class]] || current_speedometer == nil){
                            current_speedometer = @"0";
                     }if ([initial_speedometer isKindOfClass:[NSNull class]] || initial_speedometer == nil) {
                         initial_speedometer = @"0";
                     }
                     
                     double current_spped;
                     double initial_speed;
                     
                     if (current_speedometer != nil) {
                        current_spped  = [current_speedometer doubleValue];
                     }else{
                        current_spped  =  0;
                     }

                     if (initial_speedometer != nil) {
                         initial_speed  = [initial_speedometer doubleValue];
                     }else{
                         initial_speed  =  0;
                     }
                     
                     speed_image=[response objectForKey:@"speed_image"];
                     if(initial_speed <= 0.0)
                     {
                         UIButton *btnAdd = [UIButton buttonWithType:UIButtonTypeCustom];
                         btnAdd.frame = CGRectMake(20,0,50,32);
                         [btnAdd setTitle:NSLocalizedString(@"Save", @"") forState:UIControlStateNormal];
                         [btnAdd setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                         btnAdd.titleLabel.font = [UIFont systemFontOfSize:16];
                         [btnAdd addTarget:self action:@selector(saveSpeedometer:) forControlEvents:UIControlEventTouchUpInside];
                         UIBarButtonItem* item = [[UIBarButtonItem alloc] initWithCustomView:btnAdd];
                         self.navigationItem.rightBarButtonItem = item;
                         [_txtInitialSpeed setEnabled:TRUE];
                         [_btnAddImage setEnabled:TRUE];
                     }
                     
                     if ([strmiles isEqualToString:@"miles"]) {
                         strmiles = @"mi";
                     }
                     if(current_speedometer == nil || current_speedometer == (NSString*)[NSNull null])
                     {
                         current_speedometer = @"0";
                     }
                     [_imgSpeed sd_setImageWithURL:[NSURL URLWithString:speed_image] placeholderImage:[UIImage imageNamed:@"camera_96.png"]];
                     current_speedometer = [self ConvertToDKCurrency:current_speedometer];
                     initial_speedometer = [self ConvertToDKCurrency:initial_speedometer];
                     _txtCurrentSpeed.text = [NSString stringWithFormat:@"%@ %@",current_speedometer,strmiles];
                     _txtInitialSpeed.text = [NSString stringWithFormat:@"%@ %@",initial_speedometer,strmiles];
                     
                 }else{
                     //[self moveToLogin ];
                 }
             }
         }
     }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - alertView delegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if (alertView.tag == 156) {
        
        if([title isEqualToString:NSLocalizedString(@"Takenew", @"")]) {
            NSLog(@"photo was selected.");
            if ([UIImagePickerController isCameraDeviceAvailable: UIImagePickerControllerCameraDeviceRear] || [UIImagePickerController isCameraDeviceAvailable: UIImagePickerControllerCameraDeviceFront])
            {
                UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                picker.delegate = self;
                picker.allowsEditing = false;
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:picker animated:YES completion:NULL];
            }else{
                [Helper displayAlertView:@"" message:@"Camara not available"];
            }
        } else if([title isEqualToString:NSLocalizedString(@"Fromgallery", @"")])
        {
            NSLog(@"Button 2 was selected.");
            UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
            cameraUI.delegate = self;
            cameraUI.allowsEditing = NO;
            cameraUI.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [UIImagePickerController availableMediaTypesForSourceType: UIImagePickerControllerSourceTypePhotoLibrary];
            [self presentViewController:cameraUI animated:YES completion:nil];
        }
    }
}

-(void)tapDetected{
    NSLog(@"single Tap on imageview");
    
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"replaceImage", @"") message:NSLocalizedString(@"replacePhoto", @"") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* new = [UIAlertAction actionWithTitle:NSLocalizedString(@"Takenew", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
    {
        if ([UIImagePickerController isCameraDeviceAvailable: UIImagePickerControllerCameraDeviceRear] || [UIImagePickerController isCameraDeviceAvailable: UIImagePickerControllerCameraDeviceFront])
        {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = false;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:picker animated:YES completion:NULL];
        }else{
            [Helper displayAlertView:@"" message:@"Camara not available"];
        }
    }];
    
    UIAlertAction* gallery = [UIAlertAction actionWithTitle:NSLocalizedString(@"Fromgallery", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
    {
        /** What we write here???????? **/
        NSLog(@"you pressed No, thanks button");
        UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
        cameraUI.delegate = self;
        cameraUI.allowsEditing = NO;
        cameraUI.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [UIImagePickerController availableMediaTypesForSourceType: UIImagePickerControllerSourceTypePhotoLibrary];
        [self presentViewController:cameraUI animated:YES completion:nil];
    }];
    
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [self dismissViewControllerAnimated:true completion:nil];
    }];
    
    [alert addAction:new];
    [alert addAction:gallery];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)onImageDoc:(id)sender {
    [_txtInitialSpeed resignFirstResponder];
    
     UIAlertController * alert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"Addimage", @"") message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* new = [UIAlertAction actionWithTitle:NSLocalizedString(@"Takenew", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                          {
                              
                              
                              
                              if ([UIImagePickerController isCameraDeviceAvailable: UIImagePickerControllerCameraDeviceRear] || [UIImagePickerController isCameraDeviceAvailable: UIImagePickerControllerCameraDeviceFront])
                              {
                                  UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                  picker.delegate = self;
                                  picker.allowsEditing = false;
                                  picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                                  [self presentViewController:picker animated:YES completion:NULL];
                              }else{
                                  [Helper displayAlertView:@"" message:@"Camara not available"];
                              }
                          }];
    UIAlertAction* gallery = [UIAlertAction actionWithTitle:NSLocalizedString(@"Fromgallery", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                              {
                                  /** What we write here???????? **/
                                  NSLog(@"you pressed No, thanks button");
                                  UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
                                  cameraUI.delegate = self;
                                  cameraUI.allowsEditing = NO;
                                  cameraUI.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                  [UIImagePickerController availableMediaTypesForSourceType: UIImagePickerControllerSourceTypePhotoLibrary];
                                  [self presentViewController:cameraUI animated:YES completion:nil];
                              }];
    
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [self dismissViewControllerAnimated:true completion:nil];
    }];
    
    [alert addAction:new];
    [alert addAction:gallery];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
    
   
    
   /* UIAlertView *messageAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Addimage", @"") message:nil
                                                          delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"")                                                otherButtonTitles:NSLocalizedString(@"Takenew", @""),NSLocalizedString(@"Fromgallery", @""),nil];
    messageAlert.tag = 156;
    [messageAlert show];*/
}
@end
