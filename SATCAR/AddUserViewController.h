//
//  AddUserViewController.h
//  SATCAR
//
//  Created by Percept Infotech on 2018-04-30.
//  Copyright © 2018 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddUserViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPassowrd;
@property (weak, nonatomic) IBOutlet UIButton *btnAddUser;
@property (weak, nonatomic) IBOutlet UITableView *tableview;
- (IBAction)onAddUser:(id)sender;

@end
