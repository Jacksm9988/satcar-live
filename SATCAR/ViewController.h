//
//  ViewController.h
//  SATCAR
//
//  Created by Percept Infotech on 12/7/16.
//  Copyright © 2016 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController 
{
    
}
@property (weak, nonatomic) IBOutlet UIButton *BtnResiter;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
- (IBAction)RegisterPress:(id)sender;
- (IBAction)LoginPress:(id)sender;


@end

