//
//  LocationListViewController.m
//  SATCAR
//
//  Created by Percept Infotech on 16/01/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import "LocationListViewController.h"
#import "LocationListTableViewCell.h"
#import "LocationEditViewController.h"
#import "MapViewController.h"
@interface LocationListViewController ()
{
    NSMutableArray *LocationArray,*tempTrips;
    int selectedSegment;
}
@end

@implementation LocationListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _viewBackLocation.layer.cornerRadius = 5.0;
    _viewBackLocation.clipsToBounds = true;
    UIColor *borderColor = [UIColor colorWithRed:18/255.0 green:172.0/255.0 blue:242.0/255.0 alpha:1.0];

    _btnEditManually.layer.cornerRadius = 25;
    _btnEditManually.layer.borderWidth = 1;
    _btnEditManually.layer.borderColor = borderColor.CGColor;
    
    /*_btnConfirm.layer.cornerRadius = 25;
    _btnConfirm.layer.borderWidth = 1;
    _btnConfirm.layer.borderColor = borderColor.CGColor;*/
    
    _btnMap.layer.cornerRadius = 25;
    _btnMap.layer.borderWidth = 1;
    _btnMap.layer.borderColor = borderColor.CGColor;
    
    _tableview.estimatedRowHeight = 50;
    _tableview.rowHeight = UITableViewAutomaticDimension;
    _lblMessage.hidden = true;
    _lblMessage.text = NSLocalizedString(@"No_location_found", @"");
    
    [self initView];
}

-(void)initView{
    [Helper setBorderWithColorInView:_viewName];
    [Helper setBorderWithColorInView:_viewAddress];
    [Helper setBorderWithColorInView:_viewZip];
    [Helper setBorderWithColorInView:_viewCountry];
}

-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self addTitleView2:NSLocalizedString(@"Locations", @"")];
    UIButton *btnAdd = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnAdd setImage:[UIImage imageNamed:@"img_plus_menu"] forState:UIControlStateNormal];
    btnAdd.frame = CGRectMake(30, 0,32, 32);
    btnAdd.imageEdgeInsets = UIEdgeInsetsMake(7, 7, 7, 7);
    btnAdd.layer.cornerRadius = 10;
    btnAdd.clipsToBounds = YES;
    [btnAdd setTintColor:[UIColor whiteColor]];
    [btnAdd addTarget:self action:@selector(AddLocation) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* item = [[UIBarButtonItem alloc] initWithCustomView:btnAdd];
    self.navigationItem.rightBarButtonItem = item;
    
    _lblLocationpmessage.text = NSLocalizedString(@"LocationDetails", @"");
    
    /*_lblName.text = NSLocalizedString(@"Name", @"");
    _lblZip.text = NSLocalizedString(@"Zip", @"");
    _lblCity.text = NSLocalizedString(@"City", @"");
    _lblAddress.text = NSLocalizedString(@"Address", @"");
    _lblCountry.text = NSLocalizedString(@"Country", @"");
   // _lblHouseNumber.text = NSLocalizedString(@"house_number", @"");*/
    
    _txtNameValue.placeholder = NSLocalizedString(@"Name", @"");
    _txtZipValue.placeholder = NSLocalizedString(@"Zip", @"");
    _txtAddressValue.placeholder = NSLocalizedString(@"Address", @"");
    _txtCountryValue.placeholder = NSLocalizedString(@"Country", @"");
    //_txtHouseNumberValue.placeholder = NSLocalizedString(@"house_number", @"");
    
    [_btnMap setTitle:NSLocalizedString(@"ShowMap", @"") forState:UIControlStateNormal];
    //[_btnConfirm setTitle:NSLocalizedString(@"Cancel", @"") forState:UIControlStateNormal];
    [_btnConfirm setTitle:NSLocalizedString(@"Close_Trip", @"") forState:UIControlStateNormal];
    [_btnEditManually setTitle:NSLocalizedString(@"EditMenually", @"") forState:UIControlStateNormal];
    
    [self hideLocationEditView];
    [self callLocationService];
}


#pragma mark - Table View
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return LocationArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LocationListTableViewCell *cell = (LocationListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSDictionary *locationData = [[NSDictionary alloc]init];
    locationData = [LocationArray objectAtIndex:indexPath.row];
    cell.lblNameValue.text = [locationData valueForKey:@"name"];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedSegment = (int)indexPath.row;
    [self fillData];
    [self showLocationEditView];
}

#pragma mark - Custom functions
-(void)fillData{
    NSDictionary *locationData = [LocationArray objectAtIndex:selectedSegment];
    
    _txtNameValue.text = [locationData valueForKey:@"name"];
    _txtAddressValue.text = [locationData valueForKey:@"address"];
    
    NSString *zip_city = [locationData valueForKey:@"zip_and_city"];
    _txtZipValue.text = zip_city;
    /*NSArray *zipCityarray = [zip_city componentsSeparatedByString:@" "];
    if (zipCityarray.count > 0) {
        _lblZipValue.text = [zipCityarray objectAtIndex:0];
        _lblCityValue.text = [zipCityarray objectAtIndex:1];
    }*/
    _txtCountryValue.text = [locationData valueForKey:@"country"];
    //_txtHouseNumberValue.text = [locationData valueForKey:@"house_number"];
}

-(void)hideLocationEditView{
    _viewLocationDisplay.hidden = true;
    self.navigationController.navigationBar.hidden = false;
    [UIView animateWithDuration:5.0 delay:1.0 options:UIViewAnimationOptionTransitionNone animations:^{
        _IBLayoutConstraintLocationPopupTop.constant = self.view.frame.size.height;
        _IBLayoutConstraintLocationPopupHeight.constant = 0;
    }completion:nil];
}
-(void)showLocationEditView{
    
    [UIView animateWithDuration:5.0 delay:1.0 options:UIViewAnimationOptionTransitionNone animations:^{
        self.navigationController.navigationBar.hidden = true;
        _viewLocationDisplay.hidden = false;
        _IBLayoutConstraintLocationPopupTop.constant = 0;
        _IBLayoutConstraintLocationPopupHeight.constant = self.view.frame.size.height;
    }completion:nil];
    
}

#pragma mark - custome funcitons
-(void)AddLocation {
    
    //[self performSegueWithIdentifier:@"segueAddressSearch" sender:self];
    selectedSegment = -1;
    [self hideLocationEditView];
    [self performSegueWithIdentifier:@"SeguelocationAdd" sender:self];
}

#pragma mark - button action
- (IBAction)onEditManually:(id)sender {
    self.navigationController.navigationBar.hidden = false;
    [self performSegueWithIdentifier:@"SeguelocationEdit" sender:self];
}

- (IBAction)onConfirm:(id)sender {
    selectedSegment = -1;
    [self hideLocationEditView];
}

- (IBAction)onMap:(id)sender {
    [self hideLocationEditView];
    [self performSegueWithIdentifier:@"segueLocationToMap" sender:self];
}

#pragma mark - Webservice call
-(void)callLocationService{
    
    if(![self CheckNetworkConnection]) {
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *sid = [Helper getPREF:@"userID"];
    NSDictionary *parameter=@{ @"userid":sid };
    //NSLog(@"parameter : %@",parameter);
    
    [WebService httpPostWithCustomDelegateURLString:@".getLocations" parameter:parameter isDebug:false callBackData:^(NSData *data, NSError *error) {
        
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         if (error == nil) {
             NSError *localError = nil;
             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &localError];
             
             if (localError != nil) {
                 NSLog(@"%@",localError.description);
             }
             else {
                 id response = [parsedObject objectForKey:@"data"];
                 NSLog(@"Response : %@",response);
                 if ([self CheckSession:response]) {
                     LocationArray = [[NSMutableArray alloc]init];
                     LocationArray  = [response objectForKey:@"locations"];
                     if (LocationArray.count > 0) {
                         _lblMessage.hidden = true;
                         NSLog(@"Locations : %@",LocationArray);
                         [Helper setUserArrayValue:LocationArray sKey:@"locationArray"];
                         [_tableview reloadData];
                     }else{
                         _lblMessage.hidden = false;
                     }
                 } else {
                     //[self moveToLogin ];
                 }
             }
         }
     }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"SeguelocationEdit"]) {
        LocationEditViewController *controller = segue.destinationViewController;
        NSDictionary *locationData = [LocationArray objectAtIndex:selectedSegment];
        controller.locationDataDictionary = locationData;
        controller.fromScreen = @"LocationEdit";
    }else if([segue.identifier isEqualToString:@"segueLocationToMap"]){
        MapViewController *controller = segue.destinationViewController;
        NSDictionary *locationData = [LocationArray objectAtIndex:selectedSegment];
        controller.locationDataDictionary = locationData;
    }
}



@end
