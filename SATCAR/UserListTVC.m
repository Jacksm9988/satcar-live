//
//  UserListTVC.m
//  SATCAR
//
//  Created by Percept Infotech on 2018-05-17.
//  Copyright © 2018 Percept Infotech. All rights reserved.
//

#import "UserListTVC.h"

@implementation UserListTVC

- (void)awakeFromNib {
    [super awakeFromNib];
    _btnRemove.layer.cornerRadius = 10;
    _btnRemove.clipsToBounds = true;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
