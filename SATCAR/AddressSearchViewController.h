//
//  AddressSearchViewController.h
//  SATCAR
//
//  Created by Percept Infotech on 18/01/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddressSearchViewController : UIViewController
{
    
}
@property (strong,nonatomic) NSString *fromScreen;
@property (weak, nonatomic) IBOutlet UITextField *txtSearch;
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintTableBottom;
@property(nonatomic,strong)NSString *fromScreenField;
- (IBAction)onBack:(id)sender;
@end
