//
//  ReimbursementRentsViewController.h
//  SATCAR
//
//  Created by Percept Infotech on 10/02/17.
//  Copyright © 2017 Percept Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReimbursementRentsViewController : UIViewController
{
    int selectedSegment,selectedIndex;
}
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (weak, nonatomic) IBOutlet UIButton *btnUpdate;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
- (IBAction)updateDetail:(id)sender;
- (IBAction)Cancel:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *txtYear;
@property (weak, nonatomic) IBOutlet UITextField *txtFKm;
@property (weak, nonatomic) IBOutlet UITextField *txtToKm;
@property (weak, nonatomic) IBOutlet UITextField *txtAmount;
@property (weak, nonatomic) IBOutlet UITextField *txtCurrency;
@property (weak, nonatomic) IBOutlet UITextField *txtCountry;
@property (weak, nonatomic) IBOutlet UIView *viewReimDetail;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintLocationPopupTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *IBLayoutConstraintLocationPopupHeight;

@end
